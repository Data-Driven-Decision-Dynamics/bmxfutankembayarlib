################################ Loading required packages ################################
require(readr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(data.table)
require(magrittr)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_input_data  <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/raw/historical_performances"
path_output_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/processed/historical_performances"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
vec_availables_files <- dir(path_input_data)
setwd(path_input_data)
mat_historical_races_letrot <- ldply(vec_availables_files, read_csv,.parallel=TRUE)
mat_historical_races_letrot <- mat_historical_races_letrot %>% distinct() %>% as.data.frame()
setwd(path_output_data)
mat_historical_races_letrot$Gender <- gsub("FALSE","F",mat_historical_races_letrot$Gender)
mat_historical_races_letrot$Epreuve <- gsub("FALSE","F",mat_historical_races_letrot$Epreuve)
mat_historical_races_letrot$Epreuve <- str_trim(gsub("Groupe","",mat_historical_races_letrot$Epreuve))
# mat_historical_races_letrot$Driver <- unlist(lapply(mat_historical_races_letrot$Driver,correct_driver_name_letrot))
# mat_historical_races_letrot$Entraineur <- unlist(lapply(mat_historical_races_letrot$Entraineur,correct_driver_name_letrot))
# mat_historical_races_letrot$Terrain <- as.vector(gsub("Mâchefer","Machefer",mat_historical_races_letrot$Terrain))
mat_historical_races_letrot$Lieu <- gsub('.*\\(', '', mat_historical_races_letrot$Lieu)
mat_historical_races_letrot$Lieu <- gsub(')', '', mat_historical_races_letrot$Lieu)
id_location_change <- grep("^A ",mat_historical_races_letrot$Lieu)
if(length(id_location_change)>0){
  mat_historical_races_letrot$Lieu [id_location_change] <- unlist(lapply(mat_historical_races_letrot$Lieu [id_location_change],function(x){substring(x,3)}))
}
saveRDS(mat_historical_races_letrot,"mat_historical_races_letrot.rds")
################################ Parallelization Management ############################
