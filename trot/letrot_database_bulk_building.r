################################ Loading required packages ################################
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(doMC)
require(lubridate)
require(bmxFutankeMbayar)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_output_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/raw/historical_performances"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.5))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Generate and store data ############################
seq_date <- seq(as.Date("01-01-2015","%d-%m-%Y"), Sys.Date()-1, by = "day")
# seq_date <- seq(Sys.Date(), Sys.Date(), by = "day")
seq_date <- unlist(lapply(seq_date,convert_date_format_letrot))
vec_url_date_scrape <- unlist(lapply(seq_date,get_links_schedule_track_target_day_letrot))
vec_url_date_scrape <- rev(vec_url_date_scrape)
vec_url_date_scrape <- gsub("courses/programme","fiche-course",vec_url_date_scrape)
# res_races_scrape <- foreach(i=32:length(vec_url_date_scrape),.combine=rbind.fill,.errorhandling =  "pass") %dopar% {
# res_races_scrape <- foreach(i=32:length(vec_url_date_scrape),.errorhandling =  "pass") %dopar% {
for(i in 844:length(vec_url_date_scrape)){
  require(rvest)
  require(stringr)
  require(curl)
  vec_race_current_date <- get_links_races_target_track_letrot(vec_url_date_scrape[i])
  vec_race_current_date <- vec_race_current_date[-grep("facebook",vec_race_current_date)]
  if(length(vec_race_current_date)>0){
    for(j in 1:length(vec_race_current_date))
    {
      url_path_pro_current <- vec_race_current_date[j]
      url_path_res_current <- sub("partants/tableau","resultats/arrivee-definitive#sub_sub_menu_course",url_path_pro_current)
      mat_details_race <- try(get_details_target_race_letrot(url_path_pro_current))
      if(class(mat_details_race)!="try-error" & !is.null(mat_details_race)){
        mat_results_race <- try(get_results_target_race_letrot(url_path_res_current))
        if(class(mat_results_race)!="try-error" & !is.null(mat_results_race)){
          mat_results_race <- mat_results_race[,intersect(colnames(mat_results_race),c("Place","Numero","Cheval","Time","Reduction","Gains","Status"))]
          if(nrow(mat_results_race)>0){
            vec_horses_common <- intersect(mat_details_race$Cheval,mat_results_race$Cheval)
            if(length(vec_horses_common)>0) {
              mat_infos_race <- merge(mat_details_race,mat_results_race,by.x="Cheval",by.y="Cheval")
              if(length(grep("Numero",colnames(mat_infos_race)))>1) {
                colnames(mat_infos_race)[grep("Numero",colnames(mat_infos_race))[1]] <- "Numero"
                mat_infos_race <- mat_infos_race[,-grep("Numero",colnames(mat_infos_race))[2]]
              }
              mat_infos_race$Gender <- as.character(mat_infos_race$Gender )
              mat_infos_race$Gender <- gsub("FALSE","F",mat_infos_race$Gender)
              mat_infos_race$Epreuve <- as.character(mat_infos_race$Epreuve )
              mat_infos_race$Epreuve <- gsub("FALSE","F",mat_infos_race$Epreuve)
              mat_infos_race$Epreuve <- as.character(mat_infos_race$Epreuve )
              setwd(path_output_data)
              file_name_output <- rev(unlist(strsplit(tempfile(),"/")))[1] 
              file_name_output <- paste(file_name_output,".csv",sep="")
              file_name_output <- paste(path_output_data,file_name_output,sep="/")
              write.csv(mat_infos_race,file=file_name_output,row.names = FALSE)
            }
          }
        }
      }
    }
  }
}
################################ Generate and store data ############################