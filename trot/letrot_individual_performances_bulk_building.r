################################ Loading required packages ################################
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(doMC)
require(lubridate)
require(bmxFutankeMbayar)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_output_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/raw/individual-performances"
path_output_processed_horses <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/raw/mat_horses_processed"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.5))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Generate and store data ############################
if(!file.exists(path_output_processed_horses)){
  file.create(path_output_processed_horses)
}
seq_date <- seq(as.Date("01-01-2018","%d-%m-%Y"), Sys.Date()-2, by = "day")
seq_date <- unlist(lapply(seq_date,convert_date_format_letrot))
vec_url_date_scrape <- unlist(lapply(seq_date,get_links_schedule_track_target_day_letrot))
vec_url_date_scrape <- rev(vec_url_date_scrape)
vec_url_date_scrape <- gsub("courses/programme","fiche-course",vec_url_date_scrape)
# res_races_scrape <- foreach(i=32:length(vec_url_date_scrape),.combine=rbind.fill,.errorhandling =  "pass") %dopar% {
res_races_scrape <- foreach(i=116:length(vec_url_date_scrape),.errorhandling =  "pass") %dopar% {
for(i in 116:length(vec_url_date_scrape)){
  require(rvest)
  require(stringr)
  require(curl)
  vec_race_current_date <- get_links_races_target_track_letrot(vec_url_date_scrape[i])
  vec_race_current_date <- vec_race_current_date[-grep("facebook",vec_race_current_date)]
  if(length(vec_race_current_date)>0){
    for(j in 1:length(vec_race_current_date))
    {
      url_path_pro_current <- vec_race_current_date[j]
      vec_links_pages_current_races <- get_links_pages_horses_target_race_letrot(url_path_pro_current)
      if(length(vec_links_pages_current_races)>0){
        write(vec_links_pages_current_races,path_output_processed_horses,append = TRUE)
      }
      for(k in 1:length(vec_links_pages_current_races))
      {
        mat_individual_performances_current <- try(get_horse_all_last_performances_letrot(vec_links_pages_current_races[k]))
        if(class(mat_individual_performances_current)!="try-error"){
          if(!is.null(mat_individual_performances_current)){
            if(nrow(mat_individual_performances_current)>0){
              mat_individual_performances_current <- transform(mat_individual_performances_current,URL=vec_links_pages_current_races[k])
              file_name_output <- paste(rev(unlist(strsplit(tempfile(),"/")))[1],Sys.time(),sep="_")
              file_name_output <- paste(file_name_output,".csv",sep="")
              file_name_output <- paste(path_output_data,file_name_output,sep="/")
              write.csv(mat_individual_performances_current,file=file_name_output,row.names = FALSE)
            }
          }
        }
      }
    }
  }
}
################################ Generate and store data ############################