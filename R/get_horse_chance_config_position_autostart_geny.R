get_horse_chance_config_position_autostart_geny <- function(df_infos_target_race_geny = NULL,
                                                            path_df_issues_position_autostart = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_issues_position_auto.rds"){
  df_chance_config_pos_auto <- NULL
  df_infos_chance_config_pos_auto <- get_horse_issues_context_position_autostart_geny(df_infos_target_race_geny)
  if(!is.null(df_infos_chance_config_pos_auto)){
    df_stats_chance_config_pos_auto <- readRDS(path_df_issues_position_autostart)
    df_chance_config_pos_auto <- merge(df_infos_chance_config_pos_auto,df_stats_chance_config_pos_auto,by.x="Auto_Chance",by.y="Auto_Chance",all.x=TRUE)
    if(nrow(df_chance_config_pos_auto)<nrow(df_infos_target_race_geny)){
      df_chance_config_pos_auto <- merge(df_chance_config_pos_auto,df_infos_target_race_geny[,"Cheval",drop=FALSE],all.x="Cheval",all.y="Cheval",all.y=TRUE)
    }
    df_chance_config_pos_auto <- df_chance_config_pos_auto[,c("Cheval","Percent")]
    colnames(df_chance_config_pos_auto)[2] <- "ENGAGEMENT_CHANCE_CONFIG_POS_AUTO"
    df_infos_chance_config_pos_auto$ENGAGEMENT_CHANCE_CONFIG_POS_AUTO <- round(get_percentile_values(df_infos_chance_config_pos_auto$ENGAGEMENT_CHANCE_CONFIG_POS_AUTO),2)
  }
  return(df_chance_config_pos_auto)
}