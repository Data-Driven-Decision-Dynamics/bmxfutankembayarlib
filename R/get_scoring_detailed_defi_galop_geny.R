# get_scoring_detailed_defi_galop_geny <- function(df_infos_target_race_geny = NULL, start_date_defi_galop = as.Date("2021-02-27"),end_date_defi_galop = as.Date("2021-11-28"), number_point_rank = c(6,4,3,3,1), regex_defi_galop = "défi du galop|defi du galop", path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds") {
#   
#   message("Reading historical performance file")
#   if(!exists("df_historical_races_geny")) {
#     if(file.exists(path_df_historical_races_geny)){
#       df_historical_races_geny <- readRDS(path_df_historical_races_geny)
#     }
#   }
#   
#   id_items_defi_galop <- grep(regex_defi_galop,paste(df_historical_races_geny$Details,df_historical_races_geny$Course),ignore.case = TRUE)
#   if(sum(id_items_defi_galop,na.rm = TRUE)>0){
#     df_races_candidates_defi_galop <- df_historical_races_geny[id_items_defi_galop,c("Date","Cheval","Driver","Entraineur","Place","Gains","Lieu","Race","Details")]
#     df_races_candidates_defi_galop <- distinct(df_races_candidates_defi_galop)
#     id_items_defi_galop_timeframe <- df_races_candidates_defi_galop$Date <= end_date_defi_galop & df_races_candidates_defi_galop$Date >= start_date_defi_galop
#     if(sum(id_items_defi_galop_timeframe,na.rm = TRUE)>0){
#       df_races_good_defi_galop <- df_races_candidates_defi_galop[id_items_defi_galop_timeframe,]
#       df_races_good_defi_galop$Points <- NA
#       for(idx_rank in 1:length(number_point_rank))
#       {
#         idx_item_current_rank <- df_races_good_defi_galop$Place == idx_rank
#         if(sum(idx_item_current_rank,na.rm = TRUE)>0){
#           df_races_good_defi_galop[idx_item_current_rank,"Points"] <- number_point_rank[idx_rank]
#         }
#       }
#       if(sum(is.na(df_races_good_defi_galop$Points))>0){
#         df_races_good_defi_galop[is.na(df_races_good_defi_galop$Points),"Points"] <- 1
#       }
#     }
#     
#     if(sum(df_races_good_defi_galop$Place ==1,na.rm = TRUE)>0){
#       df_qualified_horses <- df_races_good_defi_galop[df_races_good_defi_galop$Place ==1,"Cheval",drop=FALSE]
#       df_qualified_horses$QUALIFIED <- 1
#     }
#     
#     df_number_participation <- df_races_good_defi_galop %>%
#       group_by(Cheval) %>%
#       dplyr::summarise(NUMBER_PARTICIPATION_HORSE = n()) %>%
#       as.data.frame()
#     df_number_participation$SCORE_PARTICIPATION <- 0
#     
#     for(idx in 1:nrow(df_points_number_particip)){
#       idx_item_part <- df_number_participation$NUMBER_PARTICIPATION_HORSE == df_points_number_particip[idx,"Size"]
#       if(sum(idx_item_part,na.rm = TRUE)>0){
#         df_number_participation[idx_item_part,"SCORE_PARTICIPATION"] <- df_points_number_particip[idx,"Points"]
#       }
#     }
#  
#     df_score_horse <- df_races_good_defi_galop %>%
#       group_by(Cheval) %>%
#       dplyr::summarise(SCORE_HORSE = sum(Points)) %>%
#       as.data.frame()
#     
#     df_score_driver <- df_races_good_defi_galop %>%
#       group_by(Driver) %>%
#       dplyr::summarise(SCORE_DRIVER = sum(Points)) %>%
#       as.data.frame()
#     
#     df_score_driver <- merge(df_score_driver,df_infos_target_race_geny[,c("Cheval","Driver")],by.x="Driver",by.y="Driver",all.y=TRUE)
#     
#     df_score_trainer <- df_races_good_defi_galop %>%
#       group_by(Entraineur) %>%
#       dplyr::summarise(SCORE_TRAINER = sum(Points)) %>%
#       as.data.frame()
#     
#     df_score_trainer <- merge(df_score_trainer,df_infos_target_race_geny[,c("Cheval","Entraineur")],by.x="Entraineur",by.y="Entraineur",all.y=TRUE)
#     
#     list_scores_gnt <- list(Participation = df_number_participation[,c("Cheval","SCORE_PARTICIPATION","NUMBER_PARTICIPATION_HORSE")], Horse = df_score_horse, Driver = df_score_driver, Trainer = df_score_trainer, Qualified= df_qualified_horses)
#     df_infos_scores_gnt <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_scores_gnt)
#     df_infos_scores_gnt$SCORE_HORSE <- df_infos_scores_gnt$SCORE_HORSE + df_infos_scores_gnt$SCORE_PARTICIPATION
#     df_infos_scores_gnt <- merge(df_infos_scores_gnt,df_infos_target_race_geny[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all.y = TRUE)
#     if(sum(is.na(df_infos_scores_gnt))>0){
#       df_infos_scores_gnt[is.na(df_infos_scores_gnt)] <- 0
#     }
#   }
#   
#   return(df_infos_scores_gnt)
# }
# 
# 
# 
# 
# 
