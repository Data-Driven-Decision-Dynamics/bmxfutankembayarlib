get_stats_numero_autostart_number_geny <- function(path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                   path_stats_numero_autostart = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_stats_number_autostart.rds",
                                                   number_days_back = 1500){

  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }

  df_historical_races_focus <- df_historical_races_geny[df_historical_races_geny$Depart == "Auto-Start", c("Date","Numero","Cheval","Lieu","Depart","Distance","Place","Discipline","Details","Race","Speed")]
  df_historical_races_focus <- unique(df_historical_races_focus)
  df_historical_races_focus$ID <- paste(df_historical_races_focus$Lieu,df_historical_races_focus$Distance,sep="-")

  if(sum(is.na(df_historical_races_focus$Date))>0){
    df_historical_races_focus <- df_historical_races_focus[!is.na(df_historical_races_focus$Date),]
  }

  if(sum(is.na(df_historical_races_focus$Distance))>0){
    df_historical_races_focus <- df_historical_races_focus[!is.na(df_historical_races_focus$Distance),]
  }
  
  df_stats_numero_autostart_number <- df_historical_races_focus %>%
    mutate(Numero = as.numeric(Numero)) %>%
    mutate(Place = as.numeric(Place)) %>%
    drop_na() %>%
    distinct() %>%
    group_by(ID,Numero) %>%
    dplyr::summarise(Size = n(),
                     Podium = get_number_top_five(Place),
                     Chance = round(Podium/Size,2)) %>%
    mutate(Identifier = paste(ID,Numero,sep="_")) %>%
    as.data.frame()
  
  df_speed_numero_autostart_number <- df_historical_races_focus %>%
    mutate(Numero = as.numeric(Numero)) %>%
    mutate(Place = as.numeric(Place)) %>%
    filter(Place <=3) %>%
    drop_na() %>%
    distinct() %>%
    group_by(ID,Numero) %>%
    dplyr::summarise(Speed = mean(Speed)) %>%
    mutate(Identifier = paste(ID,Numero,sep="_")) %>%
    as.data.frame()
  
  df_stats_numero_autostart_number <- merge(df_stats_numero_autostart_number,df_speed_numero_autostart_number [,c("Identifier","Speed")],by.x="Identifier",by.y="Identifier",all=TRUE)

  saveRDS(df_stats_numero_autostart_number,path_stats_numero_autostart)

  return(NULL)
}