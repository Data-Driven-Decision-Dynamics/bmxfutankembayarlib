#' @return Compute the probability of being in the top 7 for a given race
#' @importFrom magrittr %>%
#' @importFrom h2o h2o.loadModel
#' @importFrom dplyr top_n
#' @importFrom dplyr top_n
#' @importFrom dplyr summarise
#' @importFrom dplyr mutate
#' @examples
#' get_multiclass_predicted_probabilities_given_race_letrot(mat_infos_target_race = NULL)
#' @export

get_multiclass_predicted_probabilities_given_race_letrot <- function(df_features_target=NULL,
                                                                     path_input_models="/mnt/Master-Data/Futanke-Mbayar/France/models/le-trot/output/models"){

  message("initialization Of Final Output")
  prediction_multiclass_model <- NULL
  
  message("Constructing the path to the modle to apply")
  name_model_to_apply <- tolower(gsub("é","e",paste(unique(df_features_target$HIPPODROME),unique(df_features_target$DISCIPLINE),min(df_features_target$DISTANCE_DAY),sep="_")))
  name_model_to_apply <- gsub(" ","-",name_model_to_apply)
  path_model_to_apply <- dir(path_input_models,pattern=name_model_to_apply)
  path_model_to_apply <- dir(path_input_models,pattern=name_model_to_apply,full.names = TRUE)
  
  if(length(path_model_to_apply)>0){
    if(length(path_model_to_apply)>1) {
      path_model_to_apply <- path_model_to_apply[1]
    }
    
    message("Loading the model to apply")
    model_multiclass_current <- readRDS(path_model_to_apply)
    vec_features_needed <- names(model_multiclass_current$forest$ncat)
    
    df_features_target[is.infinite(as.matrix(df_features_target))] <- 0
    df_features_target[is.nan(as.matrix(df_features_target))] <- 0
    
    if("Class" %in% colnames(df_features_target)){
      df_features_target[,-grep("Class",colnames(df_features_target))][is.infinite(as.matrix(df_features_target[,-grep("Class",colnames(df_features_target))]))] <- 0
    }
    
    if(sum(is.na(df_features_target))>0) {
      df_features_target[is.na(df_features_target)] <- 0
    }
    
    if("CHEVAL" %in% colnames(df_features_target)) {
      df_features_target$IDENTIFIER <- paste(df_features_target$CHEVAL,df_features_target$RACE,sep="_")
    } else {
      df_features_target$IDENTIFIER <- paste(df_features_target$Cheval,df_features_target$RACE,sep="_")
    }
    
    rownames(df_features_target)  <- df_features_target$IDENTIFIER
    
    message("Testing if the data for the target race have been provided")
    if(!is.null(df_features_target)) {
      if(nrow(df_features_target)>0){
        for(i in colnames(df_features_target))
        {
          class(df_features_target[,i]) =="matrix"
          df_features_target[,i] <- as.vector(unlist(as.data.frame(df_features_target[,i])))
        }
      }
    }

    if(sum(mapply(is.infinite, df_features_target))>0){
      df_features_target[mapply(is.infinite, df_features_target)] <- NA
    }

    vec_missing_features <- setdiff(vec_features_needed,colnames(df_features_target))
    if(length(vec_missing_features)>0){
      for(missing_feature in vec_missing_features)
      {
        df_features_target[,missing_feature] <- 0
      }
    }
    
    if(sum(is.na(df_features_target))>0){
      df_features_target[is.na(df_features_target)] <- 0
    }
    
    message("Applying the dedicated model to the target race")
    prediction_multiclass_model <- predict(model_multiclass_current, df_features_target,type="prob")
    prediction_multiclass_model <- as.data.frame(prediction_multiclass_model)
    prediction_multiclass_model$Cheval <- unlist(lapply(rownames(prediction_multiclass_model),function(x){unlist(strsplit(x,"_"))[1]}))
    prediction_multiclass_model <- prediction_multiclass_model[,c("Cheval",setdiff(colnames(prediction_multiclass_model),"Cheval"))]
    rownames(prediction_multiclass_model) <- NULL
  }

  return(prediction_multiclass_model)
  
}
