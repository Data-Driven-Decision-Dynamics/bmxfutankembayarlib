#' @return Compute engagement indicator for each candidate of a given race
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_trend_ferrage_letrot(df_infos_target_race_geny = NULL)
#' @export
get_horse_trends_ferrage_geny <- function (df_infos_target_race_geny = NULL,
                                           path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                           number_days_back=366){
  
  df_epreuve <- data.frame(Epreuve=c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"),Values=seq(1:length(c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"))))
  
  message("Initialization of output")
  df_stats_previous_race <- NULL
  df_trend_ferrage <- NULL
  df_infos_ranking_focus_completed <- NULL
  
  message("Reading historical race infos file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)) {
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Reading historical race infos file")
  
  message("Initialization of the final output")
  df_trend_ferrage <- NULL
  message("Initialization of the final output")
  
  message("Extract race date and convert in the right format if need")
  df_infos_target_race_geny$Date <- as.Date(df_infos_target_race_geny$Date)
  
  if(class(unique(df_infos_target_race_geny$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race_geny$Date)
  }
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
  
  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
  
  message("Extract epreuve of current race")
  current_race_epreuve <- unique(df_infos_target_race_geny$Epreuve)
  
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
  current_race_terrain <- sub("Machefer","Cendrée",current_race_terrain)
  current_race_terrain <- sub("Mâchefer","Cendrée",current_race_terrain)
  
  message("Extract terrain of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
  
  message("Extract prize of current race")
  current_race_prix <- as.vector(unique(df_infos_target_race_geny$Prix))
  
  message("Extract distance of current race")
  current_race_distance <- as.numeric(min(df_infos_target_race_geny$Distance))
  
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Trot Attelé","Trot Monté")){
    df_historical_races_competitors_geny_focus <- df_historical_races_geny %>%
      filter(Discipline == current_race_discipline) %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(Date<current_race_date) %>%
      filter(current_race_date-Date<=number_days_back) %>%
      dplyr::select(Cheval,Date,Ferrage,Entraineur,Place,Epreuve) %>%
      distinct() %>%
      drop_na() %>%
      group_by(Cheval) %>%
      top_n(3,Date) %>%
      as.data.frame()
    
    if(nrow(df_historical_races_competitors_geny_focus)>0){
      df_historical_races_competitors_geny_focus <- rbind.fill(df_historical_races_competitors_geny_focus,df_infos_target_race_geny[,intersect(colnames(df_infos_target_race_geny),c("Cheval","Date","Ferrage","Entraineur","Place","Epreuve"))])
      df_trend_ferrage <- data.frame(Cheval = unique(df_infos_target_race_geny$Cheval), Config = NA)
      for(current_horse in df_trend_ferrage$Cheval){
        df_infos_ferrage <- df_historical_races_competitors_geny_focus[df_historical_races_competitors_geny_focus$Cheval == current_horse,]
        df_infos_ferrage <- df_infos_ferrage[order(df_infos_ferrage$Date,decreasing = FALSE),]
        if(nrow(df_infos_ferrage)>=3){
          df_trend_ferrage[df_trend_ferrage$Cheval == current_horse, "Config"] <- paste(df_infos_ferrage$Ferrage,collapse = "-")
        }
      }
      if(sum(!is.na(df_trend_ferrage$Config))==0){
        df_trend_ferrage <- NULL
      }
    }
    
    if(nrow(df_historical_races_competitors_geny_focus)>0){
      if(!is.na(current_race_epreuve)){
        current_race_epreuve_numeric <- df_epreuve[df_epreuve$Epreuve==current_race_epreuve,"Values" ]
        vec_candidate_epreuve <- df_epreuve[df_epreuve$Values <= current_race_epreuve_numeric,"Epreuve" ]
        df_infos_previous_race <- df_historical_races_competitors_geny_focus %>%
          filter(Date<current_race_date) %>%
          group_by(Cheval) %>%
          top_n(1,Date) %>%
          add_categorical_ranking() %>%
          as.data.frame()
        if(nrow(df_infos_previous_race)>0){
          df_infos_previous_race <- merge(df_infos_previous_race,df_epreuve,by.x="Epreuve",by.y="Epreuve")
          df_infos_previous_race$Delta <- current_race_epreuve_numeric -df_infos_previous_race$Values
          df_stats_previous_race <- df_infos_previous_race %>%
            add_categorical_delta_level() %>%
            select(Cheval,Ranking,Level) %>%
            mutate(Epreuve = paste(Level,Ranking,sep="-")) %>%
            select(Cheval,Epreuve) %>%
            as.data.frame()
        }
      }
    }
    
    if(nrow(df_historical_races_competitors_geny_focus)>0 & !is.null(df_trend_ferrage)){
      df_infos_ranking <- get_horse_ranking_per_configuration_geny(df_infos_target_race_geny)
      if("Turn" %in% colnames(df_infos_ranking)){
        df_infos_ranking_focus <- df_infos_ranking[,c("Cheval","Turn")]
        df_infos_ranking_focus <- get_nominal_values_based_numeric(df_infos_ranking_focus,"Turn","Class")
        df_infos_ranking_focus <- df_infos_ranking_focus[,c("Cheval","Class")]
        if(!is.null(df_stats_previous_race)){
          df_infos_ranking_focus <- merge(df_infos_ranking_focus,df_stats_previous_race,by.x="Cheval",by.y="Cheval",all.x=TRUE)
        }
        df_infos_ranking_focus_completed <- merge(df_infos_ranking_focus,df_trend_ferrage,by.x="Cheval",by.y="Cheval",all.y=TRUE)
        df_infos_ranking_focus_completed$Group <- df_infos_ranking_focus_completed$Config
        df_infos_ranking_focus_completed$Config <- paste(df_infos_ranking_focus_completed$Class,df_infos_ranking_focus_completed$Config,sep="_")
        df_infos_ranking_focus_completed <- df_infos_ranking_focus_completed[,intersect(colnames(df_infos_ranking_focus_completed),c("Cheval","Config","Group","Epreuve"))]
        df_infos_ranking_focus_completed <- merge(df_infos_ranking_focus_completed,df_infos_target_race_geny[,intersect(colnames(df_infos_target_race_geny),c("Cheval","Place","Lieu","Discipline","Longueur","Entraineur"))],by.x="Cheval",by.y="Cheval",all.y=TRUE)
        df_infos_ranking_focus_completed$Track <- paste(df_infos_ranking_focus_completed$Lieu,df_infos_ranking_focus_completed$Discipline,df_infos_ranking_focus_completed$Longueur,sep="_")
        if("Epreuve" %in% colnames(df_infos_ranking_focus_completed)){
          df_infos_ranking_focus_completed$Epreuve <- paste(df_infos_ranking_focus_completed$Epreuve,df_infos_ranking_focus_completed$Group,sep="_")
        }
      }
    }
  }

  return(df_infos_ranking_focus_completed)
}