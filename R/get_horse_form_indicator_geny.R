get_horse_form_indicator_geny <- function(df_infos_target_race_geny = NULL,
                                          path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                          threshold_winner_last_race = 45,
                                          threshold_last_perf_delay = 30,
                                          threshold_two_last_two_races_delay = 60,
                                          threshold_two_last_three_races_delay = 90,
                                          threshold_two_last_four_races_delay = 120,
                                          threshold_two_last_five_races_delay = 150,
                                          threshold_two_last_perf_delay = 100){
  
  message("Initialisation final output")
  vec_candidates_form_based_winner_last_race <- NULL
  vec_candidates_form_based_podium_two_last_races <- NULL
  vec_candidates_form_based_podium_three_last_races <- NULL
  vec_candidates_form_based_podium_four_last_races <- NULL
  vec_candidates_form_based_podium_five_races <- NULL
  vec_candidates_form_based_last_speed <- NULL
  vec_candidates_form_based_highest_earning <- NULL
  df_infos_form_indicators <- data.frame(Cheval = unique(df_infos_target_race_geny$Cheval) , Form = 0)
  message("Initialisation final output")
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      df_historical_races_geny$Cumul_Gains <- as.numeric(df_historical_races_geny$Cumul_Gains)
    }
  }
  message("Reading historical performance file")
  
  message("Starting getting details for last race")
  df_infos_last_race <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    select(Cheval,Date,Place) %>%
    unique() %>%
    drop_na() %>%
    group_by(Cheval) %>%
    top_n(1,Date) %>%
    filter(Place == 1) %>%
    mutate(Delay = as.numeric(unique(df_infos_target_race_geny$Date)-Date)) %>%
    filter(Delay <= threshold_winner_last_race) %>%
    as.data.frame()
  if(nrow(df_infos_last_race)>0){
    vec_candidates_form_based_winner_last_race <- unique(df_infos_last_race$Cheval)
  }
  message("Finishing getting details for last race")
  
  message("Starting getting details for last two races")
  df_infos_last_two_race <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    select(Cheval,Date,Place) %>%
    unique() %>%
    drop_na() %>%
    group_by(Cheval) %>%
    top_n(2,Date) %>%
    filter(Place <= 3) %>%
    mutate(Delay = as.numeric(unique(df_infos_target_race_geny$Date)-Date)) %>%
    filter(Delay <= threshold_two_last_two_races_delay) %>%
    as.data.frame()
  if(nrow(df_infos_last_two_race)>0){
    freq_vec_candidates_form_based_podium_two_last_races <- table(df_infos_last_two_race$Cheval)
    if(max(freq_vec_candidates_form_based_podium_two_last_races)>=2){
      vec_candidates_form_based_podium_two_last_races <- names(freq_vec_candidates_form_based_podium_two_last_races)[freq_vec_candidates_form_based_podium_two_last_races==2]
    }
  }
  message("Finishing getting details for two races")
  
  message("Starting getting details for last three races")
  df_infos_last_three_race <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    select(Cheval,Date,Place) %>%
    unique() %>%
    drop_na() %>%
    group_by(Cheval) %>%
    top_n(3,Date) %>%
    filter(Place <= 5) %>%
    mutate(Delay = as.numeric(unique(df_infos_target_race_geny$Date)-Date)) %>%
    filter(Delay <= threshold_two_last_three_races_delay  ) %>%
    as.data.frame()
  if(nrow(df_infos_last_three_race)>0){
    freq_vec_candidates_form_based_podium_three_races <- table(df_infos_last_three_race$Cheval)
    if(max(freq_vec_candidates_form_based_podium_three_races)>=3){
      vec_candidates_form_based_podium_three_last_races <- names(freq_vec_candidates_form_based_podium_three_races)[freq_vec_candidates_form_based_podium_three_races==3]
    }
  }
  message("Finishing getting details for three races")
  
  message("Starting getting details for last four races")
  df_infos_last_four_race <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    select(Cheval,Date,Place) %>%
    unique() %>%
    drop_na() %>%
    group_by(Cheval) %>%
    top_n(4,Date) %>%
    filter(Place <= 5) %>%
    mutate(Delay = as.numeric(unique(df_infos_target_race_geny$Date)-Date)) %>%
    filter(Delay <= threshold_two_last_four_races_delay) %>%
    as.data.frame()
  if(nrow(df_infos_last_four_race)>0){
    freq_vec_candidates_form_based_podium_four_races <- table(df_infos_last_four_race$Cheval)
    if(max(freq_vec_candidates_form_based_podium_four_races)>=3){
      vec_candidates_form_based_podium_four_last_races <- names(freq_vec_candidates_form_based_podium_four_races)[freq_vec_candidates_form_based_podium_four_races==4]
    }
  }
  message("Finishing getting details for four races")
  
  message("Starting getting details for last four races")
  df_infos_last_five_race <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    select(Cheval,Date,Place) %>%
    unique() %>%
    drop_na() %>%
    group_by(Cheval) %>%
    top_n(5,Date) %>%
    filter(Place <= 6) %>%
    mutate(Delay = as.numeric(unique(df_infos_target_race_geny$Date)-Date)) %>%
    filter(Delay <= threshold_two_last_five_races_delay) %>%
    as.data.frame()
  if(nrow(df_infos_last_five_race)>0){
    freq_vec_candidates_form_based_podium_five_races <- table(df_infos_last_five_race$Cheval)
    if(max(freq_vec_candidates_form_based_podium_five_races)>=3){
      vec_candidates_form_based_podium_five_races <- names(freq_vec_candidates_form_based_podium_five_races)[freq_vec_candidates_form_based_podium_five_races>=4]
    }
  }
  message("Finishing getting details for four races")

  message("Starting getting details for best performance based on speed")
  df_infos_best_speed <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    select(Cheval,Date,Speed) %>%
    unique() %>%
    drop_na() %>%
    group_by(Cheval) %>%
    top_n(1,Speed) %>%
    mutate(Delay = as.numeric(unique(df_infos_target_race_geny$Date)-Date)) %>%
    filter(Delay <= threshold_last_perf_delay) %>%
    as.data.frame()
  if(nrow(df_infos_best_speed)>0){
    vec_candidates_form_based_last_speed <- unique(df_infos_best_speed$Cheval)
  }
  message("Finish getting details for best performance based on speed")
  
  message("Starting getting details for best performance based on earning")
  df_infos_highest_earning <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    select(Cheval,Date,Gains) %>%
    unique() %>%
    filter(Gains>0) %>%
    group_by(Cheval) %>%
    top_n(1,Gains) %>%
    mutate(Delay = as.numeric(unique(df_infos_target_race_geny$Date)-Date)) %>%
    filter(Delay <= threshold_last_perf_delay) %>%
    as.data.frame()
  if(nrow(df_infos_highest_earning)>0){
    vec_candidates_form_based_highest_earning <- unique(df_infos_highest_earning$Cheval)
  }
  message("Finish getting details for best performance based on earning")

  message("Start compiling potential candidates")
  vec_candidates_form_compilation <- c(vec_candidates_form_based_winner_last_race,
    vec_candidates_form_based_podium_two_last_races,
    vec_candidates_form_based_podium_three_last_races,
    vec_candidates_form_based_podium_four_last_races,
    vec_candidates_form_based_podium_five_races,
    vec_candidates_form_based_last_speed,
    vec_candidates_form_based_highest_earning)
  freq_candidates_form_compilation <- table(vec_candidates_form_compilation)
  if(length(freq_candidates_form_compilation)>0){
    df_infos_form_indicators[df_infos_form_indicators$Cheval %in% unique(names(freq_candidates_form_compilation)),"Form"] <- 1
  }
  message("Start compiling potential candidates")
  
  return(df_infos_form_indicators)
  
}