#' @return Compute mean earning by distance category
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_success_current_hippodrome_letrot(mat_infos_race_input = NULL)
#' @export
get_horse_percent_success_remontada_geny <- function(df_infos_target_race = NULL,
                                                     path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                     number_days_back=366) 
{
  
  df_scores_remontada <- NULL
  df_percent_races_remontada <- NULL
  df_percent_success_remontada <- NULL
  
  if(unique(df_infos_target_race$Discipline) %in% c("Trot Monté","Trot Attelé")){
    message("Initialization of the final output")
    df_percent_success_remontada <- df_infos_target_race[,"Cheval",drop=FALSE]
    df_percent_success_remontada$REMONTADA <- 0
    
    # Retrieve race date and convert to the right format if needed
    if(class(unique(df_infos_target_race$Date))!="Date") {
      race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
    } else {
      race_date <- unique(df_infos_target_race$Date)
    }
    
    # Retrieve race characteristics
    current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))
    # Retrieve race characteristics
    current_race_discipline <- gsub("\n","",unique(df_infos_target_race$Discipline))
    # Retrieve race characteristics
    current_race_category <- gsub("\n","",unique(df_infos_target_race$Epreuve))
    
    # Reading historical race infos file
    message("Reading historical race infos file")
    if(!exists("df_historical_races_geny")) {
      if(file.exists(path_df_historical_races_geny)) {
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    } 
    
    vec_races_interet <- df_historical_races_geny %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(Date<race_date)  %>%
      filter(race_date-Date<=number_days_back)  %>%
      dplyr::select(Race) %>%
      unlist() %>%
      unique()
    
    if(length(vec_races_interet)>0) {
      df_historical_races_geny_focus_infos <-  df_historical_races_geny[ df_historical_races_geny$Race %in% vec_races_interet,]
      list_historical_races_letrot_focus <- split.data.frame(df_historical_races_geny_focus_infos,df_historical_races_geny_focus_infos$Race)
      list_historical_races_letrot_focus <- lapply(list_historical_races_letrot_focus,get_remontada_geny)
      df_historical_races_geny_focus  <- bind_rows(list_historical_races_letrot_focus)
      df_historical_races_geny_focus  <- as.data.frame(df_historical_races_geny_focus)
      df_historical_races_geny_focus  <- df_historical_races_geny_focus[df_historical_races_geny_focus$Cheval %in%current_race_horses ,]
      if(nrow(df_historical_races_geny_focus)>0) {
        df_historical_races_geny_focus$Place <- as.numeric(df_historical_races_geny_focus$Place)
        df_percent_races_remontada <- df_historical_races_geny_focus[,c("Cheval","Distance","Recul","Place")] %>%
          group_by(Cheval) %>%
          dplyr::summarise(NUMBER_RACE_TOTAL=n(),NUMBER_RACES_RECUL = sum(Recul),GLOBAL_PERCENT_RACES_REMONTADA=round(NUMBER_RACES_RECUL/NUMBER_RACE_TOTAL,2)) %>%
          as.data.frame() %>%
          select(Cheval,GLOBAL_PERCENT_RACES_REMONTADA)
        
        df_percent_success_remontada <- df_historical_races_geny_focus[,c("Cheval","Distance","Recul","Place")] %>%
          filter(Recul==1) %>%
          group_by(Cheval) %>%
          dplyr::summarise(NUMBER_RACES_RECUL=n(),NUMBER_SUCCESS_RECUL=get_number_top_five(Place),GLOBAL_PERCENT_SUCCESS_REMONTADA=round(NUMBER_SUCCESS_RECUL/NUMBER_RACES_RECUL,2)) %>%
          as.data.frame() %>%
          select(Cheval,GLOBAL_PERCENT_SUCCESS_REMONTADA) 
        
        list_scores_remontada <- list(Number=df_percent_races_remontada,Percent=df_percent_success_remontada)
        list_scores_remontada <- list_scores_remontada[lapply(list_scores_remontada,length)>0]
        
        df_scores_remontada <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_scores_remontada)
        
        if(!is.null(df_scores_remontada)){
          if(nrow(df_scores_remontada)>0){
            if(nrow(df_scores_remontada)<nrow(df_infos_target_race)){
              df_scores_remontada <- merge(df_scores_remontada,df_infos_target_race[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
            }
            if(sum(is.na(df_scores_remontada$GLOBAL_PERCENT_RACES_REMONTADA))>0) {
              df_scores_remontada$GLOBAL_PERCENT_RACES_REMONTADA[is.na(df_scores_remontada$GLOBAL_PERCENT_RACES_REMONTADA)] <- 0
            }
            if(sum(is.na(df_scores_remontada$GLOBAL_PERCENT_SUCCESS_REMONTADA))>0) {
              df_scores_remontada$GLOBAL_PERCENT_SUCCESS_REMONTADA[is.na(df_scores_remontada$GLOBAL_PERCENT_SUCCESS_REMONTADA)] <- -1
            }
          }
        }
      }
    }
  }

  return(df_scores_remontada)
}
