get_horse_infos_success_similar_weight_geny <- function(df_infos_target_race_geny = NULL, df_historical_races_competitors_geny = NULL,number_days_back=366, path_cluster_distance_range_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/mat_ranges_cluster_distance.rds"){
  
  df_infos_weight_similar_weight <- NULL
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Plat","Steeplechase","Cross","Haies")){
    if(!is.null(df_infos_target_race_geny)) {
      message("Extraction date of the race")
      if(class(unique(df_infos_target_race_geny$Date))!="Date") {
        current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
      } else {
        current_race_date <- unique(df_infos_target_race_geny$Date)
      }
      message("Extract terrain of current race")
      current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
      message("Extract terrain of current race")
      current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
      message("Extracting horse names")
      current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
      message("Extracting race type")
      current_race_discipline <- gsub("\n","",unique(df_infos_target_race_geny$Discipline))
      message("Extracting hippodrome")
      current_race_hippodrome <- gsub("\n","",unique(df_infos_target_race_geny$Lieu))
      message("Extracting race distance")
      current_race_distance <- min(df_infos_target_race_geny$Distance)
    }

    message("Reading file with cluster distance definition")
    df_cluster_distance_range <- readRDS(path_cluster_distance_range_geny)
    df_cluster_distance_range_discipline <- df_cluster_distance_range[df_cluster_distance_range$Discipline==current_race_discipline,]

    message("Adding distance cluster of the matrix with details on best speed")
    df_infos_target_race_geny$Cluster_Distance <- NA
    for(i in 1:nrow(df_cluster_distance_range_discipline))
    {
      idx_cluster_distance_group <- df_cluster_distance_range_discipline[i,"Lower"] <= unlist(df_infos_target_race_geny[,"Distance"]) & df_cluster_distance_range_discipline[i,"Upper"] >= unlist(df_infos_target_race_geny[,"Distance"])
      df_infos_target_race_geny[idx_cluster_distance_group==TRUE,"Cluster_Distance"] <- paste(df_cluster_distance_range_discipline[i,c("Lower","Upper")],collapse = "-")
    }

    current_race_cluster_distance <- unique(df_infos_target_race_geny$Cluster_Distance)[1]

    df_infos_weight_similar_weight <- df_historical_races_competitors_geny %>%
      filter(current_race_date-Date<=number_days_back) %>%
      filter(Discipline == current_race_discipline) %>%
      filter(Terrain == current_race_terrain) %>%
      # filter(current_race_distance-Distance<400) %>%
      filter(Cheval %in% current_race_horses) %>%
      # filter(Gains>0) %>%
      select(Cheval, Gains,Poids,Prix) %>%
      drop_na() %>%
      distinct() %>%
      group_by(Cheval) %>%
      top_n(1,Poids) %>%
      group_by(Cheval) %>%
      top_n(1,Prix) %>%
      group_by(Cheval) %>%
      top_n(1,Gains) %>%
      as.data.frame()

    if(nrow(df_infos_weight_similar_weight)>0){
      df_infos_target_race_geny_focus <- df_infos_target_race_geny[,c("Cheval","Poids","Prix")]
      colnames(df_infos_target_race_geny_focus) <- c("Cheval","Weight","Prize")
      df_infos_weight_similar_weight <- merge(df_infos_target_race_geny_focus,df_infos_weight_similar_weight,by.x="Cheval",by.y="Cheval",all.x=TRUE)
      df_infos_weight_similar_weight$DeltaWeight <- NA
      df_infos_weight_similar_weight$SignalWeight <- NA
      for(idx_row in 1:nrow(df_infos_weight_similar_weight)){
        df_infos_weight_similar_weight$DeltaWeight[idx_row] <- median(df_infos_weight_similar_weight[-idx_row,"Weight"] - df_infos_weight_similar_weight[idx_row,"Weight"])
        if(!is.na(df_infos_weight_similar_weight[idx_row,"Poids"])){
          df_infos_weight_similar_weight$SignalWeight[idx_row] <- df_infos_weight_similar_weight[idx_row,"Poids"]-df_infos_weight_similar_weight[idx_row,"Weight"]
        }
      }
    }
  }
  return(df_infos_weight_similar_weight)
}