get_sensitivity_final_score_geny <- function(df_final_score = NULL) {
  
  df_final_score_sensitive <- NULL
  if(!is.null(df_final_score)){
    if(length(intersect(c("Class","Aptitude","Fitness","Intention"),colnames(df_final_score)))== length(c("Class","Aptitude","Fitness","Intention"))){
      df_final_score_sensitive <- df_final_score[,c("Cheval","Class","Aptitude","Fitness","Intention")]
      for(idx_col in c("Class","Aptitude","Fitness","Intention")){
        df_final_score_sensitive[,idx_col ] <- apply(df_final_score[,setdiff(c("Class","Aptitude","Fitness","Intention"),grep(idx_col,colnames(df_final_score_sensitive),value=TRUE))], 1, function(x){sum(x,na.rm = TRUE)})  
      }
    }
  }
  return(df_final_score_sensitive)
}





