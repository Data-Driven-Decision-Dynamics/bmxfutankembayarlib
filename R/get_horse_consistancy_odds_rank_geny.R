get_horse_consistancy_odds_rank_geny <- function (df_infos_target_race_geny = NULL, target_horse = NULL, path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds", number_days_back = 366) {
  
  df_mean_odds_per_issues <- NULL
  df_engaged_ensured <- NULL
  df_stats_odds_issues <- NULL
  
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }

  current_race_date <- unique(df_infos_target_race_geny$Date)
  
  df_infos_races_current_races <- df_historical_races_geny %>%
    filter(Cheval == target_horse) %>%
    filter(current_race_date-Date<=number_days_back) %>%
    dplyr::select(Cheval,Date,Cote_1,Cote_2,Place,Race) %>%
    distinct() %>%
    drop_na() %>%
    as.data.frame()
  if(nrow(df_infos_races_current_races)>0){
    df_infos_races_current_races <- df_infos_races_current_races[complete.cases(df_infos_races_current_races),]
    df_infos_races_current_races$Cote_2 <- as.numeric(df_infos_races_current_races$Cote_2)
    df_infos_races_current_races$Cote_1 <- as.numeric(df_infos_races_current_races$Cote_1)
    df_infos_races_current_races$Place <- as.numeric(df_infos_races_current_races$Place)
    test_low_odds_and_podium <- df_infos_races_current_races$Cote_1<=20 & df_infos_races_current_races$Cote_2<=20 & df_infos_races_current_races$Place<=5
    df_infos_races_current_races$Consistancy <- 0
    df_infos_races_current_races$Podium <- 0
    if(sum(test_low_odds_and_podium)>0){
      df_infos_races_current_races[test_low_odds_and_podium,"Consistancy"] <- 1
      df_engaged_ensured <- df_infos_races_current_races %>%
        group_by(Cheval) %>%
        dplyr::summarise(NUMBER_RACES_ENGAGED = n(),
                         NUMBER_RACES_ENSURED = sum(Consistancy),
                         PERCENT_ITEM_ENSURED = round(NUMBER_RACES_ENSURED/NUMBER_RACES_ENGAGED,2)) %>%
        select(Cheval,NUMBER_RACES_ENGAGED,PERCENT_ITEM_ENSURED) %>%
        as.data.frame()
    }
  }
  
  test_horse_podium <- df_infos_races_current_races$Place<=5
  if(sum(test_horse_podium)>0){
    df_infos_races_current_races[test_horse_podium,"Podium"] <- 1
    df_mean_odds_per_issues <- df_infos_races_current_races [,c("Cheval","Cote_2","Podium")] %>%
      pivot_wider(
        names_from = Podium,
        values_from = Cote_2,
        values_fn = mean
      ) %>%
      as.data.frame()
    if(nrow(df_mean_odds_per_issues)>0){
      colnames(df_mean_odds_per_issues) <- sub("0","MEAN_ODDS_LOOSING",colnames(df_mean_odds_per_issues))
      colnames(df_mean_odds_per_issues) <- sub("1","MEAN_ODDS_WINNING",colnames(df_mean_odds_per_issues))
      df_mean_odds_per_issues[,-1] <- round(df_mean_odds_per_issues[,-1],2)
    }
  }
  
  list_infos_odds_issues <- list(Odds=df_mean_odds_per_issues,Sure=df_engaged_ensured)
  list_infos_odds_issues <- list_infos_odds_issues[lengths(list_infos_odds_issues)>0]
  
  if(length(list_infos_odds_issues)>0) {
    df_stats_odds_issues <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_infos_odds_issues)
    df_stats_odds_issues <- df_stats_odds_issues[,c("Cheval",setdiff(colnames(df_stats_odds_issues),c("Cheval")))]
    df_stats_odds_issues <- unique(df_stats_odds_issues)
  }  

  return(df_stats_odds_issues)
}


