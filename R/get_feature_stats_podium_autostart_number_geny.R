get_feature_stats_podium_autostart_number_geny <- function(df_infos_target_race_geny = NULL,
                                                           path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                           path_stats_numero_autostart = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_stats_number_autostart.rds",
                                                           number_days_back = 720){
  
  message("Initialisation of final output")
  df_best_ranking_worst_position <- NULL
  df_infos_feature_chance_autostart <- NULL
  df_features_autostart <- NULL
  message("Initialisation of final output")
  
  if(unique(df_infos_target_race_geny$Discipline) == "Trot Attelé"){
    if(unique(df_infos_target_race_geny$Discipline) == "Trot Attelé"){
      if(!exists("df_historical_races_geny")) {
        if(file.exists(path_df_historical_races_geny)){
          df_historical_races_geny <- readRDS(path_df_historical_races_geny)
        }
      }
    }
    
    df_performance_focus <- df_historical_races_geny %>%
      filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
      filter(Discipline == unique(df_infos_target_race_geny$Discipline) ) %>%
      filter(Depart == "Auto-Start") %>%
      filter(Date <= unique(df_infos_target_race_geny$Date) ) %>%
      filter(unique(df_infos_target_race_geny$Date)-Date<=number_days_back)  %>%
      select(Cheval,Numero,Lieu,Distance,Place,Numero) %>%
      filter(Place <= 3) %>%
      mutate(Numero= as.numeric(Numero)) %>%
      mutate(Merger = paste(Numero,Lieu,sep="_")) %>%
      mutate(Merger = paste(Merger,Distance,sep="-")) %>% 
      unique() %>%
      as.data.frame()
    if(nrow(df_performance_focus)>0){
      df_stats_numero_autostart <- read_rds(path_stats_numero_autostart)
      df_stats_numero_autostart$Merger <- paste(df_stats_numero_autostart$Numero,df_stats_numero_autostart$ID,sep="_")
      df_performance_focus <- merge(df_performance_focus,df_stats_numero_autostart[,c("Merger","Chance")])
      df_best_ranking_worst_position <- df_performance_focus %>%
        group_by(Cheval) %>%
        dplyr::summarise(CLASS_BEST_RANKING_WORST_POSITION = min(Chance)) %>%
        as.data.frame()
      if(nrow(df_best_ranking_worst_position)<nrow(df_infos_target_race_geny)){
        df_best_ranking_worst_position <- merge(df_best_ranking_worst_position,df_infos_target_race_geny[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
      }
      if(sum(is.na(df_best_ranking_worst_position$CLASS_BEST_RANKING_WORST_POSITION))>0){
        df_best_ranking_worst_position[is.na(df_best_ranking_worst_position$CLASS_BEST_RANKING_WORST_POSITION),"CLASS_BEST_RANKING_WORST_POSITION"] <- 1
      }
      df_best_ranking_worst_position$CLASS_BEST_RANKING_WORST_POSITION_NORMALIZED <- NA
      for(idx_row in 1:nrow(df_best_ranking_worst_position)){
        df_best_ranking_worst_position[idx_row,"CLASS_BEST_RANKING_WORST_POSITION_NORMALIZED"] <-  round(mean(df_best_ranking_worst_position[idx_row,"CLASS_BEST_RANKING_WORST_POSITION"]-df_best_ranking_worst_position[-idx_row,"CLASS_BEST_RANKING_WORST_POSITION"]),2)
      }
      df_best_ranking_worst_position <- df_best_ranking_worst_position[,-2]
    }
  }
  
  if(unique(df_infos_target_race$Discipline) =="Trot Attelé"){
    if(unique(df_infos_target_race$Depart)== "Auto-Start"){
      df_stats_numero_autostart <- read_rds(path_stats_numero_autostart)
      df_stats_numero_autostart$Merger <- paste(df_stats_numero_autostart$Numero,df_stats_numero_autostart$ID,sep="_")
      df_infos_target_race$ID <- paste(df_infos_target_race$Lieu,df_infos_target_race$Longueur,sep="-")
      df_infos_target_race$Merger <- paste(df_infos_target_race$Numero,df_infos_target_race$ID,sep="_")
      df_stats_numero_autostart <- merge(df_infos_target_race[,c("Merger","Cheval","Numero")],df_stats_numero_autostart[,c("Merger","Chance")],by.x = "Merger",by.y="Merger")
      df_stats_numero_autostart <- df_stats_numero_autostart[,c("Cheval","Chance")]
      colnames(df_stats_numero_autostart)[2] <- "ENGAGEMENT_POSITION_AUTOSTART"
      df_stats_numero_autostart$ENGAGEMENT_POSITION_AUTOSTART <- round(get_percentile_values(df_stats_numero_autostart$ENGAGEMENT_POSITION_AUTOSTART),2)
      if(!is.null(df_best_ranking_worst_position) & !is.null(df_stats_numero_autostart)){
        df_features_autostart <- merge(df_best_ranking_worst_position,df_stats_numero_autostart,by.x="Cheval",by.y="Cheval")
      }
    } else {
      df_features_autostart <- df_best_ranking_worst_position
    }
  }

  return(df_features_autostart)
}
 






