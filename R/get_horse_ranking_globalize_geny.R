get_horse_ranking_globalize_geny <- function(df_infos_target_race_geny = NULL,threshold_intrinsic = 720) {
  
  message("Initialization of the output")
  df_class_all <- NULL
  
  message("Extracting race date")
  current_race_date <- as.Date(as.factor(unique(df_infos_target_race_geny$Date)))
  
  message("Computing percentage pairwise wins")
  df_percent_wins <- get_horse_percent_win_pairwise_comparison_geny(df_infos_target_race_geny)
  colnames(df_percent_wins)[2] <-"Wins"
  
  message("Compute indicators of class based on glicko ranking for each candidate horse")
  df_class_pure <- NULL
  df_class_pure_temporal <- NULL
  infos_class_pure <- get_horse_ranking_per_distance_geny(df_infos_target_race_geny)
  df_class_pure <- infos_class_pure [["mat_res_rating_specific_compliled"]]
  df_class_pure_temporal <- infos_class_pure [["mat_res_rating_temporal"]]
  
  if(!is.null(df_class_pure)){
    df_class_pure[,"GLOBAL_MAX_PARTIAL_RANKING_HORSE"] <- round(apply(df_class_pure [,4:ncol(df_class_pure),drop=FALSE],1,function(x) {max(x,na.rm = TRUE)}),2)
    rownames(df_class_pure) <- as.vector(df_class_pure$Cheval)
    df_class_pure <- df_class_pure[,-1,drop=FALSE]
    df_class_pure <- df_class_pure[,-1,drop=FALSE]
    df_class_pure$Cheval <- rownames(df_class_pure)
    vec_num_val_clust <- apply(df_class_pure[,-ncol(df_class_pure)],2,function(x){sum(!is.na(x))})
    if(!is.null(df_class_pure)){
      if(!is.null(df_class_pure)) {
        df_class_pure$Cheval <- as.vector(rownames(df_class_pure))
        df_class_pure <- df_class_pure[,c("Cheval",setdiff(colnames(df_class_pure),"Cheval"))]
        df_class_pure[,-1] <- round(df_class_pure[,-1],2)
        df_class_pure$GLOBAL_MAX_PARTIAL_RANKING_HORSE = unlist(as.data.frame(df_class_pure$GLOBAL_MAX_PARTIAL_RANKING_HORSE))
        rownames(df_class_pure) <- NULL
        vec_candidates_times_intrinsic <- colnames(df_class_pure_temporal)[-1]
        vec_candidates_times_intrinsic <- vec_candidates_times_intrinsic[vec_candidates_times_intrinsic<current_race_date]
        time_last_available <- max(vec_candidates_times_intrinsic)
        if(length(time_last_available)==1) {
          mat_class_last_date <- df_class_pure_temporal[,c("Cheval",time_last_available)]
          colnames(mat_class_last_date)[2] <- "GLOBAL_LAST_RANKING_SCORE_HORSE"
          df_class_pure <- merge(df_class_pure,mat_class_last_date,by.x="Cheval",by.y="Cheval",all=TRUE)
        }
        
        vec_candidates_times_intrinsic <- vec_candidates_times_intrinsic [as.numeric(current_race_date-as.Date(vec_candidates_times_intrinsic)) < threshold_intrinsic ]
        if(length(vec_candidates_times_intrinsic)>=1){
          mat_class_intrinsic <- df_class_pure_temporal[,c("Cheval",vec_candidates_times_intrinsic)]
          vec_val_intrinsic <- apply(mat_class_intrinsic[,2:ncol(mat_class_intrinsic),drop=FALSE],1,function(x) {max(x,na.rm = TRUE)})
          rownames(df_class_pure) <- as.vector(df_class_pure$Cheval)
        }
        
        df_class_pure <- df_class_pure[,c("Cheval",setdiff(colnames(df_class_pure),"Cheval"))]
        rownames(df_class_pure) <- NULL
        df_class_pure <- df_class_pure[,c("Cheval",grep("MAX|LAST",colnames(df_class_pure),value=TRUE))]
        colnames(df_class_pure)[2:3] <- c("Partial","Last")
      }
    }
  }
  
  message("Compute indicators of class based on glicko ranking for beaten candidate")
  df_class_beaten <- get_horse_beaten_ranking_geny(df_infos_target_race_geny)
  if(!is.null(df_class_beaten)){
    if(sum(!is.na(df_class_beaten$GLOBAL_RANKING_DEFEATED_HORSES))>0){
      rownames(df_class_beaten) <- as.vector(df_class_beaten$Cheval)
      df_class_beaten <- df_class_beaten[,-1,drop=FALSE]
      df_class_beaten$Cheval <- as.vector(rownames(df_class_beaten))
      df_class_beaten <- df_class_beaten[,c("Cheval",setdiff(colnames(df_class_beaten),"Cheval"))]
      df_class_beaten[,-1] <- round(df_class_beaten[,-1],2)
      rownames(df_class_beaten) <- NULL
    }
  }
  
  colnames(df_class_beaten) [ncol(df_class_beaten)] <- "Defeat"
  if(sum(!is.na(df_class_beaten$Defeat))==0){
    df_class_beaten <- NULL
  }
  
  list_df_class <- list(Wins=df_percent_wins,Global=df_class_pure,Beaten=df_class_beaten)
  list_df_class <- list_df_class[lapply(list_df_class,length)>0]
  if(length(list_df_class)>0) {
    df_class_all <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_df_class)
    df_class_all <- unique(df_class_all)
  }
  
  return(df_class_all)
}
                                                

