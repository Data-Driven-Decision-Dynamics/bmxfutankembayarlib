graph_dashboard_race_geny_backup <- function (url_target_race="http://www.geny.com/partants-pmu/2019-10-18-vincennes-pmu-prix-themis_c1102666",
                                       file_name_target=NULL,
                                       path_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds",
                                       path_cluster_distance_range = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/input/mat_ranges_cluster_distance.rds")
{
  mat_infos_target_race <- get_details_race_geny(url_target_race)

  mat_historical_races_focus <- get_historical_performance_horse_race_geny(url_target_race)$mat_perf_race_target_horses
  mat_historical_races_focus <- unique(mat_historical_races_focus)

  file_name_output <- url_target_race
  file_name_output <- gsub("http://www.geny.com/partants-pmu/","",file_name_output)
  file_name_output <- gsub("é","e",file_name_output)
  file_name_output <- gsub("è","e",file_name_output)
  file_name_output <- gsub(" ","-",file_name_output)
  file_name_output <- gsub("\\(","",file_name_output)
  file_name_output <- gsub("\\)","",file_name_output)
  file_name_output <- paste("dashboard-",file_name_output,sep="")
  file_name_output <- paste(file_name_output,"tiff",sep=".")
  
  if(!is.null(url_target_race)){
    infos_scoring_ranking <- get_feature_engineering_geny(url_target_race=url_target_race)
  } else {
    infos_scoring_ranking <- get_feature_engineering_geny(url_target_race=NULL,file_name_target=file_name_target)
  }
  
  mat_scoring_ranking <- infos_scoring_ranking$mat_all_combined_features_imputed
  rownames(mat_scoring_ranking) <- as.vector(mat_scoring_ranking$Cheval)
  columns_keep <- c(c("Cheval","Numero","Gender","Age","Distance","Recul","Poids","Ferrage","Discipline","Lieu","Prix",grep('DRIVER',colnames(mat_scoring_ranking),value=TRUE)),grep("SPECIFIC_SCORE|ITR",colnames(mat_scoring_ranking),value = TRUE))
  columns_keep <- intersect(columns_keep,colnames(mat_scoring_ranking))
  mat_scoring_ranking <- mat_scoring_ranking[,columns_keep]
  

  mat_horse_epreuve_sum_earning <- NULL
  mat_cat_earning_profile <- geny_horse_category_earning_profile_geny(url_target_race)
  if(sum(!is.na(mat_cat_earning_profile$Epreuve))>0) {
    mat_cat_earning_profile_melt <- melt(mat_cat_earning_profile, id=c("Cheval", "Epreuve"),measure.vars='Gains',na.rm=TRUE)
    mat_cat_earning_profile_cast <- reshape2::acast(mat_cat_earning_profile_melt, Cheval ~ Epreuve ~ variable,mean)
    mat_horse_epreuve_sum_earning <- as.data.frame(mat_cat_earning_profile_cast)
    mat_horse_epreuve_sum_earning <- as.matrix(mat_horse_epreuve_sum_earning)
    colnames(mat_horse_epreuve_sum_earning) <- gsub(".Gains","",colnames(mat_horse_epreuve_sum_earning))
    val_groups <- grep("I",colnames(mat_horse_epreuve_sum_earning),value=TRUE)
    if(length(val_groups)>0) {
      mat_horse_epreuve_sum_earning <- mat_horse_epreuve_sum_earning[,rev(c(val_groups,setdiff(colnames(mat_horse_epreuve_sum_earning),val_groups)))]
    }
  }
  
  # cloud(mat_horse_epreuve_sum_earning, panel.3d.cloud = panel.3dbars,
  #       xbase = 0.4, ybase = 0.4, zlim = c(0, max(mat_horse_epreuve_sum_earning)),
  #       scales = list(arrows = FALSE, just = "right"), xlab = NULL, ylab = NULL,zlab=NULL,
  #       screen = list(z = 40, x = -30))
  # 
  # p <- plot_ly(
  #   x =rownames(mat_horse_epreuve_sum_earning), y = colnames(mat_horse_epreuve_sum_earning),
  #   z = t(mat_horse_epreuve_sum_earning), type = "heatmap",colors ="Blues"
  # )
  
  mat_infos_target_race   <- get_details_race_geny(url_target_race)
  mat_epreuve_race <- get_race_group_type_geny(mat_infos_target_race$Details[1])
  vec_idx_with_grp_no_epreuve <- is.na(mat_epreuve_race$Epreuve) & !is.na(mat_epreuve_race$Group)
  if(length(vec_idx_with_grp_no_epreuve)>0) {
    mat_epreuve_race [vec_idx_with_grp_no_epreuve,"Epreuve"] <- mat_epreuve_race [vec_idx_with_grp_no_epreuve,"Group"]
  }
  
  current_race_epreuve <- as.vector(unique(mat_infos_target_race$Epreuve))
  current_race_discipline <- as.vector(unique(mat_infos_target_race$Discipline))
  
  mat_cluster_distance_range <- readRDS(path_cluster_distance_range)
  mat_cluster_distance_range_discipline <- mat_cluster_distance_range[mat_cluster_distance_range$Discipline==current_race_discipline,]
  
  mat_infos_target_race$Cluster_Distance <-NULL
  for(i in 1:nrow(mat_infos_target_race))
  {
    idx_cluster_distance_group <- mat_cluster_distance_range_discipline$Lower <= unlist(mat_infos_target_race[i,"Distance"]) & mat_cluster_distance_range_discipline$Upper >= unlist(mat_infos_target_race[i,"Distance"])
    mat_infos_target_race[i,"Cluster_Distance"] <- paste(mat_cluster_distance_range_discipline[idx_cluster_distance_group,c("Lower","Upper")],collapse = "-")
  }
  
  current_race_horses <- as.vector(unique(mat_infos_target_race$Cheval))
  current_race_date <- as.vector(unique(mat_infos_target_race$Date))
  current_race_distance <- min(mat_infos_target_race$Distance)
  current_race_cluster_distance <- names(table(mat_infos_target_race$Cluster_Distance))[which.max(table(mat_infos_target_race$Cluster_Distance))]
  current_race_location <- as.vector(unique(mat_infos_target_race$Lieu))
  current_race_location <- str_trim(unlist(strsplit(current_race_location,"\\["))[1])
  current_race_cluster_distance <- as.vector(unique(mat_infos_target_race$Cluster_Distance))[1]
  
  mat_cat_earning_profile <- merge(mat_infos_target_race[,c("Cheval","Numero")],mat_cat_earning_profile,by.x="Cheval",by.y="Cheval")
  
  vec_idx_with_grp_no_epreuve <- is.na(mat_infos_target_race$Epreuve) & !is.na(mat_infos_target_race$Group)
  if(length(vec_idx_with_grp_no_epreuve)>0) {
    mat_infos_target_race [vec_idx_with_grp_no_epreuve,"Epreuve"] <- mat_infos_target_race [vec_idx_with_grp_no_epreuve,"Group"]
  }
  
  # if (sum(!is.na(mat_infos_target_race$Epreuve))>0) {
  current_race_category <- as.vector(unique(mat_infos_target_race$Epreuve))
  # }
  
  mat_cat_earning_profile <- mat_cat_earning_profile[order(mat_cat_earning_profile$Year,mat_cat_earning_profile$Week),]
  mat_cat_earning_profile$Cluster_Distance <-NULL
  for(i in 1:nrow(mat_cat_earning_profile))
  {
    idx_cluster_distance_group <- mat_cluster_distance_range_discipline$Lower <= unlist(mat_cat_earning_profile[i,"Distance"]) & mat_cluster_distance_range_discipline$Upper >= unlist(mat_cat_earning_profile[i,"Distance"])
    mat_cat_earning_profile[i,"Cluster_Distance"] <- paste(mat_cluster_distance_range_discipline[idx_cluster_distance_group,c("Lower","Upper")],collapse = "-")
  }
  
  vec_earnings <- mat_cat_earning_profile$Gains
  vec_earnings <- vec_earnings[vec_earnings>0]
  vec_categories <- mat_cat_earning_profile$Epreuve
  vec_horses <- as.vector(unique(mat_cat_earning_profile$Cheval))
  vec_times <- as.vector(unique(mat_cat_earning_profile$Time))
  # if(sum(!is.na(mat_cat_earning_profile$Epreuve))>0) {
  #   
  # }
  vec_distances <- as.numeric(as.vector(unique(mat_cat_earning_profile$Distance)))
  vec_prize <- as.numeric(as.vector(unique(mat_cat_earning_profile$Prix)))
  vec_distances_scale <- scales::rescale(vec_distances,c(.2,.5))
  names(vec_distances_scale) <- vec_distances
  vec_prize_scale <- scales::rescale(vec_prize,c(.2,.5))
  names(vec_prize_scale) <- vec_prize
  # vec_earnings_color <- colorRampPalette(c(brewer.pal(9,"Greens")[1], brewer.pal(9,"Greens")[9]))(length(vec_earnings))[rank(vec_earnings)]
  names(vec_earnings) <- vec_earnings
  # cols <- colorRampPalette(brewer.pal(9,"RdYlGn"))(length(table.data))
  red_blue_palette <- colorRampPalette(c('white','darkgreen'))
  vec_earnings_color <- red_blue_palette (40)[as.numeric(cut(vec_earnings,breaks = 40))]
  vec_earnings_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_earnings))[rank(vec_earnings)]
  names(vec_earnings_color) <- vec_earnings
  
  tiff(file_name_output, width = 750, height = 500)
  if(sum(!is.na(mat_cat_earning_profile$Epreuve))>0) {
    layout(matrix(c(1,2,3,4,1,2,3,4), 2, 4, byrow = TRUE),widths=c(5,1.3,1,1.4))
  } else {
    layout(matrix(c(1,2,3,1,2,3), 2, 3, byrow = TRUE),widths=c(5,1,1))
  }
  
  par(mar=c(4,10,4,2))
  # par(bg="grey")
  plot(0,0,xlim=c(0,length(vec_times)),ylim=c(0,length(vec_horses)),xaxt="n",yaxt="n",type="n",xlab="",ylab="")
  rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = brewer.pal(11,"RdGy")[11])
  title('Recent Performances')
  
  vec_lab_xaxis <- gsub("2018","18",vec_times)
  vec_lab_xaxis <- gsub("2019","19",vec_lab_xaxis)
  
  mat_num_horses <- unique(mat_cat_earning_profile[,c("Numero","Cheval")])
  mat_num_horses <- mat_num_horses[order(mat_num_horses$Numero),]
  
  vec_lab_yaxis <- rev(paste(mat_num_horses$Numero,mat_num_horses$Cheval,sep="-"))
  
  axis(1, 1:length(vec_times),labels=FALSE)
  text(1:length(vec_lab_xaxis), par("usr")[3] - 0.2, labels = vec_lab_xaxis, srt = 90, pos = 1, xpd = TRUE,offset = 1,cex=1)
  
  par(las=2)
  axis(2, 1:length(vec_lab_yaxis), labels = vec_lab_yaxis)
  # text(1:length(vec_lab_yaxis), par("usr")[1], labels = vec_lab_yaxis, srt = 180, pos = 2, xpd = TRUE,offset = 1,cex=0.75)
  
  for(id_horse in 1:length(vec_lab_yaxis))
  {
    mat_cat_earning_profile_current <- mat_cat_earning_profile[mat_cat_earning_profile$Cheval %in% unlist(strsplit(vec_lab_yaxis[id_horse],"-"))[2],]
    mat_cat_earning_profile_current <- as.data.frame(mat_cat_earning_profile_current) 
    mat_cat_earning_profile_current <- mat_cat_earning_profile_current[order(mat_cat_earning_profile_current$Year,mat_cat_earning_profile_current$Week),]
    mat_cat_earning_profile_current <- mat_cat_earning_profile_current[!is.na(mat_cat_earning_profile_current$Place),]
    
    for(id_row in 1:nrow(mat_cat_earning_profile_current)) {
      disc_race_current <- mat_cat_earning_profile_current[id_row,"Discipline"]
      clus_disc_race_current <- mat_cat_earning_profile_current[id_row,"Cluster_Distance"]
      name_race_current <- mat_cat_earning_profile_current[id_row,"Race"]
      loc_race_current <- mat_cat_earning_profile_current[id_row,"Lieu"]
      dist_race_current <- mat_cat_earning_profile_current[id_row,"Distance"]
      time_race_current <- mat_cat_earning_profile_current[id_row,"Time"]
      time_race_current <- sub("2018","18",time_race_current)
      time_race_current <- sub("2019","19",time_race_current)
      pos_current_time <- which(vec_lab_xaxis==time_race_current)
      category_race_current <- mat_cat_earning_profile_current[id_row,"Epreuve"]
      
      dist_race_current <- mat_cat_earning_profile_current[id_row,"Distance"]
      val_upper_lower_xaxis <- as.numeric(vec_distances_scale[as.character(dist_race_current)])
      
      prize_race_current <- mat_cat_earning_profile_current[id_row,"Prix"]
      val_upper_lower_yaxis <- as.numeric(vec_prize_scale[as.character(prize_race_current)])
      
      earning_race_current <- mat_cat_earning_profile_current[id_row,"Gains"]
      earning_race_current <- ifelse(is.na(earning_race_current),0,earning_race_current)
      col_current_earning  <- vec_earnings_color[unique(which(vec_earnings==earning_race_current))]
      
      if(earning_race_current==0) {
        col_current_earning <- "#FFFFFF"
      }
      
      place_current <- mat_cat_earning_profile_current[id_row,"Place"]
      
      if(place_current==100) {
        col_current_earning <- NA
      }
      
      val_density <- -1
      
      if(place_current==100) {
        col_current_earning <- NA
        val_density <- 100
      }
      
      val_lty <- "solid"
      val_lwd <- 1
      
      mat_perf_current_race_completed <- mat_historical_races[mat_historical_races$Race == name_race_current,]
      mat_perf_current_race_completed$Poids <- sub(",",".",mat_perf_current_race_completed$Poids)
      vec_other_horses <- mat_perf_current_race_completed[mat_perf_current_race_completed$Cheval %in% vec_horses,"Cheval"]
      
      if(disc_race_current %in% c("Haies","Plat")) {
        poids_current_horse <- as.numeric(mat_perf_current_race_completed[mat_perf_current_race_completed$Cheval == unlist(strsplit(vec_lab_yaxis[id_horse],("-")))[2],"Poids"])
        poids_other_horses  <- as.numeric(mat_perf_current_race_completed[mat_perf_current_race_completed$Cheval %in% setdiff(mat_perf_current_race_completed$Cheval,unlist(strsplit(vec_lab_yaxis[id_horse],("-")))[2]),"Poids"])
        if(poids_current_horse-median(poids_other_horses,na.rm = TRUE)>0) {
          val_lty <- 3
        }
        
        if(poids_current_horse-median(poids_other_horses,na.rm = TRUE)<=0) {
          val_lty <- "dashed"
        }
      }
      
      if(disc_race_current == "Trot Attelé") {
        recul_current_horse <- mat_perf_current_race_completed[mat_perf_current_race_completed$Cheval ==unlist(strsplit(vec_lab_yaxis[id_horse],("-")))[2] ,"Recul"]
        recul_other_horses <- mat_perf_current_race_completed[mat_perf_current_race_completed$Cheval %in% setdiff(mat_perf_current_race_completed$Cheval,unlist(strsplit(vec_lab_yaxis[id_horse],("-")))[2]),"Recul"]
        
        if(sum(recul_current_horse<recul_other_horses)>0) {
          val_lty <- "dashed"
        } 
        
        if(sum(recul_current_horse>recul_other_horses)>0) {
          val_lwd <- 3
        } 
      }
      
      val_border ="grey"
      if(loc_race_current==current_race_location & clus_disc_race_current!=current_race_cluster_distance){
        val_border <- "red"
      } 
      # 
      # if(loc_race_current==current_race_location & clus_disc_race_current==current_race_cluster_distance & category_race_current >= current_race_category) {
      #   val_border <- "red"
      # }
      
      rect(pos_current_time-val_upper_lower_xaxis,id_horse-val_upper_lower_yaxis,pos_current_time+val_upper_lower_xaxis,id_horse+val_upper_lower_yaxis,col=col_current_earning,border=val_border,density=val_density,lty=val_lty,lwd=val_lwd)
      if(length(vec_other_horses)>1) {
        x_pos <- ((pos_current_time+val_upper_lower_xaxis) +(pos_current_time-val_upper_lower_xaxis))/2
        y_pos <- ((id_horse-val_upper_lower_yaxis) +(id_horse+val_upper_lower_yaxis))/2
        text(x_pos,y_pos,length(vec_other_horses)-1,cex=0.6)
      }
    }
  }
  
  vec_col_scores <- grep("_",colnames(mat_scoring_ranking),value = TRUE)
  vec_cluster_value <- gsub("SPECIFIC_SCORE_","",vec_col_scores)
  vec_cluster_value <- gsub("ITR_","",vec_cluster_value)
  vec_cluster_value <- unique(vec_cluster_value)
  
  if(exists("mat_horse_epreuve_sum_earning") & !is.null(mat_horse_epreuve_sum_earning) ) {
    vec_epreuve <- rev(colnames(mat_horse_epreuve_sum_earning))
    vec_epreuve_values <- unlist(as.data.frame(mat_horse_epreuve_sum_earning))
    vec_epreuve_values <- vec_epreuve_values[!is.na(vec_epreuve_values)]
    vec_epreuve_values <- vec_epreuve_values[vec_epreuve_values>0]
    vec_epreuve_values_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_epreuve_values))[rank(vec_epreuve_values)]
    # vec_epreuve_values_color <- colorRampPalette(c("red", "darkgreen"))(length(vec_epreuve_values))[rank(vec_epreuve_values)]
    names(vec_epreuve_values_color) <- vec_epreuve_values
    
    if(length(grep("I|II|III",vec_epreuve,value = TRUE))>0) {
      vec_epreuve <- rev(c(sort(grep("I",vec_epreuve,value = TRUE)),setdiff(vec_epreuve,sort(grep("I",vec_epreuve,value = TRUE)))))
    }
    
    if(length(unique(mat_cat_earning_profile$Epreuve))==1) {
      vec_epreuve <- unique(mat_cat_earning_profile$Epreuve)
      if(class(mat_horse_epreuve_sum_earning)!="data.frame")
      {
        mat_horse_epreuve_sum_earning <- as.data.frame(mat_horse_epreuve_sum_earning)
        colnames(mat_horse_epreuve_sum_earning) <- vec_epreuve
      } 
    }
    
    vec_epreuve <- rev(vec_epreuve)
    vec_lab_xaxis <- vec_epreuve
    
    vec_lab_xaxis_col <- rep('black',length(vec_epreuve))
    if(length(grep(current_race_category,vec_epreuve))>0){
      vec_lab_xaxis_col[grep(current_race_category,vec_epreuve)] <-'red'
    }
    
    par(mar=c(4,1,4,2))
    par(las=1)
    plot(0,0,xlim=c(0,length(vec_epreuve)+1),ylim=c(0,length(vec_horses)),xaxt="n",yaxt="n",type="n",xlab="",ylab="")
    rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = brewer.pal(11,"RdGy")[11])
    title('Engagement')
    
    axis(1, 1:length(vec_lab_xaxis),labels=FALSE)
    text(1:length(vec_lab_xaxis), par("usr")[3] - 0.2, labels = vec_lab_xaxis, srt = 360, pos = 1, xpd = TRUE,offset = 1,cex=1,col=vec_lab_xaxis_col)
    
    if(length(grep(current_race_category,vec_epreuve))>0){
      abline(v=which(vec_epreuve==vec_epreuve[vec_epreuve==current_race_category]),col="red",lty=2)
    }

    for(id_horse in 1:length(vec_lab_yaxis))
    {
      mat_horse_epreuve_sum_earning_current <- mat_horse_epreuve_sum_earning[unlist(strsplit(vec_lab_yaxis[id_horse],"-"))[2],,drop=FALSE]
      for(id_col in 1:length(vec_epreuve))
      {
        val_epreuve_current <- mat_horse_epreuve_sum_earning_current[,vec_lab_xaxis[id_col]]
        if(!is.na(val_epreuve_current) & val_epreuve_current>0) {
          epreuve_current_col <- vec_epreuve_values_color[as.character(val_epreuve_current)]
          points(id_col,id_horse,pch=15,col=epreuve_current_col,cex=2)
        }
        if(!is.na(val_epreuve_current) & val_epreuve_current==0) {
          points(id_col,id_horse,pch=0,col="white",cex=2)
        }
      }
    }
  }
  
  # mat_speed_record <- get_features_based_speed_record (url_target_race=url_target_race)
  
  rownames(mat_scoring_ranking) <- as.vector(mat_scoring_ranking$Cheval)
  mat_scoring_ranking_scale <- mat_scoring_ranking
  for(col in vec_col_scores )
  {
    mat_scoring_ranking_scale[,col] <- scales::rescale(mat_scoring_ranking_scale[,col],c(0,1))
  }
  
  mat_phenotype_horses <- as.data.frame(matrix(NA,ncol=length(vec_cluster_value)+1,nrow=length(vec_horses)))
  colnames(mat_phenotype_horses) <- c("Cheval",vec_cluster_value)
  mat_phenotype_horses[,"Cheval"] <- vec_horses
  
  mat_ranking_horses <- as.data.frame(matrix(NA,ncol=length(vec_cluster_value)+1,nrow=length(vec_horses)))
  colnames(mat_ranking_horses) <- c("Cheval",vec_cluster_value)
  mat_ranking_horses[,"Cheval"] <- vec_horses
  rownames(mat_ranking_horses) <- as.vector(mat_ranking_horses$Cheval)
  for(clus_dist in vec_cluster_value )
  {
    current_column <- grep(clus_dist,colnames(mat_ranking_horses))
    mat_ranking_horses_current <- mat_scoring_ranking_scale[,grep(grep(clus_dist,colnames(mat_ranking_horses),value = TRUE),colnames(mat_scoring_ranking_scale)),drop=FALSE]
    val_ranking_horses_current <- apply(mat_ranking_horses_current,1,function(x){mean(x,na.rm=TRUE)})
    mat_ranking_horses[names(val_ranking_horses_current),clus_dist] <- val_ranking_horses_current
  }
  
  for(clus_dist in vec_cluster_value )
  {
    vec_horse_specialist <- NULL
    current_column <- grep(clus_dist,colnames(mat_scoring_ranking))
    for(idx_col in current_column)
    {
      mat_scoring_ranking_current <- mat_scoring_ranking[!is.na(mat_scoring_ranking[,idx_col]),]
      if(nrow(mat_scoring_ranking_current)>1) {
        vec_horse_specialist_criteria <- mat_scoring_ranking_current[order(mat_scoring_ranking_current[,idx_col],decreasing = TRUE),"Cheval"]
        vec_horse_specialist_criteria <- vec_horse_specialist_criteria[1:min(length(vec_horse_specialist_criteria),ceiling(nrow(mat_scoring_ranking_current)/2))]
        vec_horse_specialist <- c(vec_horse_specialist_criteria,vec_horse_specialist)
      }
    }
    freq_horse_specialist <- table(vec_horse_specialist)
    vec_horse_specialist <- names(freq_horse_specialist)[freq_horse_specialist>1]
    mat_phenotype_horses[mat_phenotype_horses$Cheval %in% vec_horse_specialist,clus_dist] <- 1
  }
  
  mat_phenotype_horses <- mat_phenotype_horses[,apply(mat_phenotype_horses,2,function(x){return(sum(is.na(x)))})<nrow(mat_phenotype_horses)]
  if(!is.null( nrow(mat_phenotype_horses))) {
    mat_infos_target_race_add <- mat_infos_target_race[,intersect(colnames(mat_infos_target_race),c("Numero","Cheval","Poids","Valeur","Recul","Gender","Age","Ferrage"))]
    mat_phenotype_horses <- merge(mat_phenotype_horses,mat_infos_target_race_add,by.x="Cheval",by.y="Cheval")
    rownames(mat_phenotype_horses) <- as.vector(mat_phenotype_horses[,"Cheval"])
  }
  
  
  vec_scoring_values <- unlist(mat_ranking_horses[,vec_cluster_value])
  vec_scoring_values <- vec_scoring_values[!is.na(vec_scoring_values)]
  vec_scoring_values_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_scoring_values))[rank(vec_scoring_values)]
  names(vec_scoring_values_color) <- vec_scoring_values
  
  vec_horses_labels <- paste(unique(mat_cat_earning_profile$Numero),unique(mat_cat_earning_profile$Cheval),sep="-")
  vec_cluster_value <- intersect(vec_cluster_value,colnames(mat_phenotype_horses))
  vec_cluster_value <- sort(vec_cluster_value)
  
  if(!is.null(vec_cluster_value)){
    par(mar=c(4,1,4,2))
    plot(0,0,xlim=c(0,length(vec_cluster_value)+1),ylim=c(0,length(vec_horses)),xaxt="n",yaxt="n",type="n",xlab="",ylab="")
    rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = brewer.pal(11,"RdGy")[11])
    title('Ranking')
    vec_lab_xaxis <- vec_cluster_value
    
    vec_lab_xaxis_col <- rep('black',length(vec_cluster_value))
    if(length(grep(current_race_cluster_distance,vec_cluster_value))>0){
      vec_lab_xaxis_col[grep(current_race_cluster_distance,vec_cluster_value)] <-'red'
    }
    
    axis(1, 1:length(vec_lab_xaxis),labels=FALSE)
    text(1:length(vec_lab_xaxis), par("usr")[3] - 0.2, labels = vec_lab_xaxis, srt = 90, pos = 1, xpd = TRUE,offset = 2,cex=1,col=vec_lab_xaxis_col)
    
    for(id_horse in 1:length(vec_lab_yaxis))
    {
      mat_phenotype_horses_current <- mat_phenotype_horses[mat_phenotype_horses$Cheval==unlist(strsplit(vec_lab_yaxis[id_horse],"-"))[2],]
      mat_ranking_horses_current <- mat_ranking_horses[mat_ranking_horses$Cheval==unlist(strsplit(vec_lab_yaxis[id_horse],"-"))[2],]
      
      pch_type_current <- 20
      for(id_col in 1:length(vec_cluster_value))
      {
        val_type_current <- mat_phenotype_horses_current[,vec_cluster_value[id_col]]
        if(!is.na(as.numeric(val_type_current))) {
          pch_type_current <- 11
        }
        
        val_score_current <- mat_ranking_horses_current[,vec_cluster_value[id_col]]
        if(!is.na(val_score_current)) {
          score_current_col <- vec_scoring_values_color[as.character(val_score_current)]
          points(id_col,id_horse,pch=pch_type_current,col=score_current_col,cex=2)
        }
      }
    }
  }
  
  mat_speed_distance_features <- NULL
  mat_all_features <- infos_scoring_ranking$mat_all_combined_features_imputed
  columns_add <- c("Numero","Cheval",grep('DRIVER',colnames(mat_all_features),value=TRUE),"Gender","Age","Recul","Poids","Ferrage",c(grep("SPEED",colnames(mat_all_features),value = TRUE),"GLOBAL_AVERAGE_DISTANCE"))
  columns_add <- intersect(colnames(mat_all_features),columns_add)
  mat_infos_add_dashboard <- mat_all_features[,columns_add]
  vec_num_mod <- apply(mat_infos_add_dashboard,2,function(x){length(unique(x))})
  mat_infos_add_dashboard <- mat_infos_add_dashboard[,names(vec_num_mod) [vec_num_mod>1]]
  if("Poids" %in% colnames(mat_infos_add_dashboard)){
    mat_infos_add_dashboard$Poids <- as.numeric(mat_infos_add_dashboard$Poids)
  }
  vec_desc_horse <- colnames(mat_infos_add_dashboard) [3:ncol(mat_infos_add_dashboard)]
  
  # vec_speed_distance <- c("Cheval",grep("SPEED",colnames(mat_all_features),value = TRUE),"GLOBAL_AVERAGE_DISTANCE")
  # if(length(intersect(vec_speed_distance,colnames(mat_infos_add_dashboard)))==3) {
  #   mat_speed_distance_features <- mat_infos_add_dashboard[,vec_speed_distance]
  # }
  # 
  # if(!is.null(mat_speed_distance_features)) {
  #   mat_infos_add_dashboard <- merge(mat_infos_add_dashboard,mat_speed_distance_features,by.x="Cheval",by.y="Cheval")
  # }
  
  vec_drivers_games_size <- scales::rescale(mat_infos_add_dashboard$DRIVER_GAMES,c(1,3))
  names(vec_drivers_games_size) <- mat_infos_add_dashboard$DRIVER_GAMES
  vec_drivers_games_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(mat_infos_add_dashboard$DRIVER_SUCCESS))[rank(mat_infos_add_dashboard$DRIVER_SUCCESS)]
  names(vec_drivers_games_color) <- mat_infos_add_dashboard$DRIVER_SUCCESS
  
  vec_average_speed <- scales::rescale(mat_infos_add_dashboard$GLOBAL_AVERAGE_SPEED,c(1,3))
  vec_average_speed_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_average_speed))[rank(vec_average_speed)]
  names(vec_average_speed_color) <- mat_infos_add_dashboard$GLOBAL_AVERAGE_SPEED
  
  vec_average_distance <- scales::rescale(mat_infos_add_dashboard$GLOBAL_AVERAGE_DISTANCE,c(1,3))
  vec_average_distance_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_average_distance))[rank(vec_average_distance)]
  names(vec_average_distance_color) <- mat_infos_add_dashboard$GLOBAL_AVERAGE_DISTANCE
  
  vec_lab_xaxis <- vec_desc_horse[-grep("DRIVER",vec_desc_horse)[2:3]]
  vec_lab_xaxis <- gsub("DRIVER_GLICKO","Driver",vec_lab_xaxis)
  vec_lab_xaxis <- gsub("GLOBAL_AVERAGE_","",vec_lab_xaxis)
  vec_lab_xaxis <- gsub("SPEED","Speed",vec_lab_xaxis)
  vec_lab_xaxis <- gsub("DISTANCE","Distance",vec_lab_xaxis)
  vec_lab_xaxis <- intersect(c("Gender","Age","Recul","Poids","Ferrage","Driver","Distance","Speed"),vec_lab_xaxis)
  
  par(mar=c(4,1,4,2))
  plot(0,0,xlim=c(0,length(vec_lab_xaxis)+0.5),ylim=c(0,length(vec_horses)),xaxt="n",yaxt="n",type="n",xlab="",ylab="")
  rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = brewer.pal(11,"RdGy")[11])
  title('Conditions')
  
  if("Poids" %in% colnames(mat_infos_add_dashboard)) {
    vec_poids_values <- mat_infos_add_dashboard[,"Poids"]
    vec_poids_values <- vec_poids_values[!is.na(vec_poids_values)]
    vec_poids_values_color <- colorRampPalette(rev(brewer.pal(9,"RdYlBu")))(length(vec_poids_values))[rank(vec_poids_values)]
    names(vec_poids_values_color) <- vec_poids_values
  }
  
  axis(1, 1:((length(vec_lab_xaxis))),labels=FALSE)
  text(1:((length(vec_lab_xaxis))), par("usr")[3] - 0.2, labels = vec_lab_xaxis, srt = 90, pos = 1, xpd = TRUE,offset = 1.2,cex=1)
  
  col_poss_gender <- c("green","pink","blue")
  names(col_poss_gender) <- c("M","F","H")
  
  col_poss_ferrage <- c(brewer.pal(9,"RdYlBu")[1],brewer.pal(9,"RdYlBu")[4],brewer.pal(9,"RdYlBu")[4],brewer.pal(9,"RdYlBu")[9])
  names(col_poss_ferrage) <- c("D0","DA","DP","D4")
  
  for(id_horse in 1:length(vec_lab_yaxis))
  {
    mat_infos_add_dashboard_current <- mat_infos_add_dashboard[mat_infos_add_dashboard$Cheval==unlist(strsplit(vec_lab_yaxis[id_horse],"-"))[2],]
    infos_val_size_driver <- mat_infos_add_dashboard_current$DRIVER_GAMES
    val_size_driver <- vec_drivers_games_size[as.character(infos_val_size_driver)]
    infos_val_color_driver <- mat_infos_add_dashboard_current$DRIVER_SUCCESS
    val_color_driver <- vec_drivers_games_color[as.character(infos_val_color_driver)]
    points(grep("Driver",vec_lab_xaxis),id_horse,cex=val_size_driver,col=val_color_driver,pch=15)
  }
  
  if(length(vec_lab_xaxis[-grep("Driver",vec_lab_xaxis)])>0) {
    for(id_horse in 1:length(vec_lab_yaxis))
    {
      mat_infos_add_dashboard_current <- mat_infos_add_dashboard[mat_infos_add_dashboard$Cheval==unlist(strsplit(vec_lab_yaxis[id_horse],"-"))[2],]
      for (id_col in 1:length(vec_lab_xaxis))
      {
        if(vec_lab_xaxis[id_col]!="Driver") {
          id_col_value <- grep(vec_lab_xaxis[id_col],colnames(mat_infos_add_dashboard_current),ignore.case = TRUE)
          if(length(id_col_value)>1) {
            id_col_value <- id_col_value[which(grep(vec_lab_xaxis[id_col],colnames(mat_infos_add_dashboard_current),ignore.case = TRUE,value=TRUE)=="Age")]
          }
          id_x_plot <- which(vec_lab_xaxis==vec_lab_xaxis[id_col])
          if (vec_lab_xaxis[id_col] == "Gender") {
            infos_desc <- mat_infos_add_dashboard_current[,id_col_value]
            col_current_desc <- col_poss_gender[infos_desc]
            points(id_x_plot,id_horse,cex=4,col=col_current_desc,pch=1)
            # points(id_col,id_horse,cex=3,col=col_current_desc,pch=20)
            points(id_x_plot,id_horse,cex=2,col=col_current_desc,pch=15)
          }
          
          if(vec_lab_xaxis[id_col] == "Ferrage") {
            infos_desc <- mat_infos_add_dashboard_current[,id_col_value]
            col_current_desc <- col_poss_ferrage[infos_desc]
            points(id_x_plot,id_horse,cex=2,col=col_current_desc,pch=0)
            text(id_x_plot,id_horse,infos_desc,cex=0.5,col=col_current_desc)
          }
          
          if(vec_lab_xaxis[id_col] == "Age") {
            infos_desc <- mat_infos_add_dashboard_current[,id_col_value]
            text(id_x_plot,id_horse,infos_desc,cex=1.2,col="white")
          }
          
          if(vec_lab_xaxis[id_col] == "Poids") {
            infos_desc <- mat_infos_add_dashboard_current[,id_col_value]
            points(id_x_plot,id_horse,col=vec_poids_values_color[as.character(infos_desc)],cex=2,pch=15)
          }
          
          if(vec_lab_xaxis[id_col] == "Distance") {
            infos_desc <- mat_infos_add_dashboard_current[,id_col_value]
            points(id_x_plot,id_horse,col=vec_average_distance_color[as.character(infos_desc)],cex=2,pch=15)
          }
          
          if(vec_lab_xaxis[id_col] == "Speed") {
            infos_desc <- mat_infos_add_dashboard_current[,id_col_value]
            points(id_x_plot,id_horse,col=vec_average_speed_color[as.character(infos_desc)],cex=2,pch=15)
          }
          
          pch_val <- 24
          col_point <- brewer.pal(9,"Greens") [6]
          if(vec_lab_xaxis[id_col] == "Recul") {
            infos_desc <- mat_infos_add_dashboard_current[,id_col_value]
            if(infos_desc>0){
              pch_val <- 25
              col_point <- brewer.pal(9,"Reds") [6]
            }
            points(id_x_plot,id_horse,cex=2,pch=pch_val,col=col_point)
            text(id_x_plot,id_horse,infos_desc,cex=0.6,pch=pch_val,col=col_point)
          }
        }
        
      }
      
    }
  }
  dev.off()
}





