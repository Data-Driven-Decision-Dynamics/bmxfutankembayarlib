add_config_race_geny <- function(df_infos = NULL, df_infos_target_race_geny = NULL){
  
  if(nrow(df_infos)>0){
    df_infos$Terrain <- gsub("Mâchefer","Cendrée",df_infos$Terrain)
    df_infos$Terrain <- gsub("Machefer","Cendrée",df_infos$Terrain)
    idx_final_filled_corde_terrain <- !is.na(df_infos$Corde) & !is.na(df_infos$Terrain)
    if(sum(idx_final_filled_corde_terrain,na.rm = TRUE)>0){
      idx_config <- df_infos$Corde == unique(df_infos_target_race_geny$Corde) & df_infos$Terrain == unique(df_infos_target_race_geny$Terrain) 
      idx_corde <- df_infos$Corde == unique(df_infos_target_race_geny$Corde) & df_infos$Terrain != unique(df_infos_target_race_geny$Terrain)
      idx_terrain <- df_infos$Corde != unique(df_infos_target_race_geny$Corde) & df_infos$Terrain == unique(df_infos_target_race_geny$Terrain)
      idx_else <- df_infos$Corde != unique(df_infos_target_race_geny$Corde) & df_infos$Terrain != unique(df_infos_target_race_geny$Terrain)
      df_infos$Class <- NA
      if(sum(idx_config,na.rm = TRUE)>0){
        df_infos[which(idx_config==TRUE),"Class"] <-"Config"
      }
      if(sum(idx_corde,na.rm = TRUE)>0){
        df_infos[which(idx_corde==TRUE),"Class"] <-"Turn"
      }
      if(sum(idx_else,na.rm = TRUE)>0){
        df_infos[which(idx_else==TRUE),"Class"] <-"Others"
      }
      if(sum(idx_terrain,na.rm = TRUE)>0){
        df_infos[which(idx_terrain==TRUE),"Class"] <-"Ground"
      }
      if(sum(is.na(df_infos$Class),na.rm=TRUE)>0){
        idx_turn_filled <- is.na(df_infos$Class) & !is.na(df_infos$Corde) 
        if(sum(idx_turn_filled,na.rm=TRUE)>0){
          df_infos[which(idx_turn_filled==TRUE),"Class"] <-"Turn"
        }
        idx_ground_filled <- is.na(df_infos$Class) & !is.na(df_infos$Terrain) 
        if(sum(idx_ground_filled,na.rm=TRUE)>0){
          df_infos[which(idx_ground_filled==TRUE),"Class"] <-"Ground"
        }
        idx_turn_ground_filled <- is.na(df_infos$Class) & is.na(df_infos$Terrain) & is.na(df_infos$Corde)
        if(sum(idx_turn_ground_filled,na.rm=TRUE)>0){
          df_infos[which(idx_turn_ground_filled==TRUE),"Class"] <-"Others"
        }
      }
    }
  }
  return(df_infos)
}

