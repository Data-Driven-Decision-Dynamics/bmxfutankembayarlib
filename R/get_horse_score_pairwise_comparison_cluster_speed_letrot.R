get_horse_score_pairwise_comparison_cluster_speed_letrot <- function(current_race_url = NULL,
                                                                     df_infos_target_race = NULL,
                                                                     df_historical_races_competitors = NULL,
                                                                     path_cluster_distance_range_letrot = "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/input/mat_ranges_cluster_distance.rds",
                                                                     number_days_back=366) {
  
  message("Initilization of the output")
  df_score_pairwise_comparison_cluster_speed <- NULL
  
  if(is.null(df_historical_races_competitors)){
    df_historical_races_competitors  <- get_competitors_historical_races_letrot(current_race_url)
  }
  
  if(!is.null(df_infos_target_race)) {
    message("Extraction date of the race")
    if(class(unique(df_infos_target_race$Date))!="Date") {
      current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
    } else {
      current_race_date <- unique(df_infos_target_race$Date)
    }
    
    message("Extract terrain of current race")
    current_race_terrain <- as.vector(unique(df_infos_target_race$Terrain))
    
    message("Extract terrain of current race")
    current_race_corde <- as.vector(unique(df_infos_target_race$Corde))
    
    message("Extracting horse names")
    current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))

    message("Extracting race type")
    current_race_discipline <- gsub("\n","",unique(df_infos_target_race$Discipline))
    
    message("Extracting hippodrome")
    current_race_hippodrome <- gsub("\n","",unique(df_infos_target_race$Lieu))
    
    message("Extracting race distance")
    current_race_distance <- min(df_infos_target_race$Distance)
    
    if(current_race_discipline=="Attelé"){
      df_infos_target_race$Id <- paste(df_infos_target_race$Lieu,df_infos_target_race$Discipline,df_infos_target_race$Distance,sep="_")
    }
    
    if(current_race_discipline=="Monté"){
      df_infos_target_race$Id <- paste(df_infos_target_race$Lieu,df_infos_target_race$Discipline,df_infos_target_race$Distance,df_infos_target_race$Poids,sep="_")
    } 
    
    if(current_race_discipline=="Monté"){
      df_infos_target_race$Id <- paste(df_infos_target_race$Lieu,df_infos_target_race$Discipline,df_infos_target_race$Distance,sep="_")
    }
  }

  message("Reading file with cluster distance definition")
  df_cluster_distance_range <- readRDS(path_cluster_distance_range_letrot)
  df_cluster_distance_range_discipline <- df_cluster_distance_range[df_cluster_distance_range$Discipline==paste("Trot",current_race_discipline),]
  
  df_infos_best_speed_location_distance <-  df_historical_races_competitors %>%
    filter(Cheval %in% current_race_horses) %>%
    mutate(Delay= as.numeric(current_race_date-Date)) %>%
    filter(Date<current_race_date) %>%
    filter(Delay<=number_days_back) %>%
    filter(Discipline %in% paste("Trot",current_race_discipline)) %>%
    dplyr::select(Cheval,Id,Speed) %>%
    drop_na() %>%
    as.data.frame()
  
  if(nrow(df_infos_best_speed_location_distance)>0){
    message("Adding distance cluster of the matrix with details on best speed")
    df_infos_best_speed_location_distance$Distance <- as.numeric(unlist(lapply(df_infos_best_speed_location_distance$Id,function(x){unlist(strsplit(x,"_"))[3]})))
    df_infos_best_speed_location_distance$Cluster_Distance <- NA
    for(i in 1:nrow(df_cluster_distance_range_discipline))
    {
      idx_cluster_distance_group <- df_cluster_distance_range_discipline[i,"Lower"] <= unlist(df_infos_best_speed_location_distance[,"Distance"]) & df_cluster_distance_range_discipline[i,"Upper"] >= unlist(df_infos_best_speed_location_distance[,"Distance"])
      df_infos_best_speed_location_distance[idx_cluster_distance_group==TRUE,"Cluster_Distance"] <- paste(df_cluster_distance_range_discipline[i,c("Lower","Upper")],collapse = "-")
    }
    
    vec_cluster_distance_found <- unique(df_infos_best_speed_location_distance$Cluster_Distance)
    list_score_pairwise_comparison_cluster_speed <- vector("list",length(vec_cluster_distance_found))
    names(list_score_pairwise_comparison_cluster_speed) <- vec_cluster_distance_found
    for(current_cluster_distance in vec_cluster_distance_found)
    {
      df_score_best_speed_location_distance_current <- NULL
      if(current_cluster_distance %in% unique(df_infos_best_speed_location_distance$Cluster_Distance)){
        df_infos_best_speed_location_distance_current <- df_infos_best_speed_location_distance[df_infos_best_speed_location_distance$Cluster_Distance == current_cluster_distance, ]
        df_best_speed_location_distance_current <- df_infos_best_speed_location_distance_current %>%
          dplyr::select(Cheval,Id,Speed) %>%
          pivot_wider(
            names_from = Id,
            values_from = Speed,
            values_fn = max
          ) %>%
          as.data.frame()
        
        vec_size_filled_values <- apply(df_best_speed_location_distance_current,2,function(x){sum(!is.na(x))})
        vec_location_distance  <- names(vec_size_filled_values) [vec_size_filled_values>=ceiling(nrow(df_infos_target_race)/4)]
        if(length(vec_location_distance)>1){
          df_best_speed_location_distance_current <- df_best_speed_location_distance_current[,vec_location_distance]
          df_score_best_speed_location_distance_current <- get_handcrafted_rank_scores(df_best_speed_location_distance_current,df_infos_target_race)
          if(nrow(df_score_best_speed_location_distance_current)>0){
            if(sum(!is.na(df_score_best_speed_location_distance_current$Score))>0){
              df_score_best_speed_location_distance_current$Score <- get_percentile_values(df_score_best_speed_location_distance_current$Score)
              df_score_best_speed_location_distance_current$Score <- round(df_score_best_speed_location_distance_current$Score,3)
              df_score_best_speed_location_distance_current <- df_score_best_speed_location_distance_current[,setdiff(colnames(df_score_best_speed_location_distance_current),"Numero")]
              colnames(df_score_best_speed_location_distance_current)[2] <- current_cluster_distance
            }
          }
        }
      }
      list_score_pairwise_comparison_cluster_speed[[current_cluster_distance]] <- df_score_best_speed_location_distance_current
    }
    
    list_score_pairwise_comparison_cluster_speed <- list_score_pairwise_comparison_cluster_speed[lapply(list_score_pairwise_comparison_cluster_speed,length)>0]
    if(length(list_score_pairwise_comparison_cluster_speed)>0) {
      df_score_pairwise_comparison_cluster_speed <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_score_pairwise_comparison_cluster_speed)
      df_score_pairwise_comparison_cluster_speed <- merge(df_score_pairwise_comparison_cluster_speed,df_infos_target_race[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
      df_score_pairwise_comparison_cluster_speed <- df_score_pairwise_comparison_cluster_speed[,c("Cheval",colnames(df_score_pairwise_comparison_cluster_speed)[-1][order(unlist(lapply(colnames(df_score_pairwise_comparison_cluster_speed)[-1],function(x){unlist(strsplit(x,"-"))[1]})))])]
    }
  }
  return(df_score_pairwise_comparison_cluster_speed)  
}







