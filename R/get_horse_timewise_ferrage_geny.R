get_horse_timewise_ferrage_geny <- function (df_infos_target_race_geny = NULL ,
                                             df_competitors_perfs = NULL,
                                             list_number_days_back = list(Q4=366,Q3=270,Q2=180,Q1=90)){
  
  current_race_date = unique(df_infos_target_race_geny$Date)
  
  message("Initialization of final output")
  df_ferrage_timewise <- NULL
  message("Initialization of final output")
  
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Trot Attelé","Trot Monté")){
    message("Creating pivot data frame from categories")
    df_epreuve <- data.frame(Epreuve=c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"),Values=seq(1:length(c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"))))
    message("Creating pivot data frame from categories")
    
    message("Start identifying record per time frame")
    if(!is.null(df_competitors_perfs)){
      df_competitors_perfs <- df_competitors_perfs[df_competitors_perfs$Discipline == unique(df_infos_target_race_geny$Discipline), ]
      colnames(df_competitors_perfs)[colnames(df_competitors_perfs)=="Engagement"] <- "Limit"
      df_competitors_perfs$Year <- lubridate::year(df_competitors_perfs$Date)
      if(sum(is.na(df_competitors_perfs$Epreuve))>0){
        df_competitors_perfs <- df_competitors_perfs[!is.na(df_competitors_perfs$Epreuve),]
      }
      df_competitors_perfs <- merge(df_competitors_perfs,df_epreuve,by.x="Epreuve",by.y="Epreuve",all.x=TRUE)
      if(sum(is.na(df_competitors_perfs$Values))>0){
        df_competitors_perfs <- df_competitors_perfs[!is.na(df_competitors_perfs$Values),]
      }
      df_competitors_perfs$Sequence <- NA
      for(id_time in 1:length(list_number_days_back)) 
      {
        idx_time_current <- current_race_date - df_competitors_perfs$Date <= list_number_days_back[[id_time]]
        if(sum(idx_time_current,na.rm=TRUE)>0){
          df_competitors_perfs[idx_time_current ,"Sequence"] <- names(list_number_days_back)[[id_time]]
        }
      }
      if(sum(is.na(df_competitors_perfs$Sequence),na.rm=TRUE)>0){
        df_competitors_perfs <- df_competitors_perfs[!is.na(df_competitors_perfs$Sequence),]
      }

      df_infos_ferrage_timewise <- df_competitors_perfs %>%
        group_by(Cheval,Sequence,Ferrage) %>%
        dplyr::summarise(TOTAL_EARNING_FERRAGE = max(Gains)) %>%
        select(Cheval,Sequence,Ferrage,TOTAL_EARNING_FERRAGE) %>%
        mutate(Pivot = paste(Cheval,Ferrage,sep="_")) %>%
        as.data.frame()
      df_infos_target_focus <- df_infos_target_race_geny[,c("Cheval","Ferrage")]
      df_infos_target_focus$Pivot <- paste(df_infos_target_focus$Cheval,df_infos_target_focus$Ferrage,sep="_")
      df_infos_target_focus$Grouping <- "Current"
      df_infos_ferrage_timewise <- merge(df_infos_ferrage_timewise,df_infos_target_focus,by.x="Pivot",by.y="Pivot",all=TRUE)
      df_infos_ferrage_timewise <- df_infos_ferrage_timewise[,setdiff(colnames(df_infos_ferrage_timewise),c("Cheval.y","Ferrage.y"))]
      colnames(df_infos_ferrage_timewise) <- sub("Cheval.x","Cheval", colnames(df_infos_ferrage_timewise))
      colnames(df_infos_ferrage_timewise) <- sub("Ferrage.x","Ferrage", colnames(df_infos_ferrage_timewise))
      colnames(df_infos_ferrage_timewise) <- sub("TOTAL_EARNING_FERRAGE","Earning", colnames(df_infos_ferrage_timewise))
      if(sum(is.na(df_infos_ferrage_timewise$Grouping))>0){
        df_infos_ferrage_timewise[is.na(df_infos_ferrage_timewise$Grouping),"Grouping"] <- "Others"
      }
      
      if(sum(is.na(df_infos_ferrage_timewise$Cheval))>0){
        df_infos_ferrage_timewise <- df_infos_ferrage_timewise[!is.na(df_infos_ferrage_timewise$Cheval),]
      }
      
      df_ferrage_timewise <- df_infos_ferrage_timewise %>%
        group_by(Cheval,Sequence,Grouping) %>%
        dplyr::summarise(Earning = max(Earning)) %>%
        select(Cheval,Sequence,Grouping,Earning) %>%
        as.data.frame()
      
    }
    message("Start identifying record per time frame")
  }
  
  return(df_ferrage_timewise)
}



