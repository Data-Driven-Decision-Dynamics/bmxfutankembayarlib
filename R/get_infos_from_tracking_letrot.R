get_infos_from_tracking_letrot <- function(path_list_opponent = "C:/Users/malick.paye/Desktop/PRIX D'AMERIQUE RACES ZETURF Q5.xlsx",
                                           current_path_race = "https://www.letrot.com/stats/fiche-cheval/gaspar-de-brion/ZWR8ZAQBBAYT/courses/tracking#sub_sub_menu_fichecheval") {
  
  table_data_tracking_compiled <- NULL
  if(!is.null(path_list_opponent)){
    df_opponnents <- xlsx::read.xlsx(path_list_opponent,sheetIndex = 1)
    if(nrow(df_opponnents)>0){
      for(idx_row in 1:nrow(df_opponnents))
      {
        current_path_race <- df_opponnents[idx_row,"Page"]
        current_horse_label <- df_opponnents[idx_row,"Cheval"]
        current_horse_number <- df_opponnents[idx_row,"Numero"]
        if(!is.null(current_path_race) & !is.na(current_path_race)){
          page_details <- rvest::read_html(curl::curl(current_path_race, handle = curl::new_handle("useragent" = "Mozilla/5.0")))
          tables_page_details <- rvest::html_table(page_details, fill=TRUE)
          if(length(tables_page_details)>0){
            table_data_tracking <- tables_page_details[[1]]
            table_data_tracking <- as.data.frame(table_data_tracking)
            vec_columns_to_format <- grep("Aux",colnames(table_data_tracking))
            if(length(vec_columns_to_format)>0){
              for(idx_column in 1:length(vec_columns_to_format))
              {
                val_infos_column <- table_data_tracking[1,vec_columns_to_format[idx_column]]
                label_column_initial  <- colnames(table_data_tracking)[vec_columns_to_format[idx_column]]
                label_column_modified <- sub("Aux ","",label_column_initial)
                label_column_modified <- stringr::str_trim(sub("m","",label_column_modified))
                if(val_infos_column=="Réd. Km"){
                  label_column_modified <- paste("SPEED_START",label_column_modified,sep="_")
                  colnames(table_data_tracking)[vec_columns_to_format[idx_column]] <- label_column_modified
                }
                if(val_infos_column=="Class."){
                  label_column_modified <- paste("RANK_START",label_column_modified,sep="_")
                  colnames(table_data_tracking)[vec_columns_to_format[idx_column]] <- label_column_modified
                }
              }
            }
            vec_columns_to_format <- grep("derniers",colnames(table_data_tracking))
            if(length(vec_columns_to_format)>0){
              for(idx_column in 1:length(vec_columns_to_format))
              {
                val_infos_column <- table_data_tracking[1,vec_columns_to_format[idx_column]]
                label_column_initial  <- colnames(table_data_tracking)[vec_columns_to_format[idx_column]]
                label_column_modified <- sub("Réd. Km derniers","",label_column_initial)
                label_column_modified <- sub("Réd. Kmderniers","",label_column_modified)
                label_column_modified <- stringr::str_trim(sub("m","",label_column_modified))
                if(val_infos_column %in% c("Réd. Km derniers 1000m","Réd. Kmderniers 500m")){
                  label_column_modified <- paste("SPEED_LAST",label_column_modified,sep="_")
                  colnames(table_data_tracking)[vec_columns_to_format[idx_column]] <- label_column_modified
                }
              }
            }
            table_data_tracking <- table_data_tracking[-1,]
            table_data_tracking$Cheval <- current_horse_label
            table_data_tracking$Numero <-  current_horse_number
            idx_starting_column <- grep("SPEED",colnames(table_data_tracking))
            if(length(idx_starting_column)>0){
              for(idx_col in idx_starting_column){
                table_data_tracking[,idx_col] <- unlist(lapply(table_data_tracking[,idx_col],get_correct_reduction_format_letrot))
                table_data_tracking[,idx_col] <- unlist(lapply(table_data_tracking[,idx_col],get_numeric_reduc_km_letrot))
              }
            }
            idx_starting_column <- grep("RANK",colnames(table_data_tracking))
            if(length(idx_starting_column)>0){
              for(idx_col in idx_starting_column){
                table_data_tracking[,idx_col] <- unlist(lapply(table_data_tracking[,idx_col],get_correct_reduction_format_letrot))
              }
            }
          }
        }
        if(!is.null(table_data_tracking)){
          if(nrow(table_data_tracking)>0){
            table_data_tracking_compiled <- plyr::rbind.fill(table_data_tracking_compiled,table_data_tracking)
          }
        }
      }
    }
  }
  if(!is.null(table_data_tracking_compiled)){
    if(nrow(table_data_tracking_compiled)>0){
      table_data_tracking_compiled$Date <- unlist(lapply(table_data_tracking_compiled$Date,function(x){substr(x,1,10)}))
      table_data_tracking_compiled <- table_data_tracking_compiled[,c(c("Numero","Cheval"),setdiff(colnames(table_data_tracking_compiled),c("Numero","Cheval")))]
    }
  }
  return(table_data_tracking_compiled)
}
