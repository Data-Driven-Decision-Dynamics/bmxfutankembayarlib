get_index_enough_confrontation_competitors_geny <- function(df_infos_target_race_geny = NULL,
                                                            path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                            number_days_back = 366) {
  
  message("Initialization of final output")
  index_val_confrontation <- 0
  message("Initialization of final output")
  
  message("Loading historical performance data")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Loading historical performance data")
  
  message("Selecting same races run")
  df_historical_races_focus <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
    filter(unique(df_infos_target_race_geny$Date)-Date<=number_days_back)  %>%
    select(Cheval,Lieu,Distance,Place,Race,Date,Recul) %>%
    drop_na() %>%
    as.data.frame()
  if(nrow(df_historical_races_focus)>0){
    df_historical_races_focus$Longueur <- df_historical_races_focus$Distance
    vec_races_found <- unique(df_historical_races_focus[df_historical_races_focus$Recul >0, "Race"])
    df_historical_races_focus_utils <- df_historical_races_geny[df_historical_races_geny$Race %in% vec_races_found ,]
    for(race in vec_races_found)
    {
      df_historical_races_focus[df_historical_races_focus$Race == race , "Longueur"] <- min(df_historical_races_focus_utils[df_historical_races_focus_utils$Race %in%  race, "Distance"])
    }
    df_historical_races_focus$ID_TRACK <- paste(df_historical_races_focus$Lieu,df_historical_races_focus$Longueur,sep="|")
    tab_same_track <- table(df_historical_races_focus$Cheval,df_historical_races_focus$ID_TRACK)
    df_same_track <- as.data.frame(as.matrix.data.frame(tab_same_track))
    rownames(df_same_track) <- rownames(tab_same_track)
    colnames(df_same_track) <- colnames(tab_same_track)
    vec_number_same_track <- apply(df_same_track,2,function(x){sum(x>0)})
    test_enough_same_track <- names(vec_number_same_track) [vec_number_same_track>=ceiling(nrow(df_infos_target_race_geny) *0.75)]
    
    id_track_current <- paste(unique(df_infos_target_race_geny$Lieu),min(df_infos_target_race_geny$Distance),sep="|")
    
    tab_same_race <- table(df_historical_races_focus$Cheval,df_historical_races_focus$Race)
    df_same_race <- as.data.frame(as.matrix.data.frame(tab_same_race))
    rownames(df_same_race) <- rownames(tab_same_race)
    colnames(df_same_race) <- colnames(tab_same_race)
    vec_number_same_race <- apply(df_same_race,2,function(x){sum(x>0)})
    test_enough_same_race <- names(vec_number_same_race) [vec_number_same_race>=ceiling(nrow(df_infos_target_race_geny) *0.75)]
    
    if(length(test_enough_same_race)>0){
      index_val_confrontation <- 1
    }
    
    if(length(test_enough_same_track)>0){
      val_location_enough_races <- unlist(strsplit(test_enough_same_track,"\\|"))[1]
      val_location_current_race <- unlist(strsplit(id_track_current,"\\|"))[1]
      if(length(val_location_enough_races)>0 & length(val_location_current_race)>0){
        if(val_location_enough_races==val_location_current_race){
          index_val_confrontation <- 1
        }
      }
    }
  }
  message("Selecting same races run")
  
  return(index_val_confrontation)
}