get_numeric_reduc_km_canalturf <- function(reduc_km="1'24''80") 
{
 
  if(length(grep("\\\\",reduc_km))>0){
    reduc_km <- gsub("\\\\","",reduc_km)
  }

  if(sum(grep("'",reduc_km))<1)
  {
    res <- NA
  } else {
    min <-unlist(strsplit(reduc_km,"'"))[1]
    sec.tier <- sub(min,'',reduc_km)
    sec <- unlist(strsplit(sec.tier,"'"))[2]
    tier <- unlist(strsplit(sec.tier,"'"))[4]
    res <- as.numeric(min)*60 + as.numeric(sec)
    res <- as.numeric(paste(res,'.',tier,sep=''))
  }
  return(res)
  
}

