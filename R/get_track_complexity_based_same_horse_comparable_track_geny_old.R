get_track_complexity_based_same_horse_comparable_track_geny_old <- function(path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                                        path_infos_tracks = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/Hippodrome-Master-Data.xlsx",
                                                                        path_df_one_to_one_similar_tracks = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_delta_speed_similar_tracks_geny.xlsx",
                                                                        path_df_ranking_tracks = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_ranking_tracks_geny.rds"){
  
  require(magrittr)
  require(dplyr)
  require(tidyr)
  require(igraph)
  
  df_rank_tracks <- NULL
  df_rank_tracks_discipline_ground <- NULL
  df_rank_tracks_discipline_ground_compiled <- NULL
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Reading historical performance file")
  
  message("Computing intermediary data")
  df_delta_compiled <- get_delta_reduction_same_horse_comparable_track_geny()
  xlsx::write.xlsx(df_delta_compiled,path_df_one_to_one_similar_tracks)
  message("Computing intermediary data")
  
  message("Reading master file of track")
  df_master_data_tracks <- xlsx::read.xlsx(path_infos_tracks,sheetIndex = 1)
  df_master_data_tracks <- df_master_data_tracks[,c("Hippodrome","Discipline","Longueur","Ligne.Droite","Largeur.piste","Commentaire")]
  colnames(df_master_data_tracks) <- sub("Ligne.Droite","Droite",colnames(df_master_data_tracks))
  colnames(df_master_data_tracks) <- sub("Largeur.piste","Largeur",colnames(df_master_data_tracks))
  df_master_data_tracks$Longueur <- as.numeric(df_master_data_tracks$Longueur)
  df_master_data_tracks$Largeur <- as.numeric(df_master_data_tracks$Largeur)
  df_master_data_tracks$Droite <- as.numeric(df_master_data_tracks$Droite)
  message("Reading master file of track")
  
  vec_found_discipline <- unique(df_delta_compiled$Discipline)
  for(current_discipline in vec_found_discipline)
  {
    df_infos_focus_discipline <- df_historical_races_geny[df_historical_races_geny$Discipline == current_discipline,]
    vec_found_ground <- unique(df_infos_focus_discipline$Terrain)
    if(sum(is.na(vec_found_ground))>0){
      vec_found_ground <- vec_found_ground[!is.na(vec_found_ground)]
      for(current_ground in vec_found_ground)
      {
        message("Computing record per track and level")
        df_infos_focus_discipline_ground <- df_infos_focus_discipline[df_infos_focus_discipline$Terrain == current_ground, ]
        df_records_track_discipline_ground <- df_infos_focus_discipline_ground %>%
          select(Cheval,Lieu,Longueur,Depart,Epreuve,Speed,Place,Date) %>%
          tidyr::drop_na() %>%
          filter(Place == 1) %>%
          group_by(Lieu,Longueur,Depart,Epreuve) %>%
          top_n(1,Speed) %>%
          group_by(Lieu,Longueur,Depart,Epreuve) %>%
          top_n(1,Date) %>%
          mutate(Track = paste(Lieu,Longueur,Depart,Epreuve,sep="_")) %>%
          # mutate(Discipline = current_discipline) %>%
          # mutate(Ground = current_ground) %>%
          as.data.frame() %>%
          select(Track,Speed)
        colnames(df_records_track_discipline_ground) <- sub("Speed","Record",colnames(df_records_track_discipline_ground))
        message("Computing record per track and level")
        
        message("Computing standard times per track and level")
        df_standards_track_discipline_ground <- df_infos_focus_discipline_ground %>%
          select(Cheval,Lieu,Longueur,Depart,Epreuve,Time,Place,Date) %>%
          tidyr::drop_na() %>%
          mutate(Track = paste(Lieu,Longueur,Depart,Epreuve,sep="_")) %>%
          group_by(Track) %>%
          dplyr::summarise(Standard = get_track_standard_times(Time)) %>%
          as.data.frame()
        message("Computing standard times per track and level")
        
        message("Computing pagerank score per discipline and ground")
        df_delta_compiled_discipline_ground <- df_delta_compiled %>%
          filter(Discipline == current_discipline) %>%
          filter(Ground == current_ground) %>%
          as.data.frame()
        
        if(nrow(df_delta_compiled_discipline_ground)>=20){
          if(current_discipline != "Trot Attelé"){
            vec_location <- unlist(lapply(df_delta_compiled_discipline_ground$from,function(x){unlist(strsplit(x,"_"))[1]}))
            vec_distance <- unlist(lapply(df_delta_compiled_discipline_ground$from,function(x){unlist(strsplit(x,"_"))[2]}))
            vec_epreuve <- unlist(lapply(df_delta_compiled_discipline_ground$from,function(x){unlist(strsplit(x,"_"))[3]}))
            df_delta_compiled_discipline_ground$from <- paste(vec_location,vec_distance,vec_epreuve,sep="_")
            vec_location <- unlist(lapply(df_delta_compiled_discipline_ground$to,function(x){unlist(strsplit(x,"_"))[1]}))
            vec_distance <- unlist(lapply(df_delta_compiled_discipline_ground$to,function(x){unlist(strsplit(x,"_"))[2]}))
            vec_epreuve <- unlist(lapply(df_delta_compiled_discipline_ground$to,function(x){unlist(strsplit(x,"_"))[3]}))
            df_delta_compiled_discipline_ground$to <- paste(vec_location,vec_distance,vec_epreuve,sep="_")
          }
          df_delta_compiled_discipline_ground <- unique(df_delta_compiled_discipline_ground)
          df_rank_tracks_discipline_ground <- page_rank(graph_from_data_frame(df_delta_compiled_discipline_ground , directed=TRUE))$vector
          df_rank_tracks_discipline_ground <- as.data.frame(df_rank_tracks_discipline_ground)
          colnames(df_rank_tracks_discipline_ground) <- "Score"
          df_rank_tracks_discipline_ground$Track <- rownames(df_rank_tracks_discipline_ground)
          df_rank_tracks_discipline_ground <- df_rank_tracks_discipline_ground[,rev(colnames(df_rank_tracks_discipline_ground))]
          df_rank_tracks_discipline_ground <- df_rank_tracks_discipline_ground[order(df_rank_tracks_discipline_ground$Score,decreasing = TRUE),]
          df_rank_tracks_discipline_ground$Rank <- 1:nrow(df_rank_tracks_discipline_ground)
          df_rank_tracks_discipline_ground <- df_rank_tracks_discipline_ground[,c("Rank","Track","Score")]
          val_mean_rank_discipline_groud <- mean(df_rank_tracks_discipline_ground$Score)
          val_sd_rank_discipline_groud <- sd(df_rank_tracks_discipline_ground$Score)
          df_rank_tracks_discipline_ground$Score <- (df_rank_tracks_discipline_ground$Score-val_mean_rank_discipline_groud)/val_sd_rank_discipline_groud
          df_rank_tracks_discipline_ground$Faster <- NA
          df_rank_tracks_discipline_ground$Slower <- NA
          vec_available_tracks <- unique(df_rank_tracks_discipline_ground$Track)
          for(current_track in vec_available_tracks)
          {
            df_stats_delta_per_track_discipline_track <- df_delta_compiled_discipline_ground[df_delta_compiled_discipline_ground$to==current_track,c("from","to","value")] %>%
              pivot_wider(
                names_from = to,
                values_from = value,
                values_fn = mean
              ) %>%
              as.data.frame()
            if(nrow(df_stats_delta_per_track_discipline_track)>0){
              df_rank_tracks_discipline_ground [df_rank_tracks_discipline_ground$Track == current_track,"Faster"] <- mean(df_stats_delta_per_track_discipline_track[,2])
            }
            
            df_stats_delta_per_track_discipline_track <- df_delta_compiled_discipline_ground[df_delta_compiled_discipline_ground$from==current_track,c("from","to","value")] %>%
              pivot_wider(
                names_from = from,
                values_from = value,
                values_fn = mean
              ) %>%
              as.data.frame()
            if(nrow(df_stats_delta_per_track_discipline_track)>0){
              df_rank_tracks_discipline_ground [df_rank_tracks_discipline_ground$Track == current_track,"Slower"] <- mean(df_stats_delta_per_track_discipline_track[,2])
            }
          }
          
          df_rank_tracks_discipline_ground$Ground <- current_ground
          df_rank_tracks_discipline_ground$Discipline <- current_discipline
          
          df_number_fast_discipline_ground <- as.data.frame(table(as.vector(unlist(df_delta_compiled_discipline_ground[,c("from")]))))
          colnames(df_number_fast_discipline_ground) <- c("Track","Fast")
          
          df_number_slow_discipline_ground <- as.data.frame(table(as.vector(unlist(df_delta_compiled_discipline_ground[,c("to")]))))
          colnames(df_number_slow_discipline_ground) <- c("Track","Slow")
          
          df_number_slow_fast_discipline_ground <- merge(df_number_fast_discipline_ground,df_number_slow_discipline_ground,by.x="Track",by.y="Track",all=TRUE)
          df_rank_tracks_discipline_ground <- merge(df_rank_tracks_discipline_ground,df_number_slow_fast_discipline_ground,by.x="Track",by.y="Track",all=TRUE)
          
          df_rank_tracks_discipline_ground <- merge(df_rank_tracks_discipline_ground,df_records_track_discipline_ground,by.x="Track",by.y="Track",all.x=TRUE)
          df_rank_tracks_discipline_ground <- merge(df_rank_tracks_discipline_ground,df_standards_track_discipline_ground,by.x="Track",by.y="Track",all.x=TRUE)
          df_rank_tracks_discipline_ground <- unique(df_rank_tracks_discipline_ground)
          df_rank_tracks_discipline_ground_compiled <- rbind(df_rank_tracks_discipline_ground_compiled,df_rank_tracks_discipline_ground)
        }
      }
    }
  }
  
  if(!is.null(df_rank_tracks_discipline_ground_compiled)){
    df_rank_tracks_discipline_ground_compiled$Location <-  unlist(lapply(df_rank_tracks_discipline_ground_compiled$Track,function(x) {unlist(strsplit(x,"_"))[1]}))
  }
  
  if(!is.null(df_rank_tracks_discipline_ground_compiled)){
    df_rank_tracks_discipline_ground_compiled$Longueur <-  NA
    df_rank_tracks_discipline_ground_compiled$Largeur <-  NA
    df_rank_tracks_discipline_ground_compiled$Droite <-  NA
    vec_location_found <- unique(df_rank_tracks_discipline_ground_compiled$Location)
    for(idx_loc in vec_location_found)
    {
      if(idx_loc %in% df_master_data_tracks$Hippodrome){
        df_master_data_current_track <- df_master_data_tracks[which(df_master_data_tracks$Hippodrome == idx_loc), ]
        df_master_data_current_track <- df_master_data_current_track[df_master_data_current_track$Discipline == "Trot",]
        df_master_data_current_track <- df_master_data_current_track[1,]
        df_rank_tracks_discipline_ground_compiled[df_rank_tracks_discipline_ground_compiled$Location == idx_loc, "Longueur"] <- df_master_data_current_track$Longueur
        df_rank_tracks_discipline_ground_compiled[df_rank_tracks_discipline_ground_compiled$Location == idx_loc, "Largeur"] <- df_master_data_current_track$Largeur
        df_rank_tracks_discipline_ground_compiled[df_rank_tracks_discipline_ground_compiled$Location == idx_loc, "Droite"] <- df_master_data_current_track$Droite
      }
    }
    saveRDS(df_rank_tracks_discipline_ground_compiled,path_df_ranking_tracks)
    xlsx::write.xlsx(df_rank_tracks_discipline_ground_compiled,sub(".rds",".xlsx",path_df_ranking_tracks))
  }
  
  return(df_rank_tracks_discipline_ground_compiled)
}