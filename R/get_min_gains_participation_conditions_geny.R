get_min_gains_participation_conditions_geny <- function(content_participation_details = NULL){
  
  min_gains <- NA
  if(!is.null(content_participation_details)){
    test_min_gains <- grep("ayant gagné au moins",content_participation_details,ignore.case = TRUE,value=TRUE)
    message("Extract Minimum Earning")
    if(length(test_min_gains)>0) {
      if(length(grep("ayant gagné au moins",test_min_gains))>0){
        min_gains <- unlist(strsplit(test_min_gains,"ayant gagné au moins"))[2]
        min_gains <- gsub("\\.","",min_gains)
        min_gains <- unlist(stringr::str_extract_all(min_gains, "[0-9]+"))
        min_gains <- stringr::str_trim(min_gains)
        min_gains <- as.numeric(min_gains)
        if(sum(!is.na(min_gains))>0){
          min_gains <- min_gains[1]
          if(!is.na(min_gains)){
            min_gains <- min_gains
          }
        }
      }
    }
  }

  if(length(min_gains)==0){
    min_gains = NA
  }  
  return(min_gains)
  
}
