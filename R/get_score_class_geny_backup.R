get_score_class_geny_backup <- function(df_infos_longest_sequence = NULL , df_infos_spell = NULL){
  
   if(!is.null(df_infos_longest_sequence)){
      if("LENGTH_LONGEST_SEQUENCE_FINISH" %in% colnames(df_infos_longest_sequence)){
         df_infos_longest_sequence[,"LENGTH_LONGEST_SEQUENCE_FINISH"] <- round(get_percentile_values(df_infos_longest_sequence[,"LENGTH_LONGEST_SEQUENCE_FINISH"]),2)
         df_infos_longest_sequence <- get_generic_score_based_binning (df_initial_score = df_infos_longest_sequence , id = "Cheval", label_score = "LENGTH_LONGEST_SEQUENCE_FINISH" , label_derived_score = "SCORE_CLASS_LONGEST_SEQUENCE", vec_num_points = c(2,4,6,8)) 
      }
      
      if("MAX_ENGAGEMENT_LONGEST_SEQUENCE" %in% colnames(df_infos_longest_sequence)){
         df_infos_longest_sequence[,"MAX_ENGAGEMENT_LONGEST_SEQUENCE"] <- round(get_percentile_values(df_infos_longest_sequence[,"MAX_ENGAGEMENT_LONGEST_SEQUENCE"]),2)
         df_infos_longest_sequence <- get_generic_score_based_binning (df_initial_score = df_infos_longest_sequence , id = "Cheval", label_score = "MAX_ENGAGEMENT_LONGEST_SEQUENCE" , label_derived_score = "SCORE_CLASS_MAX_ENGAGEMENT_LONGEST_SEQUENCE", vec_num_points = c(2,4,6,8)) 
      } 
      
      if("MEAN_EARNING_POSITIVE" %in% colnames(df_infos_longest_sequence)){
         df_infos_longest_sequence[,"MEAN_EARNING_POSITIVE"] <- round(get_percentile_values(df_infos_longest_sequence[,"MEAN_EARNING_POSITIVE"]),2)
         df_infos_longest_sequence <- get_generic_score_based_binning (df_initial_score = df_infos_longest_sequence , id = "Cheval", label_score = "MEAN_EARNING_POSITIVE" , label_derived_score = "SCORE_CLASS_MEAN_EARNING_POSITIVE", vec_num_points = c(2,4,6,8)) 
      } 
   }
   
   if(length(grep("SCORE",colnames(df_infos_longest_sequence),value=TRUE))>0){
      df_infos_longest_sequence$SCORE_CLASS_BASED_SEQUENCE <- apply(df_infos_longest_sequence[,grep("SCORE",colnames(df_infos_longest_sequence),value=TRUE)],1,function(x){sum(x,na.rm=TRUE)})
      df_infos_longest_sequence <- df_infos_longest_sequence[,c("Cheval","SCORE_CLASS_BASED_SEQUENCE")]
      colnames(df_infos_longest_sequence)[2] <- "Class"
      df_bonus_spell <- get_bonus_score_class_geny(df_infos_spell , df_infos_target_race_geny)
      df_infos_longest_sequence <- merge(df_infos_longest_sequence,df_bonus_spell,by.x="Cheval",by.y="Cheval",all.x=TRUE)
      if(sum(is.na(df_infos_longest_sequence))>0){
         df_infos_longest_sequence[is.na(df_infos_longest_sequence)] <- 0
      }
      df_infos_longest_sequence <-  df_infos_longest_sequence %>%
         mutate(Class = BONUS_SCORE_SPELL+Class ) %>%
         as.data.frame()
      df_infos_longest_sequence <- df_infos_longest_sequence[,c("Cheval","Class")]
   }
   return(df_infos_longest_sequence)
}