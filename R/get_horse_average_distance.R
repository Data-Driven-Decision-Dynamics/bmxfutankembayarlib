get_horse_average_distance <- function(mat_infos_target_race=NULL,path_mat_historical_races_letrot = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/02-processed/mat_historical_races_letrot.rds",number_days_back=366)
{

  message("Initialize final output")
  mat_average_distance <- NULL
  
  message("Extract race date and convert in the right format if need")
  if(class(unique(mat_infos_target_race$Date))!="Date") {
    race_date  <- as.Date(as.vector(unique(mat_infos_target_race$Date)))
  } else {
    race_date <- unique(mat_infos_target_race$Date)
  }
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(mat_infos_target_race$Cheval))
  
  message("Extract category of current race")
  current_race_category <- gsub("\n","",unique(mat_infos_target_race$Epreuve))

  message("Extract distance of current race")
  current_race_distance <- min(as.numeric(gsub("\n","",unique(mat_infos_target_race$Distance))))

  message("Extract discipline of current race")
  current_discipline <- unique(mat_infos_target_race$Discipline)
  
  message("Extract hippodrome of current race")
  current_hippodrome <- as.vector(unique(mat_infos_target_race$Lieu))
  
  message("Reading historical race infos file")
  if(!exists("mat_historical_races_letrot")) {
    if(file.exists(path_mat_historical_races_letrot)) {
      mat_historical_races_letrot <- readRDS(path_mat_historical_races_letrot)
    }
  } 
  
  if(sum(current_race_horses %in% unique(mat_historical_races_letrot$Cheval))>0) {
    mat_average_distance <- mat_historical_races_letrot %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(Discipline==current_discipline) %>%
      filter(Date<race_date) %>%
      filter(race_date-Date<=number_days_back) %>%
      mutate(Place= ifelse(is.na(Place),99,Place)) %>%
      filter(Place<6) %>%
      select(Cheval,Distance) %>%
      group_by(Cheval) %>%
      summarise(GLOBAL_MEAN_DISTANCE =mean(Distance,na.rm=TRUE)) %>%
      as.data.frame()
  }
  
  vec_missing_horse <- setdiff(mat_infos_target_race$Cheval,as.vector(mat_average_distance$Cheval))
  
  if(!is.null(mat_average_distance)) {
    if(length(mat_average_distance)>0) {
      mat_average_distance <- merge(mat_average_distance,mat_infos_target_race[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all=TRUE)
      mat_average_distance[is.na(mat_average_distance$GLOBAL_MEAN_DISTANCE),"GLOBAL_MEAN_DISTANCE"] <- round(median(mat_average_distance[,"GLOBAL_MEAN_DISTANCE"],na.rm=TRUE),2)
    }
  }

  if(!is.null(mat_average_distance)) {
    mat_average_distance$GLOBAL_MEAN_DISTANCE <- round(get_min_max_normalization(mat_average_distance$GLOBAL_MEAN_DISTANCE),2)
  }

  return(mat_average_distance)
}