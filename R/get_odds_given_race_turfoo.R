get_odds_given_race_turfoo <- function(url_odds_target_race = "https://www.turfoo.fr/programmes-courses/211109/reunion1-angers/course2-prix-plantagenet/comparateur-de-cotes"){
  
  message("Initialization of the output")
  df_table_odds <- NULL
  
  message("Reading availbale table")
  list_table_odds <- html_table(read_html(httr::GET(url_odds_target_race)), fill=TRUE)
  if(length(list_table_odds)>0){
    df_table_odds <- as.data.frame(list_table_odds[[length(list_table_odds)]])
    df_table_odds <- df_table_odds[df_table_odds[,"N° - Cheval"] != "Voir la fiche du cheval",]
    
    if("data.frame" %in% class(df_table_odds)){

      message("Extracting Odds Columns")
      if(sum(colnames(df_table_odds) == "")>0){
        vec_odds_columns <- colnames(df_table_odds) [colnames(df_table_odds) == ""]
        colnames(df_table_odds) [colnames(df_table_odds) == ""] = paste("Cote",seq(1:length(vec_odds_columns)),sep="_")
      }
      
      message("Extracting Odds Columns")
      if(sum(!is.na(as.numeric(sub("h","",colnames(df_table_odds)))))>0){
        vec_odds_columns <- colnames(df_table_odds) [!is.na(as.numeric(sub("h","",colnames(df_table_odds))))]
        colnames(df_table_odds) [!is.na(as.numeric(sub("h","",colnames(df_table_odds))))] = paste("Cote",seq(1:length(vec_odds_columns)),sep="_")
      }
      
      message("Replacing NP par missing values")
      if(sum(df_table_odds == "NP",na.rm = TRUE)>0){
        df_table_odds[df_table_odds == "NP"] <- NA
      }
      
      vec_non_missing_values <- apply(df_table_odds,2,function(x){sum(!is.na(x))})
      vec_columns_to_keep <- colnames(df_table_odds)[vec_non_missing_values>0]
      
      if(length(vec_columns_to_keep)>0){
        df_table_odds <- df_table_odds[,vec_columns_to_keep]
      }

      if("N° - Cheval" %in% colnames(df_table_odds)){
        df_table_odds$`N° - Cheval` <- gsub("\n","",df_table_odds$`N° - Cheval`)
        df_table_odds$Numero <- as.numeric(unlist(lapply(df_table_odds$`N° - Cheval`,function(x) {unlist(strsplit(x," -"))[1]})))
        df_table_odds$Cheval <- stringr::str_trim(unlist(lapply(df_table_odds$`N° - Cheval`,function(x) {unlist(strsplit(x," -"))[2]})))
      }
      
      if(length(c("Numero","Cheval",grep("Cote",colnames(df_table_odds),value = TRUE)))>2){
        df_table_odds <- df_table_odds[,c("Numero","Cheval",grep("Cote",colnames(df_table_odds),value = TRUE))]
      }
      
      message("Converting Odds to percentage on chance of winning")
      if(length(grep("Cote",colnames(df_table_odds),value = TRUE))>0){
        idx_col_odds <- grep("Cote",colnames(df_table_odds),value = TRUE)
        if(length(idx_col_odds)>0){
          df_table_odds[,idx_col_odds] <- apply(df_table_odds[,idx_col_odds,drop=FALSE],2,as.numeric)
          df_table_odds[,idx_col_odds] <- round(100 * (1/df_table_odds[,idx_col_odds]),2)
        }
      }
      
      if(!is.null(df_table_odds)){
        if(ncol(df_table_odds)>2){
          colnames(df_table_odds) <- sub("Cote_3","PMU",colnames(df_table_odds))
          colnames(df_table_odds) <- sub("Cote_4","PMU.FR",colnames(df_table_odds))
          colnames(df_table_odds) <- sub("Cote_5","Betclic",colnames(df_table_odds))
          colnames(df_table_odds) <- sub("Cote_6","Zeturf",colnames(df_table_odds))
          df_table_odds$Course <- url_odds_target_race
        }
      }
      
    }
  }
  
  return(df_table_odds)
  
}

