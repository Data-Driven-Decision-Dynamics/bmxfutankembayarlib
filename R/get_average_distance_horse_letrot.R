get_average_distance_horse_letrot <- function(mat_data=NULL) {
  
  message("Initialize the finla output")
  mat_distance <- NULL
  
  message("Removing duplicates in case")
  mat_data <- unique(mat_data)

  message("Estimate average speed while correctin for distance")
  if(!is.null(mat_data) & nrow(mat_data)>0) {
    message("Estimate average distance")
    mat_distance <- mat_data %>%
      group_by(Cheval) %>%
      dplyr::summarise(Distance=round(mean(Distance,na.rm = TRUE))) %>%
      as.data.frame()
    rownames(mat_distance) <- NULL
  }

  return(mat_distance)
  
}

