get_scoring_detailed_trophee_vert_geny <- function(df_infos_target_race_geny = NULL, start_date_tv = as.Date("2021-04-04"),end_date_tv = as.Date("2021-09-06"), df_points_place = data.frame(Place=1:7,Points = c(15,10,8,6,5,4,3)), df_points_number_particip = data.frame(Size = 2:14, Points = c(rep(1,4),rep(2,5),rep(3,4))), regex_tv = "Trophée Vert", path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds") {
  
  
  df_qualified_horses <- NULL
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  id_items_tv_label <- grep(regex_tv,df_historical_races_geny$Course,ignore.case = TRUE)
  id_items_tv_details <- grep(regex_tv,df_historical_races_geny$Details,ignore.case = TRUE)
  id_items_tv <- unique(c(id_items_tv_label,id_items_tv_details))
  if(sum(id_items_tv,na.rm = TRUE)>0){
    df_races_candidates_tv <- df_historical_races_geny[id_items_tv,c("Date","Cheval","Driver","Entraineur","Place","Gains","Course","Details")]
    df_races_candidates_tv <- distinct(df_races_candidates_tv)
    id_items_tv_timeframe <- df_races_candidates_tv$Date <= end_date_tv & df_races_candidates_tv$Date >= start_date_tv
    if(sum(id_items_tv_timeframe,na.rm = TRUE)>0){
      df_races_good_tv <- df_races_candidates_tv[id_items_tv_timeframe,]
      if(sum(df_races_good_tv$Date == as.Date("2021-04-24"),na.rm = TRUE)>0){
        df_races_good_tv <- df_races_good_tv[df_races_good_tv$Date != as.Date("2021-04-24"),]
      }
      df_races_good_tv$Points <- NA
      for(idx_rank in 1:nrow(df_points_place))
      {
        idx_item_current_rank <- df_races_good_tv$Place == df_points_place[idx_rank,"Place"]
        if(sum(idx_item_current_rank,na.rm = TRUE)>0){
          df_races_good_tv[idx_item_current_rank,"Points"] <- df_points_place[idx_rank,"Points"]
        }
      }
      if(sum(is.na(df_races_good_tv$Points))>0){
        df_races_good_tv[is.na(df_races_good_tv$Points),"Points"] <- 1
      }
    }
    
    
    if(sum(df_races_good_tv$Place ==1,na.rm = TRUE)>0){
      df_qualified_horses <- df_races_good_tv[df_races_good_tv$Place ==1,"Cheval",drop=FALSE]
      df_qualified_horses$QUALIFIED <- 1
    }

    df_number_participation <- df_races_good_tv %>%
      group_by(Cheval) %>%
      dplyr::summarise(NUMBER_PARTICIPATION_HORSE = n()) %>%
      as.data.frame()
    df_number_participation$SCORE_PARTICIPATION <- 0
    
    for(idx in 1:nrow(df_points_number_particip)){
      idx_item_part <- df_number_participation$NUMBER_PARTICIPATION_HORSE == df_points_number_particip[idx,"Size"]
      if(sum(idx_item_part,na.rm = TRUE)>0){
        df_number_participation[idx_item_part,"SCORE_PARTICIPATION"] <- df_points_number_particip[idx,"Points"]
      }
    }
 
    df_score_horse <- df_races_good_tv %>%
      group_by(Cheval) %>%
      dplyr::summarise(SCORE_HORSE = sum(Points)) %>%
      as.data.frame()

    list_scores_tv <- list(Participation = df_number_participation[,c("Cheval","NUMBER_PARTICIPATION_HORSE","SCORE_PARTICIPATION")], Horse = df_score_horse , Qualified = df_qualified_horses)
    df_infos_scores_tv <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_scores_tv)
    df_infos_scores_tv$SCORE_HORSE <- df_infos_scores_tv$SCORE_HORSE + df_infos_scores_tv$SCORE_PARTICIPATION
    df_infos_scores_tv <- unique(df_infos_scores_tv[,c("Cheval","SCORE_HORSE","QUALIFIED","NUMBER_PARTICIPATION_HORSE")])
    df_infos_scores_tv <- merge(df_infos_scores_tv,df_infos_target_race_geny[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all.y = TRUE)
    if(sum(is.na(df_infos_scores_tv))>0){
      df_infos_scores_tv[is.na(df_infos_scores_tv)] <- 0
    }
  }
  
  return(df_infos_scores_tv)
}





