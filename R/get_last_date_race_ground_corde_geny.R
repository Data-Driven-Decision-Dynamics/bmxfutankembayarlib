get_last_date_race_ground_corde_geny <- function(df_infos_target_race=NULL,
                                                 path_mat_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                 number_days_back=366){

  df_last_date_race_ground_corde <- NULL
  
  message("Extract race date and convert in the right format if need")
  if(class(unique(df_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race$Date)
  }

  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))

  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race$Discipline)

  message("Extract terrain of current race")
  current_terrain <- as.vector(unique(df_infos_target_race$Terrain))
  
  message("Extract terrain of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race$Corde))

  message("Reading historical race infos file")
  if(!exists("mat_historical_races_geny")) {
    if(file.exists(path_mat_historical_races_geny)) {
      mat_historical_races_geny <- readRDS(path_mat_historical_races_geny)
    }
  } 
  
  mat_infos_focus <- mat_historical_races_geny %>%
    dplyr::filter(Cheval %in% current_race_horses) %>%
    filter(Date<current_race_date) %>%
    filter(current_race_date-Date<=number_days_back) %>%
    filter(Discipline==current_race_discipline) %>%
    mutate(TC=paste(Terrain,Corde,sep="_")) %>%
    mutate(TC=tolower(TC)) %>%
    as.data.frame()
  
  if(nrow(mat_infos_focus)>0){
    mat_infos_focus$TC <- gsub("é","e",mat_infos_focus$TC)
    mat_infos_focus$TC <- gsub("â","a",mat_infos_focus$TC)
    df_last_date_race_ground_corde <- mat_infos_focus [,c("Cheval","Date","TC")] %>%
      pivot_wider(
        names_from = TC,
        values_from = Date,
        values_fn = max
      ) %>%
      as.data.frame()
  }
  
  if(current_race_discipline !="Trot Attelé"){
    infos_last_date_race_ground_corde <- mat_historical_races_geny %>%
      dplyr::filter(Cheval %in% current_race_horses) %>%
      filter(Date<current_race_date) %>%
      filter(current_race_date-Date<=number_days_back) %>%
      filter(Discipline==current_race_discipline) %>%
      mutate(TC=paste(Terrain,Corde,sep="_")) %>%
      mutate(TC=tolower(TC)) %>%
      group_by(Cheval,TC) %>%
      top_n(1,Date) %>%
      select(Cheval,TC,Poids) %>%
      as.data.frame()
    
    if(nrow(infos_last_date_race_ground_corde)>0){
      infos_last_date_race_ground_corde$TC <- gsub("é","e",infos_last_date_race_ground_corde$TC)
      infos_last_date_race_ground_corde$TC <- gsub("â","a",infos_last_date_race_ground_corde$TC)
      df_poids_last_date_race_ground_corde <- infos_last_date_race_ground_corde %>%
        pivot_wider(
          names_from = TC,
          values_from = Poids,
          values_fn = mean
        )
      
      df_poids_last_date_race_ground_corde <- merge(df_poids_last_date_race_ground_corde,df_infos_target_race[,c("Cheval","Poids")],by.x="Cheval",by.y="Cheval",all.y = TRUE)
      for(col in colnames(df_poids_last_date_race_ground_corde)[-1]){
        df_poids_last_date_race_ground_corde[,col] <- df_poids_last_date_race_ground_corde[,col] - df_poids_last_date_race_ground_corde[,"Poids"] 
      }
    }
  }

  if(current_race_discipline =="Trot Attelé"){
    infos_last_date_race_ground_corde <- mat_historical_races_geny %>%
      dplyr::filter(Cheval %in% current_race_horses) %>%
      filter(Date<current_race_date) %>%
      filter(current_race_date-Date<=number_days_back) %>%
      filter(Discipline==current_race_discipline) %>%
      mutate(TC=paste(Terrain,Corde,sep="_")) %>%
      mutate(TC=tolower(TC)) %>%
      group_by(Cheval,TC) %>%
      top_n(1,Date) %>%
      select(Cheval,TC,Recul) %>%
      as.data.frame()
    
    if(nrow(infos_last_date_race_ground_corde)>0){
      infos_last_date_race_ground_corde$TC <- gsub("é","e",infos_last_date_race_ground_corde$TC)
      infos_last_date_race_ground_corde$TC <- gsub("â","a",infos_last_date_race_ground_corde$TC)
      df_recul_last_date_race_ground_corde <- infos_last_date_race_ground_corde %>%
        pivot_wider(
          names_from = TC,
          values_from = Recul,
          values_fn = mean
        )
      df_recul_last_date_race_ground_corde <- merge(df_recul_last_date_race_ground_corde,df_infos_target_race[,c("Cheval","Recul")],by.x="Cheval",by.y="Cheval",all.y = TRUE)
      for(col in colnames(df_recul_last_date_race_ground_corde)[-1]){
        df_recul_last_date_race_ground_corde[,col] <- df_recul_last_date_race_ground_corde[,col] - df_recul_last_date_race_ground_corde[,"Recul"] 
      }
    }
  }
  
  if(current_race_discipline !="Trot Attelé") {
    res <- list(Date=df_last_date_race_ground_corde,Poids=df_poids_last_date_race_ground_corde)
  } else {
    res <- list(Date=df_last_date_race_ground_corde,Recul=df_recul_last_date_race_ground_corde)
  }
  
  return(res)
}

