#' @return Compute fitness of horses in a given race
#' @importFrom magrittr %>%
#' @importFrom dplyr group_by
#' @importFrom dplyr top_n
#' @importFrom dplyr top_n
#' @importFrom dplyr summarise
#' @importFrom dplyr mutate
#' @examples
#' get_horse_fitness_given_race_letrot(df_infos_target_race = NULL,number_race=4)
#' @export
get_feature_fitness_given_race_geny <- function(df_infos_target_race = NULL,
                                                path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                number_race=4) {
  
  df_infos_fitness <- NULL
  
  df_infos_target_race_add <- df_infos_target_race[,c("Numero","Cheval")]
  current_race_horses <- unique(df_infos_target_race$Cheval)
  discipline <- unique(df_infos_target_race$Discipline)
  if(class(unique(df_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race$Date)
  }

  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  } 
  
  current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))
  
  df_perf_race_target_horses <- df_historical_races_geny[df_historical_races_geny$Cheval %in% current_race_horses & df_historical_races_geny$Date<current_race_date,]

  df_details_fitness_horses <- unique(df_perf_race_target_horses[,c('Cheval','Date')]) %>% 
    group_by(Cheval) %>% 
    top_n (number_race,Date) %>%
    as.data.frame() %>%
    distinct()

  if(nrow(df_details_fitness_horses)>0)  {
    mat_details_day_since_last_race <- vector("list",length(current_race_horses) )
    names(mat_details_day_since_last_race) <- current_race_horses
    for(horse in current_race_horses)
    {
      if(length(c(current_race_date,sort(df_details_fitness_horses[df_details_fitness_horses$Cheval==horse,"Date"],decreasing = TRUE)))>1) {
        mat_details_day_since_last_race_horse_tmp <- get_consecutive_delta(c(current_race_date,sort(df_details_fitness_horses[df_details_fitness_horses$Cheval==horse,"Date"],decreasing = TRUE)),format = "mat")
        if(nrow(mat_details_day_since_last_race_horse_tmp)==1) {
          mat_details_day_since_last_race_horse = data.frame("1"=NA,"2"=NA,"3"=NA,"4"=NA)
          colnames(mat_details_day_since_last_race_horse) <- gsub ("X","",colnames(mat_details_day_since_last_race_horse))
          rownames(mat_details_day_since_last_race_horse) <- horse
          mat_details_day_since_last_race_horse[,colnames(mat_details_day_since_last_race_horse_tmp)] <- as.vector(unlist(mat_details_day_since_last_race_horse_tmp))
          mat_details_day_since_last_race <- rbind(mat_details_day_since_last_race,mat_details_day_since_last_race_horse)
        }
      }
    }
    
    df_infos_fitness <- mat_details_day_since_last_race
    df_infos_fitness$Cheval <- as.vector(rownames(df_infos_fitness))
    df_infos_fitness <- df_infos_fitness[,c("Cheval",setdiff(colnames(df_infos_fitness),"Cheval"))]
    df_infos_fitness <- as.data.frame(t(apply(df_infos_fitness[,-1],1,cumsum)))
    colnames(df_infos_fitness) <- c("FITNESS_NUMBER_DAYS_SINCE_LAST_RACE","FITNESS_NUMBER_DAYS_SINCE_SECOND_RACE","FITNESS_NUMBER_DAYS_SINCE_THIRD_RACE","FITNESS_NUMBER_DAYS_SINCE_FOURTH_RACE")
    df_infos_fitness$Cheval <- rownames(df_infos_fitness)
    df_infos_fitness <- df_infos_fitness[,rev(colnames(df_infos_fitness))]
  } 
  
  if(!is.null(df_infos_fitness)){
    df_infos_fitness$FITNESS_GOOD_PACE_REGULAR_FITHTEEN_DAYS <-0
    df_infos_fitness$FITNESS_GOOD_PACE_REGULAR_THIRTHY_DAYS  <- 0
    for(idx in 1:nrow(df_infos_fitness))
    {
      val_for_pace <- rev(unlist(df_infos_fitness[idx,2:5]))
      if(sum(is.na(val_for_pace))==0){
        regularity_val_for_pace <- diff(val_for_pace)
        if(sum(regularity_val_for_pace<=15)==3){
          df_infos_fitness$FITNESS_GOOD_PACE_REGULAR_FITHTEEN_DAYS[idx] <- 1
        }
        if(sum(regularity_val_for_pace<=30)==3){
          df_infos_fitness$FITNESS_GOOD_PACE_REGULAR_THIRTHY_DAYS[idx] <- 1
        }
      }
    }
  }
  
  return(df_infos_fitness) 
}
