verify_handcraft_rules_expert_turn_rfm_based_geny <- function(df_infos_target_race_geny = NULL,
                                                              path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"){
  
  message("Initialization of final output")
  df_score_rfm_turn_all_together <- NULL
  df_candidate_aptitude_expert_turn_rfm_based <- data.frame(Cheval = unique(df_infos_target_race_geny$Cheval), APTITUDE_EXPERT_TURN_RFM = 0 , APTITUDE_EXPERT_BOTH_TURNS_RFM = 0)
  message("Initialization of final output")
  
  if(!is.null(df_infos_target_race_geny)) {
    if(!exists("df_historical_races_geny")) {
      if(file.exists(path_df_historical_races_geny)){
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    }

    if(!is.na(unique(df_infos_target_race_geny$Corde))){
      df_number_races_turns <- df_historical_races_geny %>%
        dplyr::filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
        dplyr::filter(Discipline == unique(df_infos_target_race_geny$Discipline)) %>%
        dplyr::filter(Date <= unique(df_infos_target_race_geny$Date)) %>%
        select(Cheval,Corde,Gains) %>%
        pivot_wider(
          names_from = Corde,
          values_from = Gains,
          values_fn = length
        ) %>%
        as.data.frame()
      if(nrow(df_number_races_turns)>0){
        if(sum(is.na(df_number_races_turns))>0){
          df_number_races_turns[is.na(df_number_races_turns)] <- 0
        } else {
          df_number_races_turns <- NULL
        }
      }
      
      df_infos_races_focus <- df_historical_races_geny %>%
        dplyr::filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
        dplyr::filter(Discipline == unique(df_infos_target_race_geny$Discipline)) %>%
        dplyr::filter(Date <= unique(df_infos_target_race_geny$Date)) %>%
        select(Cheval,Corde,Gains,Date) %>%
        filter(Gains >0) %>%
        mutate(Delay = as.numeric(Sys.Date()-Date)) %>%
        group_by(Cheval,Corde) %>%
        dplyr::summarise(M=mean(Gains),
                         R=min(Delay)) %>%
        as.data.frame()
      
      if(nrow(df_infos_races_focus)>0){
        vec_turns_found <- unique(df_infos_races_focus$Corde)
        for(turn in vec_turns_found)
        {
          df_infos_races_focus_turn <- df_infos_races_focus[df_infos_races_focus$Corde == turn,]
          if(!is.null(df_number_races_turns)){
            if(turn %in% colnames(df_number_races_turns)){
              df_number_races_turns_focus <- df_number_races_turns[,c("Cheval",turn)]
              colnames(df_number_races_turns_focus)[2] <- "F"
              df_infos_races_focus_turn <- merge(df_infos_races_focus_turn[,c("Cheval","R","M")],df_number_races_turns_focus,by.x="Cheval",by.y="Cheval")
              df_infos_races_focus_turn <- df_infos_races_focus_turn[,c("Cheval","R","F","M")]
              df_rfm_turn_current <- compute_rfm_score(df_infos_target_race_geny,df_infos_races_focus_turn)
              df_rfm_turn_current <- df_rfm_turn_current[,c("Cheval","RFM_SCORE")]
              df_rfm_turn_current$Turn <- turn
              df_score_rfm_turn_all_together <- rbind(df_score_rfm_turn_all_together,df_rfm_turn_current)
            }
          }
        }
      }
    }
  }
  
  if(!is.null(df_score_rfm_turn_all_together)){
    if(unique(df_infos_target_race_geny$Corde) %in% df_score_rfm_turn_all_together$Turn){
      vec_candidate_good_at_turn <- df_score_rfm_turn_all_together[df_score_rfm_turn_all_together$Turn == unique(df_infos_target_race_geny$Corde) & df_score_rfm_turn_all_together$RFM_SCORE >= 11, "Cheval"]
      if(length(vec_candidate_good_at_turn)>0){
        df_candidate_aptitude_expert_turn_rfm_based[df_candidate_aptitude_expert_turn_rfm_based$Cheval %in% vec_candidate_good_at_turn, "APTITUDE_EXPERT_TURN_RFM"] <- 1
      }
    }
    
    if(sum(df_score_rfm_turn_all_together$RFM_SCORE>=11)>0){
      vec_table_which_turn_good_at <- table(df_score_rfm_turn_all_together[df_score_rfm_turn_all_together$RFM_SCORE>=11,"Cheval"])
      if(max(vec_table_which_turn_good_at)>=2){
        df_candidate_aptitude_expert_turn_rfm_based[df_candidate_aptitude_expert_turn_rfm_based$Cheval %in% names(vec_table_which_turn_good_at)[vec_table_which_turn_good_at>=2], "APTITUDE_EXPERT_BOTH_TURNS_RFM"] <- 1
      }
    }
  }
  
  return(df_candidate_aptitude_expert_turn_rfm_based)
}
