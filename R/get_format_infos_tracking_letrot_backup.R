# get_format_infos_tracking_letrot <- function(path_df_infos_tracking = "/mnt/Master-Data/Futanke-Mbayar/France/tracking/2022-05-29-prix-de-la-ville-de-landivisiau.rds"){
#   
#   df_stats_scores_tracks <- NULL
#   
#   if(!is.null(path_df_infos_tracking)){
#     if(file.exists(path_df_infos_tracking)){
#       
#     } 
#   }
#   
#   
#   target_date <- sort(dir(path_output_dashboard),decreasing = TRUE)[1]
#   
#   vec_details_reunions <- NULL
#   if(dir.exists(paste(path_compiled_results,as.character(target_date),sep="/"))){
#     vec_details_reunions <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),pattern = ".xlsx")
#     vec_details_reunions <- setdiff(vec_details_reunions,c("Infos.xlsx","Bettable.xlsx","Drives.xlsx"))
#     path_file_infos <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),pattern = "Infos.xlsx")
#   }
#   
#   if(length(vec_details_reunions)==1){
#     df_details_races <- read.xlsx(paste(paste(path_compiled_results,as.character(target_date),sep="/"),vec_details_reunions,sep="/"),sheetIndex = 1) %>%
#       as.data.frame()
#     df_details_races <- unique(df_details_races)
#     df_details_races <- df_details_races[order(df_details_races$Heure,decreasing = FALSE),]
#     df_details_races$Course <- gsub(" ","-",df_details_races$Race)
#     df_details_races$Course <- gsub("'","",df_details_races$Course)
#     df_details_races$Course <- gsub("\\(","",df_details_races$Course)
#     df_details_races$Course <- gsub(")","",df_details_races$Course)
#     df_details_races$Course <- gsub("--Genybet-","-Genybet",df_details_races$Course)
#     
#     if("Fit" %in% colnames(df_details_races)) {
#       df_details_races$FIT_COLOR <- "white"
#       vec_mean_fit_values <- unique(unlist(df_details_races[,"Fit"]))
#       vec_mean_fit_values <- vec_mean_fit_values[!is.na(vec_mean_fit_values)]
#       
#       idx_unfit <- df_details_races[,"Fit"]<25
#       if(sum(idx_unfit,na.rm = TRUE)>0){
#         df_details_races$FIT_COLOR[idx_unfit] <- brewer.pal(9,"OrRd")[7]
#       }
#       
#       idx_low_fit <- df_details_races[,"Fit"]>=25 & df_details_races[,"Fit"]<50
#       if(sum(idx_low_fit,na.rm = TRUE)>0){
#         df_details_races$FIT_COLOR[idx_low_fit] <- brewer.pal(9,"OrRd")[5]
#       }
#       
#       idx_good_fit <- df_details_races[,"Fit"]>=50 & df_details_races[,"Fit"]<75
#       if(sum(idx_good_fit,na.rm = TRUE)>0){
#         df_details_races$FIT_COLOR[idx_good_fit] <- brewer.pal(9,"Blues")[3]
#       }
#       
#       idx_very_fit <- df_details_races[,"Fit"]>=75
#       if(sum(idx_very_fit,na.rm = TRUE)>0){
#         df_details_races$FIT_COLOR[idx_very_fit] <- brewer.pal(9,"Blues")[5]
#       }
#     }
#     
#     if("PILOT_VALUE" %in% colnames(df_details_races)) {
#       vec_pilot_value_values <- unique(unlist(df_details_races[,"PILOT_VALUE"]))
#       vec_pilot_value_values <- vec_pilot_value_values[!is.na(vec_pilot_value_values)]
#       vec_pilot_value_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_pilot_value_values))[rank(vec_pilot_value_values)]
#       names(vec_pilot_value_color) <- vec_pilot_value_values
#     }
#     
#     if("OPPONENTS_VALUE" %in% colnames(df_details_races)) {
#       vec_opponents_value_values <- unique(unlist(df_details_races[,"OPPONENTS_VALUE"]))
#       vec_opponents_value_values <- vec_opponents_value_values[!is.na(vec_opponents_value_values)]
#       vec_opponents_value_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_opponents_value_values))[rank(vec_opponents_value_values)]
#       names(vec_opponents_value_color) <- vec_opponents_value_values
#     }
#     
#     if("Prix" %in% colnames(df_details_races)) {
#       vec_races_prize_values <- unique(unlist(df_details_races[,"Prix"]))
#       vec_races_prize_values <- vec_races_prize_values[!is.na(vec_races_prize_values)]
#       vec_races_prize_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_races_prize_values))[rank(vec_races_prize_values)]
#       names(vec_races_prize_color) <- vec_races_prize_values
#     }
#     
#     if("Number" %in% colnames(df_details_races)) {
#       vec_num_competitors_values <- unique(unlist(df_details_races[,"Number"]))
#       vec_num_competitors_values <- vec_num_competitors_values[!is.na(vec_num_competitors_values)]
#       vec_num_competitors_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_num_competitors_values))[rank(vec_num_competitors_values)]
#       names(vec_num_competitors_color) <- vec_num_competitors_values
#     }
#     
#     if("Size" %in% colnames(df_details_races)) {
#       vec_num_races_opponents_values <- unique(unlist(df_details_races[,"Size"]))
#       vec_num_races_opponents_values <- vec_num_races_opponents_values[!is.na(vec_num_races_opponents_values)]
#       vec_num_races_opponents_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_num_races_opponents_values))[rank(vec_num_races_opponents_values)]
#       names(vec_num_races_opponents_color) <- vec_num_races_opponents_values
#     }
#   }
#   
#   if(length(path_file_infos)==1){
#     df_infos_races <- read.xlsx(paste(paste(path_compiled_results,as.character(target_date),sep="/"),path_file_infos,sep="/"),sheetIndex = 1) %>%
#       as.data.frame()
#   }
#   
#   df_details_race_current <- df_details_races [df_details_races$Course == gsub("--Genybet-","-Genybet",target_race), ]
#   number_race_current_item <- substr(unlist(strsplit(target_race,'-'))[1],1,1)
#   path_current_reunion <- paste(path_compiled_results,target_date,df_details_race_current$Location,sep="/")
#   path_current_reunion_race <- paste(path_compiled_results,target_date,df_details_race_current$Location,target_race,sep="/")
#   path_file_pronostics_current_race <- dir(path_current_reunion_race,pattern = ".xlsx")
#   path_file_pronostics_current_race <- unique(setdiff(path_file_pronostics_current_race,c("Innovation.rds","Max.xlsx","Remontada.xlsx","Participation.xlsx","Probabilities.xlsx","TETF.xlsx","Distances.xlsx","Mague.xlsx","Apprentis.xlsx","Apprentices.xlsx","Year.xlsx","Signal.xlsx","SAME-RACE-H.xlsx","SAME-RACE-D.xlsx","SAME-RACE-T.xlsx","Carreer.xlsx","Amateurs.xlsx","TV.xlsx","TVA.xlsx","GNA.xlsx","Manual.xlsx","Trophee-Vert.xlsx","Grand-National-Trot.xlsx","Open-Regions.xlsx","Criterium.xlsx","df_infos_tracking.rds","Profile.xlsx","Trot.xlsx","Galop.xlsx","Ward.xlsx","Draw.xlsx","Compare.xlsx","Remontada.xlsx","AutoStart.xlsx","Auto-Start.xlsx","Aptitude.xlsx","Gore.xlsx","RTF.xlsx","Benchmark.xlsx","Advantage.xlsx","Sensitivity.xlsx","Limit.xlsx","Consecutive.xlsx","Consolidated.xlsx","Polygon.xlsx","Tankunem.xlsx","Doley.xlsx","Khaliss.xlsx","Trainers.xlsx","Horses.xlsx","Drivers.xlsx","Associations.xlsx","Year.xlsx","GNT.xlsx","Cadre.xlsx","Orderer.xlsx","Config.xlsx","International.xlsx","Placing.xlsx","Toughest.xlsx","Going.xlsx","Non-Partants.xlsx","Disqualified.xlsx","Epreuve.xlsx","Beaten.xlsx","Niveau.xlsx","Level.xlsx","Participation.xlsx","Yenn.xlsx","Handicap.xlsx","Weight.xlsx","Travel.xlsx","Infos.xlsx","Tandems.xlsx","Ecuries.xlsx","Participation.xlsx","Earning.xlsx","Ranking.xlsx","Records.xlsx","Track.xlsx","Issues.xlsx","Intention.xlsx","Spell.xlsx","Pairs.xlsx","Ferrage.xlsx","Success.xlsx","Engagement.xlsx","Fitness.xlsx","Odds.xlsx")))
#   
#   vec_file_infos_global_current_race <- dir(path_current_reunion,recursive = TRUE,full.names = TRUE,pattern="Infos")
#   if(length(vec_file_infos_global_current_race)>0){
#     file_infos_global <- grep(target_race,vec_file_infos_global_current_race,value=TRUE)
#     if(length(file_infos_global)==1){
#       df_infos_global_current <- read.xlsx(file_infos_global,sheetIndex=1) %>%
#         as.data.frame()
#     }
#   }
#   
#   if(length(dir(path_current_reunion_race,pattern="df_infos_tracking"))==1){
#     path_tracking_file <- dir(path_current_reunion_race,pattern="df_infos_tracking")
#     df_infos_tracking <- readRDS(paste(path_current_reunion_race,path_tracking_file,sep="/"))
#   }
#   
#   if(length(path_file_pronostics_current_race)==1){
#     df_infos_pronostics_current_race <- read.xlsx(paste(path_current_reunion_race,path_file_pronostics_current_race,sep="/"),sheetIndex = 1) %>%
#       as.data.frame
#   }
#   
#   df_infos_pronostics_current_race <- base::merge(df_infos_pronostics_current_race,df_infos_races[,intersect(c("Cheval","Numero","Gender","Age","Discipline","Poids"),colnames(df_infos_races))],by.x="Cheval",by.y="Cheval",all.x=TRUE)
#   df_infos_pronostics_current_race$ID <- paste(df_infos_pronostics_current_race$Numero,df_infos_pronostics_current_race$Cheval,sep="-")
#   df_infos_pronostics_current_race$Rank <- apply(apply(df_infos_pronostics_current_race[,c("Final"),drop=FALSE],2,rank),1,function(x){mean(x,na.rm=TRUE)})
#   df_infos_pronostics_current_race <- df_infos_pronostics_current_race[order(df_infos_pronostics_current_race$Rank,decreasing = TRUE),]
#   df_infos_pronostics_current_race$Pivot <- paste("P",max(df_details_races[df_details_races$Location==df_details_race_current$Location,]$Number):((max(df_details_races[df_details_races$Location==df_details_race_current$Location,]$Number)-nrow(df_infos_pronostics_current_race))+1),sep="")
#   df_infos_pronostics_current_race$Numero <- as.numeric(df_infos_pronostics_current_race$Numero)
#   df_infos_target_race_geny <- df_infos_races [df_infos_races$Cheval %in% grep(paste(sort(df_infos_pronostics_current_race$Cheval),collapse = "|"),df_infos_races$Cheval,ignore.case = TRUE,value=TRUE) ,]
#   
#   message("Start reading historical performance file")
#   if(!exists("df_historical_races_geny")) {
#     if(file.exists(path_df_historical_races_geny)){
#       df_historical_races_geny <- readRDS(path_df_historical_races_geny)
#     }
#   }
#   message("Start reading historical performance file")
#   
#   message("Start formating the tracking details file")
#   if(!is.null(df_infos_tracking)){
#     df_infos_tracking$Date <- unlist(lapply(df_infos_tracking$Date,function(x){substr(x,1,10)}))
#     df_infos_tracking$RACE <- paste(df_infos_tracking$Prix,df_infos_tracking$Date,sep="_")
#     df_infos_tracking$ID   <- paste(df_infos_tracking$Cheval,df_infos_tracking$Date,sep="_")
#     df_infos_tracking$Longueur <- NA
#     df_infos_tracking$Number <- NA
#     df_infos_tracking$Place <- NA
#     df_infos_tracking$Depart <- NA
#     df_infos_tracking$Recul <- NA
#     df_infos_tracking$Success <- NA
#     df_infos_tracking$Gains <- 0
#     df_infos_tracking$TRACK <- NA
#     df_historical_races_focus_tracking <- df_historical_races_geny[df_historical_races_geny$Date %in% as.Date(unique(df_infos_tracking$Date)),]
#     df_historical_races_focus_tracking$ID <- paste(toupper(toupper(gsub("ê","e",gsub("è","e",gsub("é","e",df_historical_races_focus_tracking$Cheval))))),df_historical_races_focus_tracking$Date,sep="_")
#     df_historical_races_focus_tracking <- df_historical_races_focus_tracking %>%
#       filter(ID %in% df_infos_tracking$ID)
#     for(idx_row in 1:nrow(df_infos_tracking)){
#       val_current_distance <- as.numeric(df_infos_tracking[idx_row,"Dist"])
#       if(sum(df_historical_races_focus_tracking$ID == df_infos_tracking[idx_row,"ID"],na.rm=TRUE)>0){
#         val_current_recul_if_any <- df_historical_races_focus_tracking[df_historical_races_focus_tracking$ID == df_infos_tracking[idx_row,"ID"], "Recul"]
#         val_current_number <- df_historical_races_focus_tracking[df_historical_races_focus_tracking$ID == df_infos_tracking[idx_row,"ID"], "Numero"]
#         val_current_rank <- df_historical_races_focus_tracking[df_historical_races_focus_tracking$ID == df_infos_tracking[idx_row,"ID"], "Place"]
#         val_current_depart <- df_historical_races_focus_tracking[df_historical_races_focus_tracking$ID == df_infos_tracking[idx_row,"ID"], "Depart"]
#         val_current_recul <- df_historical_races_focus_tracking[df_historical_races_focus_tracking$ID == df_infos_tracking[idx_row,"ID"], "Recul"]
#         val_current_gains <- df_historical_races_focus_tracking[df_historical_races_focus_tracking$ID == df_infos_tracking[idx_row,"ID"], "Gains"]
#         df_infos_tracking[idx_row,"Longueur"] <- val_current_distance-val_current_recul_if_any
#         df_infos_tracking[idx_row,"Number"] <- val_current_number
#         df_infos_tracking[idx_row,"Place"] <- val_current_rank
#         df_infos_tracking[idx_row,"Depart"] <- val_current_depart
#         df_infos_tracking[idx_row,"Recul"] <- val_current_recul
#         df_infos_tracking[idx_row,"Gains"] <- val_current_gains
#         df_infos_tracking[idx_row,"TRACK"] <- paste(df_historical_races_focus_tracking[df_historical_races_focus_tracking$ID == df_infos_tracking[idx_row,"ID"], "Lieu"],(val_current_distance-val_current_recul_if_any),sep="-")
#         df_success_number_current <- get_percent_success_podium_autostart_number_geny(df_infos_target_race_geny,unlist(strsplit(df_infos_tracking[idx_row,"TRACK"],'-'))[1],unlist(strsplit(df_infos_tracking[idx_row,"TRACK"],'-'))[2])
#         if(!is.null(df_success_number_current)){
#           if(as.numeric(df_infos_tracking[idx_row,"Number"]) %in% df_success_number_current$Numero){
#             df_infos_tracking[idx_row,"Success"] <- df_success_number_current[df_success_number_current$Numero == as.numeric(df_infos_tracking[idx_row,"Number"]),"Success"]
#           }
#         }
#       }
#     }
#     
#     df_name_horses_pivot <- unique(base::merge(df_infos_tracking[,'Numero',drop=FALSE],df_infos_target_race_geny[,c("Numero","Cheval")],by.x="Numero",by.y="Numero",all.y=TRUE))
#     rownames(df_name_horses_pivot) <- NULL
#     colnames(df_name_horses_pivot)[colnames(df_name_horses_pivot)=="Cheval"] <- "Fass"
#     df_infos_tracking <- base::merge(df_name_horses_pivot,df_infos_tracking,by.x="Numero",by.y="Numero",all.y=TRUE)
#     if(length(intersect(colnames(df_infos_tracking),"Fass.x"))>0){
#       colnames(df_infos_tracking) <- sub("Fass.x","Fass",colnames(df_infos_tracking))
#     }
#     df_infos_tracking <- df_infos_tracking[,c("Date","Numero","Fass","Depart","Recul","Success","Gains",grep("SPEED|RANK",colnames(df_infos_tracking),value = TRUE),"Longueur","Number","Place","TRACK")]
#     colnames(df_infos_tracking)[colnames(df_infos_tracking)=="Fass"] <- "Cheval"
#     
#     if(length(grep("Vincennes",df_infos_tracking$TRACK,ignore.case = TRUE))>0){
#       df_infos_tracking <- df_infos_tracking[grep("Vincennes",df_infos_tracking$TRACK,ignore.case = TRUE),]
#     }
#     
#     vec_track_found_location <- unique(df_infos_tracking$TRACK)
#     tab_horse_track <- table(df_infos_tracking$Cheval,df_infos_tracking$TRACK)
#     vec_horse_track <- apply(tab_horse_track,2,function(x){sum(x>0)})
#     vec_track_keep <- sort(names(vec_horse_track)[vec_horse_track>4])
#     
#     df_stats_scores_tracks <- NULL
#     if(length(vec_track_keep)>0){
#       for(current_track in vec_track_keep)
#       {
#         df_stats_scores_track <- NULL
#         df_infos_tracking_one_track <- df_infos_tracking[df_infos_tracking$TRACK == current_track, ]
#         for(idx in 1:nrow(df_infos_tracking_one_track))
#         {
#           val_date_idx <- df_infos_tracking_one_track[idx,"Date"]
#           val_horse_idx <- df_infos_tracking_one_track[idx,"Cheval"]
#           val_location_idx <- unlist(strsplit(df_infos_tracking_one_track$TRACK[idx],"-"))[1]
#           val_speed_start_2000 <- df_infos_tracking_one_track[idx,"SPEED_START_2000"]
#           val_speed_start_1500 <- df_infos_tracking_one_track[idx,"SPEED_START_1500"]
#           val_speed_start_1000 <- df_infos_tracking_one_track[idx,"SPEED_START_1000"]
#           val_speed_start_500 <- df_infos_tracking_one_track[idx,"SPEED_START_500"]
#           val_speed_last_1000 <- df_infos_tracking_one_track[idx,"SPEED_LAST_1000"]
#           val_speed_last_500 <- df_infos_tracking_one_track[idx,"SPEED_LAST_500"]
#           val_speed_overall <- df_infos_tracking_one_track[idx,"SPEED_OVERALL"]
#           val_distance_current_speed <- df_infos_tracking_one_track[idx,"Longueur"]
#           if(!is.na(val_speed_start_2000) & !is.na(val_speed_start_1500) & !is.na(val_speed_start_1000) & !is.na(val_speed_last_1000) & !is.na(val_speed_last_500)){
#             val_acceleration_start_2000_1500 <- val_speed_start_2000-val_speed_start_1500
#             val_acceleration_start_1500_1000 <- val_speed_start_1500-val_speed_start_1000
#             val_acceleration_start_1000_500 <- val_speed_start_1000-val_speed_start_500
#             val_acceleration_last_1000_500 <- val_speed_last_1000-val_speed_last_500
#             df_stats_scores_track_idx <- data.frame(Date = val_date_idx,
#                                                     Cheval = val_horse_idx,
#                                                     Lieu = val_location_idx,
#                                                     Distance = val_distance_current_speed,
#                                                     ACCELERATION_START_2000_1500 = val_acceleration_start_2000_1500,
#                                                     ACCELERATION_START_1500_1000 = val_acceleration_start_1500_1000,
#                                                     ACCELERATION_START_1000_500 = val_acceleration_start_1000_500,
#                                                     ACCELERATION_LAST_1000_500 = val_acceleration_last_1000_500,
#                                                     SPEED_OVERALL = val_speed_overall)
#             df_stats_scores_track <- rbind(df_stats_scores_track,df_stats_scores_track_idx)
#           }
#         }
#         df_stats_scores_tracks <- rbind(df_stats_scores_tracks,df_stats_scores_track)
#       }
#     }
#   }
#   message("Start formating the tracking details file")
#   
#   vec_max_items_track <- apply(tab_horse_track[,vec_track_keep],2,max)
#   vec_max_items_track <- sort(vec_max_items_track)
#   
#   list_colors_acceleration_tracking <- vector('list',length(vec_max_items_track))
#   names(list_colors_acceleration_tracking) <- names(vec_max_items_track)
#   for(idx_list in names(list_colors_acceleration_tracking)){
#     current_location <- unlist(strsplit(idx_list,"-"))[1]
#     current_distance <- unlist(strsplit(idx_list,"-"))[2]
#     if(current_location %in% df_stats_scores_tracks$Lieu & current_distance %in% df_stats_scores_tracks$Distance){
#       vec_acceleration_start_2000_1500_values <- df_stats_scores_tracks[df_stats_scores_tracks$Lieu == current_location & df_stats_scores_tracks$Distance == current_distance  ,"ACCELERATION_START_2000_1500"]
#       vec_acceleration_start_2000_1500_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_acceleration_start_2000_1500_values))[rank(vec_acceleration_start_2000_1500_values)]
#       names(vec_acceleration_start_2000_1500_color) <- vec_acceleration_start_2000_1500_values
#       vec_acceleration_start_1500_1000_values <- df_stats_scores_tracks[df_stats_scores_tracks$Lieu == current_location & df_stats_scores_tracks$Distance == current_distance  ,"ACCELERATION_START_1500_1000"]
#       vec_acceleration_start_1500_1000_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_acceleration_start_1500_1000_values))[rank(vec_acceleration_start_1500_1000_values)]
#       names(vec_acceleration_start_1500_1000_color) <- vec_acceleration_start_1500_1000_values
#       vec_acceleration_start_1000_500_values <- df_stats_scores_tracks[df_stats_scores_tracks$Lieu == current_location & df_stats_scores_tracks$Distance == current_distance  ,"ACCELERATION_START_1000_500"]
#       vec_acceleration_start_1000_500_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_acceleration_start_1000_500_values))[rank(vec_acceleration_start_1000_500_values)]
#       names(vec_acceleration_start_1000_500_color) <- vec_acceleration_start_1000_500_values
#       vec_speed_overall_values <- df_stats_scores_tracks[df_stats_scores_tracks$Lieu == current_location & df_stats_scores_tracks$Distance == current_distance  ,"SPEED_OVERALL"]
#       vec_speed_overall_values <- vec_speed_overall_values[!is.na(vec_speed_overall_values)]
#       vec_speed_overall_color <- colorRampPalette(rev(brewer.pal(9,"RdYlBu")))(length(vec_speed_overall_values))[rank(vec_speed_overall_values)]
#       names(vec_speed_overall_color) <- vec_speed_overall_values
#       vec_acceleration_last_1000_500_values <- df_stats_scores_tracks[df_stats_scores_tracks$Lieu == current_location & df_stats_scores_tracks$Distance == current_distance  ,"ACCELERATION_LAST_1000_500"]
#       vec_acceleration_last_1000_500_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_acceleration_last_1000_500_values))[rank(vec_acceleration_last_1000_500_values)]
#       names(vec_acceleration_last_1000_500_color) <- vec_acceleration_last_1000_500_values
#       list_colors_acceleration_tracking[[idx_list]] <- list("SPEED_START_2000_1500"=vec_acceleration_start_2000_1500_color,"SPEED_START_1500_1000"= vec_acceleration_start_1500_1000_color , "SPEED_START_1000_500" = vec_acceleration_start_1000_500_color,"SPEED_LAST_1000_500" = vec_acceleration_last_1000_500_color, "SPEED_OVERALL" = vec_speed_overall_color)
#     }
#   }
#   
#   message("Start defining the layout for the dashboard")
#   layout(matrix(c(1:(2*1)), 2, (1), byrow = FALSE),heights = rep(c(1,5),1))
#   message("Finish defining the layout for the dashboard")
#   
#   message("Start defining layout for the first plot")
#   par(mar=c(1,13,1,13))
#   message("Finish defining layout for the first plot")
#   
#   current_race_time <- toupper(unique(df_infos_global_current$Heure))
#   
#   vec_val_turn <- NULL
#   if(!is.na(unique(df_infos_global_current$Depart))){
#     if(unique(df_infos_global_current$Corde) == "Droite"){
#       vec_val_turn <- "Droite"
#     } else {
#       vec_val_turn <- "Gauche"
#     }
#   }
#   
#   vec_val_depart <- NULL
#   if(!is.na(unique(df_infos_global_current$Depart))){
#     if(unique(df_infos_global_current$Depart) == "Auto-Start"){
#       vec_val_depart <- "Start"
#     }
#   }
#   
#   if(unique(df_infos_pronostics_current_race$Discipline) %in% c("Plat","Haies","Cross","Steeplechase","Trot Attelé","Trot Monté")){
#     vec_lab_xaxis <- c(paste("C",number_race_current_item,sep=""),"Time","Length",vec_val_turn,vec_val_depart,"Prize","Size","Number","Horse","Fit","Drive")
#     if(!is.null(df_scoring_gnt)){
#       vec_lab_xaxis <- c(vec_lab_xaxis,"GNT")
#     }
#     if(!is.null(df_scoring_trophee_vert)){
#       vec_lab_xaxis <- c(vec_lab_xaxis,"TV")
#     }
#   }
#   
#   vec_freq_horse_trainer_current_horse <- table(df_infos_target_race_geny$Entraineur)
#   
#   vec_val_ecurie <- NULL
#   if(max(vec_freq_horse_trainer_current_horse)>1){
#     vec_val_ecurie <- "Ecurie"
#   }
#   
#   if(length(grep("Européenne|International",unique(df_infos_global_current$Details)))>0){
#     vec_lab_xaxis <- c(vec_lab_xaxis,"Foreign")
#   }
#   
#   plot(0,0,xlim=c(0,length(vec_lab_xaxis)+1),ylim=c(-1,1),xaxt="n",yaxt="n",type="n",xlab="",ylab="")
#   axis(1, 1:((length(vec_lab_xaxis))),labels=FALSE,tick = FALSE)
#   text(par("usr")[2]/2,0.75,toupper(df_details_race_current$Location),cex=1.5,col="darkblue")
#   text(par("usr")[2]/2,-0.75,gsub("-"," ", unlist(strsplit(target_race,"--"))[[2]]),cex=1.5)
#   for(id_dim in 1:length(vec_lab_xaxis))
#   {
#     if(vec_lab_xaxis[id_dim] == "Droite"){
#       points(id_dim,0,pch=22,cex=6,bg="darkred",col="white")
#       text(id_dim,0,labels=emoji("arrow_right"), cex=2, col="white", family='EmojiOne')
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Gauche"){
#       points(id_dim,0,pch=22,cex=6,bg="darkred",col="white")
#       text(id_dim,0,labels=emoji("arrow_left"), cex=2, col="white", family='EmojiOne')
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Start"){
#       points(id_dim,0,pch=22,cex=6,bg="lightgrey",col="white")
#       text(id_dim,0,labels=emoji("oncoming_automobile"), cex=2, col="black", family='EmojiOne')
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Time"){
#       points(id_dim,0,pch=22,cex=6,bg="lightblue",col="white")
#       text(id_dim,0,current_race_time,cex=0.5,col="black")
#     }
#     
#     if(vec_lab_xaxis[id_dim] == paste("C",number_race_current_item,sep="")){
#       points(id_dim,0,pch=22,cex=6,bg="black",col="white")
#       text(id_dim,0,paste("C",number_race_current_item,sep=""),cex=1,col="white")
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Fit"){
#       col_current_fitness_global <- df_details_race_current$FIT_COLOR
#       points(id_dim,0,pch=22,cex=6,bg=col_current_fitness_global,col="white")
#       text(id_dim,0,labels=round(df_details_race_current$Fit,0), cex=1.2, col="black")
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Size"){
#       col_current_num_competitors <- vec_num_competitors_color[as.character(df_details_race_current$Number)]
#       points(id_dim,0,pch=22,cex=6,bg=col_current_num_competitors,col="white")
#       text(id_dim,0,labels=nrow(df_infos_pronostics_current_race), cex=1.2, col="black")
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Length"){
#       points(id_dim,0,pch=22,cex=6,bg="purple",col="white")
#       text(id_dim,0,labels=paste(df_details_race_current$Distance,"m",sep=""),cex=0.5,col="white")
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Foreign"){
#       points(id_dim,0,pch=22,cex=6,bg="white",col="black")
#       text(id_dim,0,labels=emoji("earth_africa"), cex=1.75, col="blue", family='EmojiOne')
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Prize"){
#       col_current_race_prize <- "grey"
#       if(!is.na(df_details_race_current$Prix)){
#         col_current_race_prize <- vec_races_prize_color[as.character(df_details_race_current$Prix)]
#         points(id_dim,0,pch=22,cex=6,bg=col_current_race_prize,col="white")
#         text(id_dim,0,labels="\u20AC",cex=1.6)
#       }
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Number"){
#       col_current_number_races_values <- "grey"
#       if(!is.na(df_details_race_current$Size)){
#         col_current_number_races_values <- vec_num_races_opponents_color[as.character(df_details_race_current$Size)]
#         points(id_dim,0,pch=22,cex=6,bg=col_current_number_races_values,col="white")
#         text(id_dim,0,labels=emoji("horse_racing"), cex=1.75, col="green", family='EmojiOne')
#         text(id_dim,0,labels=emoji("horse_racing"), cex=1.75, col="black", family='EmojiOne')
#       }
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Horse"){
#       col_current_opponent_values <- "grey"
#       if(!is.na(df_details_race_current$OPPONENTS_VALUE)){
#         col_current_opponent_values <- vec_opponents_value_color[as.character(df_details_race_current$OPPONENTS_VALUE)]
#         points(id_dim,0,pch=22,cex=6,bg=col_current_opponent_values,col="white")
#         text(id_dim,0,labels=emoji("racehorse"), cex=1.75, col="black", family='EmojiOne')
#       }
#     }
#     
#     if(vec_lab_xaxis[id_dim] %in% c("TV","GNT")){
#       points(id_dim,0,pch=22,cex=6,bg=col_current_opponent_values,col="white")
#       text(id_dim,0,labels=emoji("trophy"), cex=1.75, col="black", family='EmojiOne')
#     }
#     
#     if(vec_lab_xaxis[id_dim] == "Drive"){
#       col_current_pilot_values <- "grey"
#       if(!is.na(df_details_race_current$PILOT_VALUE)){
#         col_current_pilot_values <- vec_pilot_value_color[as.character(df_details_race_current$PILOT_VALUE)]
#         points(id_dim,0,pch=22,cex=6,bg=col_current_pilot_values,col="white")
#         text(id_dim,0,labels=emoji("mens"), cex=1.75, col="black", family='EmojiOne')
#       }
#     }
#   }
#   
#   vec_freq_horse_trainer_current_horse <- table(df_infos_target_race_geny$Entraineur)
#   
#   vec_val_ecurie <- NULL
#   if(max(vec_freq_horse_trainer_current_horse)>1){
#     vec_val_ecurie <- "Ecurie"
#   }
#   
#   
#   vec_lab_xaxis <- NULL
#   for(idx in names(vec_max_items_track))
#   {
#     val_max_item <- as.vector(vec_max_items_track[idx])
#     vec_lab_xaxis_idx <- paste(unlist(strsplit(names(vec_max_items_track[idx]),"-"))[2],paste("0",rev(seq(1,val_max_item)),sep=""),sep="-")
#     vec_lab_xaxis <- c(vec_lab_xaxis,vec_lab_xaxis_idx)
#   }
#   
#   vec_lab_yaxis <- df_infos_pronostics_current_race$ID
#   
#   par(mar=c(5,9,1,3))
#   
#   plot(0,0,xlim=c(0,length(vec_lab_xaxis)+1),ylim=c(0,length(df_infos_target_race_geny$Cheval)),xaxt="n",yaxt="n",type="n",xlab="",ylab="")
#   vec_margin_plot <- par("usr")
#   axis(1, 1:((length(vec_lab_xaxis))),labels=FALSE,tick = FALSE)
#   text(1:((length(vec_lab_xaxis))), par("usr")[3] - 0.2, labels = vec_lab_xaxis, pos = 1, xpd = TRUE,offset = 1.9,cex=0.6,srt=90)
#   par(las=1)
#   axis(2, 1:length(df_infos_pronostics_current_race$ID), labels = df_infos_pronostics_current_race$ID,cex.axis=0.65,srt = 45)
#   
#   vec_col_tracks <- brewer.pal(9,"Blues")[5:9]
#   
#   for(idx in names(vec_max_items_track))
#   {
#     idx_item_track_current <- range(grep(unlist(strsplit(idx,"-"))[2],vec_lab_xaxis))
#     rect(idx_item_track_current[1],vec_margin_plot[3],idx_item_track_current[2],-0.2,col=vec_col_tracks[grep(idx,names(vec_max_items_track))],border=NA)
#     x_half_track <- idx_item_track_current[1] +(((idx_item_track_current[2]) - (idx_item_track_current[1]))/2)
#     y_half_track <- vec_margin_plot[3] +(-0.2-vec_margin_plot[3])/2
#     text(x_half_track,y_half_track,toupper(unlist(strsplit(idx,"-"))[1]),cex=0.75,col="white")
#   }
#   
#   for(id_horse in 1:nrow(df_infos_pronostics_current_race)){
#     df_infos_pronostics_current_race_current <-  df_infos_pronostics_current_race [id_horse,]
#     df_stats_scores_tracks_horse <- NULL
#     if(df_infos_pronostics_current_race_current$Cheval %in% df_stats_scores_tracks$Cheval){
#       df_stats_scores_tracks_horse <- df_stats_scores_tracks [df_stats_scores_tracks$Cheval==df_infos_pronostics_current_race_current$Cheval,]
#       for(id_dim in 1:length(vec_lab_xaxis)){
#         current_distance_track <- unlist(strsplit(vec_lab_xaxis[id_dim],"-"))[1]
#         if(current_distance_track %in% df_stats_scores_tracks_horse$Distance){
#           df_stats_scores_tracks_current_distance_horse <- df_stats_scores_tracks_horse[df_stats_scores_tracks_horse$Distance == current_distance_track, ]
#           df_stats_scores_tracks_current_distance_horse <- df_stats_scores_tracks_current_distance_horse[order(df_stats_scores_tracks_current_distance_horse$Date,decreasing = TRUE),]
#           current_index_track <- as.numeric(unlist(strsplit(vec_lab_xaxis[id_dim],"-"))[2])
#           val_date_track_current_start <- df_stats_scores_tracks_current_distance_horse[current_index_track,"Date"]
#           val_date_track_current_start <- as.numeric(unique(df_infos_target_race_geny$Date)-as.Date(val_date_track_current_start))
#           val_score_track_current_start_2000_1500 <- df_stats_scores_tracks_current_distance_horse[current_index_track,"ACCELERATION_START_2000_1500"]
#           val_score_track_current_start_1500_1000 <- df_stats_scores_tracks_current_distance_horse[current_index_track,"ACCELERATION_START_1500_1000"]
#           val_score_track_current_start_1000_500 <- df_stats_scores_tracks_current_distance_horse[current_index_track,"ACCELERATION_START_1000_500"]
#           val_score_track_current_last_1000_500 <- df_stats_scores_tracks_current_distance_horse[current_index_track,"ACCELERATION_LAST_1000_500"]
#           val_score_track_current_overall <- df_stats_scores_tracks_current_distance_horse[current_index_track,"SPEED_OVERALL"]
#           colors_acceleration_tracking <- list_colors_acceleration_tracking[[paste(current_location,current_distance_track,sep="-")]]
#           col_score_track_current_start_2000_1500  <- colors_acceleration_tracking$SPEED_START_2000_1500[as.character(val_score_track_current_start_2000_1500)]
#           col_score_track_current_start_1500_1000  <- colors_acceleration_tracking$SPEED_START_1500_1000[as.character(val_score_track_current_start_1500_1000)]
#           col_score_track_current_start_1000_500  <- colors_acceleration_tracking$SPEED_START_1000_500 [as.character(val_score_track_current_start_1000_500)]
#           col_score_track_current_last_1000_500  <- colors_acceleration_tracking$SPEED_LAST_1000_500[as.character(val_score_track_current_last_1000_500)]
#           col_score_track_current_overall  <- colors_acceleration_tracking$SPEED_OVERALL[as.character(val_score_track_current_overall)]
#           points(id_dim,id_horse,cex=5,col=col_score_track_current_start_2000_1500,bg="white",pch=22,lwd=1.5)
#           points(id_dim,id_horse,cex=4,col=col_score_track_current_start_1500_1000,bg="white",pch=22,lwd=1.5)
#           points(id_dim,id_horse,cex=3,col=col_score_track_current_start_1000_500,bg="white",pch=22,lwd=1.5)
#           points(id_dim,id_horse,cex=2.5,col=col_score_track_current_start_2000_1500,bg=col_score_track_current_last_1000_500,pch=22,lwd=1.5)
#           text(id_dim,id_horse,val_date_track_current_start,cex=0.5)
#         }
#       }
#     }
#   }
#   
#   return(df_stats_scores_tracks)
# }