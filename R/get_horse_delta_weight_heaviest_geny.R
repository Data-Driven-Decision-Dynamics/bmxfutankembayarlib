get_horse_delta_weight_heaviest_geny <- function(df_infos_target_race_geny = NULL,
                                                 path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                 number_days_back = 366){
  
  df_highest_weight_close_level <- NULL
  
  if (unique(df_infos_target_race_geny$Discipline) != "Trot Attelé") {
    message("Extract race date and convert in the right format if need")
    if (class(unique(df_infos_target_race_geny$Date)) != "Date") {
      current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
    } else {
      current_race_date <- unique(df_infos_target_race_geny$Date)
    }
    
    message("Extract name of current candidates")
    current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))

    message("Extract discipline of current race")
    current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
    
    message("Extract prize of current race")
    current_race_prize <- unique(df_infos_target_race_geny$Prix)

    message("Reading historical race infos file")
    if (!exists("df_historical_races_geny")) {
      if (file.exists(path_df_historical_races_geny)) {
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    }

   df_highest_weight_close_level <- df_historical_races_geny %>%
      filter(Discipline == current_race_discipline) %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(Date < current_race_date) %>%
      filter(current_race_date - Date <= number_days_back) %>%
      select(Cheval,Distance,Speed,Poids,Gains,Date,Prix,Gains) %>%
      filter(Gains>0) %>%
      filter(Prix >= (current_race_prize*0.75)) %>%
      drop_na() %>%
      group_by(Cheval) %>%
      top_n(1,Poids) %>%
      select(Cheval,Poids,Distance,Gains) %>%
      as.data.frame()
      
   if(nrow(df_highest_weight_close_level)>0){
     df_infos_target_race_geny_focus <- df_infos_target_race_geny[,c("Cheval","Poids","Distance")]
     colnames(df_infos_target_race_geny_focus) <- sub("Poids","Weight",colnames(df_infos_target_race_geny_focus))
     colnames(df_infos_target_race_geny_focus) <- sub("Distance","Length",colnames(df_infos_target_race_geny_focus))
     df_highest_weight_close_level <- merge(df_highest_weight_close_level,df_infos_target_race_geny_focus,by.x="Cheval",by.y="Cheval",all.y=TRUE)
     df_highest_weight_close_level$DeltaWeight <- df_highest_weight_close_level$Poids - df_highest_weight_close_level$Weight
     df_highest_weight_close_level$DeltaLength <- df_highest_weight_close_level$Distance - df_highest_weight_close_level$Length
   }
   
  }
   
  return(df_highest_weight_close_level)
}
  