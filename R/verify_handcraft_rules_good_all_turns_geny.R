verify_handcraft_rules_good_all_turns_geny <- function(df_infos_target_race_geny = NULL,
                                                         path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                         path_cluster_distance_range = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/mat_ranges_cluster_distance.rds",
                                                         number_days_back = 720){
  
  df_candidates_good_all_turns <- data.frame(Cheval = df_infos_target_race_geny$Cheval, CLASS_GOOD_ALL_TURNS = 0) 
  
  message("Start reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Finish reading historical performance file")
  
  message("Start extracting historical performance of competitors")
  df_performance_focus <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    filter(Discipline == unique(df_infos_target_race_geny$Discipline) ) %>%
    filter(Date <= unique(df_infos_target_race_geny$Date) ) %>%
    filter(unique(df_infos_target_race_geny$Date)-Date <= number_days_back)  %>%
    select(Cheval,Corde,Place) %>%
    filter(Place <= 3) %>%
    unique() %>%
    as.data.frame()
  if(nrow(df_performance_focus)>0){
    if(!is.null(df_performance_focus)){
      df_turn_place <- df_performance_focus %>%
        select(Cheval,Corde,Place) %>%
        pivot_wider(
          names_from = Corde,
          values_from = Place,
          values_fn = length
        ) %>%
        as.data.frame()
      if(nrow(df_turn_place)>0){
        if(ncol(df_turn_place)>=3){
          if(sum(is.na(df_turn_place))>0){
            df_turn_place[is.na(df_turn_place)] <- 0
          }
          test_good_profile <- apply(df_turn_place[,-1],1,function(x){sum(x>=3)})
          if(max(test_good_profile,na.rm = TRUE)>=2){
            df_candidates_good_all_turns[df_candidates_good_all_turns$Cheval %in% df_turn_place$Cheval[which(test_good_profile >= 2)],"CLASS_GOOD_ALL_TURNS"] <- 1
          }
        }
      }
      
    }
  }
  
  return(df_candidates_good_all_turns)
}
