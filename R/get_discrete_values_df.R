get_discrete_values_df <- function(mat = NULL, id_col_rm = 1) {
  
  vec_all_class_values <- unlist(mat[,-c(id_col_rm)])
  vec_all_class_values <- vec_all_class_values[!is.na(vec_all_class_values)]
  names(vec_all_class_values) <- NULL
  
  vec_all_class_values_recode <- rep("Moderate",sum(!is.na(vec_all_class_values)))
  
  cut_try <- tryCatch(gtools::quantcut(vec_all_class_values,3),error = function(e) {print(paste("some issues cutting", "errorr"));NULL})
  
  if(!is.null(cut_try)) {
    vec_all_class_values_recode <- gtools::quantcut(vec_all_class_values,3)
    vec_levels_cut_class_values<- levels(vec_all_class_values_recode)
    vec_labels_cut_class_values <- c("Low","Moderate","High")
    vec_all_class_values_recode <- as.vector(vec_all_class_values_recode)
    for(level in 1:length(vec_levels_cut_class_values))
    {
      vec_all_class_values_recode[vec_all_class_values_recode==vec_levels_cut_class_values[level]] <- vec_labels_cut_class_values[level]
    }
  }
  
  names(vec_all_class_values_recode) <- vec_all_class_values
  
  for(col in colnames(mat)[-c(id_col_rm)])
  {
    mat[,col] <- vec_all_class_values_recode[as.character(mat[,col])]
  }
  
  mat[is.na(mat)] <- "NEVER"
  
  return(mat)
}