#' @return Compute percent on pairwise wins when driver associated with candidate
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_signal_driver_geny(df_horse_race = NULL)
#' @export
get_signal_driver_geny <- function(df_horse_race = NULL,
                                   df_infos_target_race_geny = NULL,
                                   path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                   vec_minimal_columns = c("Cheval","Date","Lieu","Ferrage","Corde","Terrain","Discipline","Driver","Jockey","Entraineur","Gains","Place"),
                                   number_days_back=366){
  
  message("Initialization of output")
  df_stats_current_team <- NULL
  df_stats_others_team <- NULL
  df_stats_driver <- NULL
  df_stats_current_others_team <- NULL
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }

  message("Reading historical race infos file")
  if(!is.null(df_horse_race)){
    if(!exists("df_historical_races_geny")) {
      if(file.exists(path_df_historical_races_geny)){
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    }
  }
    
  if(length(intersect(vec_minimal_columns,colnames(df_horse_race)))>= length(vec_minimal_columns)-1){
    df_horse_race <- df_horse_race[,intersect(colnames(df_horse_race),vec_minimal_columns)]
  }
  
  current_race_horses <- df_infos_target_race_geny$Cheval
  
  if(class(unique(df_infos_target_race_geny$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race_geny$Date)
  }
  
  current_horse_date<-  as.Date(df_horse_race$Date)
  current_horse_discipline <-  df_horse_race$Discipline
  current_horse_name <-  df_horse_race$Cheval
  
  if(current_horse_discipline %in% c("Plat","Trot Monté","Steeplechase","Cross","Haies")){
    current_horse_driver <-  df_horse_race$Jockey
  }
  
  if(current_horse_discipline == "Trot Attelé"){
    current_horse_driver <-  df_horse_race$Driver
  }
  
  current_horse_trainer <-  df_horse_race$Entraineur
  current_horse_location <-  df_horse_race$Lieu

  if(current_horse_discipline == "Trot Attelé"){
    df_perf_race_target_horses <- df_historical_races_geny[df_historical_races_geny$Discipline == current_horse_discipline & df_historical_races_geny$Driver %in% current_horse_driver & df_historical_races_geny$Date<current_race_date,]
    df_perf_race_target_horses <- unique(df_perf_race_target_horses[,vec_minimal_columns])
    if(nrow(df_perf_race_target_horses)>0){
      df_stats_driver <- df_perf_race_target_horses %>%
        filter(Cheval %in% setdiff(unique(df_perf_race_target_horses$Cheval),current_horse_name)) %>%
        filter(current_horse_date-Date<=number_days_back) %>%
        group_by(Driver) %>%
        dplyr::summarise(CURRENT_DRIVER_NUMBER_RACES=n(),CURRENT_DRIVER_NUMBER_PLACE=get_number_podium(Place),CURRENT_DRIVER_PERCENT_SUCCESS=round((CURRENT_DRIVER_NUMBER_PLACE/CURRENT_DRIVER_NUMBER_RACES)*1,2)) %>%
        as.data.frame()
    }
  }
  
  if(current_horse_discipline %in% c("Plat","Trot Monté","Steeplechase","Cross","Haies")){
    df_perf_race_target_horses <- df_historical_races_geny[df_historical_races_geny$Discipline == current_horse_discipline & df_historical_races_geny$Jockey %in% current_horse_driver & df_historical_races_geny$Date<current_race_date,]
    df_perf_race_target_horses <- unique(df_perf_race_target_horses[,vec_minimal_columns])
    if(nrow(df_perf_race_target_horses)>0){
      df_stats_driver <- df_perf_race_target_horses %>%
        filter(Cheval %in% setdiff(unique(df_perf_race_target_horses$Cheval),current_horse_name)) %>%
        filter(current_horse_date-Date<=number_days_back) %>%
        group_by(Jockey) %>%
        dplyr::summarise(CURRENT_DRIVER_NUMBER_RACES=n(),CURRENT_DRIVER_NUMBER_PLACE=get_number_podium(Place),CURRENT_DRIVER_PERCENT_SUCCESS=round((CURRENT_DRIVER_NUMBER_PLACE/CURRENT_DRIVER_NUMBER_RACES)*1,2)) %>%
        as.data.frame()
    }
  }

  if(!is.null(df_stats_driver)){
    if(nrow(df_stats_driver)==0){
      df_stats_driver <- NULL
    }
  }

  if(current_horse_discipline == "Trot Attelé"){
    if(!is.null(df_stats_driver)){
      if(nrow(df_stats_driver)>0){
        df_stats_driver <- merge(df_stats_driver, df_infos_target_race_geny[,c("Cheval","Driver")],by.x="Driver",by.y="Driver")
      }
    }
  }
  
  if(current_horse_discipline %in% c("Plat","Trot Monté","Steeplechase","Cross","Haies")){
    if(!is.null(df_stats_driver)){
      if(nrow(df_stats_driver)>0){
        df_stats_driver <- merge(df_stats_driver, df_infos_target_race_geny[,c("Cheval","Jockey")],by.x="Jockey",by.y="Jockey")
      }
    }
  }
  
  if(current_horse_discipline == "Trot Attelé"){
    df_association_driver_horse <- df_perf_race_target_horses %>%
      filter(Cheval == current_horse_name) %>%
      filter(Driver == current_horse_driver) %>%
      filter(Discipline==current_horse_discipline) %>%
      filter(Date<current_horse_date) %>%
      filter(current_horse_date-Date<=number_days_back) %>%
      select(Cheval,Driver,Gains, Place,Discipline,Lieu,Date) %>%
      filter(Driver != "Non-partant") %>%
      distinct()%>%
      as.data.frame()
  }
  
  if(current_horse_discipline %in% c("Plat","Trot Monté","Steeplechase","Cross","Haies")){
    df_association_driver_horse <- df_perf_race_target_horses %>%
      filter(Cheval == current_horse_name) %>%
      filter(Jockey == current_horse_driver) %>%
      filter(Discipline==current_horse_discipline) %>%
      filter(Date<current_horse_date) %>%
      filter(current_horse_date-Date<=number_days_back) %>%
      select(Cheval,Jockey,Gains, Place,Discipline,Lieu,Date) %>%
      filter(Jockey != "Non-partant") %>%
      distinct()%>%
      as.data.frame()
  }
  
  if(nrow(df_association_driver_horse)>0){
    vec_columns_keep <- colnames(df_association_driver_horse) [apply(df_association_driver_horse,2,function(x) {sum(!is.na(x))})>0]
    df_association_driver_horse <- df_association_driver_horse[,vec_columns_keep] %>%
      distinct() %>%
      as.data.frame()
    
    df_stats_current_team <- df_association_driver_horse %>%
      group_by(Cheval) %>%
      dplyr::summarise(NUMBER_RACE_CURRENT_TEAM = n(),
                       MEAN_EARNING_CURRENT_TEAM = mean(Gains)) %>%
      as.data.frame
  }
  
  if(current_horse_discipline == "Trot Attelé"){
    df_perf_race_target_horses <- df_historical_races_geny[df_historical_races_geny$Cheval %in% current_horse_name  & df_historical_races_geny$Discipline == current_horse_discipline & df_historical_races_geny$Date<current_race_date,]
    df_perf_race_target_horses <- unique(df_perf_race_target_horses[,vec_minimal_columns])
    if(nrow(df_perf_race_target_horses)>0){
      df_association_others_driver_horse <- df_perf_race_target_horses %>%
        filter(Driver %in% setdiff(unique(df_perf_race_target_horses$Driver),current_horse_driver)) %>%
        filter(Discipline==current_horse_discipline) %>%
        filter(Date<current_horse_date) %>%
        filter(current_horse_date-Date<=number_days_back) %>%
        select(Cheval,Driver,Gains, Place,Discipline,Lieu,Date) %>%
        distinct()%>%
        as.data.frame()
      if(nrow(df_association_others_driver_horse)>0){
        df_association_others_driver_horse <- df_association_others_driver_horse[,intersect(colnames(df_association_others_driver_horse),vec_minimal_columns)]
        vec_columns_keep <- colnames(df_association_others_driver_horse) [apply(df_association_others_driver_horse,2,function(x) {sum(!is.na(x))})>0]
        df_association_others_driver_horse <- df_association_others_driver_horse[,vec_columns_keep] %>%
          distinct() %>%
          as.data.frame()
        df_stats_others_team <- df_association_others_driver_horse %>%
          group_by(Driver) %>%
          dplyr::summarise(NUMBER_RACE_OTHERS_TEAM = n(),
                           MEAN_EARNING_OTHERS_TEAM = mean(Gains)) %>%
          as.data.frame
        if(nrow(df_stats_others_team)>0){
          df_stats_others_team$Cheval <- current_horse_name
          df_stats_others_team <- df_stats_others_team %>%
            group_by(Cheval) %>%
            dplyr::summarise(NUMBER_RACE_OTHERS_TEAM = mean(NUMBER_RACE_OTHERS_TEAM),
                             MEAN_EARNING_OTHERS_TEAM = mean(MEAN_EARNING_OTHERS_TEAM)) %>%
            as.data.frame()
        }
      }
    }
  }
  
  if(current_horse_discipline %in% c("Plat","Trot Monté","Steeplechase","Cross","Haies")){
    df_perf_race_target_horses <- df_historical_races_geny[df_historical_races_geny$Cheval %in% current_horse_name  & df_historical_races_geny$Discipline == current_horse_discipline & df_historical_races_geny$Date<current_race_date,]
    df_perf_race_target_horses <- unique(df_perf_race_target_horses[,vec_minimal_columns])
    if(nrow(df_perf_race_target_horses)>0){
      df_association_others_driver_horse <- df_perf_race_target_horses %>%
        filter(Jockey %in% setdiff(unique(df_perf_race_target_horses$Jockey),current_horse_driver)) %>%
        filter(Discipline==current_horse_discipline) %>%
        filter(Date<current_horse_date) %>%
        filter(current_horse_date-Date<=number_days_back) %>%
        select(Cheval,Jockey,Gains, Place,Discipline,Lieu,Date) %>%
        distinct()%>%
        as.data.frame()
      if(nrow(df_association_others_driver_horse)>0){
        df_association_others_driver_horse <- df_association_others_driver_horse[,intersect(colnames(df_association_others_driver_horse),vec_minimal_columns)]
        vec_columns_keep <- colnames(df_association_others_driver_horse) [apply(df_association_others_driver_horse,2,function(x) {sum(!is.na(x))})>0]
        df_association_others_driver_horse <- df_association_others_driver_horse[,vec_columns_keep] %>%
          distinct() %>%
          as.data.frame()
        df_stats_others_team <- df_association_others_driver_horse %>%
          group_by(Jockey) %>%
          dplyr::summarise(NUMBER_RACE_OTHERS_TEAM = n(),
                           MEAN_EARNING_OTHERS_TEAM = mean(Gains)) %>%
          as.data.frame
        if(nrow(df_stats_others_team)>0){
          df_stats_others_team$Cheval <- current_horse_name
          df_stats_others_team <- df_stats_others_team %>%
            group_by(Cheval) %>%
            dplyr::summarise(NUMBER_RACE_OTHERS_TEAM = mean(NUMBER_RACE_OTHERS_TEAM),
                             MEAN_EARNING_OTHERS_TEAM = mean(MEAN_EARNING_OTHERS_TEAM)) %>%
            as.data.frame()
        }
      }
    }
  }

  list_stats_current_others_team <- list(Driver = df_stats_driver, Current = df_stats_current_team, Others = df_stats_others_team)    
  list_stats_current_others_team <- list_stats_current_others_team[lapply(list_stats_current_others_team,length)>0]
    
  if(length(list_stats_current_others_team)>0) {
    df_stats_current_others_team <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_stats_current_others_team)
  }    

  if(!is.null(df_stats_current_others_team)){
    df_stats_current_others_team <- unique(df_stats_current_others_team)
  }
  
  return(df_stats_current_others_team)
  
}


