#' @return Compute percent on pairwise wins when driver associated with candidate
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_standardized_best_performances_geny(df_infos_target_race_geny = NULL)
#' @export
get_horse_stats_record_geny <- function(df_infos_target_race_geny = NULL,
                                        path_records_perfs_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_records_all_tracks_geny.rds"){
  
  message("Initialization of final output")
  df_mean_stats_records <- NULL
  message("Initialization of final output")
  
  message("Start reading all record performance data")
  if(!exists("df_records_geny")){
    df_records_geny <- readRDS(path_records_perfs_geny)
    if(class(df_records_geny$Date)!="Date"){
      df_records_geny$Date <- as.Date(df_records_geny$Date)
    }
  }
  message("Finish reading all record performance data")
  
  message("Loading track complexity file")
  df_ranking_tracks <- readRDS(path_df_ranking_tracks)
  message("Loading track complexity file")

  message("Start extracting stats for best speed for competitors")
  df_records_focus <- df_records_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
    filter(Date < unique(df_infos_target_race_geny$Date)) %>%
    group_by(Cheval)%>%
    top_n(3,Speed) %>%
    select(Cheval,Speed,Date,Id)%>%
    as.data.frame()
  if(nrow(df_records_focus)>0){
    df_records_focus$Delay <- as.numeric(unique(df_infos_target_race_geny$Date)-df_records_focus$Date)
    df_mean_stats_records <- df_records_focus %>%
      mutate(Speed = get_percentile_values(Speed)) %>%
      group_by(Cheval) %>%
      dplyr::summarise(FORM_MEAN_SCORE_TOP_RECORD = max(Speed,na.rm=TRUE),
                       FORM_MEAN_SCORE_TOP_RECORD = round(FORM_MEAN_SCORE_TOP_RECORD,2),
                       FORM_MEAN_DELAY_TOP_RECORD = min(Delay,na.rm=TRUE),
                       FORM_MEAN_DELAY_TOP_RECORD = round(FORM_MEAN_DELAY_TOP_RECORD,2)) %>%
      as.data.frame()
  }
  message("Start extracting stats for best speed for competitors")
 
  return(df_mean_stats_records)
}