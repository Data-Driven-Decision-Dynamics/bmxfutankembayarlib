get_horse_stats_international_races_geny <- function(df_infos_target_race_geny = NULL,
                                                     path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                     number_days_back=366){
  
  df_numbers_international_races <- NULL
  df_earnings_international_races <- NULL 
  df_stats_international_races <- NULL
  
  df_historical_races_geny <- readRDS(path_df_historical_races_geny)

  message("Extract race date and convert in the right format if need")
  current_race_date  <- as.Date(unique(df_infos_target_race_geny$Date))
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))

  df_numbers_international_races <- df_historical_races_geny %>%
    dplyr::filter(Cheval %in% current_race_horses) %>%
    filter(Date<current_race_date) %>%
    select(Cheval,Date,INTERNATIONAL,EUROPEAN,Gains) %>%
    distinct() %>%
    mutate(SOMME = ifelse(INTERNATIONAL + EUROPEAN >0,1,0)) %>%
    group_by(Cheval) %>%
    dplyr::summarise(TOTAL = n(), EXPERIENCE_INTERNATIONAL = sum(SOMME)) %>%
    mutate(PERCENTAGE_INTERNATIONAL = round((EXPERIENCE_INTERNATIONAL/TOTAL),2)) %>%
    select(Cheval,EXPERIENCE_INTERNATIONAL,PERCENTAGE_INTERNATIONAL) %>%
    as.data.frame()
  
  if(nrow(df_numbers_international_races)>0){
    if(max(df_numbers_international_races$PERCENTAGE_INTERNATIONAL,na.rm=TRUE)==0){
      df_numbers_international_races <- NULL
    }
  }

  df_earnings_international_races <- df_historical_races_geny %>%
    dplyr::filter(Cheval %in% current_race_horses) %>%
    filter(Date<current_race_date) %>%
    filter(current_race_date-Date<=number_days_back) %>%
    select(Cheval,Date,INTERNATIONAL,EUROPEAN,Gains,Prix) %>%
    mutate(SOMME = ifelse(INTERNATIONAL + EUROPEAN >0,1,0)) %>%
    filter(SOMME == 1) %>%
    filter(Gains>0) %>%
    group_by(Cheval) %>%
    dplyr::summarise(MAX_EARNING_INTERNATIONAL = max(Gains), MEAN_EARNING_INTERNATIONAL = mean(Gains), MEAN_PRIZE_INTERNATIONAL = mean(Prix)) %>%
    as.data.frame()
  
  if(nrow(df_earnings_international_races) == 0){
    df_earnings_international_races <- NULL
  }

  list_stats_international_races <- list(Numbers = df_numbers_international_races, Scores = df_earnings_international_races )
  list_stats_international_races <- list_stats_international_races[lapply(list_stats_international_races,length)>0]
  
  if(length(list_stats_international_races)>0) {
    df_stats_international_races <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_stats_international_races)
  }
  
  if(length(list_stats_international_races)==0){
    df_stats_international_races <- NULL
  }
  
  if(!is.null(df_stats_international_races)){
    
    if(length(intersect("MAX_EARNING_INTERNATIONAL",colnames(df_stats_international_races)))==0){
      df_stats_international_races$MAX_EARNING_INTERNATIONAL <- 0
    }
    
    if(length(intersect("MEAN_EARNING_INTERNATIONAL",colnames(df_stats_international_races)))==0){
      df_stats_international_races$MEAN_EARNING_INTERNATIONAL <- 0
    }
    
    if(length(intersect("MEAN_PRIZE_INTERNATIONAL",colnames(df_stats_international_races)))==0){
      df_stats_international_races$MEAN_PRIZE_INTERNATIONAL <- 0
    }
    
    if(nrow(df_stats_international_races)<nrow(df_infos_target_race_geny)){
      df_stats_international_races <- merge(df_stats_international_races,df_infos_target_race_geny, by.x="Cheval",by.y="Cheval",all.y=TRUE)
    }
  }

  return(df_stats_international_races)
}