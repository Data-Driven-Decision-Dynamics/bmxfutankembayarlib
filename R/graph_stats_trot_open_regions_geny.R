graph_stats_trot_open_regions_geny <- function (target_race = "1ère-course--Prix-de-Mauriac",
                                                target_date = NULL,
                                                path_compiled_results = "/mnt/Master-Data/Futanke-Mbayar/France/report/geny",
                                                path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                path_daily_infos = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/raw/daily_performances",
                                                path_output_dashboard = "/mnt/Master-Data/Futanke-Mbayar/France/report/geny",
                                                path_infos_dash_open_regions = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/infos-open-regions-dashboard.xlsx",
                                                path_df_participation_open_regions_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_participation_outcome_open_regions_geny.rds",
                                                path_df_participation_criterium_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_participation_outcome_criterium_geny.rds"){

  
  require(magrittr)
  require(dplyr)
  require(plyr)
  require(bmxFutankeMbayar)
  require(RColorBrewer)
  require(xlsx)
  
  message("Start reading details on criterium historical outcome")
  df_experience_criterium <- readRDS(path_df_participation_criterium_geny)
  message("Finish reading details on criterium historical outcome")
  
  message("Start reading details on open regions challenge")
  df_experience_open_regions <- readRDS(path_df_participation_open_regions_geny)
  message("Start reading details on open regions challenge")
  
  df_experience_open_regions_top_horses <- df_experience_open_regions %>%
    dplyr::group_by(Region,Age) %>%
    top_n(100,SCORE_HORSE) %>%
    as.data.frame()
  
  df_experience_open_regions_top_horses <- merge(df_experience_open_regions_top_horses[,c("Cheval","Region","SCORE_HORSE")],df_experience_criterium[,c("Cheval","Age","Place","Gains")],by.x="Cheval",by.y="Cheval",all.y=TRUE)
  df_experience_open_regions_top_horses <- df_experience_open_regions_top_horses[complete.cases(df_experience_open_regions_top_horses),]
  df_stats_podium_open_region_criterium <- df_experience_open_regions_top_horses %>%
    dplyr::group_by(Region,Age) %>%
    dplyr::summarise(SIZE = n(),
           NUMBER_SUCCESS = get_number_top_five(Place),
           PERCENT_SUCCESS = NUMBER_SUCCESS/SIZE) %>%
    as.data.frame()
  
  df_stats_podium_open_region_criterium$Region <- gsub("Haute-Normandie/Ile-de-France","Ile-de-France",df_stats_podium_open_region_criterium$Region)
    
  message("Start reading details on open region challenges calendar")
  df_infos_dash_open_regions <- xlsx::read.xlsx(path_infos_dash_open_regions,sheetIndex = 1)
  message("Start reading details on open region challenges calendar")
  
  if(is.null(target_date)){
    target_date <- Sys.Date()
  }
  
  vec_details_reunions <- NULL
  if(dir.exists(paste(path_compiled_results,as.character(target_date),sep="/"))){
    vec_details_reunions <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),pattern = ".xlsx")
    vec_details_reunions <- setdiff(vec_details_reunions,c("Infos.xlsx","Bettable.xlsx"))
    path_file_infos <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),pattern = "Infos.xlsx")
    if(length(path_file_infos)==1){
      df_infos_races <- xlsx::read.xlsx(paste(paste(path_compiled_results,as.character(target_date),sep="/"),path_file_infos,sep="/"),sheetIndex = 1) %>%
        as.data.frame()
    }
  }
  
  if(length(vec_details_reunions)==1){
    df_details_races <- xlsx::read.xlsx(paste(paste(path_compiled_results,as.character(target_date),sep="/"),vec_details_reunions,sep="/"),sheetIndex = 1) %>%
      as.data.frame()
    df_details_races <- unique(df_details_races)
    df_details_races <- df_details_races[order(df_details_races$Heure,decreasing = FALSE),]
    df_details_races$Course <- gsub(" ","-",df_details_races$Race)
    df_details_races$Course <- gsub("'","",df_details_races$Course)
    df_details_races$Course <- gsub("\\(","",df_details_races$Course)
    df_details_races$Course <- gsub(")","",df_details_races$Course)
    df_details_races$Course <- gsub("--Genybet-","-Genybet",df_details_races$Course)
  }
  
  df_details_race_current <- df_details_races [df_details_races$Course == gsub("--Genybet-","-Genybet",target_race), ]
  number_race_current_item <- substr(unlist(strsplit(target_race,'-'))[1],1,1)
  path_current_reunion <- paste(path_compiled_results,target_date,df_details_race_current$Location,sep="/")
  path_current_reunion_race <- paste(path_compiled_results,target_date,df_details_race_current$Location,target_race,sep="/")
  path_file_pronostics_current_race <- dir(path_current_reunion_race,pattern = ".xlsx")
  path_file_pronostics_current_race <- unique(setdiff(path_file_pronostics_current_race,c("Trophee-Vert.xlsx","Grand-National-Trot.xlsx","Open-Regions.xlsx","Criterium.xlsx","df_infos_tracking.rds","Profile.xlsx","Trot.xlsx","Galop.xlsx","Ward.xlsx","Draw.xlsx","Compare.xlsx","Remontada.xlsx","AutoStart.xlsx","Auto-Start.xlsx","Aptitude.xlsx","Gore.xlsx","RTF.xlsx","Benchmark.xlsx","Advantage.xlsx","Sensitivity.xlsx","Limit.xlsx","Consecutive.xlsx","Consolidated.xlsx","Polygon.xlsx","Tankunem.xlsx","Doley.xlsx","Khaliss.xlsx","Trainers.xlsx","Horses.xlsx","Drivers.xlsx","Associations.xlsx","Year.xlsx","GNT.xlsx","Cadre.xlsx","Orderer.xlsx","Config.xlsx","International.xlsx","Placing.xlsx","Toughest.xlsx","Going.xlsx","Non-Partants.xlsx","Disqualified.xlsx","Epreuve.xlsx","Beaten.xlsx","Niveau.xlsx","Level.xlsx","Participation.xlsx","Yenn.xlsx","Handicap.xlsx","Weight.xlsx","Travel.xlsx","Infos.xlsx","Tandems.xlsx","Ecuries.xlsx","Participation.xlsx","Earning.xlsx","Ranking.xlsx","Records.xlsx","Track.xlsx","Issues.xlsx","Intention.xlsx","Spell.xlsx","Pairs.xlsx","Ferrage.xlsx","Success.xlsx","Engagement.xlsx","Fitness.xlsx","Odds.xlsx")))
  if(length(path_file_pronostics_current_race)==1){
    df_infos_pronostics_current_race <- xlsx::read.xlsx(paste(path_current_reunion_race,path_file_pronostics_current_race,sep="/"),sheetIndex = 1) %>%
      as.data.frame
    
    if(sum(is.na(df_infos_pronostics_current_race$Class))>0){
      df_infos_pronostics_current_race$Class[is.na(df_infos_pronostics_current_race$Class)] <- mean(df_infos_pronostics_current_race$Class[!is.na(df_infos_pronostics_current_race$Class)])
    }
    
    if(sum(is.na(df_infos_pronostics_current_race$Aptitude))>0){
      df_infos_pronostics_current_race$Aptitude[is.na(df_infos_pronostics_current_race$Aptitude)] <- mean(df_infos_pronostics_current_race$Class[!is.na(df_infos_pronostics_current_race$Aptitude)])
    }
    
    if(sum(is.na(df_infos_pronostics_current_race$Fitness))>0){
      df_infos_pronostics_current_race$Fitness[is.na(df_infos_pronostics_current_race$Fitness)] <- mean(df_infos_pronostics_current_race$Class[!is.na(df_infos_pronostics_current_race$Fitness)])
    }
    
    if(sum(is.na(df_infos_pronostics_current_race$Intention))>0){
      df_infos_pronostics_current_race$Intention[is.na(df_infos_pronostics_current_race$Intention)] <- mean(df_infos_pronostics_current_race$Class[!is.na(df_infos_pronostics_current_race$Intention)])
    }
    
    if(sum(is.na(df_infos_pronostics_current_race$Final))>0){
      df_infos_pronostics_current_race$Final <- df_infos_pronostics_current_race$Class + df_infos_pronostics_current_race$Aptitude + df_infos_pronostics_current_race$Fitness + df_infos_pronostics_current_race$Intention
    }
    
    df_infos_pronostics_current_race <- merge(df_infos_pronostics_current_race,df_infos_races[,intersect(c("Cheval","Numero","Gender","Age","Discipline","Poids"),colnames(df_infos_races))],by.x="Cheval",by.y="Cheval",all.x=TRUE)
    df_infos_pronostics_current_race$ID <- paste(df_infos_pronostics_current_race$Numero,df_infos_pronostics_current_race$Cheval,sep="-")
    df_infos_pronostics_current_race$Rank <- apply(apply(df_infos_pronostics_current_race[,c("Final"),drop=FALSE],2,rank),1,function(x){mean(x,na.rm=TRUE)})
    df_infos_pronostics_current_race <- df_infos_pronostics_current_race[order(df_infos_pronostics_current_race$Rank,decreasing = TRUE),]
    df_infos_pronostics_current_race$Pivot <- paste("P",max(df_details_races[df_details_races$Location==df_details_race_current$Location,]$Number):((max(df_details_races[df_details_races$Location==df_details_race_current$Location,]$Number)-nrow(df_infos_pronostics_current_race))+1),sep="")
    df_infos_pronostics_current_race$Numero <- as.numeric(df_infos_pronostics_current_race$Numero)
    df_infos_target_race_geny <- df_infos_races [df_infos_races$Cheval %in% df_infos_pronostics_current_race$Cheval,]
  }

  message("Start reading open regions statistics data")
  vec_file_open_regions_current_race <- dir(path_current_reunion,recursive = TRUE,full.names = TRUE,pattern="Open-Regions.xlsx")
  if(length(vec_file_open_regions_current_race)>0){
    file_open_regions <- grep(target_race,vec_file_open_regions_current_race,value=TRUE)
    if(length(file_open_regions)==1){
      df_open_regions <- xlsx::read.xlsx(file_open_regions,sheetIndex = 1) %>%
        as.data.frame()
      df_open_regions$ID <- paste(df_open_regions$Region,df_open_regions$Age,sep="_")
      df_open_regions$ID <- gsub("Haute-Normandie/Ile-de-France","Ile-de-France",df_open_regions$ID)
      df_open_regions$Region <- gsub("Haute-Normandie/Ile-de-France","Ile-de-France",df_open_regions$Region)
    }
  }
  message("Start reading open regions statistics data")
    
  df_open_regions <- merge(df_open_regions,df_infos_pronostics_current_race[,c("Cheval","ID")],by.x="Cheval",by.y="Cheval",all.y=TRUE)
  if(length(intersect(colnames(df_open_regions),"ID.x"))>0){
    colnames(df_open_regions)[colnames(df_open_regions)=="ID.x"] = "ID"
    df_open_regions <- df_open_regions[,setdiff(colnames(df_open_regions),"ID.y")]
  }
  
  df_open_regions_format <- df_open_regions[,c("ID","Cheval","SCORE_HORSE")] %>%
    tidyr::pivot_wider(
      names_from = ID,
      values_from = SCORE_HORSE,
      values_fn = mean
    ) %>%
    as.data.frame()
  
  df_size_open_regions_format <- df_open_regions[,c("ID","Cheval","SIZE_RACE")] %>%
    tidyr::pivot_wider(
      names_from = ID,
      values_from = SIZE_RACE,
      values_fn = mean
    ) %>%
    as.data.frame()
  
  df_open_regions_format = df_open_regions_format[,c("Cheval",sort(grep("_3",colnames(df_open_regions_format),value=TRUE)),sort(grep("_4",colnames(df_open_regions_format),value=TRUE)),sort(grep("_5",colnames(df_open_regions_format),value=TRUE)))]
  df_open_regions_format = df_open_regions_format[,c("Cheval",setdiff(sort(colnames(df_open_regions_format)),"Cheval"))]
  rownames(df_open_regions_format) <- df_open_regions_format$Cheval
  df_open_regions_format <- df_open_regions_format[intersect(df_infos_target_race_geny$Cheval,rownames(df_open_regions_format)),]
  
  vec_open_trot_3_years <- grep("_3",colnames(df_open_regions_format),value=TRUE)
  vec_open_trot_4_years <- grep("_4",colnames(df_open_regions_format),value=TRUE)
  vec_open_trot_5_years <- grep("_5",colnames(df_open_regions_format),value=TRUE)
  
  vec_items_open_trot <- unique(unlist(lapply(colnames(df_open_regions_format)[-1],function(x){unlist(strsplit(x,"_"))[1]})))
  vec_age_open_trot <- unique(unlist(lapply(colnames(df_open_regions_format)[-1],function(x){unlist(strsplit(x,"_"))[2]})))
  
  vec_lab_xaxis <- colnames(df_open_regions_format)[-1]
  vec_lab_yaxis <- rev(df_infos_pronostics_current_race$ID)
  
  # message("Start defining the layout for the dashboard")
  # layout(as.matrix(t(data.frame(A=1:length(vec_items_open_trot),B=1:length(vec_items_open_trot)))),heights = rep(c(1,5),1))
  # message("Finish defining the layout for the dashboard")
  
  message("Start defining the layout for the dashboard")
  layout(matrix(c(1:(2*1)), 2, (1), byrow = FALSE),heights = rep(c(1,5),1))
  message("Finish defining the layout for the dashboard")
  
  message("Start defining layout for the first plot")
  par(mar=c(1,8,2,2))
  message("Finish defining layout for the first plot")
  
  vec_margin_plot <- par("usr")
  
  vec_values_stats_podium_open_regions_values <- unlist(df_stats_podium_open_region_criterium[,"PERCENT_SUCCESS"])
  vec_values_stats_podium_open_regions_values <- unique(vec_values_stats_podium_open_regions_values[!is.na(vec_values_stats_podium_open_regions_values)])
  vec_values_stats_podium_open_regions_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_values_stats_podium_open_regions_values))[rank(vec_values_stats_podium_open_regions_values)]
  names(vec_values_stats_podium_open_regions_color) <- vec_values_stats_podium_open_regions_values
  
  plot(0,0,xlim=c(0,25),ylim=c(-1,1),xaxt="n",yaxt="n",type="n",xlab="",ylab="", main="Percentage Of Sucess In Criterium Per Region Of Origin")
  for(idx_region in 1:length(vec_items_open_trot))
  {
    for(idx in 1:nrow(df_infos_dash_open_regions ))
    {
      start_rec <- df_infos_dash_open_regions[idx,"Start"]
      end_rec   <- df_infos_dash_open_regions[idx,"End"]
      x_half_class <- (start_rec) +(((end_rec) - (start_rec))/2)
      y_half_class <- vec_margin_plot[3] +(-0.1-vec_margin_plot[3])/2
      y_half_class <- vec_margin_plot[3]-0.1
      # rect(start_rec,vec_margin_plot[3],end_rec,-0.1,col=brewer.pal(8,"Set2")[idx],border=NA)
      text(x_half_class,-0.75,vec_items_open_trot[idx],cex=1,col=brewer.pal(8,"Set2")[idx])
    }
  }
  
  for(idx in 1:nrow(df_infos_dash_open_regions)){
    start_rec <- df_infos_dash_open_regions[idx,"Start"]
    end_rec   <- df_infos_dash_open_regions[idx,"End"]
    for(current_age in c("3","4","5"))
    {
      col_stats_podium_open_regions <- "grey"
      if(sum(df_stats_podium_open_region_criterium$Region == vec_items_open_trot[idx] & df_stats_podium_open_region_criterium$Age == current_age)>0){
        value_stats_podium_open_regions <- df_stats_podium_open_region_criterium[df_stats_podium_open_region_criterium$Region == vec_items_open_trot[idx] & df_stats_podium_open_region_criterium$Age == current_age,"PERCENT_SUCCESS"]
        col_stats_podium_open_regions <- vec_values_stats_podium_open_regions_color[as.character(value_stats_podium_open_regions)]
      }
      points(seq(start_rec,end_rec)[grep(current_age,c("3","4","5"))],0,cex=4,col=col_stats_podium_open_regions,bg="white",pch=22)
      points(seq(start_rec,end_rec)[grep(current_age,c("3","4","5"))],0,cex=2,bg=col_stats_podium_open_regions,col=col_stats_podium_open_regions,pch=21)
    }
  }
  
  message("Start defining layout for the first plot")
  par(mar=c(1,8,2,2))
  message("Finish defining layout for the first plot")
  
  plot(0,0,xlim=c(0,25),ylim=c(0,length(vec_lab_yaxis)+1),xaxt="n",yaxt="n",type="n",xlab="",ylab="",main="Ranking Profile LeTROT Open Regions Circuit")
  vec_margin_plot <- par("usr")
  axis(1, 1:((length(vec_lab_xaxis))),labels=FALSE,tick = FALSE)
  par(las=1)
  axis(2, 1:length(vec_lab_yaxis), labels =vec_lab_yaxis,cex.axis=0.65,srt = 45)
  for(idx_region in 1:length(vec_items_open_trot))
  {
    for(idx in 1:nrow(df_infos_dash_open_regions ))
    {
      start_rec <- df_infos_dash_open_regions[idx,"Start"]
      end_rec   <- df_infos_dash_open_regions[idx,"End"]
      x_half_class <- (start_rec) +(((end_rec) - (start_rec))/2)
      y_half_class <- vec_margin_plot[3]- (-0.2+(vec_margin_plot[3]/2))
      # rect(start_rec,vec_margin_plot[3],end_rec,-0.2,col=brewer.pal(8,"Set2")[idx],border=NA)
      text(rep(seq(start_rec,end_rec)),rep(-0.5,3),3:5,cex=0.75,col=brewer.pal(8,"Set2")[idx])
    }
    vec_values_scoring_open_regions_values <- unlist(df_open_regions_format[,-1])
    vec_values_scoring_open_regions_values <- unique(vec_values_scoring_open_regions_values[!is.na(vec_values_scoring_open_regions_values)])
    vec_values_scoring_open_regions_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_values_scoring_open_regions_values))[rank(vec_values_scoring_open_regions_values)]
    names(vec_values_scoring_open_regions_color) <- vec_values_scoring_open_regions_values
    
    vec_size_values_scoring_open_regions_values <- unlist(df_size_open_regions_format [,-1])
    vec_size_values_scoring_open_regions_values <- unique(vec_size_values_scoring_open_regions_values[!is.na(vec_size_values_scoring_open_regions_values)])
    vec_size_scoring_open_regions_color <- colorRampPalette(rev(brewer.pal(9,"RdYlBu")))(length(vec_size_values_scoring_open_regions_values))[rank(vec_size_values_scoring_open_regions_values)]
    names(vec_size_scoring_open_regions_color) <- vec_size_values_scoring_open_regions_values
    
    for(idx in 1:nrow(df_infos_dash_open_regions)){
      start_rec <- df_infos_dash_open_regions[idx,"Start"]
      end_rec   <- df_infos_dash_open_regions[idx,"End"]
      for(current_age in c("3","4","5"))
      {
        col_size_scoring_poen_regions_horse <- "white"
        col_scoring_open_regions_horse <- "white"
        candidate_open_trot_column <- paste(vec_items_open_trot[idx],current_age,sep="_")
        for(id_horse in vec_lab_yaxis)
        {
          if(candidate_open_trot_column %in% colnames(df_open_regions_format)){
            val_size_scoring_poen_regions_horse <- df_size_open_regions_format[df_size_open_regions_format$Cheval == unlist(strsplit(id_horse,"-"))[2] ,candidate_open_trot_column]
            col_size_scoring_poen_regions_horse <- vec_size_scoring_open_regions_color[as.character(val_size_scoring_poen_regions_horse)]
            val_scoring_poen_regions_horse <- df_open_regions_format[df_open_regions_format$Cheval == unlist(strsplit(id_horse,"-"))[2] ,candidate_open_trot_column]
            col_scoring_open_regions_horse <- vec_values_scoring_open_regions_color[as.character(val_scoring_poen_regions_horse)]
          }
          points(seq(start_rec,end_rec)[grep(current_age,c("3","4","5"))],grep(id_horse,vec_lab_yaxis),cex=4,col=col_size_scoring_poen_regions_horse,bg="white",pch=22)
          points(seq(start_rec,end_rec)[grep(current_age,c("3","4","5"))],grep(id_horse,vec_lab_yaxis),cex=2,bg=col_scoring_open_regions_horse,col=col_scoring_open_regions_horse,pch=22)
        }
      }
    }
  }
  return(NULL)
}



