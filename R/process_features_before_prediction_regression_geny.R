process_features_before_prediction_regression_geny <- function(df_features_target = NULL,
                                                               path_input_features_desc  = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/Master-Handcrafted-Feature-Description-Futanke-Mbayar-geny.xlsx") {
  
  df_features_infos <- read.xlsx(path_input_features_desc,sheetIndex = 1)
  df_features_infos <- as.data.frame(df_features_infos)
  df_features_infos$Feature <- toupper(df_features_infos$Feature)
  df_features_infos <- df_features_infos[complete.cases(df_features_infos[,c("Group","Role","Class")]),]
  
  if("Class" %in% colnames(df_features_target)){
    df_features_target$Class <- as.numeric(as.vector(df_features_target$Class))
  }
  
  
  # A CORRIGER JE PENSE DANS MAT INFOS FEATURE
  
  df_features_target <- df_features_target[,-grep("GENOT",colnames(df_features_target),ignore.case = TRUE)]
  
  if(class(df_features_target$Class)== "numeric"){
    df_features_target$IDENTIFIER <- paste(df_features_target$CHEVAL,df_features_target$RACE,rownames(df_features_target),sep="_")
    rownames(df_features_target)  <- df_features_target$IDENTIFIER 
    df_features_target$Class <- as.numeric(as.vector(df_features_target$Class))
    if(sum(is.infinite(as.matrix(df_features_target[,-ncol(df_features_target)])))>0){
      df_features_target[,-ncol(df_features_target)][is.infinite(as.matrix(df_features_target[,-ncol(df_features_target)]))] <- NA
    }
    for(feature in colnames(df_features_target))
    {
      df_features_infos <- df_features_infos[!is.na(df_features_infos$Class),]
      if(feature %in% df_features_infos$Feature){
        class_feat <- as.vector(df_features_infos[df_features_infos$Feature==feature,"Class"])
        if(class_feat=="Binary"){
          df_features_target[,feature] <- as.vector(as.factor(df_features_target[,feature]))
          if(sum(is.na(df_features_target[,feature]))>0){
            df_features_target[is.na(df_features_target[,feature]),feature] <- 0
          }
        }
        if(class_feat=="Numeric"){
          if(sum(is.na(df_features_target[,feature]))>0){
            df_features_target[is.na(df_features_target[,feature]),feature] <- mean(df_features_target[!is.na(df_features_target[,feature]),feature])
          }
        }
      }
    }
  }
  return(df_features_target)
}

