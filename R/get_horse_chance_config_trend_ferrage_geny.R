get_horse_chance_config_trend_ferrage_geny <- function(df_infos_target_race_geny = NULL,
                                                       path_df_issues_trend_ferrage_epreuve = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_issues_trend_ferrage_epreuve.rds",
                                                       path_df_issues_trend_ferrage_group = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_issues_trend_ferrage_group.rds"){
  df_stats_issues_trend_ferrage_global_focus <- NULL
  df_stats_issues_trend_ferrage_epreuve_focus <- NULL
  df_stats_issues_trend_ferrage <- NULL
  
  df_infos_stats_chance_config_trend_ferrage <- get_horse_trends_ferrage_geny(df_infos_target_race_geny)
  if(!is.null(path_df_issues_trend_ferrage_group)){
    df_stats_issues_trend_ferrage_global <- readRDS(path_df_issues_trend_ferrage_group)
    df_stats_issues_trend_ferrage_global <- df_stats_issues_trend_ferrage_global[df_stats_issues_trend_ferrage_global$Group %in% df_infos_stats_chance_config_trend_ferrage$Group,]
    df_stats_issues_trend_ferrage_global_focus <- merge(df_infos_stats_chance_config_trend_ferrage[,c("Cheval","Group")],df_stats_issues_trend_ferrage_global,by.x='Group',by.y="Group",all.x=TRUE)
    df_stats_issues_trend_ferrage_global_focus <- df_stats_issues_trend_ferrage_global_focus[,c("Cheval","Percent")]
    colnames(df_stats_issues_trend_ferrage_global_focus) <- c("Cheval","INTENTION_CHANCE_SUCCESS_TREND_FERRAGE_GLOBAL")
  }
  
  if(!is.null(path_df_issues_trend_ferrage_epreuve)){
    df_stats_issues_trend_ferrage_epreuve <- readRDS(path_df_issues_trend_ferrage_epreuve)
    df_stats_issues_trend_ferrage_epreuve <- df_stats_issues_trend_ferrage_epreuve[df_stats_issues_trend_ferrage_epreuve$Epreuve %in% df_infos_stats_chance_config_trend_ferrage$Epreuve,]
    df_stats_issues_trend_ferrage_epreuve_focus <- merge(df_infos_stats_chance_config_trend_ferrage[,c("Cheval","Epreuve")],df_stats_issues_trend_ferrage_epreuve,by.x='Epreuve',by.y="Epreuve",all.x=TRUE)
    df_stats_issues_trend_ferrage_epreuve_focus <- df_stats_issues_trend_ferrage_epreuve_focus[,c("Cheval","Percent")]
    colnames(df_stats_issues_trend_ferrage_epreuve_focus) <- c("Cheval","INTENTION_CHANCE_SUCCESS_TREND_FERRAGE_EPREUVE")
  }
  
  list_stats_issues_trend_ferrage <- list(Global = df_stats_issues_trend_ferrage_global_focus, Epreuve = df_stats_issues_trend_ferrage_epreuve_focus , All = df_infos_target_race_geny[,"Cheval",drop=FALSE] )
  list_stats_issues_trend_ferrage <- list_stats_issues_trend_ferrage[lapply(list_stats_issues_trend_ferrage,length)>0]
  if(length(list_stats_issues_trend_ferrage)>0){
    df_stats_issues_trend_ferrage <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_stats_issues_trend_ferrage)
  }
    
  return(df_stats_issues_trend_ferrage)
}
