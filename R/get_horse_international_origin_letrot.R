get_horse_international_origin_letrot <- function(df_infos_target_race = NULL,df_historical_races_competitors = NULL){
  
  df_international_origin <- NULL
  message("Extract race date and convert in the right format if need")
  current_race_date  <- as.Date(unique(df_infos_target_race$Date))
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))
  
  message("Computing global statistics on current timeframe")
  df_qualification_date <- df_historical_races_competitors %>%
    dplyr::filter(Cheval %in% current_race_horses) %>%
    filter(Date<current_race_date) %>%
    filter( Course == "QUALIFICATION") %>%
    select(Cheval,Date) %>%
    as.data.frame()
  
  if(nrow(df_qualification_date)>0){
    if(nrow(df_qualification_date)<nrow(df_infos_target_race)){
      df_international_origin <- merge(df_qualification_date,df_infos_target_race[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
      df_international_origin$Foreign <- 0
      if(sum(is.na(df_international_origin$Date))>0){
        df_international_origin$Foreign[is.na(df_international_origin$Date)] <- 1
        df_international_origin  <- df_international_origin[,c("Cheval","Foreign")]
      }
    }
  }
  
  return(df_international_origin)
}