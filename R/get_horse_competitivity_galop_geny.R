#' @return Compute engagement indicator for each candidate of a given race
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_competitivity_profile_geny(df_infos_target_race_geny = NULL)
#' @export
get_horse_competitivity_galop_geny <- function (df_infos_target_race_geny = NULL, number_days_back = 366){
  
  message("Initialization of the final output")
  df_issues_competitivity <- NULL
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  message("Extract race date and convert in the right format if need")
  if(class(unique(df_infos_target_race_geny$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race_geny$Date)
  }
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
  
  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
  
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
  
  message("Extract terrain of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
  
  if(current_race_discipline!="Trot Attelé"){
    df_issues_competitivity <- df_historical_races_geny %>%
      filter(Discipline == current_race_discipline) %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(Date<current_race_date) %>%
      filter(current_race_date-Date<=number_days_back) %>%
      dplyr::select(Cheval,Date,Discipline,Place,Gains,Corde,Terrain,Prix,Distance,Poids,Speed) %>%
      distinct() %>%
      drop_na() %>%
      as.data.frame()
    
    if(nrow(df_issues_competitivity)>0){
      df_issues_competitivity$COMPET <- NA
      idx_top_03 <- df_issues_competitivity$Place <4
      if(sum(idx_top_03,na.rm=TRUE)>0){
        df_issues_competitivity$COMPET [idx_top_03] <- "01"
      }
      idx_top_06 <- df_issues_competitivity$Place >= 4 & df_issues_competitivity$Place <6
      if(sum(idx_top_06,na.rm=TRUE)>0){
        df_issues_competitivity$COMPET [idx_top_06] <- "02"
      }
      idx_top_09 <- df_issues_competitivity$Place >= 6 & df_issues_competitivity$Place <9
      if(sum(idx_top_09,na.rm=TRUE)>0){
        df_issues_competitivity$COMPET [idx_top_09] <- "03"
      }
      idx_top_20 <- df_issues_competitivity$Place >= 9
      if(sum(idx_top_20,na.rm=TRUE)>0){
        df_issues_competitivity$COMPET [idx_top_20] <- "04"
      }
      
      df_infos_target_add <- df_infos_target_race_geny[,c("Cheval","Date","Discipline","Distance","Prix","Poids","Corde","Terrain")]
      df_infos_target_add$Gains <- NA
      df_infos_target_add$Place <- NA
      df_infos_target_add$COMPET <- NA
      df_infos_target_add$Speed <- NA
      df_issues_competitivity <- rbind(df_issues_competitivity,df_infos_target_add)
      df_infos_target_add_weight <- df_infos_target_add[,c("Cheval","Poids")]
      colnames(df_infos_target_add_weight)[2] <- "Weight"
      df_issues_competitivity <- merge(df_issues_competitivity,df_infos_target_add_weight,by.x="Cheval",by.y="Cheval",all.y=TRUE)
      df_issues_competitivity$DeltaWeight <- 100*((df_issues_competitivity$Poids-df_issues_competitivity$Weight)/df_issues_competitivity$Weight)
      df_issues_competitivity$DeltaWeight <- round(df_issues_competitivity$DeltaWeight,2)
      df_issues_competitivity$DeltaPrize <-  100*((df_issues_competitivity$Prix-unique(df_infos_target_race_geny$Prix))/unique(df_infos_target_race_geny$Prix))
      df_issues_competitivity$DeltaDistance <-  100*((df_issues_competitivity$Distance-min(df_infos_target_race_geny$Distance))/min(df_infos_target_race_geny$Distance))
      df_issues_competitivity$ShareEarning <- round(100*(df_issues_competitivity$Gains/unique(df_infos_target_race_geny$Prix)),2)
      idx_turn_gound_filled <- !is.na(df_issues_competitivity$Corde) & !is.na(df_issues_competitivity$Terrain)
      if(sum(idx_turn_gound_filled,na.rm = TRUE)>0){
        df_issues_competitivity <- df_issues_competitivity [idx_turn_gound_filled,]
        idx_config <- df_issues_competitivity$Corde == current_race_corde & df_issues_competitivity$Terrain ==current_race_terrain
        idx_corde <- df_issues_competitivity$Corde == current_race_corde & df_issues_competitivity$Terrain !=current_race_terrain
        idx_terrain <- df_issues_competitivity$Corde != current_race_corde & df_issues_competitivity$Terrain ==current_race_terrain
        idx_else <- df_issues_competitivity$Corde != current_race_corde & df_issues_competitivity$Terrain !=current_race_terrain
        df_issues_competitivity$Class = NA
        if(sum(idx_config,na.rm = TRUE)>0){
          df_issues_competitivity[idx_config,"Class"] <-"Config"
        }
        if(sum(idx_corde,na.rm = TRUE)>0){
          df_issues_competitivity[idx_corde,"Class"] <-"Turn"
        }
        if(sum(idx_else,na.rm = TRUE)>0){
          df_issues_competitivity[idx_else,"Class"] <-"Others"
        }
        if(sum(idx_terrain,na.rm = TRUE)>0){
          df_issues_competitivity[idx_terrain,"Class"] <-"Ground"
        }
      }
    }
    
    if(!is.null(df_issues_competitivity)){
      if(nrow(df_issues_competitivity)>0){
        df_issues_competitivity <- df_issues_competitivity[!is.na(df_issues_competitivity$Speed),]
      }
    }
  }

  return(df_issues_competitivity)
}
