get_horse_intern_europe_amat_reclamer_geny <- function(df_infos_target_race=NULL,
                                                       path_mat_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                       number_days_back=366){
  
  mat_percent_race_type <- NULL
  mat_percent_gains_race_type <- NULL
  mat_infos_race_type <- NULL
  
  message("Reading historical race infos file")
  if(!exists("mat_historical_races_geny")) {
    if(file.exists(path_mat_historical_races_geny)) {
      mat_historical_races_geny <- readRDS(path_mat_historical_races_geny)
    }
  } 
  
  message("Extract race date and convert in the right format if need")
  if(class(unique(df_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race$Date)
  }
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))

  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race$Discipline)
  
  message("Calculate percentage of earning per discipline")
  if(sum(current_race_horses %in% unique(mat_historical_races_geny$Cheval))>0) {
    mat_infos_historical_races_focus <- mat_historical_races_geny %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(Date<current_race_date) %>%
      filter(Discipline==current_race_discipline) %>%
      filter(current_race_date-Date<=number_days_back) %>%
      dplyr::select(Cheval,Details,Gains,Date) %>%
      as.data.frame()
    
    if(nrow(mat_infos_historical_races_focus)>0){
      mat_infos_historical_races_focus$INTERNATIONAL <- 0
      mat_infos_historical_races_focus$EUROPEAN <- 0
      mat_infos_historical_races_focus$AMATEUR <- 0
      mat_infos_historical_races_focus$RECLAMER <- 0
      
      id_international <- grep("internationale",mat_infos_historical_races_focus$Details,ignore.case = "TRUE")
      if(length(id_international)>0){
        mat_infos_historical_races_focus$INTERNATIONAL[id_international] <- 1
      }
      
      id_european <- grep("européenne",mat_infos_historical_races_focus$Details,ignore.case = "TRUE")
      if(length(id_european)>0){
        mat_infos_historical_races_focus$EUROPEAN[id_european] <- 1
      }
      
      id_amateur <- grep("amateurs",mat_infos_historical_races_focus$Details,ignore.case = "TRUE")
      if(length(id_amateur)>0){
        mat_infos_historical_races_focus$AMATEUR[id_amateur] <- 1
      }
      
      id_reclamer <- grep("a reclamer",mat_infos_historical_races_focus$Details,ignore.case = "TRUE")
      if(length(id_reclamer)>0){
        mat_infos_historical_races_focus$RECLAMER[id_reclamer] <- 1
      }

    mat_percent_race_type <- mat_infos_historical_races_focus%>%
      group_by(Cheval) %>%
      dplyr::summarise(Size=n(),
                       PERCENT_RACES_INTERNATIONAL=round(sum(INTERNATIONAL)/Size,2),
                       PERCENT_RACES_EUROPEAN=round(sum(EUROPEAN)/Size,2),
                       PERCENT_RACES_AMATEUR=round(sum(AMATEUR)/Size,2),
                       PERCENT_RACES_RECLAMER=round(sum(RECLAMER)/Size,2)) %>%
      select(-c(Size)) %>% 
            as.data.frame()
    
    mat_percent_gains_race_type <- mat_infos_historical_races_focus%>%
      group_by(Cheval) %>%
      dplyr::summarise(Total=sum(Gains),
                       PERCENT_EARNING_INTERNATIONAL=round(sum(INTERNATIONAL*Gains)/Total,2),
                       PERCENT_EARNING_EUROPEAN=round(sum(EUROPEAN*Gains)/Total,2),
                       PERCENT_EARNING_AMATEUR=round(sum(AMATEUR*Gains)/Total,2),
                       PERCENT_EARNING_RECLAMER=round(sum(RECLAMER*Gains)/Total,2)) %>%
      select(-c(Total)) %>%
      as.data.frame()
    
    if(sum(is.na(mat_percent_gains_race_type))>0){
      mat_percent_gains_race_type[is.na(mat_percent_gains_race_type)] <- 0
    }
    
  }
 
  if(!is.null(mat_percent_race_type) & !is.null(mat_percent_gains_race_type)){
    mat_infos_race_type <- merge(mat_percent_race_type,mat_percent_gains_race_type,by.x="Cheval",by.y="Cheval",all = TRUE)
  }
  
  if(!is.null(mat_percent_race_type) & is.null(mat_percent_gains_race_type)){
    mat_infos_race_type <- mat_percent_race_type
  }
  
  if(!is.null(mat_percent_gains_race_type) & is.null(mat_percent_race_type)){
    mat_infos_race_type <- mat_percent_gains_race_type
  }
  
  if(!is.null(mat_infos_race_type)){
    mat_infos_race_type <- merge(mat_infos_race_type,df_infos_target_race[,'Cheval',drop=FALSE],by.x="Cheval",by.y="Cheval",all = TRUE)
  }
}
  
  return(mat_infos_race_type)
}







# aa_intern = mat_historical_races_geny[mat_historical_races_geny$Date=="2020-03-01" & mat_historical_races_geny$Cheval=="BUNKER DU PRIEURE",]
# aa_europ = mat_historical_races_geny[mat_historical_races_geny$Date=="2018-05-10" & mat_historical_races_geny$Cheval=="ARIANGA MAGIC",]
# aa_amateur = mat_historical_races_geny[mat_historical_races_geny$Date=="2020-05-24" & mat_historical_races_geny$Cheval=="DEFI DREAM",]
# aa_amateur = mat_historical_races_geny[mat_historical_races_geny$Date=="2020-05-24" & mat_historical_races_geny$Cheval=="HORACE DES CHAMPS",]







