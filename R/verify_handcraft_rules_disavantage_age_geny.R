verify_handcraft_rules_disavantage_age_geny <- function(df_infos_target_race_geny = NULL,
                                                        path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                        number_days_back = 720){

  
  message("Start initializing final output")
  df_candidate_disavantage_age <- data.frame(Cheval = unique(df_infos_target_race_geny$Cheval), ENGAGEMENT_DISAVANTAGE_RUNS_TO_EARNING = 0)
  message("Finish initializing final output")

  message("Start reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Finish reading historical performance file")
  
  message("Start testing if competitors have already won on the same track")
  df_stats_number_races <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    filter(Discipline == unique(df_infos_target_race_geny$Discipline) ) %>%
    filter(Corde == unique(df_infos_target_race_geny$Corde) ) %>%
    filter(Date <= unique(df_infos_target_race_geny$Date) ) %>%
    select(Cheval,Gains) %>%
    unique() %>%
    group_by(Cheval) %>%
    dplyr::summarise(NUMBER_RACES = n(),
                     TOTAL_EARNING = sum( Gains),
                     MEAN_EARNING = mean(Gains)) %>%
    mutate(MEAN_EARNING= round(MEAN_EARNING,2)) %>%
    as.data.frame()
  
  if(nrow(df_stats_number_races)>0){
    for(idx_row in 1:nrow(df_stats_number_races)) {
      test_candidates_more_races_less_succesfull <- NULL
      test_candidates_more_total_earning_less_succesfull <- NULL
      if(sum(is.na(unlist(df_stats_number_races[idx_row,]))) == 0){
        vec_more_races <- which(df_stats_number_races[idx_row,"NUMBER_RACES"] >= df_stats_number_races[-idx_row,"NUMBER_RACES"])
        if(length(vec_more_races)>0){
          vec_competitors_less_races <- df_stats_number_races[-idx_row,"Cheval"][vec_more_races]
          test_candidates_less_races_more_succesfull <- df_stats_number_races[df_stats_number_races$Cheval %in% vec_competitors_less_races, "TOTAL_EARNING"] > df_stats_number_races[idx_row,"TOTAL_EARNING"] & df_stats_number_races[df_stats_number_races$Cheval %in% vec_competitors_less_races, "MEAN_EARNING"] > df_stats_number_races[idx_row,"MEAN_EARNING"]
          if(sum(test_candidates_less_races_more_succesfull,na.rm=TRUE) >= ceiling(nrow(df_stats_number_races)/2)){
            df_candidate_disavantage_age[df_candidate_disavantage_age$Cheval %in% df_stats_number_races[idx_row,"Cheval"], "ENGAGEMENT_DISAVANTAGE_RUNS_TO_EARNING"] <- -2
          }
        }

        vec_competitors_more_races_races <- which(df_stats_number_races[idx_row,"NUMBER_RACES"] < df_stats_number_races[-idx_row,"NUMBER_RACES"])
        if(length(vec_competitors_more_races_races)>0){
          vec_label_competitors_less_races <- df_stats_number_races[-idx_row,"Cheval"][vec_competitors_more_races_races]
          test_candidates_more_races_less_succesfull <- df_stats_number_races[df_stats_number_races$Cheval %in% vec_label_competitors_less_races, "TOTAL_EARNING"] < df_stats_number_races[idx_row,"TOTAL_EARNING"] & df_stats_number_races[df_stats_number_races$Cheval %in% vec_label_competitors_less_races, "MEAN_EARNING"] < df_stats_number_races[idx_row,"MEAN_EARNING"]
        }
        vec_competitors_more_total_earning <- which(df_stats_number_races[idx_row,"TOTAL_EARNING"] < df_stats_number_races[-idx_row,"TOTAL_EARNING"])
        if(length(vec_competitors_more_total_earning)>0){
          vec_label_competitors_more_total_earning <- df_stats_number_races[-idx_row,"Cheval"][vec_competitors_more_total_earning]
          test_candidates_more_total_earning_less_succesfull <- df_stats_number_races[df_stats_number_races$Cheval %in% vec_label_competitors_more_total_earning, "NUMBER_RACES"] >= df_stats_number_races[idx_row,"NUMBER_RACES"] & df_stats_number_races[df_stats_number_races$Cheval %in% vec_label_competitors_more_total_earning, "MEAN_EARNING"] < df_stats_number_races[idx_row,"MEAN_EARNING"]
        }
        if(sum(c(test_candidates_more_races_less_succesfull,test_candidates_more_total_earning_less_succesfull),na.rm=TRUE) >= (nrow(df_infos_target_race_geny)-4) ){
          df_candidate_disavantage_age[df_candidate_disavantage_age$Cheval %in% df_stats_number_races[idx_row,"Cheval"], "ENGAGEMENT_DISAVANTAGE_RUNS_TO_EARNING"] <- 2
        }
      }
    }
  }
  
  return(df_candidate_disavantage_age)
  
}