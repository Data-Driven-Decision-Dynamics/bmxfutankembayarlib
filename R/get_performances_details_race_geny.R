get_performances_details_race_geny <- function(url_path_race_results = "http://www.geny.com/arrivee-et-rapports-pmu/2023-04-27-pmu-prix-thierry-denuault_c1399689"){
  
  message("Rebuilding race url path")
  url_path_race_program <- sub('arrivee-et-rapports-pmu','partants-pmu',url_path_race_results)
  
  message("Getting the details on the race")
  mat_details_race <- get_details_race_geny(url_path_race_program)
  if(!is.null(mat_details_race)){
    if(nrow(mat_details_race)>0){
      for(current_column in colnames(mat_details_race)){
        if(class(mat_details_race[,current_column])=="factor"){
          mat_details_race[,current_column] <- as.vector(mat_details_race[,current_column])
        }
      }
    }
  }

  message("Getting the results of the race")
  mat_results_race <- get_results_race_geny(url_path_race_results)
  if(!is.null(mat_results_race)){
    if(nrow(mat_results_race)>0){
      for(current_column in colnames(mat_results_race)){
        if(class(mat_results_race[,current_column])=="factor"){
          mat_results_race[,current_column] <- as.vector(mat_results_race[,current_column])
        }
      }
    }
  }
  
  message("Identifying candidates for which we are deatils and results")
  vec_horses_common <- intersect(mat_details_race$Cheval,mat_results_race$Cheval)
  if(length(vec_horses_common)>0) {
    
    message("Merging details and results of the race")
    mat_infos_race <- merge(mat_details_race,mat_results_race,by.x="Cheval",by.y="Cheval",all=TRUE)
    if(unique(mat_details_race$Discipline) %in% c("Plat","Haies","Steeplechase","Cross")) {
      mat_infos_race <- get_reduction_based_ecart_geny(mat_infos_race)
    } else {
      mat_infos_race$Time <- unlist(lapply(mat_infos_race$Ecart,get_numeric_values_reduc_ecart_geny))
    }
    
    mat_infos_race <- get_amount_win_horse_given_race_geny(mat_perf_race_complete=mat_infos_race)
    mat_infos_race <- get_international_european_amateur_reclamer_geny(mat_infos_race)
  }
  
  mat_infos_race$Epreuve <- gsub("AATTELÉ","A",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("AMONTÉ", "A",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("BATTELÉ","B",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("BMONTÉ", "B",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("CATTELÉ","C",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("CMONTÉ", "C",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("DATTELÉ","D",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("DMONTÉ", "D",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("EATTELÉ","E",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("EMONTÉ", "E",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("FATTELÉ","F",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("FMONTÉ", "F",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("GATTELÉ","G",mat_infos_race$Epreuve)
  mat_infos_race$Epreuve <- gsub("GMONTÉ", "G",mat_infos_race$Epreuve)
  
  if(sum(mat_infos_race$Epreuve %in% c("CINQ","CLUB","GÉNY","GENYS","MOINS","PRINCIPALE"))>0){
    mat_infos_race$Epreuve[mat_infos_race$Epreuve %in% c("CINQ","CLUB","GÉNY","GENYS","MOINS","PRINCIPALE")] <- NA
  }
  
  if(sum(mat_infos_race$Epreuve==FALSE,na.rm = TRUE)>0){
    mat_infos_race$Epreuve[mat_infos_race$Epreuve=="FALSE"] <- "F"
  }

  mat_infos_race$Speed <- NA
  
  if(sum(!is.na(mat_infos_race$Time))>0){
    if(unique(mat_infos_race$Discipline) %in% c("Trot Attelé","Trot Monté")) {
      mat_infos_race[,"Time"]  <- mat_infos_race[,"Time"] * as.numeric(mat_infos_race[,"Distance"])/1000
    } else {
      mat_infos_race[,"Time"]  <- mat_infos_race[,"Time"]
    }
    mat_infos_race[,"Speed"] <- as.numeric(mat_infos_race[,"Distance"]) /mat_infos_race[,"Time"] 
  }
 
  if(!is.null(mat_infos_race)){
    if(nrow(mat_infos_race)>0){
      mat_infos_race$Race <- paste(mat_infos_race$Date,mat_infos_race$Heure,mat_infos_race$Course,sep="_")
    }
  }
  
  if(!is.null(mat_infos_race)){
    if(nrow(mat_infos_race)>0){
      if("Race" %in% colnames(mat_infos_race)){
        mat_infos_race$Race <- gsub("--Prix","-Prix",mat_infos_race$Race)
        mat_infos_race$Race <- gsub("course -","course-",mat_infos_race$Race)
      }
      if("Course" %in% colnames(mat_infos_race)){
        mat_infos_race$Course <- gsub("--Prix","-Prix",mat_infos_race$Course)
        mat_infos_race$Course <- gsub("course -","course-",mat_infos_race$Course)
      }
    }
  }

  return(mat_infos_race)
}
