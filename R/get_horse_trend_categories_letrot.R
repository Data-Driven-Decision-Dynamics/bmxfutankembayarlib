#' @return Computes the trend in terms of category between previous and current race
#' @importFrom magrittr %>%
#' @examples
#' get_average_speed_horse_letrot(mat_infos_target_race=NULL)
#' @export
get_horse_trend_categories_letrot <- function (mat_infos_target_race=NULL,path_mat_historical_races_letrot = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/02-processed/mat_historical_races_letrot.rds"){

  mat_trend_categories <- NULL
  
  if(class(unique(mat_infos_target_race$Date))!="Date") {
    race_date  <- as.Date(as.vector(unique(mat_infos_target_race$Date)))
  } else {
    race_date <- unique(mat_infos_target_race$Date)
  }
  
  # Retrieve race characteristics
  current_race_horses <- gsub("\n","",unique(mat_infos_target_race$Cheval))
  # Retrieve race category
  current_race_category <- gsub("\n","",unique(mat_infos_target_race$Epreuve))
  # Retrieve race distance
  current_race_distance <- as.numeric(gsub("\n","",unique(mat_infos_target_race$Distance)))
  
  # Reading historical race infos file
  if(!exists("mat_historical_races_letrot")) {
    if(file.exists(path_mat_historical_races_letrot)) {
      mat_historical_races_letrot <- readRDS(path_mat_historical_races_letrot)
    }
  } 
  
  mat_infos_last_races <- mat_historical_races_letrot %>%
    filter(Cheval %in% current_race_horses) %>%
    group_by(Cheval) %>%
    top_n(1,Date) %>%
    select(Cheval,Epreuve,Distance,Gains,Date) %>%
    as.data.frame()
  
  vec_ordered_categories <- c("I","II","III",LETTERS)
  
  for(horse in current_race_horses) {
    if(horse %in% mat_infos_last_races$Cheval) {
      val_category_last_race <- mat_infos_last_races[mat_infos_last_races$Cheval==horse,"Epreuve"]
      rank_last_category <- grep(val_category_last_race,vec_ordered_categories)
      rank_current_category <- grep(current_race_category,vec_ordered_categories)
      val_distance_last_race <- mat_infos_last_races[mat_infos_last_races$Cheval==horse,"Distance"]
      trend_distance <- val_distance_last_race-current_race_distance
      val_gains_last_race <- mat_infos_last_races[mat_infos_last_races$Cheval==horse,"Gains"]
      if(rank_last_category==rank_current_category) {
        trend_category <- 0 
      } else if (rank_last_category<rank_current_category) {
        trend_category <- 1 
      } else {
        trend_category <- -1 
        }
      }
    mat_trend_categories_horse <- data.frame(Cheval=horse,Trend=trend_category,Length=trend_distance,Gains=val_gains_last_race)
    mat_trend_categories <- rbind(mat_trend_categories_horse,mat_trend_categories)
  }
return(mat_trend_categories)
}

