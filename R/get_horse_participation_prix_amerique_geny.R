get_horse_participation_prix_amerique_geny <- function(regex_prix_amerique = "Grand Prix d'Amérique| Prix Amérique|Prix d'Amérique",
                                                       regex_anti_prix_amerique = "Prix du Bourbonnais |Prix de  Belgique | Prix de Bretagne|Prix de France|Prix de Paris | Qualif |Q1|Q2|Q3|Q4|Q5|Q6",
                                                       path_df_participation_prix_amerique_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_participation_outcome_prix_amerique_geny.rds",
                                                       path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds") {
  
  df_races_candidates_prix_amerique <- NULL
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Reading historical performance file")

  message("Extracting Previous TV races")
  id_items_prix_amerique <- grep(regex_prix_amerique,df_historical_races_geny$Course,ignore.case = TRUE)
  id_items_missing_prix_amerique <- grep("-Grand Prix d'Amérique",df_historical_races_geny$Course,ignore.case = TRUE)
  if(sum(id_items_prix_amerique,na.rm = TRUE)>0){
    df_races_candidates_prix_amerique <- df_historical_races_geny[id_items_prix_amerique,c("Date","Cheval","Age","Driver","Entraineur","Place","Gains","Course","Details","Prix","Lieu")]
    df_races_candidates_prix_amerique <- distinct(df_races_candidates_prix_amerique)
    id_items_anti_prix_amerique <- grep(regex_anti_prix_amerique,paste(df_races_candidates_prix_amerique$Course,df_races_candidates_prix_amerique$Details))
    df_races_candidates_prix_amerique <- df_races_candidates_prix_amerique[-id_items_anti_prix_amerique,]
    df_races_candidates_prix_amerique <- df_races_candidates_prix_amerique[df_races_candidates_prix_amerique$Lieu =="Vincennes" ,]
    
    df_races_missing_candidates_prix_amerique <- df_historical_races_geny[id_items_missing_prix_amerique,c("Date","Cheval","Age","Driver","Entraineur","Place","Gains","Course","Details","Prix","Lieu")]
    df_races_missing_candidates_prix_amerique <- distinct(df_races_missing_candidates_prix_amerique)
    
    df_races_candidates_prix_amerique <- rbind(df_races_candidates_prix_amerique,df_races_missing_candidates_prix_amerique)
    df_races_candidates_prix_amerique <- distinct(df_races_candidates_prix_amerique)

    df_races_candidates_prix_amerique <- distinct(df_races_candidates_prix_amerique)
    if(nrow(df_races_candidates_prix_amerique)>0){
      df_races_candidates_prix_amerique$Year <- lubridate::year(df_races_candidates_prix_amerique$Date)-1
    }
  }
  
  if(!is.null(df_races_candidates_prix_amerique)){
    df_races_candidates_prix_amerique$Item <- "Grand-Prix-Amerique"
    df_races_candidates_prix_amerique <- df_races_candidates_prix_amerique[,c("Date","Year","Cheval","Age","Place","Gains","Prix","Item")]
    saveRDS(df_races_candidates_prix_amerique,path_df_participation_prix_amerique_geny)
  }
    
  return(df_races_candidates_prix_amerique)
}





