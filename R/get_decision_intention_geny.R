get_decision_intention_geny <- function (df_score_intention = NULL){
  
  df_score_intention <- as.data.frame(df_score_intention)
  vec_candidates_intention_compiled <- NULL
  if(!is.null(df_score_intention)) {
    rownames(df_score_intention) <- df_score_intention$Cheval
    df_score_intention_binarize <- df_score_intention
    for(idx_col in 2:ncol(df_score_intention)){
      df_score_intention_binarize[,idx_col] <- ifelse(df_score_intention[,idx_col]-as.vector(quantile(df_score_intention[,idx_col],na.rm=TRUE)[3])>=0,1,0)
    }
    
    df_score_intention_binarize <- df_score_intention_binarize[,-1]
    if(sum(is.na(df_score_intention_binarize))>0){
      df_score_intention_binarize [is.na(df_score_intention_binarize)] <- 0
    }
    for(idx_col in 1:ncol(df_score_intention_binarize))
    {
      if(sum(df_score_intention_binarize[,idx_col]==1,na.rm=TRUE)>0){
        vec_candidates_quality_current <- rownames(df_score_intention_binarize[df_score_intention_binarize[,idx_col]==1,])
        if(length(vec_candidates_quality_current)>0){
          vec_candidates_intention_compiled <- c(vec_candidates_intention_compiled,vec_candidates_quality_current)
        }
      }
    }
  }
  
  vec_selected_intention <- names(table(vec_candidates_intention_compiled))[table(vec_candidates_intention_compiled)>=ceiling(ncol(df_score_intention_binarize)/2)]
  
  return(vec_selected_intention)
}
