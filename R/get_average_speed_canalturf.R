get_average_speed_canalturf <- function(mat_data=NULL) {
  
  message("Initialize the finla output")
  mat_speed <- NULL
  
  message("Estimate average speed while correctin for distance")
  if(!is.null(mat_data) & nrow(mat_data)>0) {
    mat_data <- mat_data[,c('Cheval','Distance','Speed','Time')]
    mat_data$SumTime <- sum(mat_data$Time,na.rm = TRUE)
    mat_data <- transform(mat_data,WeightTime=Time/SumTime) 
    mat_data <- transform(mat_data,TimeWeighted=WeightTime*Time) 
    mat_data$AverageSpeed <- round(sum(mat_data[,'Speed']*mat_data[,'TimeWeighted'],na.rm = TRUE)/sum(mat_data$TimeWeighted,na.rm = TRUE),2)
    mat_speed <- mat_data[1,c("Cheval","AverageSpeed")]
    colnames(mat_speed) <- c("Cheval", "Speed")
    rownames(mat_speed) <- NULL
  }

  return(mat_speed)
  
}

