get_horse_intention_geny <- function(df_infos_target_race = NULL) {
  
  df_intention <- NULL
  
  df_signal_horse <- NULL
  df_signal_driver <- NULL
  df_driver_stats <- NULL
  
  df_signal_horse <- get_horse_trend_weight_prize_geny(df_infos_target_race)
  df_signal_horse <- df_signal_horse[,intersect(c("Cheval","PERCENT_SUCCESS_FERRAGE","IMPROVEMENT_CURRENT_FERRAGE","PRIZE"),colnames(df_signal_horse)),drop=FALSE]
  
  
  df_signal_driver <- get_horse_signal_driver_geny(df_infos_target_race)
  if(!is.null(df_signal_driver)) {
    if(nrow(df_signal_driver)>0){
      df_signal_driver <- transform(df_signal_driver,IMPROVEMENT_DRIVER=CURRENT-PREVIOUS)
      colnames(df_signal_driver) <- sub("CURRENT","SUCCESS_DRIVER",colnames(df_signal_driver))
      if("PREVIOUS" %in% colnames(df_signal_driver)){
        df_signal_driver <- df_signal_driver[,setdiff(colnames(df_signal_driver),"PREVIOUS")]
      }
      if("IMPROVEMENT_DRIVER" %in% colnames(df_signal_driver)){
        if(sum(df_signal_driver$IMPROVEMENT_DRIVER==0,na.rm = TRUE)>0){
          df_signal_driver$IMPROVEMENT_DRIVER[df_signal_driver$IMPROVEMENT_DRIVER==0] <- NA
        }
      }
    }
  }

  df_driver_stats <- get_horse_driver_stats_geny(df_infos_target_race)
  vec_indicators_driver <- intersect(c("Cheval","PERCENT_RACES_LOCATION_DRIVER","PERCENT_PODIUM_LOCATION_DRIVER","ADVANTAGE_PODIUM_LOCATION_DRIVER"),colnames(df_driver_stats))
  if(length(vec_indicators_driver)>0){
    df_driver_stats <- df_driver_stats[,vec_indicators_driver] 
  }
  
  list_intention <- list(df_signal_horse=df_signal_horse,df_signal_driver=df_signal_driver,df_driver_stats=df_driver_stats)
  list_intention <- list_intention[lapply(list_intention,length)>0]
  
  if(length(list_intention)>0) {
    df_intention <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_intention)
    df_intention <- unique(df_intention)
  }
  
  return(df_intention)
}