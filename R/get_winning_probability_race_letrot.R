get_winning_probability_race_letrot <- function (mat_infos_target_race = NULL,
                                                 path_output_model = "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/models",
                                                 path_mat_historical_races_letrot = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/02-processed/mat_historical_races_letrot.rds",
                                                 path_mat_historical_line_by_line_letrot = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/02-processed/mat_line_by_line_rating_letrot.rds",
                                                 path_input_features_desc = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot//input/Master-Feature-Description-Futanke-Mbayar_letrot.xlsx") {

  message("Reading Feature Description File")
  mat_features_infos <- readxl::read_excel(path_input_features_desc,sheet = 1)
  mat_features_infos <- as.data.frame(mat_features_infos)
  mat_features_infos$Feature <- toupper(mat_features_infos$Feature)
  mat_features_infos <- mat_features_infos[complete.cases(mat_features_infos[,c("Group","Role","Class")]),]
  
  message("Initialize final output models with the probabilities of success")
  mat_prediction_current_race_combined = NULL
  mat_prediction_current_race_combined_dt = NULL
  
  if(nrow(mat_infos_target_race)>1) { 
    message("Initialize h2o service to perform predictions")
    h2o::h2o.init(ip = "localhost", port = 54321, nthreads= -1,max_mem_size = "16g")
    
    message("Basic recoding for the gender column")
    mat_infos_target_race$Gender <- gsub("FALSE","F",mat_infos_target_race$Gender)
    if(length(intersect("Recul",colnames(mat_infos_target_race)))==0) {
      mat_infos_target_race$Recul <- 0
      min_distance <- min(mat_infos_target_race$Distance)
      if(sum(mat_infos_target_race$Distance>min_distance)>0) {
        mat_infos_target_race[mat_infos_target_race$Distance>min_distance,"Recul"] <- 1
      }
    }
    
    message("Extract some basic characteristics of the race")
    current_race_horses <- gsub("\n","",unique(mat_infos_target_race$Cheval))
    current_race_name <- gsub("\n","",unique(mat_infos_target_race$Course))
    current_race_location <- as.vector(unique(mat_infos_target_race$Lieu))
    current_race_distance <- min(mat_infos_target_race$Distance)
    current_race_epreuve  <- as.vector(unique(mat_infos_target_race$Epreuve))
    current_race_date     <- as.Date(unique(mat_infos_target_race$Date))
    message("Create the caption label for the output table")
    table_caption_label <- paste(current_race_name,current_race_location,paste(current_race_distance,"m",sep=""), paste("Categorie"),current_race_epreuve,sep="-")
    message("Prepare some columns to add to the final output dataset")
    vec_context_columns <- c("Numero","Cheval","Age","Gender","Ferrage","Poids","Recul","Distance")
    vec_context_columns <- intersect(vec_context_columns,colnames(mat_infos_target_race))
    mat_infos_target_race_add <- mat_infos_target_race[,vec_context_columns]
    vec_col_add <- colnames(mat_infos_target_race_add)[base::apply(mat_infos_target_race_add,2,function(x){return(sum(!is.na(x)))})>0]
    mat_infos_target_race_add <- mat_infos_target_race_add[,vec_col_add]
    
    message("Create the potential model name and test if the model directory exists")
    hippo_discipline <- tolower(paste(unique(mat_infos_target_race$Lieu),sub("é","e",unique(mat_infos_target_race$Discipline)),sep="_"))
    hippo_discipline <- gsub(" ","-",hippo_discipline)
    vec_available_models_hippo_discipline <- grep(hippo_discipline,dir(path_output_model),value=TRUE)
    message("Initialization of the final matrix that will store the probability of success")
    list_prediction_current_race_combined <- vector("list",length(vec_available_models_hippo_discipline))
    names(list_prediction_current_race_combined) <- vec_available_models_hippo_discipline
    if(length(vec_available_models_hippo_discipline)>0) {
      message("Computing the features need to run predict the likelihood to be part of the winning candidates")
      mat_features_model_building <- get_processed_numerical_data_predictive_model_letrot(mat_infos_target_race,operating_mode="prediction")
      rownames(mat_features_model_building) <- as.vector(mat_features_model_building$Cheval)
      mat_features_model_building <- transform(mat_features_model_building,Model=paste(mat_features_model_building$HIPPODROME,mat_features_model_building$DISCIPLINE,mat_features_model_building$DISTANCE,sep="_"))
      colnames(mat_features_model_building) <- gsub("\\+","_",colnames(mat_features_model_building))
      colnames(mat_features_model_building) <- gsub("\\.","_",colnames(mat_features_model_building))
      colnames(mat_features_model_building) <- toupper(colnames(mat_features_model_building))
      
      if(!is.null(mat_features_model_building)) {
        message("Looping over the vailable models")
        for(current_model in vec_available_models_hippo_discipline) {
          message("Create the path to the directory of potential models")
          path_current_model <- dir(paste(path_output_model,current_model,sep="/"),full.names = TRUE)
          if(file.exists(path_current_model)) {
            model_current <- h2o.loadModel(path_current_model)
            vec_features_current_model <- model_current@parameters$x
            vec_missing_features_current_model <- setdiff(vec_features_current_model,colnames(mat_features_model_building))
            if(length(vec_missing_features_current_model)>0) {
              for(feature in vec_missing_features_current_model)
              {
                mat_features_model_building[,feature] <- 0
              }
            }
            
            if(length(grep("\\.",colnames(mat_features_model_building)))>0) {
              mat_features_model_building <- mat_features_model_building[,-grep("\\.",colnames(mat_features_model_building))]
            }
            
            for(i in colnames(mat_features_model_building))
            {
              print(class(mat_features_model_building[,i]))
              if(class(mat_features_model_building[,i])=="matrix"){
                mat_features_model_building[,i] <- unlist(as.data.frame(mat_features_model_building[,i]))
              }
            }
            
            mat_features_model_building_hex <- as.h2o(mat_features_model_building[,intersect(colnames(mat_features_model_building),vec_features_current_model)],  destination_frame  = "mat_features_model_building_hex")
            
            prediction_current_race <- as.data.frame(h2o.predict(model_current, mat_features_model_building_hex)[,2,drop=FALSE])
            colnames(prediction_current_race) <- "Chance"
            prediction_current_race <- as.data.frame(prediction_current_race)
            rownames(prediction_current_race) <- rownames(mat_features_model_building)
            prediction_current_race$Cheval <- rownames(prediction_current_race)
            prediction_current_race [,1] <- round(prediction_current_race [,1],2)
            rownames(prediction_current_race) <- NULL
            prediction_current_race <- merge(mat_infos_target_race_add,prediction_current_race, by.x="Cheval",by.y="Cheval",all=TRUE)
            prediction_current_race <- prediction_current_race[,colnames(prediction_current_race)[apply(prediction_current_race,2,function(x){sum(!is.na(x))})>0]]
            prediction_current_race <- prediction_current_race[,c("Numero","Cheval",setdiff(colnames(prediction_current_race),c("Numero","Cheval")))]
            prediction_current_race <- prediction_current_race[,c("Cheval","Chance")]
            colnames(prediction_current_race)[2] <- paste(unlist(strsplit(current_model,"_"))[3],unlist(strsplit(current_model,"_"))[5],sep="_")
            list_prediction_current_race_combined[[current_model]] <- prediction_current_race
            list_prediction_current_race_combined <- list_prediction_current_race_combined[lapply(list_prediction_current_race_combined,length)>0]
            
          }
        }
        
      }
      
      if(length(list_prediction_current_race_combined)>0) {
          mat_prediction_current_race_combined <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_prediction_current_race_combined)
      }

      mat_prediction_current_race_combined <- merge(mat_infos_target_race_add,mat_prediction_current_race_combined,by.x="Cheval",by.y="Cheval",all=TRUE)
      mat_prediction_current_race_combined <- mat_prediction_current_race_combined[,c("Numero","Cheval",setdiff(colnames(mat_prediction_current_race_combined),c("Numero","Cheval")))]
      if(length(grep(min(mat_prediction_current_race_combined$Distance),colnames(mat_prediction_current_race_combined)))==1) {
        mat_prediction_current_race_combined <- mat_prediction_current_race_combined[order(mat_prediction_current_race_combined[,grep(min(mat_prediction_current_race_combined$Distance),colnames(mat_prediction_current_race_combined))],decreasing = TRUE),]
      }
      
      test_vec_vec_prob <- !is.na(as.numeric(colnames(mat_prediction_current_race_combined))) & nchar(colnames(mat_prediction_current_race_combined))>1
      if(sum(test_vec_vec_prob)>0) {
        vec_vec_prob <- colnames(mat_prediction_current_race_combined)[test_vec_vec_prob]
        mat_prediction_current_race_combined <- mat_prediction_current_race_combined[,c(setdiff(colnames(mat_prediction_current_race_combined),vec_vec_prob),vec_vec_prob)]
      }
      
      if("Ferrage" %in% colnames(mat_prediction_current_race_combined) ){
        colnames(mat_prediction_current_race_combined)[colnames(mat_prediction_current_race_combined)=="Ferrage"] <-"Fer"
      }
      
      if("Gender" %in% colnames(mat_prediction_current_race_combined) ){
        colnames(mat_prediction_current_race_combined)[colnames(mat_prediction_current_race_combined)=="Gender"] <-"Sex"
      }

      mat_prediction_current_race_combined_dt <- datatable(mat_prediction_current_race_combined,rownames = FALSE,options(list(pageLength=nrow(mat_prediction_current_race_combined))),caption = htmltools::tags$caption(
        style = 'caption-side: bottom; text-align: center;',
        'Table 2: ', htmltools::em(table_caption_label)),escape = FALSE) 
      
      if("Fer" %in% colnames(mat_prediction_current_race_combined)) {
        message("Formatting coloring scheme of the ferrage column")
        mat_prediction_current_race_combined_dt <- mat_prediction_current_race_combined_dt %>%
          formatStyle(
            'Fer',
            backgroundColor = styleEqual(
              c("D0","DA","DP","D4"), c('darkred', 'orange', 'orange','lightgreen')
            )
          )
      }
      
      if(length(unique(mat_prediction_current_race_combined$Sex))>1) {
        message("Formatting coloring scheme of the gender column")
        mat_prediction_current_race_combined_dt <- mat_prediction_current_race_combined_dt %>%
          formatStyle(
            'Sex',
            backgroundColor = styleEqual(
              c("F","M","H"), c('pink', 'darkblue', 'lightblue')
            )
          )
      }
      
      if(length(unique(mat_prediction_current_race_combined$Recul))>1) {
        message("Formatting coloring scheme of the gender column")
        mat_prediction_current_race_combined_dt <- mat_prediction_current_race_combined_dt %>%
          formatStyle(
            'Recul',
            backgroundColor = styleEqual(
              c(0,1), c('lightgreen', 'darkred')
            )
          )
      }
      
      test_vec_vec_prob <- !is.na(as.numeric(colnames(mat_prediction_current_race_combined))) & nchar(colnames(mat_prediction_current_race_combined))>1
      if(sum(test_vec_vec_prob)>0) {
        vec_vec_prob <- colnames(mat_prediction_current_race_combined)[test_vec_vec_prob]
        for( current_prob in vec_vec_prob)
        {
          if(current_prob %in% colnames(mat_prediction_current_race_combined)) {
            message("Formatting coloring scheme of the predicted probabilities")
            mat_prediction_current_race_combined_dt <- mat_prediction_current_race_combined_dt %>%
              formatStyle(
                current_prob,
                backgroundColor = styleEqual(
                  mat_prediction_current_race_combined[,current_prob], colorRampPalette(brewer.pal(9,"RdYlGn"))(length(mat_prediction_current_race_combined[,current_prob]))[rank(mat_prediction_current_race_combined[,current_prob])]
                )
              )
          }
        }
      }
    }
  }

  # df_profiles_items <- compute_aggregated_score_hand_crafted_ml(df_race_to_score=mat_features_model_building)
  # 
  # mat_prediction_current_race_combined <- merge(mat_prediction_current_race_combined,df_profiles_items,by.x="Cheval",by.y="Cheval")
  vec_ordered_columns <- c(setdiff(colnames(mat_prediction_current_race_combined),vec_vec_prob),vec_vec_prob)
  mat_prediction_current_race_combined <- mat_prediction_current_race_combined[,vec_ordered_columns]
  return(list(mat_prob_table=mat_prediction_current_race_combined,mat_prob_table_pretty=mat_prediction_current_race_combined_dt))
}