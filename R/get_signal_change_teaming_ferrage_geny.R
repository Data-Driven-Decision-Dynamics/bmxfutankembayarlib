#' @return Compute percent of success focusing on same ferrage configuration
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_mean_earning_ferrage(df_horse_race = NULL)
#' @export
get_signal_change_teaming_ferrage_geny <- function(df_horse_race = NULL,
                                                   path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                   vec_minimal_columns = c("Cheval","Date","Lieu","Driver","Ferrage","Corde","Terrain","Discipline"),
                                                   number_days_back=366){
  
  message("initialization of the final output")
  previous_driver_name <- NULL
  previous_horse_ferrage <- NULL
  df_historical_races_focus_current <- NULL
  df_historical_races_focus_previous <- NULL
  df_historical_races_focus_current_driver <- NULL
  df_historical_races_focus_previous_driver <- NULL
  df_mean_earning_current_ferrage <- NULL
  df_mean_earning_previous_ferrage <- NULL
  df_mean_earning_current_driver <- NULL
  df_mean_earning_previous_driver <- NULL
  df_signal_change_teaming_ferrage  <- NULL
  df_signal_change_ferrage  <- NULL
  df_signal_change_driver   <- NULL

  message("Reading historical race infos file")
  if(!is.null(df_horse_race)){
    message("Reading historical performance file")
    if(!exists("df_historical_races_geny")) {
      if(file.exists(path_df_historical_races_geny)){
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    }
  }

  if(df_horse_race$Discipline == "Trot Monté" ){
    vec_minimal_columns <- sub("Driver","Jockey",vec_minimal_columns)
  }
  
  if(length(intersect(vec_minimal_columns,colnames(df_horse_race)))== length(vec_minimal_columns)){
    df_horse_race <- df_horse_race[,vec_minimal_columns]
    current_horse_name  <- gsub("\n","",unique(df_horse_race$Cheval))
    if(df_horse_race$Discipline == "Trot Attelé"){
      current_driver_name <- gsub("\n","",unique(df_horse_race$Driver))
    } else {
      current_driver_name <- gsub("\n","",unique(df_horse_race$Jockey))
    }
    current_date_label  <- unique(df_horse_race$Date)
    current_horse_ferrage  <- gsub("\n","",unique(df_horse_race$Ferrage))
    current_horse_discipline  <- gsub("\n","",unique(df_horse_race$Discipline))
  }
  
  if(df_horse_race$Discipline == "Trot Monté"){
    if(!is.null(df_horse_race)){
      df_historical_races_focus <- df_historical_races_geny %>%
        filter(Cheval %in% current_horse_name) %>%
        filter(Date<current_date_label)  %>%
        filter(current_date_label-Date<=number_days_back)  %>%
        filter(Discipline==current_horse_discipline) %>%
        select(Cheval,Jockey,Ferrage,Gains,Place) %>%
        unique() %>%
        unique() %>%
        as.data.frame()
      
      if(nrow(df_historical_races_focus)>0){
        df_infos_previous_race <- df_historical_races_geny %>%
          filter(Cheval %in% current_horse_name) %>%
          filter(Date<current_date_label)  %>%
          top_n(1,Date)
        if(!is.null(df_infos_previous_race)){
          if(nrow(df_infos_previous_race)>0){
            previous_driver_name <- gsub("\n","",unique(df_infos_previous_race$Jockey))
            previous_horse_ferrage  <- gsub("\n","",unique(df_infos_previous_race$Ferrage))
          }
        }
        
        if(sum(!is.na(df_historical_races_focus$Ferrage))>0) {
          message("Adding pivot variable for ferrage configuration")
          df_historical_races_focus$Pivot <- paste(df_historical_races_focus$Cheval,df_historical_races_focus$Ferrage,sep="-")
          df_horse_race$Pivot <- paste(df_horse_race$Cheval,df_horse_race$Ferrage,sep="-")
          df_infos_previous_race$Pivot <- paste(df_infos_previous_race$Cheval,df_infos_previous_race$Ferrage,sep="-")
          df_historical_races_focus_current <- df_historical_races_focus[df_historical_races_focus$Pivot %in% unique(df_horse_race$Pivot), ]
          if(nrow(df_infos_previous_race)){
            df_historical_races_focus_previous  <- df_historical_races_focus[df_historical_races_focus$Pivot %in% df_infos_previous_race$Pivot, ]
          }
        }
        if(!is.null(df_historical_races_focus_current)) {
          if(nrow(df_historical_races_focus_current)>0) {
            message("Replacing missing values for column place")
            if(sum(is.na(df_historical_races_focus_current$Place))>0) {
              df_historical_races_focus_current$Place[is.na(df_historical_races_focus_current$Place)] <- 99
            }
            df_mean_earning_current_ferrage <- df_historical_races_focus_current %>%
              dplyr::select(Cheval,Gains,Ferrage) %>%
              group_by(Cheval) %>%
              dplyr::summarise(MEAN_EARNING_CURRENT_FERRAGE = mean(Gains)) %>%
              mutate(MEAN_EARNING_CURRENT_FERRAGE=round(MEAN_EARNING_CURRENT_FERRAGE,0)) %>%
              as.data.frame()
          }
        }
        if(!is.null(df_historical_races_focus_previous)) {
          if(nrow(df_historical_races_focus_previous)>0) {
            message("Replacing missing values for column place")
            if(sum(is.na(df_historical_races_focus_previous$Place))>0) {
              df_historical_races_focus_previous$Place[is.na(df_historical_races_focus_previous$Place)] <- 99
            }
            df_mean_earning_previous_ferrage <- df_historical_races_focus_previous %>%
              dplyr::select(Cheval,Gains,Ferrage) %>%
              group_by(Cheval) %>%
              dplyr::summarise(MEAN_EARNING_PREVIOUS_FERRAGE = mean(Gains)) %>%
              mutate(MEAN_EARNING_PREVIOUS_FERRAGE=round(MEAN_EARNING_PREVIOUS_FERRAGE,0)) %>%
              as.data.frame()
          }
        }
        
        list_mean_earning_ferrage <- list(Current=df_mean_earning_current_ferrage,Others=df_mean_earning_previous_ferrage)
        list_mean_earning_ferrage <- list_mean_earning_ferrage[lengths(list_mean_earning_ferrage)>0]
        
        if(length(list_mean_earning_ferrage)>0) {
          df_signal_change_ferrage <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_mean_earning_ferrage)
          df_signal_change_ferrage <- merge(df_signal_change_ferrage,df_horse_race[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all=TRUE)
          df_signal_change_ferrage <- df_signal_change_ferrage[,c("Cheval",setdiff(colnames(df_signal_change_ferrage),c("Cheval")))]
          if(length(intersect("MEAN_EARNING_CURRENT_FERRAGE",colnames(df_signal_change_ferrage))) == 0){
            df_signal_change_ferrage$"MEAN_EARNING_CURRENT_FERRAGE" <- NA
          }
          if(length(intersect("MEAN_EARNING_PREVIOUS_FERRAGE",colnames(df_signal_change_ferrage))) == 0){
            df_signal_change_ferrage$"MEAN_EARNING_PREVIOUS_FERRAGE" <- NA
          }
        }
        
        if(length(list_mean_earning_ferrage)==0){
          df_signal_change_ferrage <- data.frame(Cheval = df_horse_race$Cheval, MEAN_EARNING_CURRENT_FERRAGE= NA, MEAN_EARNING_PREVIOUS_FERRAGE=NA ) 
        }
      }
      
      message("Filtering results on current driver")
      if(!is.null(df_horse_race)){
        df_historical_races_focus_current_driver <- df_historical_races_geny %>%
          filter(Jockey %in% current_driver_name) %>%
          filter(Date<current_date_label)  %>%
          filter(current_date_label-Date<=number_days_back)  %>%
          filter(Discipline==current_horse_discipline) %>%
          select(Cheval,Jockey,Gains,Place) %>%
          unique() %>%
          as.data.frame()
      }
      message("Filtering results on current driver")
      if(!is.null(previous_driver_name)){
        if(!is.null(df_horse_race)){
          df_historical_races_focus_previous_driver <- df_historical_races_geny %>%
            filter(Jockey %in% previous_driver_name) %>%
            filter(Date<current_date_label)  %>%
            filter(current_date_label-Date<=number_days_back)  %>%
            filter(Discipline==current_horse_discipline) %>%
            select(Cheval,Jockey,Gains,Place) %>%
            unique() %>%
            as.data.frame()
        }
      }
      
      if(!is.null(df_historical_races_focus_current_driver)){
        if(nrow(df_historical_races_focus_current_driver)>0) {
          message("Replacing missing values for column place")
          if(sum(is.na(df_historical_races_focus_current_driver$Place))>0) {
            df_historical_races_focus_current_driver$Place[is.na(df_historical_races_focus_current_driver$Place)] <- 99
          }
          df_mean_earning_current_driver <- df_historical_races_focus_current_driver %>%
            filter(Jockey==current_driver_name) %>%
            dplyr::select(Jockey,Gains) %>%
            group_by(Jockey) %>%
            dplyr::summarise(MEAN_EARNING_CURRENT_DRIVER = mean(Gains)) %>%
            mutate(MEAN_EARNING_CURRENT_DRIVER=round(MEAN_EARNING_CURRENT_DRIVER,0)) %>%
            as.data.frame()
          if(nrow(df_mean_earning_current_driver)>0){
            colnames(df_mean_earning_current_driver) <- sub("Jockey","Cheval", colnames(df_mean_earning_current_driver))
            df_mean_earning_current_driver[1,1] <- current_horse_name
          }
        }
      }
      
      message("Replacing missing values for column place")
      if(!is.null(df_historical_races_focus_previous_driver)){
        if(nrow(df_historical_races_focus_previous_driver)>0) {
          if(sum(is.na(df_historical_races_focus_previous_driver$Place))>0) {
            df_historical_races_focus_previous_driver$Place[is.na(df_historical_races_focus_previous_driver$Place)] <- 99
          }
          df_mean_earning_previous_driver <- df_historical_races_focus_previous_driver %>%
            filter(Jockey==previous_driver_name) %>%
            dplyr::select(Jockey,Gains) %>%
            group_by(Jockey) %>%
            dplyr::summarise(MEAN_EARNING_PREVIOUS_DRIVER = mean(Gains)) %>%
            mutate(MEAN_EARNING_PREVIOUS_DRIVER=round(MEAN_EARNING_PREVIOUS_DRIVER,0)) %>%
            as.data.frame()
          if(nrow(df_mean_earning_previous_driver)>0){
            colnames(df_mean_earning_previous_driver) <- sub("Jockey","Cheval", colnames(df_mean_earning_previous_driver))
            df_mean_earning_previous_driver[1,1] <- current_horse_name
          }
        }
      }
      
      list_mean_earning_driver <- list(Current=df_mean_earning_current_driver,Others=df_mean_earning_previous_driver)
      list_mean_earning_driver <- list_mean_earning_driver[lengths(list_mean_earning_driver)>0]
      
      if(length(list_mean_earning_driver)>0) {
        df_signal_change_driver <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_mean_earning_driver)
        if(length(intersect("MEAN_EARNING_CURRENT_DRIVER",colnames(df_signal_change_driver))) == 0){
          df_signal_change_driver$"MEAN_EARNING_CURRENT_DRIVER" <- NA
        }
        if(length(intersect("MEAN_EARNING_PREVIOUS_DRIVER",colnames(df_signal_change_driver))) == 0){
          df_signal_change_driver$"MEAN_EARNING_PREVIOUS_DRIVER" <- NA
        }
      }
      
      if(length(list_mean_earning_driver)==0){
        df_signal_change_driver <- data.frame(Cheval = df_horse_race$Cheval, MEAN_EARNING_CURRENT_DRIVER= NA, MEAN_EARNING_PREVIOUS_DRIVER = NA)
      }
    }
  }
  
  if(df_horse_race$Discipline == "Trot Attelé"){
    if(!is.null(df_horse_race)){
      df_historical_races_focus <- df_historical_races_geny %>%
        filter(Cheval %in% current_horse_name) %>%
        filter(Date<current_date_label)  %>%
        filter(current_date_label-Date<=number_days_back)  %>%
        filter(Discipline==current_horse_discipline) %>%
        select(Cheval,Driver,Ferrage,Gains,Place) %>%
        unique() %>%
        unique() %>%
        as.data.frame()
      
      if(nrow(df_historical_races_focus)>0){
        df_infos_previous_race <- df_historical_races_geny %>%
          filter(Cheval %in% current_horse_name) %>%
          filter(Date<current_date_label)  %>%
          top_n(1,Date)
        if(!is.null(df_infos_previous_race)){
          if(nrow(df_infos_previous_race)>0){
            previous_driver_name <- gsub("\n","",unique(df_infos_previous_race$Driver))
            previous_horse_ferrage  <- gsub("\n","",unique(df_infos_previous_race$Ferrage))
          }
        }
        
        if(sum(!is.na(df_historical_races_focus$Ferrage))>0) {
          message("Adding pivot variable for ferrage configuration")
          df_historical_races_focus$Pivot <- paste(df_historical_races_focus$Cheval,df_historical_races_focus$Ferrage,sep="-")
          df_horse_race$Pivot <- paste(df_horse_race$Cheval,df_horse_race$Ferrage,sep="-")
          df_infos_previous_race$Pivot <- paste(df_infos_previous_race$Cheval,df_infos_previous_race$Ferrage,sep="-")
          df_historical_races_focus_current <- df_historical_races_focus[df_historical_races_focus$Pivot %in% unique(df_horse_race$Pivot), ]
          if(nrow(df_infos_previous_race)){
            df_historical_races_focus_previous  <- df_historical_races_focus[df_historical_races_focus$Pivot %in% df_infos_previous_race$Pivot, ]
          }
        }
        if(!is.null(df_historical_races_focus_current)) {
          if(nrow(df_historical_races_focus_current)>0) {
            message("Replacing missing values for column place")
            if(sum(is.na(df_historical_races_focus_current$Place))>0) {
              df_historical_races_focus_current$Place[is.na(df_historical_races_focus_current$Place)] <- 99
            }
            df_mean_earning_current_ferrage <- df_historical_races_focus_current %>%
              dplyr::select(Cheval,Gains,Ferrage) %>%
              group_by(Cheval) %>%
              dplyr::summarise(MEAN_EARNING_CURRENT_FERRAGE = mean(Gains)) %>%
              mutate(MEAN_EARNING_CURRENT_FERRAGE=round(MEAN_EARNING_CURRENT_FERRAGE,0)) %>%
              as.data.frame()
          }
        }
        if(!is.null(df_historical_races_focus_previous)) {
          if(nrow(df_historical_races_focus_previous)>0) {
            message("Replacing missing values for column place")
            if(sum(is.na(df_historical_races_focus_previous$Place))>0) {
              df_historical_races_focus_previous$Place[is.na(df_historical_races_focus_previous$Place)] <- 99
            }
            df_mean_earning_previous_ferrage <- df_historical_races_focus_previous %>%
              dplyr::select(Cheval,Gains,Ferrage) %>%
              group_by(Cheval) %>%
              dplyr::summarise(MEAN_EARNING_PREVIOUS_FERRAGE = mean(Gains)) %>%
              mutate(MEAN_EARNING_PREVIOUS_FERRAGE=round(MEAN_EARNING_PREVIOUS_FERRAGE,0)) %>%
              as.data.frame()
          }
        }
        
        list_mean_earning_ferrage <- list(Current=df_mean_earning_current_ferrage,Others=df_mean_earning_previous_ferrage)
        list_mean_earning_ferrage <- list_mean_earning_ferrage[lengths(list_mean_earning_ferrage)>0]
        
        if(length(list_mean_earning_ferrage)>0) {
          df_signal_change_ferrage <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_mean_earning_ferrage)
          df_signal_change_ferrage <- merge(df_signal_change_ferrage,df_horse_race[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all=TRUE)
          df_signal_change_ferrage <- df_signal_change_ferrage[,c("Cheval",setdiff(colnames(df_signal_change_ferrage),c("Cheval")))]
          if(length(intersect("MEAN_EARNING_CURRENT_FERRAGE",colnames(df_signal_change_ferrage))) == 0){
            df_signal_change_ferrage$"MEAN_EARNING_CURRENT_FERRAGE" <- NA
          }
          if(length(intersect("MEAN_EARNING_PREVIOUS_FERRAGE",colnames(df_signal_change_ferrage))) == 0){
            df_signal_change_ferrage$"MEAN_EARNING_PREVIOUS_FERRAGE" <- NA
          }
        }
        
        if(length(list_mean_earning_ferrage)==0){
          df_signal_change_ferrage <- data.frame(Cheval = df_horse_race$Cheval, MEAN_EARNING_CURRENT_FERRAGE= NA, MEAN_EARNING_PREVIOUS_FERRAGE=NA ) 
        }
      }
      
      message("Filtering results on current driver")
      if(!is.null(df_horse_race)){
        df_historical_races_focus_current_driver <- df_historical_races_geny %>%
          filter(Driver %in% current_driver_name) %>%
          filter(Date<current_date_label)  %>%
          filter(current_date_label-Date<=number_days_back)  %>%
          filter(Discipline==current_horse_discipline) %>%
          select(Cheval,Driver,Gains,Place) %>%
          unique() %>%
          as.data.frame()
      }
      message("Filtering results on current driver")
      if(!is.null(previous_driver_name)){
        if(!is.null(df_horse_race)){
          df_historical_races_focus_previous_driver <- df_historical_races_geny %>%
            filter(Driver %in% previous_driver_name) %>%
            filter(Date<current_date_label)  %>%
            filter(current_date_label-Date<=number_days_back)  %>%
            filter(Discipline==current_horse_discipline) %>%
            select(Cheval,Driver,Gains,Place) %>%
            unique() %>%
            as.data.frame()
        }
      }
      
      if(!is.null(df_historical_races_focus_current_driver)){
        if(nrow(df_historical_races_focus_current_driver)>0) {
          message("Replacing missing values for column place")
          if(sum(is.na(df_historical_races_focus_current_driver$Place))>0) {
            df_historical_races_focus_current_driver$Place[is.na(df_historical_races_focus_current_driver$Place)] <- 99
          }
          df_mean_earning_current_driver <- df_historical_races_focus_current_driver %>%
            filter(Driver==current_driver_name) %>%
            dplyr::select(Driver,Gains) %>%
            group_by(Driver) %>%
            dplyr::summarise(MEAN_EARNING_CURRENT_DRIVER = mean(Gains)) %>%
            mutate(MEAN_EARNING_CURRENT_DRIVER=round(MEAN_EARNING_CURRENT_DRIVER,0)) %>%
            as.data.frame()
          if(nrow(df_mean_earning_current_driver)>0){
            colnames(df_mean_earning_current_driver) <- sub("Driver","Cheval", colnames(df_mean_earning_current_driver))
            df_mean_earning_current_driver[1,1] <- current_horse_name
          }
        }
      }
      
      message("Replacing missing values for column place")
      if(!is.null(df_historical_races_focus_previous_driver)){
        if(nrow(df_historical_races_focus_previous_driver)>0) {
          if(sum(is.na(df_historical_races_focus_previous_driver$Place))>0) {
            df_historical_races_focus_previous_driver$Place[is.na(df_historical_races_focus_previous_driver$Place)] <- 99
          }
          df_mean_earning_previous_driver <- df_historical_races_focus_previous_driver %>%
            filter(Driver==previous_driver_name) %>%
            dplyr::select(Driver,Gains) %>%
            group_by(Driver) %>%
            dplyr::summarise(MEAN_EARNING_PREVIOUS_DRIVER = mean(Gains)) %>%
            mutate(MEAN_EARNING_PREVIOUS_DRIVER=round(MEAN_EARNING_PREVIOUS_DRIVER,0)) %>%
            as.data.frame()
          if(nrow(df_mean_earning_previous_driver)>0){
            colnames(df_mean_earning_previous_driver) <- sub("Driver","Cheval", colnames(df_mean_earning_previous_driver))
            df_mean_earning_previous_driver[1,1] <- current_horse_name
          }
        }
      }
      
      list_mean_earning_driver <- list(Current=df_mean_earning_current_driver,Others=df_mean_earning_previous_driver)
      list_mean_earning_driver <- list_mean_earning_driver[lengths(list_mean_earning_driver)>0]
      
      if(length(list_mean_earning_driver)>0) {
        df_signal_change_driver <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_mean_earning_driver)
        if(length(intersect("MEAN_EARNING_CURRENT_DRIVER",colnames(df_signal_change_driver))) == 0){
          df_signal_change_driver$"MEAN_EARNING_CURRENT_DRIVER" <- NA
        }
        if(length(intersect("MEAN_EARNING_PREVIOUS_DRIVER",colnames(df_signal_change_driver))) == 0){
          df_signal_change_driver$"MEAN_EARNING_PREVIOUS_DRIVER" <- NA
        }
      }
      
      if(length(list_mean_earning_driver)==0){
        df_signal_change_driver <- data.frame(Cheval = df_horse_race$Cheval, MEAN_EARNING_CURRENT_DRIVER= NA, MEAN_EARNING_PREVIOUS_DRIVER = NA)
      }
    }
  }
  
  
  list_mean_earning_ferrage_driver <- list(Current=df_signal_change_ferrage,Others=df_signal_change_driver)
  list_mean_earning_ferrage_driver <- list_mean_earning_ferrage_driver[lapply(list_mean_earning_ferrage_driver,length)>0]
  
  if(length(list_mean_earning_ferrage_driver)>0) {
    df_signal_change_teaming_ferrage <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_mean_earning_ferrage_driver)
  }
  
  if(length(intersect("MEAN_EARNING_PREVIOUS_FERRAGE",colnames(df_signal_change_teaming_ferrage)))==0){
    df_signal_change_teaming_ferrage$"MEAN_EARNING_PREVIOUS_FERRAGE" <- NA
  }
  
  if(length(intersect("MEAN_EARNING_CURRENT_FERRAGE",colnames(df_signal_change_teaming_ferrage)))==0){
    df_signal_change_teaming_ferrage$"MEAN_EARNING_CURRENT_FERRAGE" <- NA
  }
  
  df_signal_change_teaming_ferrage$BONUS_INTENTION_CHANGE_FERRAGE <- 0
  df_signal_change_teaming_ferrage$BONUS_INTENTION_CHANGE_DRIVER  <- 0
  
  if(!is.na(df_signal_change_teaming_ferrage$MEAN_EARNING_CURRENT_FERRAGE) & !is.na(df_signal_change_teaming_ferrage$MEAN_EARNING_PREVIOUS_FERRAGE)){
    if(df_signal_change_teaming_ferrage$MEAN_EARNING_CURRENT_FERRAGE-df_signal_change_teaming_ferrage$MEAN_EARNING_PREVIOUS_FERRAGE >= 1500){
      df_signal_change_teaming_ferrage$BONUS_INTENTION_CHANGE_FERRAGE <- 2
    }
    if(df_signal_change_teaming_ferrage$MEAN_EARNING_CURRENT_FERRAGE-df_signal_change_teaming_ferrage$MEAN_EARNING_PREVIOUS_FERRAGE >= 3000){
      df_signal_change_teaming_ferrage$BONUS_INTENTION_CHANGE_FERRAGE <- 3
    }
    if(df_signal_change_teaming_ferrage$MEAN_EARNING_CURRENT_FERRAGE-df_signal_change_teaming_ferrage$MEAN_EARNING_PREVIOUS_FERRAGE >= 4500){
      df_signal_change_teaming_ferrage$BONUS_INTENTION_CHANGE_FERRAGE <- 4
    }
  }
  
  if(!is.na(df_signal_change_teaming_ferrage$MEAN_EARNING_CURRENT_DRIVER) & !is.na(df_signal_change_teaming_ferrage$MEAN_EARNING_PREVIOUS_DRIVER)){
    if(df_signal_change_teaming_ferrage$MEAN_EARNING_CURRENT_DRIVER-df_signal_change_teaming_ferrage$MEAN_EARNING_PREVIOUS_DRIVER >= 1500){
      df_signal_change_teaming_ferrage$BONUS_INTENTION_CHANGE_DRIVER <- 2
    }
    if(df_signal_change_teaming_ferrage$MEAN_EARNING_CURRENT_DRIVER-df_signal_change_teaming_ferrage$MEAN_EARNING_PREVIOUS_DRIVER >= 3000){
      df_signal_change_teaming_ferrage$BONUS_INTENTION_CHANGE_DRIVER <- 3
    }
    if(df_signal_change_teaming_ferrage$MEAN_EARNING_CURRENT_DRIVER-df_signal_change_teaming_ferrage$MEAN_EARNING_PREVIOUS_DRIVER >= 4500){
      df_signal_change_teaming_ferrage$BONUS_INTENTION_CHANGE_DRIVER <- 4
    }
  }

  return(df_signal_change_teaming_ferrage)
}

