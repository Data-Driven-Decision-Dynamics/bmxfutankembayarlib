get_feature_driver_trainer_geny <- function(df_infos_target_race = NULL){
  
  df_infos_features_driver <- NULL
  df_infos_features_trainer <- NULL 
  df_infos_features_trainer_driver <- NULL
  
  list_df_horse_race <- split(df_infos_target_race,df_infos_target_race$Cheval)
  df_signal_change_driver_previous_race <- NULL
  for(idx_row in 1:length(list_df_horse_race))
  {
    df_signal_change_driver_previous_race_current <- get_signal_change_driver_previous_current_geny(df_horse_race = list_df_horse_race[[idx_row]])
    if(!is.null(df_signal_change_driver_previous_race_current)){
      if(nrow(df_signal_change_driver_previous_race_current)>0){
        df_signal_change_driver_previous_race <- rbind.fill(df_signal_change_driver_previous_race,df_signal_change_driver_previous_race_current)
      }
    }
  }
  
  if("GLOBAL_CURRENT_DRIVER_PERCENT_SUCCESS" %in% colnames(df_signal_change_driver_previous_race)){
    if(sum(!is.na(df_signal_change_driver_previous_race$GLOBAL_CURRENT_DRIVER_PERCENT_SUCCESS))>5){
      df_infos_features_driver <- df_signal_change_driver_previous_race[,c("Cheval","GLOBAL_CURRENT_DRIVER_PERCENT_SUCCESS")]
      df_infos_features_driver$GLOBAL_CURRENT_DRIVER_PERCENT_SUCCESS <- round(get_percentile_values(df_infos_features_driver$GLOBAL_CURRENT_DRIVER_PERCENT_SUCCESS),2)
      colnames(df_infos_features_driver)[2] <- "INTENTION_PERCENT_SUCCESS_DRIVER" 
    }
  }
  
  list_df_horse_race <- split(df_infos_target_race,df_infos_target_race$Cheval)
  df_infos_features_trainer <- NULL
  for(idx_row in 1:length(list_df_horse_race))
  {
    df_infos_features_trainer_current <- get_trainer_percent_success_geny(df_horse_race = list_df_horse_race[[idx_row]])
    if(!is.null(df_infos_features_trainer_current)){
      if(nrow(df_infos_features_trainer_current)>0){
        df_infos_features_trainer <- rbind.fill(df_infos_features_trainer,df_infos_features_trainer_current)
      }
    }
  }
  if(!is.null(df_infos_features_trainer)){
    if("PERCENT_SUCCESS_CURRENT_LOCATION" %in% colnames(df_infos_features_trainer)){
      if(sum(!is.na(df_infos_features_trainer$PERCENT_SUCCESS_CURRENT_LOCATION))>5){
        if("PERCENT_SUCCESS_CURRENT_LOCATION" %in% colnames(df_infos_features_trainer)){
          df_infos_features_trainer <- df_infos_features_trainer[,c("Cheval","PERCENT_SUCCESS_CURRENT_LOCATION")]
          df_infos_features_trainer$PERCENT_SUCCESS_CURRENT_LOCATION <- round(get_percentile_values(df_infos_features_trainer$PERCENT_SUCCESS_CURRENT_LOCATION),2)
          colnames(df_infos_features_trainer)[2] <- "INTENTION_PERCENT_SUCCESS_TRAINER"
        }
      } else {
        df_infos_features_trainer <- NULL
      }
    }
  }
  
  list_infos_features_trainer_driver <- list(Driver=df_infos_features_driver,Trainer=df_infos_features_trainer)
  list_infos_features_trainer_driver <- list_infos_features_trainer_driver[lengths(list_infos_features_trainer_driver) != 0]
  if(length(list_infos_features_trainer_driver)>0) {
    df_infos_features_trainer_driver <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_infos_features_trainer_driver)
    df_infos_features_trainer_driver <- df_infos_features_trainer_driver[,c("Cheval",setdiff(colnames(df_infos_features_trainer_driver),c("Cheval")))]
    df_infos_features_trainer_driver <- unique(df_infos_features_trainer_driver)
  }
  
  return(df_infos_features_trainer_driver)
}


