get_signal_multiple_wins_per_year_geny <- function(df_infos_target_race = NULL,
                                                   path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"){
  
  df_signal_multiple_wins_per_year <- data.frame(Cheval = unique(df_infos_target_race$Cheval) , 
                                                 CLASS_MULTIPLE_WINS_PER_YEAR = 0,
                                                 CLASS_MULTIPLE_PODIUM_PER_YEAR = 0,
                                                 CLASS_LOW_DISQ_PER_YEAR = 0,
                                                 CLASS_WINS_ALL_CONFIGS = 0)
  
  if(min(as.numeric(df_infos_target_race$Age))>= 4){
    message("Start reading historical performance file")
    if(!exists("df_historical_races_geny")) {
      if(file.exists(path_df_historical_races_geny)){
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    }
    message("Finish reading historical performance file") 
    
    message("Start computing stats on winning configuration") 
    df_infos_stats_wins <- df_historical_races_geny %>%
      filter(Cheval %in% unique(df_infos_target_race$Cheval)) %>%
      filter(Discipline == unique(df_infos_target_race$Discipline)) %>%
      select(Date,Cheval,Age,Place) %>%
      distinct() %>%
      group_by(Cheval,Age)%>%
      dplyr::summarise(Size = n(),
                       Wins = get_number_winner(Place),
                       Podium = get_number_podium(Place),
                       Disqual = get_number_disqualification(Place)) %>%
      as.data.frame()
    
    if(nrow(df_infos_stats_wins)>0){
      for(horse in unique(df_signal_multiple_wins_per_year$Cheval))
      {
        df_infos_stats_wins_horse <- df_infos_stats_wins[df_infos_stats_wins$Cheval == horse,]
        df_infos_stats_wins_horse$Age <- as.numeric(df_infos_stats_wins_horse$Age)
        case_wins_year <- sum(df_infos_stats_wins_horse$Wins >=2)
        case_podium_year <- sum(df_infos_stats_wins_horse$Podium >=5)
        case_disqual_year <- sum(df_infos_stats_wins_horse$Disqual <2)
        if(case_wins_year == nrow(df_infos_stats_wins_horse)){
          df_signal_multiple_wins_per_year[df_signal_multiple_wins_per_year$Cheval == horse,"CLASS_MULTIPLE_WINS_PER_YEAR"] <- 1
        }
        if(case_podium_year == nrow(df_infos_stats_wins_horse)){
          df_signal_multiple_wins_per_year[df_signal_multiple_wins_per_year$Cheval == horse,"CLASS_MULTIPLE_PODIUM_PER_YEAR"] <- 1
        }
        if(case_disqual_year == nrow(df_infos_stats_wins_horse)){
          df_signal_multiple_wins_per_year[df_signal_multiple_wins_per_year$Cheval == horse,"CLASS_LOW_DISQ_PER_YEAR"] <- 1
        }
      }
    }
    
    df_infos_details_wins <- df_historical_races_geny %>%
      filter(Cheval %in% unique(df_infos_target_race$Cheval)) %>%
      filter(Discipline == unique(df_infos_target_race$Discipline)) %>%
      filter(Place==1) %>%
      select(Date,Cheval,Place,Corde,Terrain,Lieu) %>%
      drop_na() %>%
      distinct() %>%
      as.data.frame()
    if(nrow(df_infos_details_wins)>0){
      for(horse in unique(df_infos_details_wins$Cheval))
      {
        df_infos_details_wins_horse <- df_infos_details_wins[df_infos_details_wins$Cheval == horse,]
        vec_freq_turn <- table(df_infos_details_wins_horse$Corde)
        vec_freq_ground <- table(df_infos_details_wins_horse$Terrain)
        if(all(vec_freq_turn>=2) & (sum(vec_freq_ground>=2))>=3){
          df_signal_multiple_wins_per_year[df_signal_multiple_wins_per_year$Cheval == horse,"CLASS_WINS_ALL_CONFIGS"] <- 1
        }
      }
    }
    message("Start computing stats on winning configuration")
  }
  
  return(df_signal_multiple_wins_per_year)
}