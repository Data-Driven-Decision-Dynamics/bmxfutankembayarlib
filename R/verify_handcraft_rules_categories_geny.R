verify_handcraft_rules_categories_geny <- function (df_infos_target_race_geny = NULL , df_competitors_perfs = NULL){
  
  message("Start initializing final output")
  df_stats_profile_category <- data.frame(Cheval = unique(df_infos_target_race_geny$Cheval) , ENGAGEMENT_CONFIRMED_CATEGORIES = 0)
  message("Start initializing final output")
  
  message("Start initializing intermediary output")
  vec_candidates_class_based_all_quarter_close_cat <- NULL
  vec_candidates_class_based_q1_close_cat <- NULL
  vec_candidates_class_based_q1_higher_cat <- NULL
  vec_candidates_class_based_q1_way_higher_cat <- NULL
  vec_candidates_class_based_q1_q2_close_cat <- NULL
  vec_candidates_class_based_q1_q2_at_least_equal_cat <- NULL
  vec_candidates_class_based_q2_higher_cat <- NULL
  vec_candidates_class_based_q3_higher_cat <- NULL
  vec_candidates_class_based_q4_higher_cat <- NULL
  vec_candidates_often_sup_minus_one <- NULL
  message("Start initializing intermediary output")
  
  if(!is.null(df_infos_target_race_geny)){
    message("Start computing delay between consecutive races")
    df_all_infos_delay_between_races <- get_horse_fitness_given_race_geny(df_infos_target_race_geny)
    df_infos_delay_between_races <- df_all_infos_delay_between_races$Delay
    rownames(df_infos_delay_between_races) <- df_infos_delay_between_races$Cheval
    df_infos_rank_between_races <- df_all_infos_delay_between_races$Rank
    df_infos_rank_between_races$Cheval <- rownames(df_infos_rank_between_races)
    df_infos_rank_between_races <- df_infos_rank_between_races[,c("Cheval",rev(setdiff(colnames(df_infos_rank_between_races),"Cheval")))]
    rownames(df_infos_rank_between_races) <- df_infos_rank_between_races$Cheval
    df_infos_rank_between_races <- df_infos_rank_between_races[rownames(df_infos_delay_between_races),]
    if(sum(df_infos_rank_between_races[,-1] == 0,na.rm = TRUE)){
      df_infos_rank_between_races[,-1][df_infos_rank_between_races[,-1] == 0] <- 100
    }
    message("Start computing delay between consecutive races")
    
    message("Start testing if some candidates have good overrall category profile")
    df_stats_confirmation <- get_horse_highest_category_confirmed_geny(df_infos_target_race_geny,df_competitors_perfs)
    df_infos_test_class_based_all_quarter_close_cat <- df_stats_confirmation %>%
      filter(Delta >= (-1)) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_all_quarter_close_cat)>0){
      freq_infos_test_class_based_all_quarter_close_cat <- table(df_infos_test_class_based_all_quarter_close_cat$Cheval)
      if(max(freq_infos_test_class_based_all_quarter_close_cat,na.rm = TRUE) >= 3){
        vec_candidates_class_based_all_quarter_close_cat <- names(freq_infos_test_class_based_all_quarter_close_cat)[freq_infos_test_class_based_all_quarter_close_cat==4]
      }
    }
    message("Start testing if some candidates have good overall category profile")
    
    message("Start testing if any candidate have a class higher that the one of the current race")
    df_infos_test_class_based_q1_close_cat <- df_stats_confirmation %>%
      filter(Sequence == "Q1") %>%
      filter(Delta == 1) %>%
      filter(Effective == 1) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_q1_close_cat) > 0){
      vec_candidates_class_based_q1_higher_cat <- unique(df_infos_test_class_based_q1_close_cat$Cheval)
      vec_candidates_class_based_rank_delay <- df_infos_delay_between_races[df_infos_delay_between_races$`1` <= 45 & df_infos_rank_between_races$`1`<= 7,"Cheval"] 
      if(length(intersect(vec_candidates_class_based_q1_higher_cat,vec_candidates_class_based_rank_delay)) > 0){
        vec_candidates_class_based_q1_higher_cat <- intersect(vec_candidates_class_based_q1_higher_cat,vec_candidates_class_based_rank_delay)
      }
    }
    message("Finish testing if any candidate have a class higher that the one of the current race")
    
    message("Start testing if any candidate have a class way higher that the one of the current race")
    df_infos_test_class_based_q1_close_cat <- df_stats_confirmation %>%
      filter(Sequence == "Q1") %>%
      filter(Delta > 1) %>%
      filter(Effective == 1) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_q1_close_cat) > 0){
      vec_candidates_class_based_q1_way_higher_cat <- unique(df_infos_test_class_based_q1_close_cat$Cheval)
      vec_candidates_class_based_rank_delay <- df_infos_delay_between_races[df_infos_delay_between_races$`1` <= 45 & df_infos_rank_between_races$`1`<= 12,"Cheval"] 
      if(length(intersect(vec_candidates_class_based_q1_way_higher_cat,vec_candidates_class_based_rank_delay)) > 0){
        vec_candidates_class_based_q1_way_higher_cat <- intersect(vec_candidates_class_based_q1_way_higher_cat,vec_candidates_class_based_rank_delay)
      }
    }
    message("Finish testing if any candidate have a class way higher that the one of the current race")
    
    message("start testing if any candidate have the class of day and last race within one month and at most 5 ieme")
    df_infos_test_class_based_q1_close_cat <- df_stats_confirmation %>%
      filter(Sequence == "Q1") %>%
      filter(Delta >= (-1)) %>%
      filter(Effective == 1) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_q1_close_cat) > 0){
      vec_candidates_class_based_q1_close_cat <- unique(df_infos_test_class_based_q1_close_cat$Cheval)
      vec_candidates_class_based_rank_delay <- df_infos_delay_between_races[df_infos_delay_between_races$`1` <= 30 & df_infos_rank_between_races$`1`<= 100,"Cheval"] 
      if(length(intersect(vec_candidates_class_based_q1_close_cat,vec_candidates_class_based_rank_delay)) > 0){
        vec_candidates_class_based_q1_close_cat <- intersect(vec_candidates_class_based_q1_close_cat,vec_candidates_class_based_rank_delay)
      }
    }
    message("Finish testing if any candidate have the class of day and last race within one month and at most 5 ieme")
    
    message("Start testing if any candidate have good Q1 and Q2")
    df_infos_test_class_based_q1_q2_close_cat <- df_stats_confirmation %>%
      filter(Sequence %in% c("Q1","Q2")) %>%
      filter(Delta >= 0 ) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_q1_q2_close_cat)>0){
      freq_infos_test_class_based_q1_q2_close_cat <- table(df_infos_test_class_based_q1_q2_close_cat$Cheval)
      if(max(freq_infos_test_class_based_q1_q2_close_cat,na.rm = TRUE)>=2){
        vec_candidates_class_based_q1_q2_close_cat <- names(freq_infos_test_class_based_q1_q2_close_cat)[freq_infos_test_class_based_q1_q2_close_cat>=2]
        vec_candidates_class_based_rank_delay <- df_infos_delay_between_races[df_infos_delay_between_races$`1` <= 30 & df_infos_rank_between_races$`1`<=3,"Cheval"]
        if(length(intersect(vec_candidates_class_based_q1_q2_close_cat,vec_candidates_class_based_rank_delay))>0){
          vec_candidates_class_based_q1_q2_close_cat <- intersect(vec_candidates_class_based_q1_q2_close_cat,vec_candidates_class_based_rank_delay)
        }
      }
    }
    message("Start testing if any candidate have good Q1 and Q2")
    
    message("Start testing if any candidate have almost good Q1 and Q2")
    df_infos_test_class_based_q1_q2_at_least_equal_cat <- df_stats_confirmation %>%
      filter(Sequence %in% c("Q1","Q2")) %>%
      filter(Delta >= 0) %>%
      filter(Effective == 1) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_q1_q2_at_least_equal_cat)>0){
      freq_infos_test_class_based_q1_q2_at_least_equal_cat <- table(df_infos_test_class_based_q1_q2_at_least_equal_cat$Cheval)
      if(max(freq_infos_test_class_based_q1_q2_at_least_equal_cat,na.rm = TRUE)>=2){
        vec_candidates_class_based_q1_q2_at_least_equal_cat <- names(freq_infos_test_class_based_q1_q2_at_least_equal_cat)[freq_infos_test_class_based_q1_q2_at_least_equal_cat>=2]
        vec_candidates_class_based_rank_delay <- df_infos_delay_between_races[df_infos_delay_between_races$`1` <= 30 & df_infos_rank_between_races$`1`<=5,"Cheval"]
        if(length(intersect(vec_candidates_class_based_q1_q2_at_least_equal_cat,vec_candidates_class_based_rank_delay))>0){
          vec_candidates_class_based_q1_q2_at_least_equal_cat <- intersect(vec_candidates_class_based_q1_q2_at_least_equal_cat,vec_candidates_class_based_rank_delay)
        }
      }
    }
    message("Finish testing if any candidate have almost good Q1 and Q2")
    
    message("Start testing if any candidate have a good Q2 profile")
    df_infos_test_class_based_q2_higher_cat <- df_stats_confirmation %>%
      filter(Sequence == "Q2") %>%
      filter(Delta >= 1) %>%
      filter(Effective == 1) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_q2_higher_cat)>0){
      vec_candidates_class_based_q2_higher_cat <- unique(df_infos_test_class_based_q2_higher_cat$Cheval)
      vec_candidates_class_based_rank_delay <- df_infos_delay_between_races[df_infos_delay_between_races$`1` <= 30 & df_infos_rank_between_races$`1`<=3,"Cheval"]
      if(length(intersect(vec_candidates_class_based_q2_higher_cat,vec_candidates_class_based_rank_delay))>0){
        vec_candidates_class_based_q2_higher_cat <- intersect(vec_candidates_class_based_q2_higher_cat,vec_candidates_class_based_rank_delay)
      }
    }
    message("Finish testing if any candidate have a good Q2 profile") 
    
    message("Start testing if any candidate have a good Q3 profile")
    df_infos_test_class_based_q3_higher_cat <- df_stats_confirmation %>%
      filter(Sequence == "Q3") %>%
      filter(Delta >= 2) %>%
      filter(Effective == 1) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_q3_higher_cat)>0){
      vec_candidates_class_based_q3_higher_cat <- unique(df_infos_test_class_based_q3_higher_cat$Cheval)
      vec_candidates_class_based_rank_delay <- df_infos_delay_between_races[df_infos_delay_between_races$`1` <= 30 & df_infos_rank_between_races$`1`<=3,"Cheval"]
      if(length(intersect(vec_candidates_class_based_q3_higher_cat,vec_candidates_class_based_rank_delay))>0){
        vec_candidates_class_based_q3_higher_cat <- intersect(vec_candidates_class_based_q3_higher_cat,vec_candidates_class_based_rank_delay)
      }
    }
    message("Finish testing if any candidate have a good Q3 profile")
    
    message("Start testing if any candidate have a good Q4 profile")
    df_infos_test_class_based_q4_higher_cat <- df_stats_confirmation %>%
      filter(Sequence == "Q4") %>%
      filter(Delta >= 3) %>%
      filter(Effective == 1) %>%
      as.data.frame()
    if(nrow(df_infos_test_class_based_q4_higher_cat)>0){
      vec_candidates_class_based_q4_higher_cat <- unique(df_infos_test_class_based_q4_higher_cat$Cheval)
      vec_candidates_class_based_rank_delay <- df_infos_delay_between_races[df_infos_delay_between_races$`1` <= 30 & df_infos_rank_between_races$`1`<=3,"Cheval"]
      if(length(intersect(vec_candidates_class_based_q4_higher_cat,vec_candidates_class_based_rank_delay))>0){
        vec_candidates_class_based_q4_higher_cat <- intersect(vec_candidates_class_based_q4_higher_cat,vec_candidates_class_based_rank_delay)
      }
    }
    message("Finish testing if any candidate have a good Q4 profile")
    
    message("Start testing if any candidate have at leat a level at least equal at the current level minus one")
    df_infos_test_often_sup_minus_one <- df_stats_confirmation %>%
      filter(Delta > (-2)) %>%
      filter(Effective == 1) %>%
      as.data.frame()
    if(nrow(df_infos_test_often_sup_minus_one)>0){
      freq_often_sup_minus_one <- table(df_infos_test_often_sup_minus_one$Cheval)
      if(max(freq_often_sup_minus_one)>=3){
        vec_candidates_often_sup_minus_one <- names(freq_often_sup_minus_one)[which(freq_often_sup_minus_one>=3)]
      }
    }
      
    message("Finish testing if any candidate have at leat a level at least equal at the current level minus one")
    
    message("Combining all candidtaes based on category profile")
    vec_candidates_class_based_on_all_category_rules <- unique(c(vec_candidates_often_sup_minus_one,vec_candidates_class_based_all_quarter_close_cat,vec_candidates_class_based_q1_close_cat,vec_candidates_class_based_q1_q2_close_cat,vec_candidates_class_based_q2_higher_cat,vec_candidates_class_based_q3_higher_cat,vec_candidates_class_based_q4_higher_cat))
    if(length(vec_candidates_class_based_on_all_category_rules)>0){
      df_stats_profile_category[df_stats_profile_category$Cheval %in% vec_candidates_class_based_on_all_category_rules ,"ENGAGEMENT_CONFIRMED_CATEGORIES"] <- 1
    }
    message("Combining all candidtaes based on category profile")
  }
  
  return(df_stats_profile_category)

}