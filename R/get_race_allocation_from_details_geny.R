#' @return Extracts prize from race details
#' @importFrom stringr str_trim
#' @examples
#' get_race_allocation_from_details(val_details= NULL)
#' @export
get_race_allocation_from_details_geny <- function(val_details= NULL)
{
  res <- NA
  val_details <- val_details[1]
  if(nchar(val_details)>0) {
    res_details <- unlist(strsplit(val_details,"_"))
    res_details <- res_details[grep("€",res_details)]
    if(length(res_details)>0) {
      res <- res_details[1]
      res <- stringr::str_trim(sub("€\t","",res))
      res <- as.numeric(unlist(strsplit(res,"")))
      res <- res[!is.na(res)]
      res <- as.numeric(paste(res,collapse = ""))
    }
  }
  # mat_all_details <- as.data.frame(mat_all_details)
  # mat_all_details <- transform(mat_all_details, Prix=res)
  return(res)
}
