#' @return Compute percent on pairwise wins when driver associated with candidate
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_driver_horse_percent_win_pairwise_comparison_letrot(mat_infos_race_input = NULL)
#' @export
get_horse_driver_stats_canalturf <- function(mat_infos_target_race = NULL,path_mat_historical_races_canalturf = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/output/02-processed/mat_historical_races_canalturf.rds",number_days_back=720) 
{
  
  message("Basic processing to identify association between horses and drivers")
  mat_infos_target_race$Couple <- paste(mat_infos_target_race$Jockey,mat_infos_target_race$Cheval,sep="_")
  current_race_location <- as.vector(unique(mat_infos_target_race$Lieu))
  current_race_horses <- unique(mat_infos_target_race$Cheval)
  current_race_drivers <- unique(mat_infos_target_race$Driver)
  current_race_discipline <- unique(mat_infos_target_race$Discipline)
  
  if(class(unique(mat_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(mat_infos_target_race$Date)))
  } else {
    current_race_date <- unique(mat_infos_target_race$Date)
  }
  
  message("Reading historical race infos file")
  if(!exists("mat_historical_races_canalturf")) {
    if(file.exists(path_mat_historical_races_canalturf)) {
      mat_historical_races_canalturf <- readRDS(path_mat_historical_races_canalturf)
    }
  }
  
  message("Some filtering to focus on relevant associations")
  mat_infos_historical_races_focus <- mat_historical_races_canalturf %>%
    filter(Cheval %in% current_race_horses) %>%
    filter(Discipline==current_race_discipline) %>%
    filter(Date<current_race_date) %>%
    filter(current_race_date-Date<=number_days_back) %>%
    as.data.frame()
  
  mat_number_race_horse <- mat_infos_historical_races_focus %>% 
    group_by(Cheval) %>%
    dplyr::summarise(NUMBER_RACES_HORSE=n()) %>%
    as.data.frame()
  
  message("Adding association information to the historical data")
  mat_infos_historical_races_focus$Couple <- paste(mat_infos_historical_races_focus$Driver,mat_infos_historical_races_focus$Cheval,sep="_")
  mat_infos_historical_races_focus <- mat_infos_historical_races_focus[mat_infos_historical_races_focus$Couple %in% as.vector(unique(mat_infos_target_race$Couple)),]
  
  if(sum(is.na(mat_infos_historical_races_focus$Place))>0) {
    mat_infos_historical_races_focus$Place [is.na(mat_infos_historical_races_focus$Place)] <-99
  }

  mat_assos_driver_horse <- mat_infos_historical_races_focus %>% 
    group_by(Couple) %>%
    dplyr::summarise(NUMBER_DRIVER_HORSE=n(),NUMBER_DRIVER_HORSE_PLACE=get_number_podium(Place),PERCENT_SUCCESS_HORSE_DRIVER=round(1*(NUMBER_DRIVER_HORSE_PLACE/NUMBER_DRIVER_HORSE),2)) %>%
    as.data.frame()

  mat_assos_driver_horse <- merge(mat_infos_target_race[,c("Couple","Driver","Cheval")],mat_assos_driver_horse,by.x="Couple",by.y="Couple",all=TRUE)
  
  if(sum(is.na(mat_assos_driver_horse$NUMBER_DRIVER_HORSE))>0) {
    mat_assos_driver_horse$NUMBER_DRIVER_HORSE [is.na(mat_assos_driver_horse$NUMBER_DRIVER_HORSE)] <-0
  }
  
  if(sum(is.na(mat_assos_driver_horse$NUMBER_DRIVER_HORSE_PLACE))>0) {
    mat_assos_driver_horse$NUMBER_DRIVER_HORSE_PLACE [is.na(mat_assos_driver_horse$NUMBER_DRIVER_HORSE_PLACE)] <-0
  }
  
  if(sum(is.na(mat_assos_driver_horse$PERCENT_SUCCESS_DRIVER_HORSE))>0) {
    mat_assos_driver_horse$PERCENT_SUCCESS_HORSE_DRIVER [is.na(mat_assos_driver_horse$PERCENT_SUCCESS_HORSE_DRIVER)] <-0
  }
  
  mat_assos_driver_horse <- merge(mat_assos_driver_horse,mat_number_race_horse,by.x = "Cheval",by.y="Cheval",all=TRUE)
  mat_assos_driver_horse$PERCENT_RACE_HORSE_DRIVER <- round(1*(mat_assos_driver_horse$NUMBER_DRIVER_HORSE/mat_assos_driver_horse$NUMBER_RACES_HORSE),2)
  
  message("Some filtering to focus on relevant races")
  mat_infos_historical_races_focus <- mat_historical_races_canalturf %>%
    filter(Driver %in% current_race_drivers) %>%
    filter(Discipline==current_race_discipline) %>%
    filter(Date<current_race_date) %>%
    filter(current_race_date-Date<=number_days_back) %>%
    as.data.frame()
  
  mat_percent_success_driver <- mat_infos_historical_races_focus %>%
    filter(Driver %in% current_race_drivers) %>%
    group_by(Driver) %>%
    dplyr::summarise(NUMBER_RACES=n(),NUMBER_PLACE=get_number_podium(Place),PERCENT_PODIUM_DRIVER=round((NUMBER_PLACE/NUMBER_RACES)*1,2)) %>%
    select(Driver,PERCENT_PODIUM_DRIVER) %>%
    as.data.frame()
  
  mat_number_race_driver <- mat_infos_historical_races_focus %>%
    filter(Driver %in% current_race_drivers) %>%
    group_by(Driver) %>%
    dplyr::summarise(NUMBER_RACES_DRIVER=n()) %>%
    select(Driver,NUMBER_RACES_DRIVER) %>%
    as.data.frame()
  
  mat_number_race_location_driver <- mat_infos_historical_races_focus %>%
    filter(Driver %in% current_race_drivers) %>%
    group_by(Driver) %>%
    filter(Lieu==current_race_location) %>%
    dplyr::summarise(FREQUENCY_DRIVER_LOCATION=n(),FREQUENCY_PLACE_DRIVER_LOCATION=get_number_podium(Place),PERCENT_PODIUM_LOCATION_DRIVER=round(1*(FREQUENCY_PLACE_DRIVER_LOCATION/FREQUENCY_DRIVER_LOCATION),2)) %>%
    select(Driver,FREQUENCY_DRIVER_LOCATION,PERCENT_PODIUM_LOCATION_DRIVER) %>%
    as.data.frame()
  
  mat_number_race_location_driver <- merge(mat_number_race_location_driver,mat_number_race_driver,by.x="Driver",by.y="Driver",all=TRUE)
  mat_number_race_location_driver$PERCENT_RACES_LOCATION_DRIVER <- round(1*(mat_number_race_location_driver$FREQUENCY_DRIVER_LOCATION/mat_number_race_location_driver$NUMBER_RACES_DRIVER),2)
  mat_number_race_location_driver <- mat_number_race_location_driver [,c(1,3,5)]
  
  if(nrow(mat_number_race_location_driver)>0) {
    mat_percent_success_driver <- merge(mat_percent_success_driver,mat_number_race_location_driver,by.x="Driver",by.y="Driver",all=TRUE)
  }
  
  mat_percent_success_driver <- merge(mat_infos_target_race[,c("Driver","Cheval")],mat_percent_success_driver,by.x="Driver",by.y="Driver",all=TRUE)
  mat_assos_driver_horse <- merge(mat_assos_driver_horse,mat_percent_success_driver[,-2],by.x="Driver",by.y="Driver",all=TRUE)
  mat_assos_driver_horse <- mat_assos_driver_horse [,c(2,6,8:11)]
  
  mat_assos_driver_horse$ADVANTAGE_PODIUM_LOCATION_DRIVER <- round(((mat_assos_driver_horse$PERCENT_PODIUM_LOCATION_DRIVER-mat_assos_driver_horse$PERCENT_PODIUM_DRIVER)/mat_assos_driver_horse$PERCENT_PODIUM_DRIVER)*1,2)
  
  if(sum(is.na(mat_assos_driver_horse))>0) {
    mat_assos_driver_horse[is.na(mat_assos_driver_horse)] <- 0
  }

  return(mat_assos_driver_horse)
}