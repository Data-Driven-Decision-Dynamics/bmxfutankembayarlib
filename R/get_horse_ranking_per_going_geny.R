# get_horse_ranking_per_going_geny <- function(df_infos_target_race_geny = NULL,
#                                              path_res_ranking = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/candidates_ranking",
#                                              path_cluster_distance_range_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/mat_ranges_cluster_distance.rds",
#                                              min_number_value=0) {
# 
#   message("Initialize final output")
#   df_res_rating_specific_compliled <- NULL
# 
#   if(unique(df_infos_target_race_geny$Discipline %in% c("Trot Attelé","Trot Monté"))){
#     df_res_rating_specific_compliled <- NULL
#   } else {
#     message("Extract location of the target race")
#     current_race_location <- tolower(as.vector(unique(df_infos_target_race_geny$Lieu)))
# 
#     message("Extract horses of the target race")
#     current_race_horses <- as.vector(unique(df_infos_target_race_geny$Cheval))
# 
#     message("Extract discipline of the target race")
#     current_race_discipline <- gsub(" ","_",gsub("é","e",tolower(as.vector(unique(df_infos_target_race_geny$Discipline)))))
# 
#     message("Extract corde of the target race")
#     current_race_corde <- tolower(as.vector(unique(df_infos_target_race_geny$Corde)))
# 
#     message("Extract ground of the target race")
#     current_race_terrain <- tolower(as.vector(unique(df_infos_target_race_geny$Terrain)))
# 
#     message("Extract going of the target race")
#     current_race_going <- tolower(as.vector(unique(df_infos_target_race_geny$ETAT_TERRAIN)))
# 
#     if(as.vector(unique(df_infos_target_race_geny$Terrain)) =="Sable Fibré") {
#       current_race_going <- "Standard"
#     }
#     
#     message("Extract distance of the target race")
#     current_race_distance <- as.numeric((min(unique(df_infos_target_race_geny$Distance))))
# 
#     message("Reading file with cluster distance definition")
#     df_cluster_distance_range <- readRDS(path_cluster_distance_range_geny)
#     df_cluster_distance_range_discipline <- df_cluster_distance_range[df_cluster_distance_range$Discipline==unique(df_infos_target_race_geny$Discipline),]
# 
#     df_infos_target_race_geny$Cluster_Distance <- NA
#     for(i in 1:nrow(df_cluster_distance_range_discipline))
#     {
#       idx_cluster_distance_group <- df_cluster_distance_range_discipline[i,"Lower"] <= unlist(df_infos_target_race_geny[,"Distance"]) & df_cluster_distance_range_discipline[i,"Upper"] >= unlist(df_infos_target_race_geny[,"Distance"])
#       df_infos_target_race_geny[idx_cluster_distance_group==TRUE,"Cluster_Distance"] <- paste(df_cluster_distance_range_discipline[i,c("Lower","Upper")],collapse = "-")
#     }
# 
#     message("Extract date of the target race")
#     if(class(unique(df_infos_target_race_geny$Date))!="Date") {
#       current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
#     } else {
#       current_race_date <- unique(df_infos_target_race_geny$Date)
#     }
# 
#     rownames(df_infos_target_race_geny) <- as.vector(df_infos_target_race_geny$Cheval)
# 
#     current_race_cluster_distance <- unique(df_infos_target_race_geny$Cluster_Distance)
# 
#     pattern_conditions_ground_corde <- paste("current_going_",current_race_discipline,sep="")
# 
#     message("Identifying necessary ranking files")
#     vec_files_res_ranking <- dir(path_res_ranking,pattern="mat_ranking_monthly_temporal")
#     message("Here are the ranking files found")
#     vec_files_res_ranking <- grep(pattern_conditions_ground_corde,vec_files_res_ranking,value=TRUE,ignore.case = TRUE)
#     vec_files_res_ranking <- grep(sub("é","e",sub(" ","-",current_race_terrain)),vec_files_res_ranking,value=TRUE)
# 
#     lab_files_res_ranking <- sub(paste(".*",paste(pattern_conditions_ground_corde,"_",sep=""),sep=""),"",vec_files_res_ranking)
#     # lab_files_res_ranking <- gsub("-","_",lab_files_res_ranking)
#     lab_files_res_ranking <- gsub(".rds","",lab_files_res_ranking)
#     lab_files_res_ranking <- gsub("é","e",lab_files_res_ranking)
#     lab_files_res_ranking <- gsub("â","a",lab_files_res_ranking)
# 
#     df_infos_target_race_geny_focus <- df_infos_target_race_geny[,intersect(c('Cheval'),colnames(df_infos_target_race_geny)),drop=FALSE]
# 
#     list_res_rating_all_together <- vector("list",length(lab_files_res_ranking))
#     names(list_res_rating_all_together) <- lab_files_res_ranking
#     for(id_current_file_res_ranking in 1:length(lab_files_res_ranking))
#     {
#       df_ranking_current_gather <- NULL
#       df_ranking_current <- readRDS(paste(path_res_ranking,vec_files_res_ranking[id_current_file_res_ranking],sep="/"))
#       vec_candidate_last_dates <- setdiff(as.Date(colnames(df_ranking_current)[-1]),current_race_date)
#       vec_candidate_last_dates <- as.Date(vec_candidate_last_dates,origin="1970-01-01")
# 
#       if(sum(vec_candidate_last_dates<current_race_date)>0) {
#         val_last_date <- max(vec_candidate_last_dates[vec_candidate_last_dates<current_race_date])
#         if(length(intersect(current_race_horses,df_ranking_current$Cheval))>0) {
#           df_ranking_current <- df_ranking_current[df_ranking_current$Cheval %in% current_race_horses,]
#           df_ranking_current <- df_ranking_current[,c("Cheval", grep(as.Date(as.numeric(val_last_date),origin="1970-01-01"),colnames(df_ranking_current),value=TRUE))]
#           colnames(df_ranking_current)[2] <- lab_files_res_ranking[id_current_file_res_ranking]
#           df_ranking_current <- merge(df_ranking_current,df_infos_target_race_geny_focus,by.x="Cheval",by.y="Cheval")
#           df_ranking_current_gather <- df_ranking_current[,c(c("Cheval"),setdiff(colnames(df_ranking_current),c("Cheval")))]
#           list_res_rating_all_together[[id_current_file_res_ranking]] <- df_ranking_current_gather
#         }
#       }
#     }
# 
#     list_res_rating_all_together <- list_res_rating_all_together[lapply(list_res_rating_all_together,length)>0]
# 
#     if(length(list_res_rating_all_together)>0) {
#       df_res_rating_specific_compliled <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_res_rating_all_together)
#       df_res_rating_specific_compliled <- merge(df_res_rating_specific_compliled,df_infos_target_race_geny[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all=TRUE)
#       df_res_rating_specific_compliled <- df_res_rating_specific_compliled[,c("Cheval",setdiff(colnames(df_res_rating_specific_compliled),c("Cheval")))]
#       df_res_rating_specific_compliled <- df_res_rating_specific_compliled[,apply(df_res_rating_specific_compliled,2,function(x){sum(!is.na(x))})>min_number_value]
#     }
#   
#     df_res_rating_specific_compliled_formated <- NULL
#     for(horse in unique(df_res_rating_specific_compliled$Cheval)){
#       df_res_rating_specific_compliled_current <- df_res_rating_specific_compliled[df_res_rating_specific_compliled$Cheval==horse,]
#       df_res_rating_specific_compliled_formated_current <- data.frame(Cheval=horse,Class=colnames(df_res_rating_specific_compliled_current)[-c(1:2)],Score=unlist(df_res_rating_specific_compliled_current[,-c(1:2)]))
#       df_res_rating_specific_compliled_formated <- rbind(df_res_rating_specific_compliled_formated,df_res_rating_specific_compliled_formated_current)
#     }
#     
#     df_res_rating_specific_compliled_formated$Terrain <- unlist(lapply(df_res_rating_specific_compliled_formated$Class,function(x){unlist(strsplit(x,"_"))[1]}))
#     df_res_rating_specific_compliled_formated$Going <- unlist(lapply(df_res_rating_specific_compliled_formated$Class,function(x){unlist(strsplit(x,"_"))[2]}))
#     df_res_rating_specific_compliled_formated <- df_res_rating_specific_compliled_formated[complete.cases(df_res_rating_specific_compliled_formated),]
#     
#     
#     idx_config <- df_res_rating_specific_compliled_formated$Going == tolower(current_race_going) & df_res_rating_specific_compliled_formated$Terrain ==sub("é","e",sub(" ","-",current_race_terrain))
#     idx_going <- df_res_rating_specific_compliled_formated$Going == tolower(current_race_going) & df_res_rating_specific_compliled_formated$Terrain !=sub("é","e",sub(" ","-",current_race_terrain))
#     idx_terrain <- df_res_rating_specific_compliled_formated$Going != tolower(current_race_going) & df_res_rating_specific_compliled_formated$Terrain ==sub("é","e",sub(" ","-",current_race_terrain))
#     idx_others <- df_res_rating_specific_compliled_formated$Going != tolower(current_race_going) & df_res_rating_specific_compliled_formated$Terrain !=sub("é","e",sub(" ","-",current_race_terrain))
#     
#     df_res_rating_specific_compliled_formated$Class = NA
#     if(sum(idx_config>0)){
#       df_res_rating_specific_compliled_formated[idx_config,"Class"] <-"Config"
#     }
#     
#     if(sum(idx_terrain>0)){
#       df_res_rating_specific_compliled_formated[idx_terrain,"Class"] <-"Ground"
#     }
#     
#     if(sum(idx_others>0)){
#       df_res_rating_specific_compliled_formated[idx_others,"Class"] <-"Others"
#     }
#     
#     if(sum(idx_going>0)){
#       df_res_rating_specific_compliled_formated[idx_going,"Class"] <-"Going"
#     }
#     
#     df_res_rating_specific_compliled_final <- df_res_rating_specific_compliled_formated [,c("Cheval","Class","Score")] %>%
#       pivot_wider(
#         names_from = Class,
#         values_from = Score,
#         values_fn = max
#       ) %>%
#       as.data.frame()
#     
#     if(!is.null(df_res_rating_specific_compliled_final)){
#       if(nrow(df_res_rating_specific_compliled_final)>0){
#         colnames(df_res_rating_specific_compliled_final) <- stringi::stri_trans_totitle(colnames(df_res_rating_specific_compliled_final))
#         df_res_rating_specific_compliled_final <- df_res_rating_specific_compliled_final[,intersect(c("Cheval","Others","Ground","Going","Config"),colnames(df_res_rating_specific_compliled_final))]
#       }
#     }
#   }
#   return(df_res_rating_specific_compliled_final)
# }
