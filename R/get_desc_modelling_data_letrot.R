get_desc_modelling_data_letrot <- function(path_input_train = "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/train",path_input_testing = "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/test") {
  
  mat_description_modelling_data_compiled <- NULL
  vec_training_files <- dir(path_input_train)
  for(training_file in vec_training_files)
  {
    model_name_current <- sub(".rds","",sub("mat_training_","",training_file))
    df_training_current <- readRDS(paste(path_input_train,training_file,sep="/"))
    num_races_training <- length(unique(df_training_current$RACE))
    num_horses_training <-  length(unique(df_training_current$CHEVAL))
    num_items_training <- nrow(df_training_current)
    range_dates_races_training <- range(df_training_current$DATE,na.rm = TRUE)
    
    df_testing_current <-readRDS(paste(path_input_testing,sub("mat_training","mat_testing",training_file),sep="/"))
    num_races_testing <- length(unique(df_testing_current$RACE))
    num_horses_testing <-  length(unique(df_testing_current$CHEVAL))
    num_items_testing <- nrow(df_testing_current)
    range_dates_races_testing <- range(df_testing_current$DATE,na.rm = TRUE)
    
    mat_description_modelling_data <- data.frame(Model=model_name_current,TRAIN_SIZE=num_items_training,NUMBER_RACE_TRAIN=num_races_training,NUM_HORSE_TRAIN=num_horses_training,MIN_DATE_TRAIN=range_dates_races_training[1],MAX_DATE_TRAIN=range_dates_races_training[2], TEST_SIZE=num_items_testing,NUMBER_RACE_TEST=num_races_testing,NUM_HORSE_TEST=num_horses_testing,MIN_DATE_TEST=range_dates_races_testing[1],MAX_DATE_TEST=range_dates_races_testing[2])
    mat_description_modelling_data_compiled <- rbind(mat_description_modelling_data_compiled,mat_description_modelling_data)
  }
  
  return(mat_description_modelling_data_compiled)
}
