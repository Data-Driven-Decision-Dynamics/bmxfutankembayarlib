#' @return Provides ITR score over previous years
#' @importFrom reshape2 acast
#' @importFrom reshape2 melt
#' @examples
#' get_scoring_itr_given_race_geny(url_target_race ="http://www.geny.com/partants-pmu/2019-02-08-mont-de-marsan-pmu-prix-du-marensin_c1038006")
#' @export
get_scoring_itr_given_race_geny <- function(url_target_race ="http://www.geny.com/partants-pmu/2019-10-28-le-croise-laroche-pmu-prix-de-clairmarais_c1105041",
                                            path_input_itr_scoring = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed") {
  
  mat_infos_races <- get_details_race_geny(vec_races_current_date[i])
  vec_column_add <- c("Numero","Cheval","Age","Gender","Distance","Poids","Jockey","Driver" ,"Ferrage","Musique",grep("cot",colnames(mat_infos_races),ignore.case = TRUE,value=TRUE))
  vec_column_add <- intersect(vec_column_add,colnames(mat_infos_races))
  mat_infos_races_current_date_joint <- mat_infos_races[,vec_column_add]
  current_candidates <- mat_infos_races[,"Cheval"]
  current_discipline <- as.vector(unique(mat_infos_races[,"Discipline"]))
  current_discipline <- tolower(current_discipline)
  current_discipline <- gsub(" ","-",current_discipline)
  current_discipline <- gsub("é","e",current_discipline)
  current_discipline <- gsub("è","e",current_discipline)
  current_scoring_file <- grep(current_discipline,dir(path_input_itr_scoring,pattern = "scoring"),value=TRUE)
  if(length(current_scoring_file)>0) {
    mat_compilation_scoring <- readRDS(paste(path_input_itr_scoring,current_scoring_file,sep="/"))
    mat_compilation_scoring_current_race <- subset(mat_compilation_scoring,Cheval %in% current_candidates)
    if(nrow(mat_compilation_scoring_current_race)>1)
    {
      mat_compilation_scoring_current_race <- merge(mat_infos_races_current_date_joint,mat_compilation_scoring_current_race,by.x="Cheval",by.y="Cheval")
      mat_compilation_scoring_current_race$ID <- paste(mat_compilation_scoring_current_race$Year,mat_compilation_scoring_current_race$Cluster_Distance,sep="_")
      mat_compilation_scoring_current_race_melt <- reshape2::melt(mat_compilation_scoring_current_race, id.vars=c("Cheval", "ID"), measure.vars="SCORE_ITR",na.rm=TRUE)
      
      mat_compilation_scoring_current_race_format <- acast(mat_compilation_scoring_current_race_melt, Cheval ~ ID,function(x){mean(x,na.rm = TRUE)})
      mat_compilation_scoring_current_race_format <- as.data.frame(mat_compilation_scoring_current_race_format)
      mat_compilation_scoring_current_race_format$Cheval <- rownames(mat_compilation_scoring_current_race_format)
      mat_compilation_scoring_current_race_format <- mat_compilation_scoring_current_race_format[,c("Cheval",colnames(mat_compilation_scoring_current_race_format)[-ncol(mat_compilation_scoring_current_race_format)])]
      mat_compilation_scoring_current_race_format <- merge(mat_infos_races_current_date_joint,mat_compilation_scoring_current_race_format,by.x="Cheval",by.y="Cheval",all=TRUE)
      mat_compilation_scoring_current_race_format <- mat_compilation_scoring_current_race_format[,c("Numero",setdiff(colnames(mat_compilation_scoring_current_race_format),"Numero"))]
      mat_compilation_scoring_current_race_format[is.na(mat_compilation_scoring_current_race_format)] <-NA
    }
  }
return(mat_compilation_scoring_current_race_format)
}
