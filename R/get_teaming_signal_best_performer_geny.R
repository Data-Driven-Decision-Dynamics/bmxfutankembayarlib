get_teaming_signal_best_performer_geny <- function(df_infos_target_race_geny = NULL,
                                                   path_stats_success_tandem = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_success_tandems.rds"){
  
  message("Initialization of final output")
  df_teaming_signal_best_performer <- NULL
  message("Initialization of final output")
  
  message("Loading the file with tandem success rate per location")
  if(file.exists(path_stats_success_tandem)){
    df_stats_success_tandem <- readRDS(path_stats_success_tandem)
  }
  message("Loading the file with tandem success rate per location")

  if(unique(df_infos_target_race_geny$Discipline) == "Trot Attelé"){
    val_type_pilot <- "Driver"
  } else{
    val_type_pilot <- "Jockey"
  }
  
  df_infos_target_race_geny$TOP_PILOT <- 0
  for(idx in 1:nrow(df_infos_target_race_geny))
  {
    val_location <- df_infos_target_race_geny[idx,"Lieu"] 
    val_trainer_process <- df_infos_target_race_geny[idx,"Entraineur"] 
    val_discipline_process <- df_infos_target_race_geny[idx,"Discipline"]
    val_pilot_process <- df_infos_target_race_geny[idx,val_type_pilot] 
    df_stats_success_tandem_trainer <- df_stats_success_tandem[df_stats_success_tandem$Discipline== val_discipline_process &  df_stats_success_tandem$Entrainer == val_trainer_process & df_stats_success_tandem$Location == val_location,  ]
    if(nrow(df_stats_success_tandem_trainer)>0){
      df_stats_success_tandem_trainer <- df_stats_success_tandem_trainer[order(df_stats_success_tandem_trainer$Percent,decreasing = TRUE),]
      df_stats_success_tandem_trainer <- df_stats_success_tandem_trainer[df_stats_success_tandem_trainer$Size>=5,]
      if(nrow(df_stats_success_tandem_trainer)>0)
      {
        df_stats_success_tandem_trainer <- df_stats_success_tandem_trainer[order(df_stats_success_tandem_trainer$Percent,decreasing = TRUE),]
        val_top_pilots <- df_stats_success_tandem_trainer[1,"Pilot"]
        if(length(intersect(val_top_pilots,val_pilot_process))>0){
          df_infos_target_race_geny[idx,"TOP_PILOT"] <- 1
        }
      }
    }
  }
  df_teaming_signal_best_performer <- df_infos_target_race_geny[,c("Cheval","TOP_PILOT")]
  
  return(df_teaming_signal_best_performer)
  
}