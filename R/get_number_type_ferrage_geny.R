#' @return Compute percent of success focusing on same ferrage configuration
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_mean_earning_ferrage(df_horse_race = NULL)
#' @export
get_number_type_ferrage_geny <- function(df_horse_race = NULL, perf_db = "geny",
                                         path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                         vec_minimal_columns = c("Cheval","Date","Lieu","Ferrage","Corde","Terrain","Discipline"),
                                         number_days_back=366){
  
  message("initialization of the final output")
  df_number_type_ferrage  <- as.data.frame(matrix(0,ncol=3,nrow=1))
  colnames(df_number_type_ferrage) <- c("Cheval","NUMBER_RACE_CURRENT_FERRAGE","NUMBER_RACE_OTHERS_FERRAGE")
  rownames(df_number_type_ferrage) <- df_horse_race$Cheval
  df_number_type_ferrage$Cheval <- df_horse_race$Cheval
  
    message("Reading historical race infos file")
  if(perf_db == "geny"){
    if(exists("df_historical_races_geny")){
      df_historical_races <- df_historical_races_geny
    } else {
      if(file.exists(path_df_historical_races)) {
        df_historical_races <- readRDS(path_df_historical_races)
      }
    }
  }
  
  if(!is.null(df_horse_race)){
    message("Reading historical race infos file")
    if(perf_db == "geny"){
      if(exists("df_historical_races_geny")){
        df_historical_races <- df_historical_races_geny
      } else {
        if(file.exists(path_df_historical_races)) {
          df_historical_races <- readRDS(path_df_historical_races)
        }
      }
    }
    
    if(length(intersect(vec_minimal_columns,colnames(df_horse_race)))== length(vec_minimal_columns)){
      
      df_horse_race <- df_horse_race[,vec_minimal_columns]
      
      message("Retrieve  current horse name")
      current_horse_name <- gsub("\n","",unique(df_horse_race$Cheval))
      
      message("Retrieve race date and convert to the right format if needed")
      if(class(unique(df_horse_race$Date))!="Date") {
        current_race_date_current_horse  <- as.Date(as.vector(unique(df_horse_race$Date)))
      } else {
        current_race_date_current_horse <- unique(df_horse_race$Date)
      }
      
      message("Retrieve race characteristics")
      current_race_discipline_current_horse <- gsub("\n","",unique(df_horse_race$Discipline))
      
      message("Filtering results on candidates horses")
      df_historical_races_focus <- df_historical_races %>%
        filter(Cheval %in% current_horse_name) %>%
        filter(Date<current_race_date_current_horse)  %>%
        filter(current_race_date_current_horse-Date<=number_days_back)  %>%
        filter(Discipline==current_race_discipline_current_horse) %>%
        select(Cheval,Ferrage,Gains,Place) %>%
        unique() %>%
        as.data.frame()
    }
    
    if(nrow(df_historical_races_focus)>0){
      if(sum(!is.na(df_historical_races_focus$Ferrage))>0)
      {
        if(nrow(df_historical_races_focus)>0) {
          message("Adding pivot variable for ferrage configuration")
          df_historical_races_focus$Pivot <- paste(df_historical_races_focus$Cheval,df_historical_races_focus$Ferrage,sep="-")
          df_horse_race$Pivot <- paste(df_horse_race$Cheval,df_horse_race$Ferrage,sep="-")
          idx_current_ferrage <- df_historical_races_focus$Pivot %in% unique(df_horse_race$Pivot)
          idx_others_ferrage <- df_historical_races_focus$Pivot %in% setdiff(unique(df_historical_races_focus$Pivot),unique(df_horse_race$Pivot))
          
          if(sum(idx_current_ferrage,na.rm = TRUE)>0){
            df_number_type_ferrage[,"NUMBER_RACE_CURRENT_FERRAGE"] <- sum(idx_current_ferrage,na.rm = TRUE)
          }
          
          if(sum(idx_others_ferrage,na.rm = TRUE)>0){
            df_number_type_ferrage[,"NUMBER_RACE_OTHERS_FERRAGE"] <- sum(idx_others_ferrage,na.rm = TRUE)
          }
        }
      }
    }
  }
    
  return(df_number_type_ferrage)
}











