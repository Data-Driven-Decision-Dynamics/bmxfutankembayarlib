generate_infos_mobile_dashboard_geny <- function(target_date = Sys.Date(),
                                                 path_output_dashboard = "/mnt/Master-Data/Futanke-Mbayar/France/report/geny",
                                                 path_compiled_results = "/mnt/Master-Data/Futanke-Mbayar/France/report/geny",
                                                 path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                 path_google_drive_mobile_app = "https://drive.google.com/drive/folders/18MFrV4q_JPrBfnS89yXUAmhLyZogmDGe",
                                                 path_stats_cotournada_autostart = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_stats_contournada_autostart.rds",
                                                 generate_id_cards = FALSE) {

  
  require(googlesheets4)
  require(googledrive)
  require(bmxFutankeMbayar)
  require(dplyr)
  require(xlsx)
  
  vec_all_races_found <- dir(paste(path_compiled_results,target_date,sep="/"),recursive = TRUE,pattern = "Decision",full.names = TRUE)
  
  df_compiled_decision_final <- NULL
  
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  if(dir.exists(paste(path_compiled_results,as.character(target_date),sep="/"))){
    vec_details_reunions <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),pattern = ".xlsx")
    vec_details_reunions <- setdiff(vec_details_reunions,c("Infos.xlsx","Bettable.xlsx"))
    path_file_infos <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),pattern = "Infos.xlsx")
    if(length(path_file_infos)==1){
      df_infos_races <- readxl::read_xlsx(paste(paste(path_compiled_results,as.character(target_date),sep="/"),path_file_infos,sep="/")) %>%
        as.data.frame()
      df_infos_races$Course <- unlist(lapply(df_infos_races$Course,correct_race_label_geny))
      if(length(grep("\\[",df_infos_races$Lieu))>0){
        df_infos_races$Lieu <- stringr::str_trim(unlist(lapply(df_infos_races$Lieu,function(x){unlist(strsplit(x,"\\["))[1]})))
      }
    }
  }
  
  if(file.exists(paste(paste(paste(path_compiled_results,as.character(target_date),sep="/")),"Bettable.xlsx",sep="/"))){
    df_bettable_races <- xlsx::read.xlsx(paste(paste(paste(path_compiled_results,as.character(target_date),sep="/")),"Bettable.xlsx",sep="/"),sheetIndex = 1) %>%
      as.data.frame()
    df_bettable_races$Race <- unlist(lapply(df_bettable_races$Race,correct_race_label_geny))
    df_bettable_races$Race <- gsub("--","-",df_bettable_races$Race)
  }
  
  if(dir.exists(paste(path_compiled_results,as.character(target_date),sep="/"))){
    vec_files_decisions <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),recursive = TRUE,pattern = "Decision.rds",full.names = TRUE)
    for(file_decision in vec_files_decisions )
    {
      infos_compiled_decision_final_current <- readRDS(file_decision)
      df_compiled_decision_final_current <- infos_compiled_decision_final_current$Global
      tab_vec_courses_found <- table(df_infos_races[df_infos_races$Cheval %in% df_compiled_decision_final_current$Cheval, "Course"])
      candidate_race_found <- names(tab_vec_courses_found)[which.max(tab_vec_courses_found)]
      df_compiled_decision_final_current$Course <- candidate_race_found
      df_compiled_decision_final_current$Race <- rev(unlist(strsplit(file_decision,"/")))[2]
      df_compiled_decision_final_current$Lieu <- rev(unlist(strsplit(file_decision,"/")))[3]
      df_compiled_decision_final_current$Date <- rev(unlist(strsplit(file_decision,"/")))[4]
      df_compiled_decision_final_current <- merge(df_compiled_decision_final_current,df_infos_races[,c("Cheval","Numero","Gender","Age","Ferrage","Poids","Recul")],by.x="Cheval",by.y="Cheval",all.x = TRUE)
      df_compiled_decision_final <- rbind(df_compiled_decision_final,df_compiled_decision_final_current)
    }
  }

  df_compiled_decision_final <- merge(df_compiled_decision_final,df_bettable_races[,intersect(colnames(df_bettable_races),c("Race","Heure","Size","Pairs","Fit","F2F","Dominance","Delay","Play"))],by.x="Race",by.y="Race")

  df_infos_reunions <- df_compiled_decision_final %>%
    group_by(Lieu) %>%
    dplyr::summarise(Debut = min(Heure),
              Fin = max(Heure),
              Nombre = length(unique(Course))) %>%
    as.data.frame
  
  if(length(grep("\\[",df_infos_reunions$Lieu))>0){
    df_infos_reunions$Lieu <- stringr::str_trim(unlist(lapply(df_infos_reunions$Lieu,function(x){unlist(strsplit(x,"\\["))[1]})))
  }
  
  if(nrow(df_infos_reunions)>0){
    colnames(df_infos_reunions) <- sub("Lieu","Hippodrome",colnames(df_infos_reunions))
  }
  
  df_infos_reunions$Image <- NA
  
  # df_infos_courses <- df_compiled_decision_final %>%
  #   group_by(Course) %>%
  #   select(Lieu,Course,Heure) %>%
  #   as.data.frame() %>%
  #   unique()
  
  df_infos_courses <- df_infos_races %>%
    group_by(Course) %>%
    select(Lieu,Course,Discipline,Distance,Depart,Heure) %>%
    as.data.frame() %>%
    unique()
  
  df_infos_courses$Back <- "-"
  for(race in unique(df_infos_courses$Course))
  {
    if(unique(df_infos_races[df_infos_races$Course == race,"Discipline"]) %in% c("Trot Attelé","Trot Monté")){
      val_recul <- unique(df_infos_races[df_infos_races$Course == race,"Recul"])
      if(length(val_recul)>1){
        label_recul <- paste(val_recul,collapse = "+")
        df_infos_courses[df_infos_courses$Course == race,"Back"] <- label_recul
      }
    }
  }
  
  df_infos_courses$Course <- unlist(lapply(df_infos_courses$Course,correct_race_label_geny))
  
  if(sum(df_infos_courses$Discipline %in% c("Plat","Steeplechase","Cross","Haies"))>0){
    df_infos_courses[df_infos_courses$Discipline %in% c("Plat","Steeplechase","Cross","Haies"),"Depart"] <- "-"
  }
  
  if(sum(df_infos_courses$Depart %in% c("NA"))>0){
    df_infos_courses[df_infos_courses$Depart %in% c("NA"),"Depart"] <- "Volte"
  }
  
  df_infos_courses$Depart <- sub("Auto-Start","Auto",df_infos_courses$Depart)
  df_infos_courses$Discipline <- sub("Trot Attelé","Attelé",df_infos_courses$Discipline)
  df_infos_courses$Discipline <- sub("Trot Monté" ,"Monté",df_infos_courses$Discipline)
  
  if(length(grep("\\[",df_infos_courses$Lieu))>0){
    df_infos_courses$Lieu <- stringr::str_trim(unlist(lapply(df_infos_courses$Lieu,function(x){unlist(strsplit(x,"\\["))[1]})))
  }
  
  if(nrow(df_infos_courses)>0){
    colnames(df_infos_courses) <- sub("Lieu","Hippodrome",colnames(df_infos_courses))
  }
  
  df_infos_courses$Recul <- ""
  df_infos_courses$Label <- "" 
  # for(race in unique(df_infos_courses$Course) )
  for(race in unique(df_infos_courses$Course) )
  {
    if(unique(df_infos_courses[df_infos_courses$Course == race,"Discipline"]) %in% c("Attelé","Monté")){
      infos_recul <- df_infos_courses[df_infos_courses$Course == race,"Back"]
      if(length(grep("+", infos_recul))>0){
        df_infos_courses[df_infos_courses$Course == race,"Recul"] <- infos_recul
        df_infos_courses[df_infos_courses$Course == race,"Label"] <- paste(unique(df_infos_courses[df_infos_courses$Course == race,"Discipline"]),unique(df_infos_courses[df_infos_courses$Course == race,"Distance"]),unique(df_infos_courses[df_infos_courses$Course == race,"Depart"]),unique(df_infos_courses[df_infos_courses$Course == race,"Recul"]),sep="-")
      } else {
        df_infos_courses[df_infos_courses$Course == race,"Label"] <- paste(unique(df_infos_courses[df_infos_courses$Course == race,"Discipline"]),unique(df_infos_courses[df_infos_courses$Course == race,"Distance"]),unique(df_infos_courses[df_infos_courses$Course == race,"Depart"]),sep="-")
      }
    }
    
    if(unique(df_infos_courses[df_infos_courses$Course == race,"Discipline"]) %in% c("Plat","Steeplechase","Cross","Haies")){
      df_infos_courses[df_infos_courses$Course == race,"Label"] <- paste(unique(df_infos_courses[df_infos_courses$Course == race,"Discipline"]),unique(df_infos_courses[df_infos_courses$Course == race,"Distance"]),sep="-")
    }
  }
  
  df_infos_courses$Label <- gsub("--","",df_infos_courses$Label )

  df_infos_courses$Image <- NA

  df_infos_chevaux <- df_infos_races %>%
    group_by(Cheval) %>%
    select(Course,Lieu,Discipline,Cheval,Heure,Numero,Gender,Age,Poids,Recul) %>%
    as.data.frame() %>%
    mutate(Label = paste(Gender,Age,sep="-")) %>%
    mutate(ID=paste(Numero,Cheval,sep="-")) %>%
    unique()
  
  df_infos_chevaux$Label <- ""
  for(idx in 1:nrow(df_infos_chevaux))
  {
    age_current_horse <- df_infos_chevaux[idx,"Age"]
    gender_current_horse <- df_infos_chevaux[idx,"Gender"]
    race_current_horse <- df_infos_chevaux[idx,"Course"]
    lab_current_horse <- df_infos_chevaux[idx,"Cheval"]
    poids_current_horse <- df_infos_chevaux[idx,"Poids"]
    recul_current_horse <- df_infos_chevaux[idx,"Recul"]
    dis_current_horse <- unique(df_infos_races[df_infos_races$Course == race_current_horse ,"Discipline"])
    
    if(dis_current_horse %in% c("Plat","Steeplechase","Cross","Haies")){
      df_infos_chevaux$Label[idx] <- paste(gender_current_horse,age_current_horse,poids_current_horse,sep="-")
    }
    
    if(dis_current_horse %in% c("Trot Attelé")){
      if(length(unique(df_infos_races[df_infos_races$Course == race_current_horse,"Recul"]))>1){
        df_infos_chevaux$Label[idx] <- paste(gender_current_horse,age_current_horse,recul_current_horse,sep="-")
      } else {
        df_infos_chevaux$Label[idx] <- paste(gender_current_horse,age_current_horse,sep="-")
      }
    }
    
    if(dis_current_horse %in% c("Trot Monté")){
      if(length(unique(df_infos_races[df_infos_races$Course == race_current_horse,"Recul"]))>1){
        df_infos_chevaux$Label[idx] <- paste(gender_current_horse,age_current_horse,poids_current_horse,recul_current_horse,sep="-")
      } else {
        df_infos_chevaux$Label[idx] <- paste(gender_current_horse,age_current_horse,poids_current_horse,sep="-")
      }
    }
  }

  if(nrow(df_infos_chevaux)>0){
    colnames(df_infos_chevaux) <- sub("Lieu","Hippodrome",colnames(df_infos_chevaux))
  }
  
  df_infos_chevaux$Course <- unlist(lapply(df_infos_chevaux$Course,correct_race_label_geny))
  df_infos_chevaux$Card <- NA

  infos_directory_current_date = drive_mkdir(as.character(target_date),path = path_google_drive_mobile_app)
  infos_directory_current_date = as.data.frame(infos_directory_current_date)
  vec_location_found <- unique(df_infos_courses$Hippodrome)
  
  vec_location_found <- dir(paste(path_compiled_results,target_date,sep="/"))
  vec_location_found <- vec_location_found[-grep(".xlsx",vec_location_found)]
  # vec_location_found <- setdiff(vec_location_found,"Wolvega")

  for(location in vec_location_found)
  {
    
    df_bettable_races_location <- df_bettable_races[df_bettable_races$Location == location,]

    infos_directory_current_date_location = drive_mkdir(location,path = paste("https://drive.google.com/drive/folders/",as.vector(infos_directory_current_date$id),sep=""))
    infos_directory_current_date_location = as.data.frame(infos_directory_current_date_location)
    
    infos_directory_current_date_location_boards = drive_mkdir("Boards",path = paste(paste("https://drive.google.com/drive/folders/", as.vector(infos_directory_current_date_location$id),sep=""),as.vector(infos_directory_current_date_location$id),sep="/"))
    infos_directory_current_date_location_boards = as.data.frame(infos_directory_current_date_location_boards)
    
    meeting_board_tmp_file_path <- tempfile(pattern = paste(location,"-",sep=""), fileext = ".png")
    meeting_board_tmp_file_path <- gsub("'","-",meeting_board_tmp_file_path)
    graph_meeting_board_mobile_geny(df_bettable_races_location,meeting_board_tmp_file_path,running_mode = "Prod")
  
    infos_image_board_meeting_google_drive_file_path <- drive_upload(meeting_board_tmp_file_path, path=paste("https://drive.google.com/drive/folders/",as.vector(as.data.frame(infos_directory_current_date_location_boards)$id),sep=""),name=basename(meeting_board_tmp_file_path),overwrite =TRUE)
    infos_image_board_meeting_google_drive_file_path <- as.data.frame(infos_image_board_meeting_google_drive_file_path)
    df_infos_reunions[df_infos_reunions$Hippodrome == location, "Image"] <- paste("https://drive.google.com/file/d/",as.character(infos_image_board_meeting_google_drive_file_path$id),sep="")
    
    infos_directory_current_date_location_dashboards = drive_mkdir("Dashboards",path = paste(paste("https://drive.google.com/drive/folders/", as.vector(infos_directory_current_date_location$id),sep=""),as.vector(infos_directory_current_date_location$id),sep="/"))
    infos_directory_current_date_location_dashboards = as.data.frame(infos_directory_current_date_location_dashboards)
    
    infos_directory_current_date_location_id_cards = drive_mkdir("Id-Cards",path = paste(paste("https://drive.google.com/drive/folders/", as.vector(infos_directory_current_date_location$id),sep=""),as.vector(infos_directory_current_date_location$id),sep="/"))
    infos_directory_current_date_location_id_cards = as.data.frame(infos_directory_current_date_location_id_cards)
    
    vec_races_location <- df_infos_courses[df_infos_courses$Hippodrome == location,"Course"]
    vec_races_location_good_label <- unlist(lapply(vec_races_location,correct_race_label_geny))
    vec_races_location_good_label <- unique(vec_races_location_good_label)
    vec_races_location_good_label <- setdiff(vec_races_location_good_label,"3ème-course-Cheung-Sha-Handicap")
    
    for(race in unique(vec_races_location_good_label))
    {
      if(length(grep(race,vec_all_races_found))>0 & length(grep(race,df_bettable_races$Race))>0){
        race_tmp_file_path <- tempfile(pattern = paste(race,"-",sep=""), fileext = ".png")
        race_tmp_file_path <- gsub("'"," ",race_tmp_file_path)
        race_tmp_file_path <- gsub("\"","",race_tmp_file_path)
        graph_dashboard_mobile_geny(target_race = race, path_temporary_image_mobile = race_tmp_file_path,running_mode = "Prod")
        infos_image_dashboard_race_google_drive_file_path <- drive_upload(race_tmp_file_path, path=paste("https://drive.google.com/drive/folders/",as.vector(as.data.frame(infos_directory_current_date_location_dashboards)$id),sep=""),name=basename(race_tmp_file_path),overwrite =TRUE)
        infos_image_dashboard_race_google_drive_file_path <- as.data.frame(infos_image_dashboard_race_google_drive_file_path)
        if(race %in% df_infos_courses$Course ){
          df_infos_courses[df_infos_courses$Course == race, "Image"] <- paste("https://drive.google.com/file/d/",as.character(infos_image_dashboard_race_google_drive_file_path$id),sep="")
        }
        
        df_infos_horses_race <- df_infos_chevaux[df_infos_chevaux$Course == race,]
        for(horse in df_infos_horses_race$Cheval)
        {
          horse_tmp_file_path <- tempfile(pattern = paste(horse,"-",sep=""), fileext = ".png")
          horse_tmp_file_path <- gsub("'"," ",horse_tmp_file_path)
          horse_tmp_file_path <- gsub("\"","",horse_tmp_file_path)
          horse_tmp_file_path <- gsub("é","e",horse_tmp_file_path)
          horse_tmp_file_path <- gsub("è","e",horse_tmp_file_path)
          if(generate_id_cards==TRUE){
            graph_ultimate_identity_card_horse_geny(target_race = race, target_horse = horse , path_temporary_image_id_card_mobile = horse_tmp_file_path )
            infos_image_id_card_horse_google_drive_file_path <- drive_upload(horse_tmp_file_path, path=paste("https://drive.google.com/drive/folders/",as.vector(as.data.frame(infos_directory_current_date_location_id_cards)$id),sep=""),name=basename(horse_tmp_file_path),overwrite =TRUE)
            infos_image_id_card_horse_google_drive_file_path <- as.data.frame(infos_image_id_card_horse_google_drive_file_path)
            df_infos_chevaux[df_infos_chevaux$Cheval == horse,"Card"] <- paste("https://drive.google.com/file/d/",as.character(infos_image_id_card_horse_google_drive_file_path$id),sep="")
          }
        }
      }
    }
  }

  all_tmp_file_path <- tempfile(pattern = "Selbe_", fileext = ".xlsx")
  df_bettable_races_focus <- df_bettable_races[,c("Location","Heure","Race","Discipline","Class","Quality")]
  df_bettable_races_focus$Race <- gsub("--","-",df_bettable_races_focus$Race)
  df_bettable_races_focus$Course <- unlist(lapply( df_bettable_races_focus$Race,function(x){unlist(strsplit(x,"course-"))[2]}))
  df_bettable_races_focus <- df_bettable_races_focus[,c("Location","Heure","Quality","Course","Discipline","Class")]
  
  df_compiled_decision_final_focus <- df_compiled_decision_final [,c("Numero","Cheval","Race","Decision","Lieu")]
  df_compiled_decision_final_focus$Course <- unlist(lapply( df_compiled_decision_final_focus$Race,function(x){unlist(strsplit(x,"course-"))[2]}))
  df_compiled_decision_final_focus <- df_compiled_decision_final_focus [,c("Numero","Cheval","Decision","Course","Lieu")]
  write.xlsx(df_bettable_races_focus,all_tmp_file_path,sheetName = "Programme",row.names = FALSE,append = TRUE)
  write.xlsx(df_infos_reunions,all_tmp_file_path,sheetName = "Reunions",row.names = FALSE,append = TRUE)
  write.xlsx(df_infos_courses[rownames(unique(df_infos_courses[,c("Hippodrome","Course")])),],all_tmp_file_path,sheetName = "Courses",append = TRUE,row.names = FALSE)
  write.xlsx(df_infos_chevaux,all_tmp_file_path,sheetName = "Chevaux",append = TRUE,row.names = FALSE)
  write.xlsx(df_compiled_decision_final_focus,all_tmp_file_path,sheetName = "Quick",row.names = FALSE,append = TRUE)
  infos_data_all_races_google_drive_file_path <- drive_upload(all_tmp_file_path, path=paste("https://drive.google.com/drive/folders/",as.vector("1wuBJgAyejpgm6TSNoZYN9RmrnZcyoeAi"),sep=""),name=paste(unlist(strsplit(basename(all_tmp_file_path),"_"))[1],".xlsx",sep=""),overwrite =TRUE)
}
