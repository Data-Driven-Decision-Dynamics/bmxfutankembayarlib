get_odds_evolution_races_current_date <- function (date_current = as.character(Sys.Date()),
                                                   path_output_dashboard = "/mnt/Master-Data/Futanke-Mbayar/France/Odds/geny",
                                                   path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"){
  
  require(bmxFutankeMbayar)
  require(stringr)
  require(xml2)
  require(rvest)
  require(curl)
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Reading historical performance file")
  
  setwd(path_output_dashboard)
  dir.create(as.character(Sys.Date()))
  setwd(paste(path_output_dashboard,as.character(Sys.Date()),sep="/"))
  
  df_races_current_date_completed <- get_infos_race_current_date_geny()
  vec_path_races_current_date <- unique(df_races_current_date_completed$URL)

  df_odds_turfoo <- get_odds_turfoo_all_race_given_date()
  for(idx_time in 1:1200)
  {
    df_odds_timewise_current <- NULL
    df_odds_turfoo <- get_odds_turfoo_all_race_given_date()
    for(current_race in vec_path_races_current_date)
    {
      df_infos_geny <- get_details_race_geny(current_race)
      df_odds_values <- get_consolidated_odds_geny(df_infos_target_race_geny=df_infos_geny,df_infos_odds_turfoo=df_odds_turfoo)
      df_odds_values <- merge(df_infos_geny[,c("Cheval","Course","Prix","Lieu","Discipline","Heure")],df_odds_values,by.x="Cheval",by.y="Cheval")
      df_odds_values$Time <- Sys.time()
      if(!is.null(df_odds_values)){
        if(nrow(df_odds_values)>0){
          df_odds_timewise_current <- rbind.fill(df_odds_values,df_odds_timewise_current)
        }
      }
    }
    writexl::write_xlsx(df_odds_timewise_current,paste(as.character(Sys.time()),".xlsx",sep=""))
    Sys.sleep(60)
  }
}

