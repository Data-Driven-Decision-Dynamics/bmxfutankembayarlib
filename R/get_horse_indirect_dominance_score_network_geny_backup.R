# get_horse_indirect_dominance_score_network_geny_backup <- function(df_infos_target_race_geny = NULL,
#                                                             path_records_perfs_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_records_all_tracks_geny.rds",
#                                                             number_days_back=720){
# 
#   df_score_page_rank <- NULL
#   df_line_by_line_track_compiled <- NULL
#   df_in_out_edges <- NULL
#   
#   message("Extrack race date and convert in the right format if need")
#   if(class(unique(df_infos_target_race_geny$Date))!="Date") {
#     current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
#   } else {
#     current_race_date <- unique(df_infos_target_race_geny$Date)
#   }
#   
#   if(!is.null(df_infos_target_race_geny)){
#     df_infos_target_race_geny$Epreuve <- gsub("FALSE","F",df_infos_target_race_geny$Epreuve)
#   }
#   
#   message("Extrack name of current candidates")
#   current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
#   
#   message("Extrack discipline of current race")
#   current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
#   
#   message("Extrack terrain of current race")
#   current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
#   
#   message("Extrack terrain of current race")
#   current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
#   
#   message("Reading record data")
#   if(!exists("df_records_geny")){
#     df_records_geny <- readRDS(path_records_perfs_geny)
#     if(class(df_records_geny$Date)!="Date"){
#       df_records_geny$Date <- as.Date(df_records_geny$Date)
#     } 
#   }
#   message("Reading record data")
#   
#   
#   df_records_all_tracks_focus <- df_records_geny %>%
#     filter(Cheval %in%  current_race_horses) %>%
#     filter(Discipline == current_race_discipline) %>%
#     as.data.frame()
#   
#   if(nrow(df_records_all_tracks_focus)>0){
#     idx_terrain_corde <- df_records_all_tracks_focus$Discipline == unique(df_infos_target_race_geny$Discipline) &  !is.na(df_records_all_tracks_focus$Distance) & !is.na(df_records_all_tracks_focus$Terrain) & !is.na(df_records_all_tracks_focus$Corde) & (df_records_all_tracks_focus$Terrain == current_race_terrain | df_records_all_tracks_focus$Corde == current_race_corde)
#     if(sum(idx_terrain_corde)>0){
#       df_records_all_tracks_focus <- df_records_all_tracks_focus[idx_terrain_corde,]
#       df_records_formated <- df_records_all_tracks_focus [,c("Cheval","Speed","Id")] %>%
#         pivot_wider(
#           names_from = Id,
#           values_from = Speed,
#           values_fn = max
#         ) %>%
#         as.data.frame()
#       
#       vec_freq_horses_track <- sort(apply(df_records_formated[,-1],2,function(x){sum(!is.na(x))}))
#       vec_tracks_found <- names(vec_freq_horses_track)[vec_freq_horses_track >= ceiling(nrow(df_infos_target_race_geny)/2)]
#       for(track in vec_tracks_found)
#       {
#         val_filled_values_track <- sum(!is.na(df_records_formated[,track]))
#         if(val_filled_values_track>1){
#           df_line_by_line_track_current <- NULL
#           df_records_formated_track <- df_records_formated[,c("Cheval",track),drop=FALSE]
#           df_records_formated_track <- df_records_formated_track[complete.cases(df_records_formated_track),]
#           df_records_formated_track <- df_records_formated_track[order(df_records_formated_track[,track],decreasing = TRUE),]
#           for(idx in 1:nrow(df_records_formated_track))
#           {
#             df_records_formated_track_complement <- df_records_formated_track[-idx,]
#             test_current_sup_others <- df_records_formated_track[idx,2] > df_records_formated_track_complement[,2]
#             if(sum(test_current_sup_others)>0){
#               vec_horses_beaten <- df_records_formated_track_complement$Cheval[test_current_sup_others]
#               df_line_by_line_track_current_current <- data.frame(from = df_records_formated_track[idx,"Cheval"] , to = vec_horses_beaten , value = df_records_formated_track[idx,track] - df_records_formated_track_complement[test_current_sup_others,track],track = track)
#               df_line_by_line_track_current <- rbind(df_line_by_line_track_current,df_line_by_line_track_current_current)
#             }
#             df_line_by_line_track_compiled <- rbind(df_line_by_line_track_compiled,df_line_by_line_track_current)
#           }
#         }
#       }
#       
#       df_line_by_line_track_compiled <- unique(df_line_by_line_track_compiled)
#       
#       edge_indirect_confrontation <- merge(df_line_by_line_track_compiled,df_infos_target_race_geny[,c("Cheval","Numero")],by.x="from",by.y="Cheval",all.x=TRUE)
#       edge_indirect_confrontation$from <- paste(edge_indirect_confrontation$Numero,edge_indirect_confrontation$from,sep="-")
#       edge_indirect_confrontation <- edge_indirect_confrontation[,-grep("Numero",colnames(edge_indirect_confrontation))]
#       
#       edge_indirect_confrontation <- merge(edge_indirect_confrontation,df_infos_target_race_geny[,c("Cheval","Numero"),drop=FALSE],by.x="to",by.y="Cheval",all.x=TRUE)
#       edge_indirect_confrontation$to   <- paste(edge_indirect_confrontation$Numero,edge_indirect_confrontation$to,sep="-")
#       edge_indirect_confrontation <- edge_indirect_confrontation[,-grep("Numero",colnames(edge_indirect_confrontation))]
#       edge_indirect_confrontation <- edge_indirect_confrontation[,c("from","to")]
#       
#       df_infos_target_race_geny$id <- paste(df_infos_target_race_geny$Numero,df_infos_target_race_geny$Cheval,sep="-")
#       vec_missing_nodes <- setdiff(df_infos_target_race_geny$id,unique(unlist(edge_indirect_confrontation[,c("from","to")])))
#       if(length(vec_missing_nodes)>0){
#         edge_indirect_confrontation_complement <- edge_indirect_confrontation[1:length(vec_missing_nodes),,drop=FALSE]
#         for(idx_missing_node in 1:length(vec_missing_nodes)) 
#         {
#           edge_indirect_confrontation_complement[idx_missing_node,"to"]   <- vec_missing_nodes[idx_missing_node]
#           edge_indirect_confrontation_complement[idx_missing_node,"from"] <- vec_missing_nodes[idx_missing_node]
#         }
#         edge_indirect_confrontation <- rbind( edge_indirect_confrontation,edge_indirect_confrontation_complement)
#       }
#       
#       tab_from_to <- table(edge_indirect_confrontation$from,edge_indirect_confrontation$to)
#       df_from_to <- as.data.frame(as.matrix.data.frame(tab_from_to))
#       colnames(df_from_to) <- colnames(tab_from_to)
#       rownames(df_from_to) <- rownames(tab_from_to)
#       if(length(rownames(df_from_to)) < length(df_infos_target_race_geny$id)){
#         missing_nodes_from <- setdiff(df_infos_target_race_geny$id,rownames(df_from_to))
#         for(node_from_to_add in missing_nodes_from){
#           df_from_to_add <- as.data.frame(matrix(0,ncol=ncol(df_from_to),nrow=1))
#           colnames(df_from_to_add) <- colnames(df_from_to)
#           rownames(df_from_to_add) <- node_from_to_add
#           df_from_to <- rbind(df_from_to,df_from_to_add)
#         }
#       }
#       
#       if(length(colnames(df_from_to)) < length(df_infos_target_race_geny$id)){
#         missing_nodes_to <- setdiff(df_infos_target_race_geny$id,colnames(df_from_to))
#         for(node_to_to_add in missing_nodes_to){
#           df_from_to[,node_to_to_add] <- 0
#         }
#       }
#       
#       df_edge_from_to_built <- NULL
#       for(idx_lab in rownames(df_from_to))
#       {
#         for(idy_lab in colnames(df_from_to))
#         {
#           if(idy_lab %in% rownames(df_from_to) & idx_lab %in% colnames(df_from_to) ){
#             if(df_from_to[idx_lab,idy_lab]>df_from_to[idy_lab,idx_lab]){
#               df_edge_from_to_built_current <- data.frame(from=idx_lab,to=idy_lab,val=df_from_to[idx_lab,idy_lab]-df_from_to[idy_lab,idx_lab])
#               df_edge_from_to_built <- rbind(df_edge_from_to_built,df_edge_from_to_built_current)
#             }
#           }
#         }
#       }
#       
#       vec_nodes_found <- unique(unlist(df_edge_from_to_built[,c("from","to")]))
#       if(length(vec_nodes_found) < length(df_infos_target_race_geny$id)) {
#         vec_nodes_found <- df_infos_target_race_geny$id
#       }
#       
#       df_node_from_to_built <- data.frame(id = vec_nodes_found, label = vec_nodes_found)
#       
#       network_confrontations_config <- igraph::graph_from_edgelist(as.matrix(df_edge_from_to_built[,c("from","to")]), directed = TRUE)
#       
#       vec_incoming_edges  <- degree(network_confrontations_config,mode="in")
#       vec_outcoming_edges <- degree(network_confrontations_config,mode="out")
#       df_in_out_edges <- data.frame(Cheval = names(vec_incoming_edges) , In = as.vector(vec_incoming_edges), Out = as.vector(vec_outcoming_edges), Boss = 0)
#       idx_candidates_penalties <- df_in_out_edges$Out >= ceiling(nrow(df_in_out_edges)/3) & df_in_out_edges$In <2
#       if(length(which(idx_candidates_penalties==TRUE))>0){
#         df_in_out_edges[which(idx_candidates_penalties==TRUE),"Boss"] <- 1
#       }
#       
#       df_score_page_rank <- as.data.frame(page_rank(graph_from_edgelist(as.matrix(df_edge_from_to_built[,c("to","from")])))$vector)
#       colnames(df_score_page_rank) <- "rank"
#       df_score_page_rank$id <- rownames(df_score_page_rank)
#       df_score_page_rank <- df_score_page_rank[,c("id","rank")]
#     }
#   }
# 
#   return(list(Rank=df_score_page_rank,Boss=df_in_out_edges))
# }
# 
# 
