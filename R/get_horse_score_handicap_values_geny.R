get_horse_score_handicap_values_geny <- function (df_infos_target_race=NULL,
                                                  path_mat_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                  path_cluster_distance_range = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/mat_ranges_cluster_distance.rds",
                                                  number_days_back=366)
{

  message("Initialize final output")
  mat_score_handicap <- NULL
  
  if(unique(df_infos_target_race$Discipline %in%  c("Plat","Steeplechase","Cross","Haies"))){
    message("Extract race date and convert in the right format if need")
    if(class(unique(df_infos_target_race$Date))!="Date") {
      current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
    } else {
      current_race_date <- unique(df_infos_target_race$Date)
    }
    
    if(!is.null(df_infos_target_race)){
      df_infos_target_race$Epreuve <- gsub("FALSE","F",df_infos_target_race$Epreuve)
    }
    
    message("Extract name of current candidates")
    current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))
    
    message("Extract category of current race")
    current_race_category <- gsub("\n","",unique(df_infos_target_race$Epreuve))
    
    message("Extract distance of current race")
    current_race_distance <- min(as.numeric(gsub("\n","",unique(df_infos_target_race$Distance))))
    
    message("Extract discipline of current race")
    current_race_discipline <- unique(df_infos_target_race$Discipline)
    
    message("Extract hippodrome of current race")
    current_hippodrome <- as.vector(unique(df_infos_target_race$Lieu))
    
    message("Extract terrain of current race")
    current_terrain <- as.vector(unique(df_infos_target_race$Terrain))
    
    message("Reading historical race infos file")
    if(!exists("mat_historical_races_geny")) {
      if(file.exists(path_mat_historical_races_geny)) {
        mat_historical_races_geny <- readRDS(path_mat_historical_races_geny)
      }
    } 
    
    message("Calculate percentage of earning per discipline")
    if(sum(current_race_horses %in% unique(mat_historical_races_geny$Cheval))>0) {
      mat_score_handicap <- mat_historical_races_geny %>%
        filter(Discipline==current_race_discipline) %>%
        filter(Cheval %in% current_race_horses) %>%
        filter(Date<current_race_date) %>%
        filter(current_race_date-Date<=number_days_back) %>%
        dplyr::select(Cheval,Discipline,Date,Handicap,Gains,Place,Poids) %>%
        drop_na() %>%
        filter(Place <4) %>%
        group_by(Cheval) %>%
        top_n(1,Gains) %>%
        as.data.frame()
      if(nrow(mat_score_handicap)>0){
        if(nrow(mat_score_handicap)< nrow(df_infos_target_race)){
          mat_score_handicap <- merge(mat_score_handicap,df_infos_target_race[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
          mat_score_handicap <- mat_score_handicap %>%
            select(-(Discipline))
        }
      }
    }
   }
    
  return(mat_score_handicap)
}