#' @return Compute percent of success focusing on same ferrage configuration
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' mat_speed_cluster_distance(mat_infos_target_race = NULL)
#' @export
get_horse_average_speed_cluster_distance_canalturf <- function(mat_infos_target_race = NULL,
                                                               path_mat_historical_races_canalturf = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/output/02-processed/mat_historical_races_canalturf.rds",
                                                               path_cluster_distance_range = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/input/mat_ranges_cluster_distance.rds",
                                                               number_days_back=366) 
{
  
  message("Initialization of the output")
  mat_speed_cluster_distance <- NULL
  
  mat_cluster_distance_range <- readRDS(path_cluster_distance_range)

  message("Retrieve discipline")
  current_race_discipline <- gsub("\n","",unique(mat_infos_target_race$Discipline))
  
  message("Correct the type of the Date variable if needed")
  if(class(unique(mat_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(mat_infos_target_race$Date)))
  } else {
    current_race_date <- unique(mat_infos_target_race$Date)
  }
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(mat_infos_target_race$Cheval))
  
  message("Reading historical race infos file")
  if(!exists("mat_historical_races_canalturf")) {
    if(file.exists(path_mat_historical_races_canalturf)) {
      mat_historical_races_canalturf <- readRDS(path_mat_historical_races_canalturf)
    }
  } 
  
  mat_cluster_distance_range_discipline <- mat_cluster_distance_range[gsub("É","E",toupper(mat_cluster_distance_range$Discipline))==current_race_discipline,]
  
  message("Filtering results on candidates horses")
  mat_historical_races_canalturf_focus <- mat_historical_races_canalturf %>%
    filter(Cheval %in% current_race_horses) %>%
    filter(Date<current_race_date)  %>%
    filter(current_race_date-Date<=number_days_back)  %>%
    filter(Discipline==current_race_discipline) %>%
    as.data.frame()
  
  if(nrow(mat_historical_races_canalturf_focus)>0){
    mat_historical_races_canalturf_focus$Cluster_Distance <- NA
    for(i in 1:nrow(mat_cluster_distance_range_discipline))
    {
      idx_cluster_distance_group <- mat_cluster_distance_range_discipline[i,"Lower"] <= unlist(mat_historical_races_canalturf_focus[,"Distance"]) & mat_cluster_distance_range_discipline[i,"Upper"] >= unlist(mat_historical_races_canalturf_focus[,"Distance"])
      mat_historical_races_canalturf_focus[idx_cluster_distance_group==TRUE,"Cluster_Distance"] <- paste(mat_cluster_distance_range_discipline[i,c("Lower","Upper")],collapse = "-")
    }
    
    mat_speed_cluster_distance <- NULL
    
    mat_speed_cluster_distance <- mat_historical_races_canalturf_focus %>%
      select(Cheval,Time,Cluster_Distance,Distance) %>%
      tidyr::drop_na() %>%
      mutate(Speed=round(Distance/Time,4))
    
    if(nrow(mat_speed_cluster_distance)>0){
      mat_speed_cluster_distance <- mat_speed_cluster_distance %>%
        select(Cheval,Cluster_Distance,Speed) %>%
        pivot_wider(
          names_from = Cluster_Distance,
          values_from = Speed,
          values_fn = list(Speed = mean)) %>%
        as.data.frame()
      vec_col_speed_keep <- colnames(mat_speed_cluster_distance)[apply(mat_speed_cluster_distance,2,function(x){sum(!is.na(x))})>4]
      if(length(vec_col_speed_keep)>1){
        mat_speed_cluster_distance <- mat_speed_cluster_distance[,vec_col_speed_keep]
        if(nrow(mat_speed_cluster_distance)>0){
          for(idx_col in 2:ncol(mat_speed_cluster_distance))
          {
            if(sum(is.na(mat_speed_cluster_distance[,idx_col]))>0) {
              mat_speed_cluster_distance[is.na(mat_speed_cluster_distance[,idx_col]),idx_col] <- median(mat_speed_cluster_distance[!is.na(mat_speed_cluster_distance[,idx_col]),idx_col])
            }
            mat_speed_cluster_distance[,idx_col] <- round(get_percentile_values( mat_speed_cluster_distance[,idx_col]),4)
          }
        }
      }
    }
  }
  
  if(!is.null(mat_speed_cluster_distance)) {
    if(ncol(mat_speed_cluster_distance)>1) {
      colnames(mat_speed_cluster_distance)[2:ncol(mat_speed_cluster_distance)] <- paste("AVERAGE_SPEED_DISTANCE",colnames(mat_speed_cluster_distance)[2:ncol(mat_speed_cluster_distance)],sep="_")
      colnames(mat_speed_cluster_distance)[2:ncol(mat_speed_cluster_distance)] <- gsub("-","_", colnames(mat_speed_cluster_distance)[2:ncol(mat_speed_cluster_distance)])
    }
  }

  return(mat_speed_cluster_distance)
}

