convert_scores_blue_scale <- function(x) {
  vec_res <- NULL
  
  require(RColorBrewer)
  
  if(length(x)>0) {
    vec_res <- rep(brewer.pal(9,"Blues")[1],length(x))
    test_01 <- x==1
    test_02 <- x==2
    test_03 <- x==3
    test_04 <- x>=4
    if(sum(test_01)>0){
      vec_res [which(test_01==TRUE)] <- brewer.pal(9,"Blues")[3]
    }
    
    if(sum(test_02)>0){
      vec_res [which(test_02==TRUE)] <- brewer.pal(9,"Blues")[5]
    }
    
    if(sum(test_03)>0){
      vec_res [which(test_03==TRUE)] <- brewer.pal(9,"Blues")[7]
    }
    if(sum(test_04)>0){
      vec_res [which(test_04==TRUE)] <- brewer.pal(9,"Blues")[9]
    }
  }
  return(vec_res)
}