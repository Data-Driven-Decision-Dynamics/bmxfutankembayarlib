verify_handcraft_rules_performer_over_several_years_geny <- function(df_infos_target_race_geny = NULL,
                                                                     df_profile_issues_race = NULL,
                                                                     path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"){

  message("Initialization of final output")
  df_infos_performer_over_years <- NULL
  df_candidates_performer_over_years <- data.frame(Cheval = df_infos_target_race_geny$Cheval, CLASS_PERFORMER_OVER_YEARS = 0)
  message("Initialization of final output")

  message("Start computing stats per issue class")
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Trot Monté","Trot Attelé")){

    if(!is.null(df_profile_issues_race)){
      vec_sequences <- sort(unique(df_profile_issues_race$Sequence),decreasing = TRUE)
      if(length(vec_sequences)>0){
        df_infos_performer_over_years <- data.frame(Cheval = df_infos_target_race_geny$Cheval)
        for(sequence in vec_sequences){
          df_infos_performer_over_years[,sequence] <- 0
        }
        for(sequence in vec_sequences)
        {
          df_profile_issues_race_sequence <- df_profile_issues_race[df_profile_issues_race$Sequence == sequence,]
          if("P02" %in% df_profile_issues_race_sequence$ISSUES){
            df_profile_issues_race_sequence_place <- df_profile_issues_race_sequence[df_profile_issues_race_sequence$ISSUES == "P02",]
            if(nrow(df_profile_issues_race_sequence_place)>=6){
              df_profile_issues_race_sequence_place <- df_profile_issues_race_sequence_place[order(df_profile_issues_race_sequence_place$LENGTH_LONGUEST_PATH,decreasing = TRUE),]
              vec_candidate_longuest_sequence_item <- df_profile_issues_race_sequence_place$Cheval[1:ceiling(nrow(df_profile_issues_race_sequence_place)/2)]
              df_profile_issues_race_sequence_place <- df_profile_issues_race_sequence_place[order(df_profile_issues_race_sequence_place$PERCENT_SUCCESS,decreasing = TRUE),]
              vec_candidate_percent_success_item <- df_profile_issues_race_sequence_place$Cheval[1:ceiling(nrow(df_profile_issues_race_sequence_place)/2)]
              df_profile_issues_race_sequence_place <- df_profile_issues_race_sequence_place[order(df_profile_issues_race_sequence_place$ENGAGEMENT,decreasing = TRUE),]
              vec_candidate_highest_engagements_item <- df_profile_issues_race_sequence_place$Cheval[1:ceiling(nrow(df_profile_issues_race_sequence_place)/2)]
              freq_candidates_consolidated_sequence <- table(c(vec_candidate_longuest_sequence_item,vec_candidate_percent_success_item,vec_candidate_highest_engagements_item))
              if(max(freq_candidates_consolidated_sequence)>=3){
                vec_candidates_consolidated_sequence <- names(freq_candidates_consolidated_sequence)[freq_candidates_consolidated_sequence>=3]
                df_infos_performer_over_years[df_infos_performer_over_years$Cheval %in% vec_candidates_consolidated_sequence , sequence] <- 1
              }
            }
          }
        }
      }
      if("S1" %in% colnames(df_infos_performer_over_years)){
        if(max(df_infos_performer_over_years$S1) > 0){
          vec_candidate_performers_s1 <- df_infos_performer_over_years$Cheval[df_infos_performer_over_years$S1 > 0]
          df_candidates_performer_over_years [df_candidates_performer_over_years$Cheval %in% vec_candidate_performers_s1, "CLASS_PERFORMER_OVER_YEARS"] <- 1
        }
      }

      if(length(setdiff(colnames(df_infos_performer_over_years),"S1"))>= length(vec_sequences)-1){
        freq_others_sequences <- apply(df_infos_performer_over_years[,intersect(colnames(df_infos_performer_over_years),setdiff(c("S1","S2","S3","S4"),"S1")),drop=FALSE],1,sum)
        if(max(freq_others_sequences)>=2){
          vec_candidate_performers_others_sequences <- df_infos_performer_over_years$Cheval[freq_others_sequences>=2]
          df_candidates_performer_over_years [df_candidates_performer_over_years$Cheval %in% vec_candidate_performers_others_sequences, "CLASS_PERFORMER_OVER_YEARS"] <- 1
        }
      }
    }
  }

  return(df_candidates_performer_over_years)
}
