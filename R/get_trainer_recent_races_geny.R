get_trainer_recent_races_geny <- function(df_infos_target_race_geny = NULL,
                                          target_trainer = NULL,
                                          path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                          number_days_form = 15){
  
  message("Start reading historical race infos file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)) {
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Finish reading historical race infos file")
  
  message("Start extracting race date and convert in the right format if need")
  if(class(unique(df_infos_target_race_geny$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race_geny$Date)
  }
  message("Finish extracting race date and convert in the right format if need")
  
  message("Start filtering trainer recent races")
  df_infos_recent_races_trainer <- df_historical_races_geny %>%
    filter(Entraineur == target_trainer) %>%
    filter(Date<current_race_date) %>%
    filter(current_race_date-Date<=number_days_form) %>%
    as.data.frame()
  if(nrow(df_infos_recent_races_trainer)>0){
    vec_potential_columns <- c("Cheval", "Details","Place","Gains","Prix","Date","Race",rev(grep("Cot",colnames(df_infos_recent_races_trainer),value=TRUE))[1],"Entraineur")
    df_infos_recent_races_trainer <- df_infos_recent_races_trainer [,vec_potential_columns] %>%
      distinct() %>%
      as.data.frame()
    colnames(df_infos_recent_races_trainer) <- sub(grep("Cot",colnames(df_infos_recent_races_trainer),value=TRUE),"ODDS",colnames(df_infos_recent_races_trainer))
    if("ODDS" %in% colnames(df_infos_recent_races_trainer)){
      df_infos_recent_races_trainer$ODDS <- as.numeric(df_infos_recent_races_trainer$ODDS)
    }
  }
  message("Finish filtering trainer recent races")
 
  return(df_infos_recent_races_trainer)
  
}