get_feature_advantage_gender_geny <- function(df_infos_target_race = NULL) {
  
  df_infos_adv_cumul_diff_gender <- NULL
  df_infos_adv_cumul_diff_gender <- df_infos_target_race[,c("Cheval","Gender"),drop=FALSE]
  df_infos_adv_cumul_diff_gender$Gender <- gsub("H","2",df_infos_adv_cumul_diff_gender$Gender)
  df_infos_adv_cumul_diff_gender$Gender <- gsub("F","0",df_infos_adv_cumul_diff_gender$Gender)
  df_infos_adv_cumul_diff_gender$Gender <- gsub("M","2",df_infos_adv_cumul_diff_gender$Gender)
  df_infos_adv_cumul_diff_gender$Gender <- as.numeric( df_infos_adv_cumul_diff_gender$Gender)
  df_infos_adv_cumul_diff_gender$"ENGAGEMENT_SIGNAL_CUMUL_ADVANTAGE_GENDER" <- 0
  for(idx in 1:nrow(df_infos_adv_cumul_diff_gender))
  {
    sum_delta_gender <- sum(df_infos_adv_cumul_diff_gender[idx,"Gender"]-df_infos_adv_cumul_diff_gender[-idx,"Gender"])
    df_infos_adv_cumul_diff_gender[idx,"ENGAGEMENT_SIGNAL_CUMUL_ADVANTAGE_GENDER"] <- sum_delta_gender
  }
  df_infos_adv_cumul_diff_gender <- df_infos_adv_cumul_diff_gender[,setdiff(colnames(df_infos_adv_cumul_diff_gender),"Gender")]
  return(df_infos_adv_cumul_diff_gender)
}


# get_feature_advantage_gender_geny <- function(df_infos_target_race = NULL,
#                                               path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
#                                               number_days_back=720){
# 
#   message("Reading historical performance file")
#   if(!exists("df_historical_races_geny")) {
#     if(file.exists(path_df_historical_races_geny)){
#       df_historical_races_geny <- readRDS(path_df_historical_races_geny)
#     }
#   }
#   message("Reading historical performance file")
# 
#   message("Extracting race date and convert in the right format if need")
#   if(class(unique(df_infos_target_race$Date))!="Date") {
#     current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
#   } else {
#     current_race_date <- unique(df_infos_target_race$Date)
#   }
# 
#   message("Extracting name of current candidates")
#   current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))
# 
#   message("Extracting discipline of current race")
#   current_race_discipline <- unique(df_infos_target_race$Discipline)
# 
#   message("Extracting hippodrome of current race")
#   current_race_location <- as.vector(unique(df_infos_target_race$Lieu))
# 
#   df_infos_earning <- df_historical_races_geny %>%
#     dplyr::filter(Lieu %in% current_race_location) %>%
#     filter(Discipline == current_race_discipline) %>%
#     filter(Date<current_race_date) %>%
#     filter(current_race_date-Date<=number_days_back) %>%
#     select(Gender,Gains,Distance,Place) %>%
#     drop_na() %>%
#     group_by(Gender) %>%
#     dplyr::summarise(NUMBER_RACES_GENDER=n(),
#                      NUMBER_PODIUM_GENDER = get_number_top_five(Place)) %>%
#     as.data.frame()
# 
# 
# 
# }