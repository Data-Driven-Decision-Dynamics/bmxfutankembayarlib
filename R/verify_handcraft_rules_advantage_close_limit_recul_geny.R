verify_handcraft_rules_advantage_close_limit_recul_geny <- function(df_infos_target_race_geny = NULL,
                                                                    path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                                    number_days_back = 366){

  
  message("Initialization of final output")
  df_candidates_close_limit_recul <- NULL
  message("Initialization of final output")
  
  if(length(unique(df_infos_target_race_geny$Recul))>1){
    message("Initialization of final output")
    vec_candidate_advantage_limit_recul <- NULL
    vec_candidates_higher_mean_earning <- NULL
    df_candidates_close_limit_recul <- data.frame(Cheval = df_infos_target_race_geny$Cheval,ENGAGEMENT_CLOSE_LIMIT_RECUL = 0) 
    message("Initialization of final output")
    message("Computing engagement signal in case of recul")
    if(unique(df_infos_target_race_geny$Discipline) %in% c("Trot Monté","Trot Attelé")){
      message("Computing number of races for each competitor")
      df_number_races <- df_historical_races_geny %>%
        filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
        filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
        filter(Date <= unique(df_infos_target_race_geny$Date)) %>%
        group_by(Cheval) %>%
        dplyr::summarise(NUMBER_RACES = n(),MEAN_EARNING = mean(Gains)) %>%
        as.data.frame()
      if(nrow(df_number_races)>0){
        df_number_races <- df_number_races[order(df_number_races$MEAN_EARNING,decreasing = TRUE),]
        vec_candidates_higher_mean_earning <- df_number_races$Cheval[1:ceiling(nrow(df_number_races)/2)]
      }
      message("Computing number of races for each competitor")
      
      message("Testing if there is candidate close to the limit of participation and that are younger")
      if(length(unique(df_infos_target_race_geny$Recul))>1){
        df_infos_race_limit <- get_race_maximum_limit_participation_geny(df_infos_target_race_geny[1,])
        if(!is.null(df_infos_race_limit)){
          if("LIMIT_01" %in% colnames(df_infos_race_limit)){
            current_race_limit_recul <- df_infos_race_limit$LIMIT_01
            df_infos_target_race_geny$Age <- as.numeric(df_infos_target_race_geny$Age)
            if(!is.na(current_race_limit_recul)){
              df_infos_target_race_geny$Cumul_Gains <- as.numeric(df_infos_target_race_geny$Cumul_Gains)
              test_close_limit_first_line <- df_infos_target_race_geny$Recul == 0 & (df_infos_target_race_geny$Cumul_Gains - 0.95 * current_race_limit_recul)>0
              if(sum(test_close_limit_first_line,na.rm = TRUE)>0){
                vec_candidate_advantage_limit_recul <- df_infos_target_race_geny[df_infos_target_race_geny$Cheval %in% df_infos_target_race_geny$Cheval [which(test_close_limit_first_line==TRUE)],"Cheval"]
                vec_candidate_advantage_limit_recul <- intersect(vec_candidate_advantage_limit_recul,vec_candidates_higher_mean_earning)
                if(length(vec_candidate_advantage_limit_recul)==0){
                  vec_candidate_advantage_limit_recul <- NULL
                }
              }
            }
          }
        }
      }
      message("Testing if there is candidate close to the limit of participation and that are younger")
      
      message("Filling the indicator value")
      if(length(vec_candidate_advantage_limit_recul)>0){
        df_candidates_close_limit_recul[df_candidates_close_limit_recul$Cheval %in% vec_candidate_advantage_limit_recul,"ENGAGEMENT_CLOSE_LIMIT_RECUL"] <- 1
      }
      message("Filling the indicator value")
    }
    
    message("Initialization of final output")
  }
    
  return(df_candidates_close_limit_recul)
}
