get_track_complexity_based_same_horse_comparable_track_geny <- function(path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                                        path_infos_tracks = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/Hippodrome-Master-Data.xlsx",
                                                                        path_df_detailled_ranking_tracks = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_infos_detailed_ranking_tracks_geny.xlsx",
                                                                        path_df_ranking_tracks = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_ranking_tracks_geny.rds"){
  
  require(magrittr)
  require(dplyr)
  require(tidyr)
  require(igraph)
  
  df_rank_tracks <- NULL
  df_rank_tracks_discipline_compiled <- NULL
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Reading historical performance file")
  
  message("Computing intermediary data")
  df_infos_delta_compiled <- get_delta_reduction_same_horse_comparable_track_geny()
  df_delta_compiled <- df_infos_delta_compiled$Formated
  df_detailed_delta_compiled <- df_infos_delta_compiled$Details
  xlsx::write.xlsx(df_detailed_delta_compiled,path_df_detailled_ranking_tracks)
  message("Computing intermediary data")
  
  message("Reading master file of track")
  df_master_data_tracks <- xlsx::read.xlsx(path_infos_tracks,sheetIndex = 1)
  df_master_data_tracks <- df_master_data_tracks[,c("Hippodrome","Discipline","Longueur","Ligne.Droite","Largeur.piste","Commentaire")]
  colnames(df_master_data_tracks) <- sub("Ligne.Droite","Droite",colnames(df_master_data_tracks))
  colnames(df_master_data_tracks) <- sub("Largeur.piste","Largeur",colnames(df_master_data_tracks))
  df_master_data_tracks$Longueur <- as.numeric(df_master_data_tracks$Longueur)
  df_master_data_tracks$Largeur <- as.numeric(df_master_data_tracks$Largeur)
  df_master_data_tracks$Droite <- as.numeric(df_master_data_tracks$Droite)
  message("Reading master file of track")
  
  vec_found_discipline <- unique(df_delta_compiled$Discipline)
  for(current_discipline in vec_found_discipline)
  {
    df_infos_focus_discipline <- df_historical_races_geny[df_historical_races_geny$Discipline == current_discipline,]
    
    if(current_discipline == "Trot Attelé"){
      message("Computing record per track and level")
      df_records_track_discipline <- df_infos_focus_discipline %>%
        select(Cheval,Lieu,Terrain,Longueur,Depart,Speed,Place,Date) %>%
        tidyr::drop_na() %>%
        filter(Place == 1) %>%
        group_by(Lieu,Terrain,Longueur,Depart) %>%
        top_n(1,Speed) %>%
        group_by(Lieu,Terrain,Longueur,Depart) %>%
        top_n(1,Date) %>%
        mutate(Track = paste(Lieu,Terrain,Longueur,Depart,sep="_")) %>%
        as.data.frame() %>%
        select(Track,Speed)
      colnames(df_records_track_discipline) <- sub("Speed","Record",colnames(df_records_track_discipline))
      message("Computing record per track and level")
      
      message("Computing standard times per track and level")
      df_standards_track_discipline <- df_infos_focus_discipline %>%
        select(Cheval,Lieu,Terrain,Longueur,Depart,Epreuve,Time,Place,Date) %>%
        tidyr::drop_na() %>%
        mutate(Track = paste(Lieu,Terrain,Longueur,Depart,sep="_")) %>%
        group_by(Track) %>%
        dplyr::summarise(Standard = get_track_standard_times(Time)) %>%
        as.data.frame()
      message("Computing standard times per track and level")
    } else {
      message("Computing record per track and level")
      df_records_track_discipline <- df_infos_focus_discipline %>%
        select(Cheval,Lieu,Terrain,Longueur,Speed,Place,Date) %>%
        tidyr::drop_na() %>%
        filter(Place == 1) %>%
        group_by(Lieu,Terrain,Longueur) %>%
        top_n(1,Speed) %>%
        group_by(Lieu,Terrain,Longueur) %>%
        top_n(1,Date) %>%
        mutate(Track = paste(Lieu,Terrain,Longueur,sep="_")) %>%
        as.data.frame() %>%
        select(Track,Speed)
      colnames(df_records_track_discipline) <- sub("Speed","Record",colnames(df_records_track_discipline))
      message("Computing record per track and level")
      
      message("Computing standard times per track and level")
      df_standards_track_discipline <- df_infos_focus_discipline %>%
        select(Cheval,Lieu,Terrain,Longueur,Epreuve,Time,Place,Date) %>%
        tidyr::drop_na() %>%
        mutate(Track = paste(Lieu,Terrain,Longueur,sep="_")) %>%
        group_by(Track) %>%
        dplyr::summarise(Standard = get_track_standard_times(Time)) %>%
        as.data.frame()
      message("Computing standard times per track and level")
    }
    
    message("Computing pagerank score per discipline and ground")
    df_delta_compiled_discipline <- df_delta_compiled %>%
      filter(Discipline == current_discipline) %>%
      as.data.frame()
    
    if(nrow(df_delta_compiled_discipline)>=10){
      # if(current_discipline != "Trot Attelé"){
      #   vec_location <- unlist(lapply(df_delta_compiled_discipline$from,function(x){unlist(strsplit(x,"_"))[1]}))
      #   vec_distance <- unlist(lapply(df_delta_compiled_discipline$from,function(x){unlist(strsplit(x,"_"))[2]}))
      #   df_delta_compiled_discipline$from <- paste(vec_location,vec_distance,sep="_")
      #   vec_location <- unlist(lapply(df_delta_compiled_discipline$to,function(x){unlist(strsplit(x,"_"))[1]}))
      #   vec_distance <- unlist(lapply(df_delta_compiled_discipline$to,function(x){unlist(strsplit(x,"_"))[2]}))
      #   df_delta_compiled_discipline$to <- paste(vec_location,vec_distance,sep="_")
      # }
      df_delta_compiled_discipline <- unique(df_delta_compiled_discipline)
      df_rank_tracks_discipline <- page_rank(graph_from_data_frame(df_delta_compiled_discipline , directed=TRUE))$vector
      df_rank_tracks_discipline <- as.data.frame(df_rank_tracks_discipline)
      colnames(df_rank_tracks_discipline) <- "Score"
      df_rank_tracks_discipline$Track <- rownames(df_rank_tracks_discipline)
      df_rank_tracks_discipline <- df_rank_tracks_discipline[,rev(colnames(df_rank_tracks_discipline))]
      df_rank_tracks_discipline <- df_rank_tracks_discipline[order(df_rank_tracks_discipline$Score,decreasing = TRUE),]
      df_rank_tracks_discipline$Rank <- 1:nrow(df_rank_tracks_discipline)
      df_rank_tracks_discipline <- df_rank_tracks_discipline[,c("Rank","Track","Score")]
      val_mean_rank_discipline <- mean(df_rank_tracks_discipline$Score)
      val_sd_rank_discipline <- sd(df_rank_tracks_discipline$Score)
      df_rank_tracks_discipline$Score <- (df_rank_tracks_discipline$Score-val_mean_rank_discipline)/val_sd_rank_discipline
      df_rank_tracks_discipline <- merge(df_rank_tracks_discipline,df_records_track_discipline,by.x="Track",by.y="Track",all.x=TRUE)
      df_rank_tracks_discipline <- merge(df_rank_tracks_discipline,df_standards_track_discipline,by.x="Track",by.y="Track",all.x=TRUE)
      df_rank_tracks_discipline <- unique(df_rank_tracks_discipline)
      df_rank_tracks_discipline$Discipline <- current_discipline
      df_rank_tracks_discipline_compiled <- rbind.fill(df_rank_tracks_discipline_compiled,df_rank_tracks_discipline)
    }
  }
  
  if(!is.null(df_rank_tracks_discipline_compiled)){
    df_rank_tracks_discipline_compiled$Location <-  unlist(lapply(df_rank_tracks_discipline_compiled$Track,function(x) {unlist(strsplit(x,"_"))[1]}))
  }
  
  if(!is.null(df_rank_tracks_discipline_compiled)){
    df_rank_tracks_discipline_compiled$Longueur <-  NA
    df_rank_tracks_discipline_compiled$Largeur <-  NA
    df_rank_tracks_discipline_compiled$Droite <-  NA
    vec_location_found <- unique(df_rank_tracks_discipline_compiled$Location)
    for(idx_loc in vec_location_found)
    {
      if(idx_loc %in% df_master_data_tracks$Hippodrome){
        df_master_data_current_track <- df_master_data_tracks[which(df_master_data_tracks$Hippodrome == idx_loc), ]
        df_master_data_current_track <- df_master_data_current_track[df_master_data_current_track$Discipline == "Trot",]
        df_master_data_current_track <- df_master_data_current_track[1,]
        df_rank_tracks_discipline_compiled[df_rank_tracks_discipline_compiled$Location == idx_loc, "Longueur"] <- df_master_data_current_track$Longueur
        df_rank_tracks_discipline_compiled[df_rank_tracks_discipline_compiled$Location == idx_loc, "Largeur"] <- df_master_data_current_track$Largeur
        df_rank_tracks_discipline_compiled[df_rank_tracks_discipline_compiled$Location == idx_loc, "Droite"] <- df_master_data_current_track$Droite
      }
    }
  }
  
  if(!is.null(df_rank_tracks_discipline_compiled)){
    df_rank_tracks_discipline_compiled$Turn <- NA
    df_rank_tracks_discipline_compiled$Ground <- NA
    vec_found_locations <- unique(df_rank_tracks_discipline_compiled$Location)
    for(location in vec_found_locations)
    {
      vec_found_discipline <- unique(df_rank_tracks_discipline_compiled[df_rank_tracks_discipline_compiled$Location ==location,"Discipline"])
      for(discipline in vec_found_discipline){
        vec_derived_turn <- NA
        vec_derived_ground <- NA
        if(length(which(df_rank_tracks_discipline_compiled$Location == location & df_rank_tracks_discipline_compiled$Discipline ==discipline ))>0){
          df_infos_ground_turn <- df_historical_races_geny %>%
            filter(Lieu == location ) %>%
            filter(Discipline == discipline ) %>%
            select(Corde,Terrain) %>%
            as.data.frame()
          if(nrow(df_infos_ground_turn)>0){
            vec_derived_turn <- names(which.max(table(df_infos_ground_turn$Corde)))
            vec_derived_ground <- names(which.max(table(df_infos_ground_turn$Terrain)))
          }
          if(length(vec_derived_turn)>0){
            df_rank_tracks_discipline_compiled[df_rank_tracks_discipline_compiled$Location == location & df_rank_tracks_discipline_compiled$Discipline == discipline,"Turn"] <- vec_derived_turn
          }
          if(length(vec_derived_ground)>0){
            df_rank_tracks_discipline_compiled[df_rank_tracks_discipline_compiled$Location == location & df_rank_tracks_discipline_compiled$Discipline == discipline,"Ground"] <- vec_derived_ground
          }
        }
      }
    }
  }
  
  if(!is.null(df_rank_tracks_discipline_compiled)){
    if(nrow(df_rank_tracks_discipline_compiled)>0){
      df_rank_tracks_discipline_compiled$Benchmark <- NA
      for(idx in 1:nrow(df_rank_tracks_discipline_compiled))
      {
        discipline_track_current <- df_rank_tracks_discipline_compiled[idx,"Discipline"]
        record_track_current <- df_rank_tracks_discipline_compiled[idx,"Record"]
        lab_track_current <- df_rank_tracks_discipline_compiled[idx,"Track"]
        comps_lab_track_current <- unlist(strsplit(lab_track_current,"_"))
        location_track_current <- comps_lab_track_current[1]
        ground_track_current <- comps_lab_track_current[2]
        distance_track_current <- as.numeric(comps_lab_track_current[3])
        if(length(comps_lab_track_current) == 4){
          depart_track_current <- comps_lab_track_current[4]
        }
        df_rank_tracks_current_discipline <- df_rank_tracks_discipline_compiled[df_rank_tracks_discipline_compiled[,"Discipline"] == discipline_track_current,]
        df_rank_tracks_current_discipline_ground <- df_rank_tracks_current_discipline[grep(ground_track_current,df_rank_tracks_current_discipline$Ground),]
        if(length(comps_lab_track_current) == 4){
          df_rank_tracks_current_discipline_ground_depart <- df_rank_tracks_current_discipline_ground[grep(depart_track_current,df_rank_tracks_current_discipline_ground$Track),]
          vec_delta_distance_track_current <- as.numeric(unlist(lapply(df_rank_tracks_current_discipline_ground_depart$Track,function(x){unlist(strsplit(x,"_"))[3]})))
          vec_delta_distance_track_current <- abs(distance_track_current-vec_delta_distance_track_current) <= 150
          if(sum(vec_delta_distance_track_current)>0){
            df_rank_tracks_current_discipline_ground_depart_distance <- df_rank_tracks_current_discipline_ground_depart[vec_delta_distance_track_current,]
            df_rank_tracks_current_discipline_ground_depart_distance <- df_rank_tracks_current_discipline_ground_depart_distance[-grep(lab_track_current,df_rank_tracks_current_discipline_ground_depart_distance$Track),]
            df_rank_tracks_discipline_compiled[idx,"Benchmark"] <- mean(df_rank_tracks_current_discipline_ground_depart_distance$Record)
          }
        }
        if(length(comps_lab_track_current) == 3){
          vec_delta_distance_track_current <- as.numeric(unlist(lapply(df_rank_tracks_current_discipline_ground$Track,function(x){unlist(strsplit(x,"_"))[3]})))
          vec_delta_distance_track_current <- abs(distance_track_current-vec_delta_distance_track_current) <= 150
          if(sum(vec_delta_distance_track_current)>0){
            df_rank_tracks_current_discipline_ground_distance <- df_rank_tracks_current_discipline_ground[vec_delta_distance_track_current,]
            df_rank_tracks_current_discipline_ground_distance <- df_rank_tracks_current_discipline_ground_distance[-grep(lab_track_current,df_rank_tracks_current_discipline_ground_distance$Track),]
            df_rank_tracks_discipline_compiled[idx,"Benchmark"] <- mean(df_rank_tracks_current_discipline_ground_distance$Record)
          }
        }
      }
    }
    df_rank_tracks_discipline_compiled$Gap <- ((df_rank_tracks_discipline_compiled$Benchmark-df_rank_tracks_discipline_compiled$Record)/df_rank_tracks_discipline_compiled$Record)*100
    df_rank_tracks_discipline_compiled$Gap <- round(df_rank_tracks_discipline_compiled$Gap,2)
  }
  
  if(!is.null(df_rank_tracks_discipline_compiled)){
    if(nrow(df_rank_tracks_discipline_compiled)>0){
      saveRDS(df_rank_tracks_discipline_compiled,path_df_ranking_tracks)
      xlsx::write.xlsx(df_rank_tracks_discipline_compiled,sub(".rds",".xlsx",path_df_ranking_tracks))
    }
  }
  
  return(df_rank_tracks_discipline_compiled)
}