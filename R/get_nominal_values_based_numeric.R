get_nominal_values_based_numeric <- function(df_initial_infos = NULL , col_process = NULL , col_recode = NULL) {
  
  df_initial_infos[,col_recode] <- NA
  
  vec_all_values_to_recode <- df_initial_infos[,col_process]
  names(vec_all_values_to_recode) <- NULL
  vec_all_values_to_recode_recode <- rep("Low",sum(!is.na(vec_all_values_to_recode)))
  cut_try <- tryCatch(bmxFutankeMbayar::quantcut(vec_all_values_to_recode,4),error = function(e) {print(paste("some issues cutting", "errorr"));NULL})
  if(!is.null(cut_try)) {
    vec_all_values_to_recode_recode <- bmxFutankeMbayar::quantcut(vec_all_values_to_recode,4)
    vec_levels_cut_sum_earning_values <- levels(vec_all_values_to_recode_recode)
    vec_labels_cut_sum_earning_values <- c("Very Low","Low","Moderate","High")
    vec_all_values_to_recode_recode <- as.vector(vec_all_values_to_recode_recode)
    for(level in 1:length(vec_levels_cut_sum_earning_values))
    {
      vec_all_values_to_recode_recode[vec_all_values_to_recode_recode==vec_levels_cut_sum_earning_values[level]] <- vec_labels_cut_sum_earning_values[level]
    }
    df_initial_infos[,col_recode] <- vec_all_values_to_recode_recode
  } else {
    df_initial_infos[,col_recode] <- vec_all_values_to_recode_recode
  }
  return(df_initial_infos)
}