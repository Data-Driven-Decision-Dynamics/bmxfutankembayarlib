#' @return Compute percent on pairwise wins when trainer associated with candidate
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_Entraineur_horse_percent_win_pairwise_comparison_letrot(mat_infos_race_input = NULL)
#' @export
get_horse_trainer_percent_success_geny <- function(df_infos_target_race = NULL,
                                                   path_mat_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                   num_past_years=1/4,
                                                   number_days_back=366) 
{
  
  mat_stats_trainers <- NULL
  
  mat_number_candiates <- as.data.frame(table(df_infos_target_race$Entraineur))
  colnames(mat_number_candiates) <- c("Entraineur","BELONG_ECURIE")
  mat_number_candiates$BELONG_ECURIE <- ifelse(mat_number_candiates$BELONG_ECURIE>1,1,0)
  
  current_race_horses <- unique(df_infos_target_race$Cheval)
  current_race_trainers <- unique(df_infos_target_race$Entraineur)
  current_race_discipline <- unique(df_infos_target_race$Discipline)
  current_race_location <- as.vector(unique(df_infos_target_race$Lieu))
  
  if(class(unique(df_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race$Date)
  }
  
  # Reading historical race infos file
  if(!exists("mat_historical_races_geny")) {
    if(file.exists(path_mat_historical_races_geny)) {
      mat_historical_races_geny <- readRDS(path_mat_historical_races_geny)
    }
  }
  
  message("Some filtering to focus on relevant races")
  mat_infos_historical_races_focus <- mat_historical_races_geny %>%
    filter(Entraineur %in% current_race_trainers) %>%
    filter(Discipline==current_race_discipline) %>%
    filter(Date<current_race_date) %>%
    filter(current_race_date-Date<=number_days_back) %>%
    as.data.frame()
  
  if(nrow(mat_infos_historical_races_focus)>0){
    mat_infos_historical_races_focus$Entraineur <- as.vector(mat_infos_historical_races_focus$Entraineur)
    mat_infos_historical_races_focus$Driver <- as.vector(mat_infos_historical_races_focus$Driver)
  }
  
  mat_number_race_trainer <- mat_infos_historical_races_focus %>%
    group_by(Entraineur) %>%
    dplyr::summarise(NUMBER_RACES_TRAINER=n()) %>%
    dplyr::select(Entraineur,NUMBER_RACES_TRAINER) %>%
    as.data.frame()
  
  mat_number_race_location_trainer <- mat_infos_historical_races_focus %>%
    group_by(Entraineur) %>%
    filter(Lieu==current_race_location) %>%
    dplyr::summarise(NUMBER_RACES_LOCATION_TRAINER=n(),FREQUENCY_PLACE_LOCATION_TRAINER=get_number_podium(Place),PERCENT_PODIUM_LOCATION_TRAINER=round(1*(FREQUENCY_PLACE_LOCATION_TRAINER/NUMBER_RACES_LOCATION_TRAINER),2)) %>%
    dplyr::select(Entraineur,NUMBER_RACES_LOCATION_TRAINER,PERCENT_PODIUM_LOCATION_TRAINER) %>%
    as.data.frame()
  
  if(nrow(mat_number_race_trainer)>0 & nrow(mat_number_race_location_trainer)>0 ){
    mat_number_race_trainer <- merge(mat_number_race_location_trainer,mat_number_race_trainer,by.x="Entraineur",by.y="Entraineur",all=TRUE) 
    mat_number_race_trainer <- mat_number_race_trainer %>%
      mutate(PERCENTAGE_RACES_LOCATION_TRAINER=round(NUMBER_RACES_LOCATION_TRAINER/NUMBER_RACES_TRAINER,2),
             NUMBER_RACES_LOCATION_TRAINER = NUMBER_RACES_LOCATION_TRAINER/sum(NUMBER_RACES_LOCATION_TRAINER,na.rm = TRUE),
             NUMBER_RACES_TRAINER = NUMBER_RACES_TRAINER/sum(NUMBER_RACES_TRAINER,na.rm = TRUE)
      )
  }
  
  if(nrow(mat_number_race_trainer)>0){
    mat_number_race_trainer %>%
      mutate(NUMBER_RACES_TRAINER = NUMBER_RACES_TRAINER)
    mat_number_race_trainer[,-1] <- round(mat_number_race_trainer[,-1] ,2)
  }
  
  mat_infos_historical_races_focus <- mat_historical_races_geny[as.numeric(current_race_date-mat_historical_races_geny$Date)<=(366*num_past_years) & mat_historical_races_geny$Entraineur %in% current_race_trainers & mat_historical_races_geny$Discipline==current_race_discipline & mat_historical_races_geny$Date<current_race_date,]
  
  if(sum(is.na(mat_infos_historical_races_focus$Place))>0) {
    mat_infos_historical_races_focus$Place [is.na(mat_infos_historical_races_focus$Place)] <-99
  }
 
  mat_percent_success_trainers <- mat_infos_historical_races_focus %>% 
    group_by(Entraineur) %>%
    dplyr::summarize(N=n(),Size=get_number_top_five(Place))%>%
    mutate(PERCENT_SUCCESS_TRAINER=round((Size/N),2)) %>%
    as.data.frame()

  if(nrow(mat_percent_success_trainers)<nrow(df_infos_target_race)){
    mat_percent_success_trainers <- merge(mat_percent_success_trainers,df_infos_target_race[,"Entraineur",drop=FALSE],by.x="Entraineur",by.y="Entraineur")
  }
  
  if(nrow(mat_percent_success_trainers)>0) {
    mat_percent_success_trainers <- unique(mat_percent_success_trainers)
  }
  
  if(nrow(mat_percent_success_trainers)>0) {
    mat_percent_success_trainers$Entraineur <- as.vector(mat_percent_success_trainers$Entraineur)
  }

  mat_percent_success_trainers <- merge(mat_percent_success_trainers,mat_number_race_trainer,by.x="Entraineur",by.y="Entraineur",all=TRUE)
  
  if(nrow(mat_percent_success_trainers)>0){
    if("Driver" %in% colnames(df_infos_target_race)){
      mat_percent_success_trainers <- merge(mat_percent_success_trainers,df_infos_target_race[,c("Cheval","Entraineur","Driver")],by.x="Entraineur",by.y="Entraineur",all=TRUE)
    }
    if("Jockey" %in% colnames(df_infos_target_race)){
      mat_percent_success_trainers <- merge(mat_percent_success_trainers,df_infos_target_race[,c("Cheval","Entraineur","Jockey")],by.x="Entraineur",by.y="Entraineur",all=TRUE)
    }
    mat_stats_trainers <- merge(mat_percent_success_trainers,mat_number_candiates,by.x="Entraineur",by.y="Entraineur",all=TRUE)
    
    if("Driver" %in% colnames(df_infos_target_race)){
      mat_stats_trainers$TRAINER_DRIVING <- ifelse(mat_stats_trainers$Entraineur==mat_stats_trainers$Driver,1,0)
    }
    
    if("Jockey" %in% colnames(df_infos_target_race)){
      mat_stats_trainers$TRAINER_DRIVING <- ifelse(mat_stats_trainers$Entraineur==mat_stats_trainers$Jockey,1,0)
    }
    
    if("Cheval.x" %in% colnames(mat_stats_trainers)){
      colnames(mat_stats_trainers)[colnames(mat_stats_trainers)=="Cheval.x"] <- "Cheval"
    }
    
    mat_stats_trainers <- mat_stats_trainers [,intersect(c("Cheval","BELONG_ECURIE","TRAINER_DRIVING","PERCENT_PODIUM_LOCATION_TRAINER","NUMBER_RACES_LOCATION_TRAINER","NUMBER_RACES_TRAINER", "PERCENTAGE_RACES_LOCATION_TRAINER" ,"PERCENT_SUCCESS_TRAINER"),colnames(mat_stats_trainers))]
  } else {
    if("Driver" %in% colnames(df_infos_target_race)){
      mat_stats_trainers <- merge(mat_number_candiates,df_infos_target_race[,c("Cheval","Entraineur","Driver")],by.x="Entraineur",by.y="Entraineur",all=TRUE)
    }
    if("Jockey" %in% colnames(df_infos_target_race)){
      mat_stats_trainers <- merge(mat_number_candiates,df_infos_target_race[,c("Cheval","Entraineur","Jockey")],by.x="Entraineur",by.y="Entraineur",all=TRUE)
    }
    
    mat_stats_trainers$Same <- ifelse(mat_stats_trainers$Entraineur==mat_stats_trainers$Driver,1,0)
    mat_stats_trainers <- mat_stats_trainers [,intersect(c("Cheval","BELONG_ECURIE","TRAINER_DRIVING","PERCENT_PODIUM_LOCATION_TRAINER","NUMBER_RACES_LOCATION_TRAINER","NUMBER_RACES_TRAINER", "PERCENTAGE_RACES_LOCATION_TRAINER" ,"PERCENT_SUCCESS_TRAINER"),colnames(mat_stats_trainers))]
  }
  
  if(!is.null(mat_stats_trainers)){
    if(nrow(mat_stats_trainers)>0){
      mat_stats_trainers <- unique(mat_stats_trainers)
    }
  }

  return(mat_stats_trainers)
}
