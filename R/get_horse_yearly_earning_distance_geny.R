get_horse_yearly_earning_distance_geny <- function(df_infos_target_race_geny = NULL,
                                                   path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                   path_cluster_distance_range = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/mat_ranges_cluster_distance.rds") {
  
  df_gains_yearly_distance <- NULL
  
  message("Extract race date and convert in the right format if need")
  if(class(unique(df_infos_target_race_geny$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race_geny$Date)
  }

  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race_geny$Discipline)

  message("Extract corde of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
  
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
  
  message("Extract terrain of current race")
  current_race_horses <- as.vector(unique(df_infos_target_race_geny$Cheval))
  
  message("Reading file with cluster distance definition")
  df_cluster_distance_range <- readRDS(path_cluster_distance_range)
  df_cluster_distance_range_discipline <- df_cluster_distance_range[df_cluster_distance_range$Discipline==current_race_discipline,]
  
  message("Reading historical race infos file")
  if (!exists("df_historical_races_geny")) {
    if (file.exists(path_df_historical_races_geny)) {
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  if(class(df_historical_races_geny$Date)!="Date"){
    df_historical_races_geny$Date <- as.Date(df_historical_races_geny$Date)
  }
  
  df_historical_races_geny_focus <- df_historical_races_geny %>%
    mutate(Age=as.numeric(Age)) %>%
    filter(Cheval %in% current_race_horses) %>%
    filter(Discipline == current_race_discipline) %>%
    filter(Date<current_race_date) %>%
    mutate(Year = year(Date) ) %>%
    select(Cheval,Gains,Year,Distance) %>%
    group_by(Distance,Year) %>%
    as.data.frame()

  if(nrow(df_historical_races_geny_focus)>0){
    
    df_historical_races_geny_focus$Cluster_Distance <- NA
    
    for(i in 1:nrow(df_cluster_distance_range_discipline))
    {
      idx_cluster_distance_group <- df_cluster_distance_range_discipline[i,"Lower"] <= unlist(df_historical_races_geny_focus[,"Distance"]) & df_cluster_distance_range_discipline[i,"Upper"] >= unlist(df_historical_races_geny_focus[,"Distance"])
      df_historical_races_geny_focus[idx_cluster_distance_group==TRUE,"Cluster_Distance"] <- paste(df_cluster_distance_range_discipline[i,c("Lower","Upper")],collapse = "-")
    } 
    
    df_gains_yearly_distance <- NULL
    
    if(nrow(df_historical_races_geny_focus)>0){
      for(current_horse in current_race_horses){
        df_gains_yearly_distance_current_horse <- df_historical_races_geny_focus[,c("Cheval","Year","Gains","Cluster_Distance")] %>%
          filter(Cheval == current_horse) %>%
          select("Year","Gains","Cluster_Distance") %>%
          tidyr::pivot_wider(
            names_from = Cluster_Distance,
            values_from = Gains,
            values_fn = mean
          ) %>%
          mutate(Cheval=current_horse) %>%
          as.data.frame()
        
        if(nrow(df_gains_yearly_distance_current_horse)>0){
          df_gains_yearly_distance <- rbind.fill(df_gains_yearly_distance,df_gains_yearly_distance_current_horse)
        }
      } 
    }
    
    vec_colums_ordered <- c("Cheval","Year",setdiff(colnames(df_gains_yearly_distance), c("Cheval","Year")))
    df_gains_yearly_distance <- df_gains_yearly_distance[,vec_colums_ordered]
    vec_start_distance <- rev(colnames(df_gains_yearly_distance)[-c(1,2)][rank(unlist(lapply(colnames(df_gains_yearly_distance)[-c(1,2)],function(x){unlist(strsplit(x,"-"))[1]})))])
    vec_colums_ordered <- c("Cheval","Year",vec_start_distance)
    df_gains_yearly_distance <- df_gains_yearly_distance[,vec_colums_ordered]
    
  }
 
  return(df_gains_yearly_distance)
}

