#' @return Compute percent on pairwise Wins
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_percent_win_pairwise_comparison_letrot(mat_infos_race_input = NULL)
#' @export
get_feature_percent_win_pairwise_comparison_geny <- function(df_infos_target_race_geny = NULL,
                                                             path_df_historical_line_by_line_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/pairwise_comparisons/mat_line_by_line_rating_geny.rds",
                                                             num_past_years=2,
                                                             filter_distance_run = FALSE,
                                                             filter_fitness=FALSE,
                                                             filter_ferrage_status=FALSE,
                                                             filter_configuration=FALSE,
                                                             threshold_fitness=90) {
  
  df_percent_win_pairwise <- NULL
  df_infos_number_wins <- data.frame(Cheval = df_infos_target_race_geny$Cheval,Number = 0)
  
  vec_horses <- unique(df_infos_target_race_geny$Cheval)
  discipline <- unique(df_infos_target_race_geny$Discipline)
  
  if(class(unique(df_infos_target_race_geny$Date))!="Date") {
    race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
  } else {
    race_date <- unique(df_infos_target_race_geny$Date)
  }  
  
  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
  
  message("Extract hippodrome of current race")
  current_race_location <- as.vector(unique(df_infos_target_race_geny$Lieu))
  
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
  
  message("Extract corde of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
  
  if(!exists("df_historical_line_by_line_geny")) {
    df_historical_line_by_line_geny <- readRDS(path_df_historical_line_by_line_geny)
    df_historical_line_by_line_geny <- df_historical_line_by_line_geny[!is.na(df_historical_line_by_line_geny$Discipline),]
    df_historical_line_by_line_geny <- df_historical_line_by_line_geny[!is.na(df_historical_line_by_line_geny$Looser),]
    df_historical_line_by_line_geny <- df_historical_line_by_line_geny[!is.na(df_historical_line_by_line_geny$Winner),]
  }
  
  if(class(df_historical_line_by_line_geny$Date)!="Date"){
    df_historical_line_by_line_geny$Date <- as.Date(df_historical_line_by_line_geny$Date,origin="1970-01-01")
  }
  
  df_historical_line_by_line_geny_focus <- subset(df_historical_line_by_line_geny, Discipline == discipline)
  df_historical_line_by_line_geny_focus <- df_historical_line_by_line_geny_focus[!is.na(df_historical_line_by_line_geny_focus$Date),]
  df_historical_line_by_line_geny_focus <- df_historical_line_by_line_geny_focus[df_historical_line_by_line_geny_focus$Date < race_date & as.numeric(race_date-df_historical_line_by_line_geny_focus$Date)<=(366*num_past_years),]
  
  if(filter_distance_run == TRUE) {
    df_historical_line_by_line_geny_focus <- df_historical_line_by_line_geny_focus[df_historical_line_by_line_geny_focus$Winner_Distance>=df_historical_line_by_line_geny_focus$Looser_Distance,]
  }
  
  if(filter_fitness == TRUE) {
    df_historical_line_by_line_geny_focus <- df_historical_line_by_line_geny_focus[df_historical_line_by_line_geny_focus$Looser_Day_Since_Last_Race<threshold_fitness,]
  }
  
  if(filter_configuration == TRUE){
    if(sum(df_historical_line_by_line_geny_focus$Terrain == current_race_terrain & df_historical_line_by_line_geny_focus$Corde == current_race_corde,na.rm=TRUE)>0){
      idx_comparison_configuration <- !is.na(df_historical_line_by_line_geny_focus$Terrain) & !is.na(df_historical_line_by_line_geny_focus$Corde) & df_historical_line_by_line_geny_focus$Terrain == current_race_terrain & df_historical_line_by_line_geny_focus$Corde == current_race_corde
      if(sum(idx_comparison_configuration)>0){
        df_historical_line_by_line_geny_focus <- df_historical_line_by_line_geny_focus[idx_comparison_configuration,]
      }
    }
  }
  
  idx_items_compare <- df_historical_line_by_line_geny_focus$Winner %in% vec_horses & df_historical_line_by_line_geny_focus$Looser %in% vec_horses
  if(sum(idx_items_compare)>0) {
    df_historical_line_by_line_geny_focus <- df_historical_line_by_line_geny_focus[idx_items_compare,]
    df_historical_line_by_line_geny_focus <- df_historical_line_by_line_geny_focus[rownames(unique(df_historical_line_by_line_geny_focus[,c("Winner","Looser","Date","Location","Discipline")])),]
    if(nrow(df_historical_line_by_line_geny_focus)>0)
    {
      if(sum(idx_items_compare)) {
        df_percent_win_pairwise <- as.data.frame(matrix(NA,ncol=3,nrow=length(vec_horses)))
        rownames(df_percent_win_pairwise) <- vec_horses
        colnames(df_percent_win_pairwise) <- c("Cheval","ENGAGEMENT_NUMBER_BEATEN_HORSES","ENGAGEMENT_NUMBER_BEATED_HORSES")
        df_percent_win_pairwise$Cheval <- vec_horses
        for(horse in vec_horses) {
          idx_items_Winning <- df_historical_line_by_line_geny_focus$Winner %in% horse
          idx_items_Loosing <- df_historical_line_by_line_geny_focus$Looser %in% horse
          if(sum(idx_items_Winning)>0){
            df_percent_win_pairwise [df_percent_win_pairwise$Cheval == horse,"ENGAGEMENT_NUMBER_BEATEN_HORSES"] <- length(unique(df_historical_line_by_line_geny_focus[idx_items_Winning,"Looser"]))
          }
          if(sum(idx_items_Loosing)>0){
            df_percent_win_pairwise [df_percent_win_pairwise$Cheval == horse,"ENGAGEMENT_NUMBER_BEATED_HORSES"] <- length(unique(df_historical_line_by_line_geny_focus[idx_items_Loosing,"Winner"]))
          }
        }
      }
      
      df_percent_win_pairwise$"ENGAGEMENT_SCORE_NUMBER_BEATED_HORSES" <- df_percent_win_pairwise$ENGAGEMENT_NUMBER_BEATEN_HORSES-df_percent_win_pairwise$ENGAGEMENT_NUMBER_BEATED_HORSES
      df_percent_win_pairwise$"ENGAGEMENT_SCORE_NUMBER_BEATED_HORSES" <- round(df_percent_win_pairwise$"ENGAGEMENT_SCORE_NUMBER_BEATED_HORSES"/nrow(df_infos_target_race_geny),2)
      df_percent_win_pairwise <- df_percent_win_pairwise[,c("Cheval","ENGAGEMENT_SCORE_NUMBER_BEATED_HORSES")]
      
      if(!is.null(df_percent_win_pairwise)){
        df_percent_win_pairwise <- merge(df_percent_win_pairwise,df_infos_target_race_geny[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
      }
      
      if(sum(is.na(df_percent_win_pairwise$ENGAGEMENT_SCORE_NUMBER_BEATED_HORSES))>0){
        df_percent_win_pairwise$ENGAGEMENT_SCORE_NUMBER_BEATED_HORSES[is.na(df_percent_win_pairwise$ENGAGEMENT_SCORE_NUMBER_BEATED_HORSES)] <- 0
      }
    }
  }
  
  return(df_percent_win_pairwise)
}













