#' @return Compute fitness of horses in a given race
#' @importFrom magrittr %>%
#' @importFrom dplyr group_by
#' @importFrom dplyr top_n
#' @importFrom dplyr top_n
#' @importFrom dplyr summarise
#' @importFrom dplyr mutate
#' @examples
#' get_horse_good_shape_given_race_letrot(mat_perf_horse = NULL)
#' @export
get_horse_good_shape_given_race_canalturf <- function(mat_perf_horse=NULL)
{
  
  mat_ideal_delay <- data.frame(Cheval=unique(mat_perf_horse$Cheval),Loose=NA,Win=NA)
  rownames(mat_ideal_delay) <- NULL
  # mat_perf_horse <- mat_historical_races_letrot[mat_historical_races_letrot$Cheval == "DEFI PIERJI",]
  if(nrow(mat_perf_horse)>1) {
    mat_perf_horse$Place[is.na(mat_perf_horse$Place)] <- 99
    mat_perf_horse <- mat_perf_horse[order(mat_perf_horse$Date),c("Cheval","Date","Place")]
    mat_perf_horse$Delay <- NA
    for(idx_row in 1:((nrow(mat_perf_horse)-1))) {
      mat_perf_horse$Delay[idx_row+1] <- as.numeric(mat_perf_horse[idx_row+1,"Date"]-mat_perf_horse[idx_row,"Date"])
    }
  }

  if(sum(mat_perf_horse$Place<6)>0) {
    mat_ideal_delay [,'Win'] <- mean(mat_perf_horse[mat_perf_horse$Place<6,"Delay"],na.rm=TRUE)
  }
  
  if(sum(mat_perf_horse$Place>=6)>0) {
    mat_ideal_delay [,'Loose'] <- mean(mat_perf_horse[mat_perf_horse$Place>=6,"Delay"],na.rm=TRUE)
  }
  return(mat_ideal_delay) 
}




