get_horse_inferred_timewise_highest_category_confirmed_geny_backup <- function (target_horse = NULL,
                                                                         df_infos_target_race_geny = NULL ,
                                                                         path_cluster_distance_range = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/mat_ranges_cluster_distance.rds",
                                                                         path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                                         list_number_days_back = list(Q4=366,Q3=270,Q2=180,Q1=90)){
  
  
  message("Initialization of final output")
  df_infos_target_horse <- NULL
  df_cat_confirmed <- NULL
  message("Initialization of final output")
  
  message("Creating pivot data frame from categories")
  df_epreuve <- data.frame(Epreuve=c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"),Values=seq(1:length(c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"))))
  message("Creating pivot data frame from categories")
  
  message("Extract category of current race")
  if(!is.null(df_infos_target_race_geny)){
    message("Extract date of current candidates")
    current_race_date <- gsub("\n","",unique(df_infos_target_race_geny$Date))
    if(class(current_race_date)!="Date"){
      current_race_date <- as.Date(current_race_date)
    }
    message("Extract discipline of current race")
    current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
    message("Extract terrain of current race")
    current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
    current_race_terrain <- sub("Machefer","Cendrée",current_race_terrain)
    current_race_terrain <- sub("Mâchefer","Cendrée",current_race_terrain)
    message("Extract terrain of current race")
    current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
    current_race_category <- get_race_category_geny(df_infos_target_race_geny)
    current_race_category <- gsub("\\(GALOP)","",current_race_category)
    infos_current_race_limit <- get_race_maximum_limit_participation_geny(df_infos_target_race_geny[1,])
    if("Engagement" %in% colnames(infos_current_race_limit)){
      current_race_limit <- infos_current_race_limit$Engagement
    }
  }
  current_race_prize <- as.vector(unique(df_infos_target_race_geny$Prix))
  message("Extract category of current race")
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      df_historical_races_geny$Cumul_Gains <- as.numeric(df_historical_races_geny$Cumul_Gains)
    }
  }
  message("Reading historical performance file")
  
  message("Start reading cluster definition file")
  df_cluster_distance_range <- readRDS(path_cluster_distance_range)
  df_cluster_distance_range_discipline <- df_cluster_distance_range[df_cluster_distance_range$Discipline==current_race_discipline,]
  message("Finish reading cluster definition file")
  
  if(!is.na(current_race_category)){
    message("Extracting historical races of target horse")
    df_infos_target_horse <- df_historical_races_geny %>%
      filter(Cheval %in% target_horse ) %>%
      filter(Discipline == current_race_discipline ) %>%
      filter(Date<current_race_date) %>%
      # filter(current_race_date-Date<=number_days_back) %>%
      select(Cheval,Date,Cumul_Gains,Lieu,Distance,Discipline,Corde,Terrain,Distance,Gains,Details,Speed,Place,Prix) %>%
      unique() %>%
      as.data.frame()
    
    if(nrow(df_infos_target_horse)>0){
      df_infos_target_horse$Limit <- NA
      for(idx in 1:nrow(df_infos_target_horse))
      {
        df_infos_target_horse[idx,"Epreuve"] <- get_race_category_geny(df_infos_target_horse[idx,])
        df_infos_limit_idx <- get_race_maximum_limit_participation_geny(df_infos_target_horse[idx,])
        if(!is.null(df_infos_limit_idx)){
          if("Engagement" %in% colnames(df_infos_limit_idx)){
            df_infos_target_horse[idx,"Limit"] <- df_infos_limit_idx$Engagement
          }
        }
      }
    }
    message("Extracting historical races of target horse")
    
    message("Start extracting details of current race")
    if(!is.null(df_infos_target_horse)){
      message("Start adding distacne cluster for candidate races")
      if(nrow(df_infos_target_horse)>0){
        df_infos_target_horse$Cluster_Distance <- NA
        for(i in 1:nrow(df_cluster_distance_range_discipline))
        {
          idx_cluster_distance_group <- df_cluster_distance_range_discipline[i,"Lower"] <= unlist(df_infos_target_horse[,"Distance"]) & df_cluster_distance_range_discipline[i,"Upper"] >= unlist(df_infos_target_horse[,"Distance"])
          df_infos_target_horse[idx_cluster_distance_group==TRUE,"Cluster_Distance"] <- paste(df_cluster_distance_range_discipline[i,c("Lower","Upper")],collapse = "-")
        }
      }
    }
    message("Finish extracting details of current race")
    
    message("Start adding configuration on historical performance of competitors")
    if(nrow(df_infos_target_horse)>0){
      df_infos_target_horse$Terrain <- gsub("Mâchefer","Cendrée",df_infos_target_horse$Terrain)
      df_infos_target_horse$Terrain <- gsub("Machefer","Cendrée",df_infos_target_horse$Terrain)
      idx_final_filled_corde_terrain <- !is.na(df_infos_target_horse$Corde) & !is.na(df_infos_target_horse$Terrain)
      if(sum(idx_final_filled_corde_terrain,na.rm = TRUE)>0){
        idx_config <- df_infos_target_horse$Corde == unique(df_infos_target_race_geny$Corde) & df_infos_target_horse$Terrain == unique(df_infos_target_race_geny$Terrain) 
        idx_corde <- df_infos_target_horse$Corde == unique(df_infos_target_race_geny$Corde) & df_infos_target_horse$Terrain != unique(df_infos_target_race_geny$Terrain)
        idx_terrain <- df_infos_target_horse$Corde != unique(df_infos_target_race_geny$Corde) & df_infos_target_horse$Terrain == unique(df_infos_target_race_geny$Terrain)
        idx_else <- df_infos_target_horse$Corde != unique(df_infos_target_race_geny$Corde) & df_infos_target_horse$Terrain != unique(df_infos_target_race_geny$Terrain)
        df_infos_target_horse$Class <- NA
        if(sum(idx_config,na.rm = TRUE)>0){
          df_infos_target_horse[which(idx_config==TRUE),"Class"] <-"Config"
        }
        if(sum(idx_corde,na.rm = TRUE)>0){
          df_infos_target_horse[which(idx_corde==TRUE),"Class"] <-"Turn"
        }
        if(sum(idx_else,na.rm = TRUE)>0){
          df_infos_target_horse[which(idx_else==TRUE),"Class"] <-"Others"
        }
        if(sum(idx_terrain,na.rm = TRUE)>0){
          df_infos_target_horse[which(idx_terrain==TRUE),"Class"] <-"Ground"
        }
        if(sum(is.na(df_infos_target_horse$Class),na.rm=TRUE)>0){
          idx_turn_filled <- is.na(df_infos_target_horse$Class) & !is.na(df_infos_target_horse$Corde) 
          if(sum(idx_turn_filled,na.rm=TRUE)>0){
            df_infos_target_horse[which(idx_turn_filled==TRUE),"Class"] <-"Turn"
          }
          idx_ground_filled <- is.na(df_infos_target_horse$Class) & !is.na(df_infos_target_horse$Terrain) 
          if(sum(idx_ground_filled,na.rm=TRUE)>0){
            df_infos_target_horse[which(idx_ground_filled==TRUE),"Class"] <-"Ground"
          }
          idx_turn_ground_filled <- is.na(df_infos_target_horse$Class) & is.na(df_infos_target_horse$Terrain) & is.na(df_infos_target_horse$Corde)
          if(sum(idx_turn_ground_filled,na.rm=TRUE)>0){
            df_infos_target_horse[which(idx_turn_ground_filled==TRUE),"Class"] <-"Others"
          }
        }
      }
    }
    message("Finish adding configuration on historical performance of competitors")
    
    if(nrow(df_infos_target_horse)>0){
      df_infos_target_horse <- merge(df_infos_target_horse,df_epreuve,by.x="Epreuve",by.y="Epreuve",all.x=TRUE)
      df_infos_target_horse$Sequence <- NA
      for(date_limit in 1:length(list_number_days_back))
      {
        idx_sequence_current <- as.numeric(current_race_date - df_infos_target_horse$Date)<=list_number_days_back[[date_limit]]
        if(sum(idx_sequence_current)>0){
          df_infos_target_horse$Sequence[which(idx_sequence_current==TRUE)] <- names(list_number_days_back)[date_limit]
        }
      }
      df_infos_target_horse <- df_infos_target_horse[order(df_infos_target_horse$Date),]
      df_infos_target_horse$Success <- 0
      for(idx in 2:(nrow(df_infos_target_horse)))
      {
        value_category_current <- df_infos_target_horse[idx,"Values"]
        value_category_previous <- df_infos_target_horse[idx-1,"Values"]
        value_limit_current <- df_infos_target_horse[idx,"Limit"]
        value_limit_previous <- df_infos_target_horse[idx-1,"Limit"]
        value_prize_current <- df_infos_target_horse[idx,"Prix"]
        value_prize_previous <- df_infos_target_horse[idx-1,"Prix"]
        value_rank_current <- df_infos_target_horse[idx,"Place"]
        if(!is.na(value_category_current) & !is.na(value_category_previous) & !is.na(value_limit_current) & !is.na(value_limit_previous) & !is.na(value_prize_current) & !is.na(value_prize_previous) & !is.na(value_rank_current)){
          if(value_category_current<=value_category_previous & value_limit_current>=value_limit_previous & value_prize_current>=value_prize_previous & value_rank_current <=5){
            df_infos_target_horse$Success[idx] <- 1 
          }
        }
      }
      
      if(sum(is.na(df_infos_target_horse$Sequence))>0){
        df_infos_target_horse <- df_infos_target_horse[!is.na(df_infos_target_horse$Sequence),]
      }
      
      df_cat_confirmed <- df_infos_target_horse %>%
        filter(Success==1) %>%
        group_by(Cheval,Sequence) %>%
        top_n(-1,Values) %>%
        group_by(Cheval,Sequence) %>%
        top_n(-1,Limit) %>%
        select(Cheval,Sequence,Epreuve,Values,Limit,Prix,Place,Gains) %>%
        as.data.frame()
      
      if(!is.null(df_cat_confirmed)){
        if(nrow(df_cat_confirmed)>0){
          df_cat_confirmed$Reference <- df_epreuve[df_epreuve$Epreuve==current_race_category,"Values"]
          df_cat_confirmed$Delta <- df_cat_confirmed$Reference - df_cat_confirmed$Values
        }
      }
    }
  }

  if(!is.null(df_cat_confirmed)){
    if(nrow(df_cat_confirmed)==0){
      df_cat_confirmed <- NULL
    }
  }
  
  return(df_cat_confirmed)
  
}

