#' @return Compute fitness of horses in a given race
#' @importFrom magrittr %>%
#' @importFrom dplyr group_by
#' @importFrom dplyr top_n
#' @importFrom dplyr top_n
#' @importFrom dplyr summarise
#' @importFrom dplyr mutate
#' @examples
#' get_horse_fitness_given_race_letrot(df_infos_target_race = NULL,number_race=4)
#' @export
get_horse_score_draw_geny <- function(df_infos_target_race = NULL)
{
  
  message("Initialization of the output")
  df_score_draw <- NULL
  
  if(!is.null(df_infos_target_race)){
    if("Draw" %in% colnames(df_infos_target_race)){
      if(sum(!is.na(df_infos_target_race$Draw))>0){
        df_infos_target_race$Draw <- as.numeric(df_infos_target_race$Draw)
        df_score_draw <- data.frame(Cheval=as.vector(df_infos_target_race$Cheval),ADVANTAGE_DRAW_NUMBER=round(get_percentile_values(df_infos_target_race$Draw),2))
      }
    }
  }
  
  return(df_score_draw) 
}
