#' @return Compute the probability of being in the top 7 for a given race
#' @importFrom magrittr %>%
#' @importFrom h2o h2o.loadModel
#' @importFrom dplyr top_n
#' @importFrom dplyr top_n
#' @importFrom dplyr summarise
#' @importFrom dplyr mutate
#' @examples
#' get_horse_category_adequacy_given_race_letrot(mat_infos_target_race = NULL)
#' @export

get_predicted_probabilities_given_race_letrot <- function(df_features_target=NULL,
                                                          path_input_models="/mnt/Master-Data/Futanke-Mbayar/France/models/le-trot/output/models") {
  
  message("Initialization Of Final Output")
  prediction_automl_current_h2o <- NULL
  
  if(!is.null(df_features_target)){
    if("CHEVAL" %in% colnames(df_features_target)) {
      df_features_target$IDENTIFIER <- paste(df_features_target$CHEVAL,df_features_target$RACE,sep="_")
    } else {
      df_features_target$IDENTIFIER <- paste(df_features_target$Cheval,df_features_target$RACE,sep="_")
    }
    rownames(df_features_target)  <- df_features_target$IDENTIFIER
    message("Constructing the path to the modle to apply")
    name_model_to_apply <- tolower(gsub("é","e",paste(unique(df_features_target$HIPPODROME),unique(df_features_target$DISCIPLINE),min(df_features_target$DISTANCE_DAY),sep="_")))
    name_model_to_apply <- gsub(" ","-",name_model_to_apply)
    path_model_to_apply <- dir(paste(path_input_models,name_model_to_apply,sep="/"),full.names = TRUE)
    if(length(path_model_to_apply)>1) {
      path_model_to_apply <- path_model_to_apply[1]
    }
    
    if(length(path_model_to_apply)==1){
      message("Testing if the data for the target race have been provided")
      if(!is.null(df_features_target)) {
        if(nrow(df_features_target)>0){
          for(i in colnames(df_features_target))
          {
            class(df_features_target[,i]) =="matrix"
            df_features_target[,i] <- as.vector(unlist(as.data.frame(df_features_target[,i])))
          }
        }
      }
      message("Initialization of a h2o connexion")
      h2o::h2o.init(ip = "localhost", port = 54321, nthreads= -1,max_mem_size = "16g")
      if(file.exists(path_model_to_apply)) {
        message("Loading the modle to apply")
        model_automl_current_h2o <- h2o::h2o.loadModel(path_model_to_apply)
        vec_features_needed <- setdiff(model_automl_current_h2o@model$names,"Class")
        if(sum(mapply(is.infinite, df_features_target))>0){
          df_features_target[mapply(is.infinite, df_features_target)] <- NA
        }
        vec_missing_features <- setdiff(vec_features_needed,colnames(df_features_target))
        if(length(vec_missing_features)>0){
          for(missing_feature in vec_missing_features)
          {
            df_features_target[,missing_feature] <- 0
          }
        }
        if(sum(is.na(df_features_target))>0){
          df_features_target[is.na(df_features_target)] <- 0
        }
      }
      
      message("Converting testing data to h2o format")
      df_features_target_hex  = h2o::as.h2o(df_features_target[,vec_features_needed],  destination_frame  = "df_features_target_hex")
      message("Applying the dedicated model to the target race")
      prediction_automl_current_h2o <- h2o::h2o.predict(model_automl_current_h2o, df_features_target_hex)
      prediction_automl_current_h2o <- as.data.frame(prediction_automl_current_h2o)
      rownames(prediction_automl_current_h2o) <- rownames(df_features_target)
      prediction_automl_current_h2o <- prediction_automl_current_h2o[,-1]  
      colnames(prediction_automl_current_h2o) <- sub("X01.Winners","Success",colnames(prediction_automl_current_h2o))
      colnames(prediction_automl_current_h2o) <- sub("X02.Loosers","Loose",colnames(prediction_automl_current_h2o))
      prediction_automl_current_h2o$Cheval <- unlist(lapply(rownames(prediction_automl_current_h2o),function(x){unlist(strsplit(x,"_"))[1]}))
      prediction_automl_current_h2o <- prediction_automl_current_h2o[,c("Cheval",setdiff(colnames(prediction_automl_current_h2o),"Cheval"))]
      rownames(prediction_automl_current_h2o) <- NULL
    }
  }

  return(prediction_automl_current_h2o)
}
