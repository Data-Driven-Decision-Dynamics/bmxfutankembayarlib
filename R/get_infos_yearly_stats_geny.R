#' @return Compute percent of success focusing on same ferrage configuration
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_success_current_hippodrome_letrot(mat_infos_race_input = NULL)
#' @export
get_infos_yearly_stats_geny <- function (df_infos_target_race_geny = NULL,
                                         df_competitors_perfs = NULL,
                                         path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                         path_cluster_distance_range = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/mat_ranges_cluster_distance.rds",
                                         number_days_back = 720,
                                         threshold_number_item_filled = 6){
  
  message("Start initialization of yearly statistics")
  list_yearly_stats <- NULL
  df_stats_track <- NULL
  df_stats_total_earning <- NULL
  df_stats_speed_records <- NULL
  df_stats_mean_earning_config <- NULL
  df_stats_highest_limits <- NULL
  message("Finish initialization of yearly statistics")
  
  message("Start reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Finish reading historical performance file")
  
  message("Finish adding distacne cluster for candidate races")
  message("Extract name of current candidates")
  current_race_cluster_distance <- as.vector(unique(df_infos_target_race_geny$Cluster_Distance)[1])
  current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
  current_race_terrain <- sub("Machefer","Cendrée",current_race_terrain)
  current_race_terrain <- sub("Mâchefer","Cendrée",current_race_terrain)
  message("Extract terrain of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
  message("Extract terrain of current race")
  current_race_date  <- unique(df_infos_target_race_geny$Date)
  current_race_category <- get_race_category_geny (df_infos_target_race_geny)
  current_race_limit_participation <- get_limit_participation_geny(unique(df_infos_target_race_geny$Details))
  if(is.na(current_race_limit_participation)){
    current_race_limit_participation <- get_limit_participation_geny(df_infos_target_race_geny=df_infos_target_race_geny)
  }
  
  message("Start reading cluster definition file")
  df_cluster_distance_range <- readRDS(path_cluster_distance_range)
  df_cluster_distance_range_discipline <- df_cluster_distance_range[df_cluster_distance_range$Discipline==current_race_discipline,]
  message("Finish reading cluster definition file")
  
  message("Start extracting details of current race")
  if(!is.null(df_infos_target_race_geny)){
    df_infos_target_race_geny <- add_cluster_distance_geny(df_infos_target_race_geny)
  }
  message("Finish extracting details of current race")
  
  message("Start extracting historical performance of competitors")
  df_performance_focus <- df_competitors_perfs %>%
    filter(Discipline == current_race_discipline ) %>%
    # select(Cheval,Age,Date,Lieu,Distance,Corde,Terrain,Gains,Details,Epreuve,Speed,Place,Race) %>%
    unique() %>%
    as.data.frame()
  message("Finish extracting historical performance of competitors")
  
  message("Start testing if any competitor already win on current track")
  if(nrow(df_performance_focus)>0){
    df_winning_experience <- df_competitors_perfs %>%
      filter(Discipline == current_race_discipline ) %>%
      filter(Lieu %in% unique(df_infos_target_race_geny$Lieu) ) %>%
      group_by(Cheval) %>%
      top_n(-1,Place) %>%
      group_by(Cheval) %>%
      top_n(1,Prix) %>%
      group_by(Cheval) %>%
      top_n(1,Date) %>%
      # select(Cheval,Age,Date,Lieu,Distance,Corde,Terrain,Gains,Details,Epreuve,Speed,Place,Race) %>%
      unique() %>%
      as.data.frame()
  }
  message("Start testing if any competitor already win on current track")
  
  message("Start computing stats on given track")
  df_stats_track <- df_performance_focus %>%
    filter(Lieu == unique(df_infos_target_race_geny$Lieu)) %>%
    group_by(Cheval) %>%
    dplyr::summarise(NUMBER_RACE_TRACK = n(),
                     NUMBER_PLACE_TRACK = get_number_podium(Place),
                     SUM_EARNING_TRACK= sum(Gains)) %>%
    as.data.frame()
  if(nrow(df_stats_track)==0){
    df_stats_track <- NULL
  }
  if(!is.null(df_stats_track)){
    if(nrow(df_stats_track)<nrow(df_infos_target_race_geny)){
      df_stats_track <- merge(df_stats_track,df_infos_target_race_geny[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
    }
  }
  
  if(!is.null(df_stats_track)){
    if(nrow(df_winning_experience)>0){
      if(length(intersect(df_winning_experience$Cheval,df_stats_track$Cheval))>0){
        df_stats_track <- merge(df_stats_track,df_winning_experience[,c("Cheval","Place","Date")],by.x="Cheval",by.y="Cheval")
      }
    }
  }
  message("Finish computing stats on given track")
  
  message("Start adding distacne cluster for candidate races")
  if(nrow(df_performance_focus)>0){
    if(sum(is.na(df_performance_focus$Distance))>0){
      df_performance_focus <- df_performance_focus[!is.na(df_performance_focus$Distance),]
    }
    df_performance_focus <- add_cluster_distance_geny(df_performance_focus)
  }
  message("Finish adding distacne cluster for candidate races")
  
  message("Start adding year on historical performance of competitors")
  if(nrow(df_performance_focus)>0){
    df_performance_focus <- df_performance_focus %>%
      mutate(Year=lubridate::year(df_performance_focus$Date))
  }
  message("Stop adding year on historical performance of competitors")
  
  message("Start adding participation limit on historical performance of competitors")
  if(nrow(df_performance_focus)>0){
    colnames(df_performance_focus) [colnames(df_performance_focus) == "Engagement"] <- "Limit"
  }
  message("Finish adding participation limit on historical performance of competitors")
  
  # message("Start normalization of main scoring dimension")
  # df_performance_focus <- df_performance_focus %>%
  #   group_by(Age) %>%
  #   mutate(Limit = get_percentile_values(Limit)) %>%
  #   mutate(Limit = round(Limit,2)) %>%
  #   group_by(Age) %>%
  #   mutate(Gains = get_percentile_values(Gains)) %>%
  #   mutate(Gains = round(Gains,2)) %>%
  #   group_by(Age) %>%
  #   mutate(Speed = get_percentile_values(Speed)) %>%
  #   mutate(Speed = round(Speed,2)) %>%
  #   as.data.frame()
  # message("Finish normalization of main scoring dimension")
  
  message("Start adding configuration on historical performance of competitors")
  if(nrow(df_performance_focus)>0){
    df_performance_focus <- add_config_race_geny(df_performance_focus,df_infos_target_race_geny )
  }
  message("Finish adding configuration on historical performance of competitors")

  message("Start computing total earning related statistics of competitors")
  df_stats_total_earning <- df_performance_focus %>%
    group_by(Cheval,Year) %>%
    dplyr::summarise(NUMBER_RACES = n(),
                     NUMBER_DISQUALIFICATIONS=get_number_disqualification(Place),
                     TOTAL_EARNING = sum(Gains)) %>%
    as.data.frame()
  message("Finish computing total earning related statistics of competitors")
  
  message("Start computing speed related statistics of competitors")
  df_stats_speed_records <- df_performance_focus %>%
    filter(Date<current_race_date)  %>%
    # filter(current_race_date-Date<=number_days_back)  %>%
    filter(!is.na(Speed)) %>%
    group_by(Cheval,Corde,Cluster_Distance) %>%
    dplyr::summarise(BEST_SPEED=max(Speed)) %>%
    as.data.frame()
  message("Finish computing speed related statistics of competitors")
  
  message("Start computing earning related statistics of competitors")
  df_stats_mean_earning_config <- df_performance_focus %>%
    filter(Gains>0) %>%
    group_by(Cheval,Year,Class) %>%
    dplyr::summarise(MEAN_EARNING=mean(Gains)) %>%
    as.data.frame()
  if(nrow(df_stats_mean_earning_config)>0){
    df_stats_mean_earning_config$EXPERIENCE <- 0
    vec_ages_highest_mean_earning <- unique(df_stats_mean_earning_config[,"Year"])
    for(current_age in vec_ages_highest_mean_earning)
    {
      df_stats_highest_mean_earning_age <- df_stats_mean_earning_config[df_stats_mean_earning_config$Age == current_age,]
      vec_config_found_speed_age <- unique(df_stats_highest_mean_earning_age$Class)
      for(config_found in vec_config_found_speed_age)
      {
        df_stats_highest_mean_earning_age_config <- df_stats_highest_mean_earning_age[df_stats_highest_mean_earning_age$Class == config_found, ]
        df_stats_highest_mean_earning_age_config <- df_stats_highest_mean_earning_age_config[order(df_stats_highest_mean_earning_age_config$MEAN_EARNING,decreasing = TRUE),]
        idx_horse_tag_highest_mean_earning <- df_stats_highest_mean_earning_age_config[1,"Cheval"]
        idx_age_tag_highest_mean_earning <- df_stats_highest_mean_earning_age_config[1,"Year"]
        idx_config_tag_highest_mean_earning <- df_stats_highest_mean_earning_age_config[1,"Class"]
        idx_to_tag_highest_mean_earning <- df_stats_mean_earning_config$Cheval == idx_horse_tag_highest_mean_earning & df_stats_mean_earning_config$Age == idx_age_tag_highest_mean_earning & df_stats_mean_earning_config$Class == idx_config_tag_highest_mean_earning
        if(length(which(idx_to_tag_highest_mean_earning==TRUE))>0){
          df_stats_mean_earning_config[which(idx_to_tag_highest_mean_earning==TRUE),"EXPERIENCE"] <- 1
        }
      }
    }
  }
  message("Finish computing earning related statistics of competitors")
  
  message("Start computing limit related statistics of competitors")
  df_stats_highest_limits <- df_performance_focus %>%
    filter(!is.na(Limit)) %>%
    group_by(Cheval,Year,Issues) %>%
    dplyr::summarise(HIGHEST_LIMIT=max(Limit)) %>%
    as.data.frame()
  if(nrow(df_stats_highest_limits)>0){
    df_stats_highest_limits$EXPERIENCE <- 0
    if(sum(df_stats_highest_limits$HIGHEST_LIMIT>=current_race_limit_participation & df_stats_highest_limits$Issues == "P01",na.rm = TRUE)){
      df_stats_highest_limits$EXPERIENCE[which(df_stats_highest_limits$HIGHEST_LIMIT>=current_race_limit_participation & df_stats_highest_limits$Issues == "P01")] <- 1
    }
  }
  message("Finish computing limit related statistics of competitors")
  
  # message("Finish identify strong performers on a given turn")
  # if(!is.null(df_stats_speed_records)){
  #   df_stats_speed_records$Specialist <- 0
  #   if(nrow(df_stats_speed_records)>0){
  #     vec_turn_found <- sort(unique(df_stats_speed_records$Corde))
  #     for(currrent_turn in vec_turn_found)
  #     {
  #       df_stats_speed_records_turn <- df_stats_speed_records[df_stats_speed_records$Corde == currrent_turn, ]
  #       if(sum(is.na(df_stats_speed_records_turn$Cheval))>0){
  #         df_stats_speed_records_turn <- df_stats_speed_records_turn[!is.na(df_stats_speed_records_turn$Cheval),]
  #       }
  #       test_existence_several_clust_dist <- unique(df_stats_speed_records_turn$Cluster_Distance)
  #       if(sum(is.na(test_existence_several_clust_dist))>0){
  #         test_existence_several_clust_dist <- test_existence_several_clust_dist[!is.na(test_existence_several_clust_dist)]
  #       }
  #       if(length(test_existence_several_clust_dist)>2){
  #         tab_nom_horse_num_conf <- table(df_stats_speed_records_turn$Cluster_Distance,df_stats_speed_records_turn$Cheval)
  #         nb_val_config <- apply(tab_nom_horse_num_conf,1,function(x) {sum(x)})
  #         if(sum(nb_val_config>=threshold_number_item_filled)>=2){
  #           infos_lab_horse_good_various_clus_dist <- NULL
  #           for(current_clus_dis in names(nb_val_config)[nb_val_config>=threshold_number_item_filled]) {
  #             df_stats_speed_records_turn_clust_dist <- df_stats_speed_records_turn[df_stats_speed_records_turn$Cluster_Distance == current_clus_dis,]
  #             df_stats_speed_records_turn_clust_dist <- df_stats_speed_records_turn_clust_dist[order(df_stats_speed_records_turn_clust_dist$BEST_SPEED,decreasing = TRUE),]
  #             lab_horse_good_turn_clus_dis <- df_stats_speed_records_turn_clust_dist[1:3,"Cheval"]
  #             infos_lab_horse_good_various_clus_dist <- c(infos_lab_horse_good_various_clus_dist,lab_horse_good_turn_clus_dis)
  #           }
  #           freq_lab_horse_good_various_clus_dist <- table(infos_lab_horse_good_various_clus_dist)
  #           if(length(names(nb_val_config)[nb_val_config>=threshold_number_item_filled])==2){
  #             if(max(freq_lab_horse_good_various_clus_dist)>=2){
  #               idx_to_change <- df_stats_speed_records$Cluster_Distance == current_clus_dis  &  df_stats_speed_records$Corde == currrent_turn & df_stats_speed_records$Cheval %in% names(freq_lab_horse_good_various_clus_dist)[freq_lab_horse_good_various_clus_dist>=2]
  #               df_stats_speed_records[idx_to_change,"Specialist"] <- 1
  #             }
  #           }
  #           
  #           if(length(names(nb_val_config)[nb_val_config>=threshold_number_item_filled])>2){
  #             if(max(freq_lab_horse_good_various_clus_dist)>=2){
  #               idx_to_change <- df_stats_speed_records$Cluster_Distance == current_clus_dis  & df_stats_speed_records$Corde == currrent_turn & df_stats_speed_records$Cheval %in% names(freq_lab_horse_good_various_clus_dist)[freq_lab_horse_good_various_clus_dist>=(length(names(nb_val_config)[nb_val_config>=threshold_number_item_filled])-1)]
  #               df_stats_speed_records[idx_to_change,"Specialist"] <- 1
  #             }
  #           }
  #         }
  #       }
  #     }
  #   }
  # }
  # message("Finish identify strong performers on a given turn")
  
  message("Start identify strong performers on both turns")
  if(!is.null(df_stats_speed_records)){
    df_stats_speed_records$Expert <- 0
    if(nrow(df_stats_speed_records)>0){
      vec_clus_dist_found <- sort(unique(df_stats_speed_records$Cluster_Distance))
      for(currrent_clus_dis in vec_clus_dist_found)
      {
        df_stats_speed_records_clust_dist <- df_stats_speed_records[df_stats_speed_records$Cluster_Distance == currrent_clus_dis, ]
        test_existence_both_turns <- unique(df_stats_speed_records_clust_dist$Corde)
        if(sum(is.na(test_existence_both_turns))>0){
          test_existence_both_turns <- test_existence_both_turns[!is.na(test_existence_both_turns)]
        }
        if(length(test_existence_both_turns)==2){
          tab_bum_horse_num_conf <- table(df_stats_speed_records_clust_dist$Corde,df_stats_speed_records_clust_dist$Cheval)
          nb_val_config <- apply(tab_bum_horse_num_conf,1,function(x) {sum(x)})
          if(sum(nb_val_config>=threshold_number_item_filled)==2){
            df_stats_speed_records_clust_dist_turn_left <- df_stats_speed_records_clust_dist[df_stats_speed_records_clust_dist$Corde == "Gauche",]
            df_stats_speed_records_clust_dist_turn_left <- df_stats_speed_records_clust_dist_turn_left[order(df_stats_speed_records_clust_dist_turn_left$BEST_SPEED,decreasing = TRUE),]
            df_stats_speed_records_clust_dist_turn_right <- df_stats_speed_records_clust_dist[df_stats_speed_records_clust_dist$Corde == "Droite",]
            df_stats_speed_records_clust_dist_turn_right <- df_stats_speed_records_clust_dist_turn_right[order(df_stats_speed_records_clust_dist_turn_right$BEST_SPEED,decreasing = TRUE),]
            lab_horse_good_left <- df_stats_speed_records_clust_dist_turn_left[1:5,"Cheval"]
            lab_horse_good_right <- df_stats_speed_records_clust_dist_turn_right[1:5,"Cheval"]
            infos_lab_horse_good_left_right <- c(lab_horse_good_left,lab_horse_good_right)
            freq_lab_horse_good_left_right <- table(infos_lab_horse_good_left_right)
            if(max(freq_lab_horse_good_left_right)>=2){
              idx_to_change <- df_stats_speed_records$Cluster_Distance == currrent_clus_dis & df_stats_speed_records$Cheval %in% names(freq_lab_horse_good_left_right)[freq_lab_horse_good_left_right==2]
              df_stats_speed_records[idx_to_change,"Expert"] <- 1
            }
          }
        }
      }
    }
  }
  
  message("Finish identify strong performers on both turns")
  
  message("Start identify top earners per config and turn")
  df_stats_mean_earning_config$TOP_EARNER <- 0
  vec_years <- unique(df_stats_mean_earning_config$Year)
  for(current_year in vec_years)
  {
    df_stats_mean_earning_config_year <- df_stats_mean_earning_config[df_stats_mean_earning_config$Year == current_year, ]
    vec_config_cound <- intersect(c("Config","Turn") ,unique(df_stats_mean_earning_config_year$Class))
    if(length(vec_config_cound)>=1){
      for(current_item in vec_config_cound)
      {
        df_stats_mean_earning_config_year_config <- df_stats_mean_earning_config_year[df_stats_mean_earning_config_year$Class ==current_item,]
        val_horse_year_config <- df_stats_mean_earning_config_year_config[df_stats_mean_earning_config_year_config$MEAN_EARNING==max(df_stats_mean_earning_config_year_config$MEAN_EARNING,na.rm = TRUE),"Cheval"]
        idx_lab_op_value <- df_stats_mean_earning_config$Cheval %in% val_horse_year_config & df_stats_mean_earning_config$Class == current_item & df_stats_mean_earning_config$Year == current_year
        if(sum(idx_lab_op_value,na.rm = TRUE)>0){
          df_stats_mean_earning_config[which(idx_lab_op_value==TRUE),"TOP_EARNER"] <- 1
        }
      }
    }
  }
  message("Finish identify top earners per config and turn")
  
  list_yearly_stats <- list(Total = df_stats_total_earning, Speed = df_stats_speed_records, Mean = df_stats_mean_earning_config, Limit = df_stats_highest_limits,Track = df_stats_track)
  
  return(list_yearly_stats)
}