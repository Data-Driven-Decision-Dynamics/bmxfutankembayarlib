get_gender_mixity <- function(mat=NULL) {
  number_ages <- length(unique(mat$Age))
  test_mixity <- ifelse(number_ages>1,1,0)
  mat$GENDER_MIXTED <- test_mixity
  return(mat)
}