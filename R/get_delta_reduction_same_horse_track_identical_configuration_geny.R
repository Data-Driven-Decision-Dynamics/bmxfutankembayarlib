#' @return Compute percent of success focusing on same ferrage configuration
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_delta_reduction_same_horse_track_identical_configuration_geny()
#' @export
get_delta_reduction_same_horse_track_identical_configuration_geny <- function (path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                                               number_days_back = 366){
  
  message("Initialization of the final output")
  df_mean_diff_test_tracks_compiled <- NULL
  df_mean_diff_test_tracks_compiled_formated <- NULL
  message("Initialization of the final output")
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Reading historical performance file")
  
  message("Extracting available ground found")
  vec_discipline_found <- sort(unique(df_historical_races_geny$Discipline))
  message("Extracting available ground found")
  
  
  idx_races_trot_attele <- df_historical_races_geny$Discipline == "Trot Attelé" & !is.na(df_historical_races_geny$Ecart) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Depart) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
  idx_races_trot_monte <- df_historical_races_geny$Discipline == "Trot Monté" & !is.na(df_historical_races_geny$Ecart) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
  idx_races_galop <- df_historical_races_geny$Discipline %in% c("Plat","Haies","Steeplechase","Cross") & !is.na(df_historical_races_geny$Speed) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
  df_historical_races_geny$Track <- NA
  df_historical_races_geny$Reduction <- NA
  
  df_historical_races_geny[idx_races_trot_attele,"Track"] <- paste(df_historical_races_geny[idx_races_trot_attele,"Lieu"], df_historical_races_geny[idx_races_trot_attele,"Terrain"],df_historical_races_geny[idx_races_trot_attele,"Longueur"],df_historical_races_geny[idx_races_trot_attele,"Depart"],sep="_")
  df_historical_races_geny[idx_races_trot_attele,"Reduction"] <- unlist(lapply(df_historical_races_geny[idx_races_trot_attele,"Ecart"],get_numeric_values_reduc_ecart_geny))
  df_historical_races_geny[idx_races_trot_attele,"Speed"] <- 1000/df_historical_races_geny[idx_races_trot_attele,"Reduction"]
  
  df_historical_races_geny[idx_races_trot_monte,"Track"] <- paste(df_historical_races_geny[idx_races_trot_monte,"Lieu"], df_historical_races_geny[idx_races_trot_monte,"Terrain"],df_historical_races_geny[idx_races_trot_monte,"Longueur"],sep="_")
  df_historical_races_geny[idx_races_trot_monte,"Reduction"] <- unlist(lapply(df_historical_races_geny[idx_races_trot_monte,"Ecart"],get_numeric_values_reduc_ecart_geny))
  df_historical_races_geny[idx_races_trot_monte,"Speed"] <- 1000/df_historical_races_geny[idx_races_trot_monte,"Reduction"]
  
  df_historical_races_geny[idx_races_galop,"Track"] <- paste(df_historical_races_geny[idx_races_galop,"Lieu"], df_historical_races_geny[idx_races_galop,"Terrain"],df_historical_races_geny[idx_races_galop,"Longueur"],sep="_")
  
  df_historical_races_focus_track <- df_historical_races_geny[!is.na(df_historical_races_geny$Track),]
  
  vec_discipline_found <- unique(df_historical_races_focus_track$Discipline)
  
  vec_freq_tracks <- table(df_historical_races_focus_track$Track)
  df_historical_races_focus_track <- df_historical_races_focus_track[df_historical_races_focus_track$Track %in% names(vec_freq_tracks)[vec_freq_tracks>1], ]
  vec_freq_horses <- table(df_historical_races_focus_track$Cheval)
  df_historical_races_focus_track <- df_historical_races_focus_track[df_historical_races_focus_track$Cheval %in% names(vec_freq_horses)[vec_freq_horses>1],]
  
  for(current_discipline in vec_discipline_found) 
  {
    df_infos_focus <- df_historical_races_focus_track %>%
      filter(Discipline == current_discipline ) %>%
      filter(as.numeric(Sys.Date()-Date) <= number_days_back) %>%
      select(Cheval,Track,Speed) %>%
      pivot_wider(
        names_from = Track,
        values_from = Speed,
        values_fn = max
      ) %>%
      as.data.frame()
    if(nrow(df_infos_focus)>0){
      vec_track_found <- colnames(df_infos_focus)[-1]
      for(idx_track in 1:(length(vec_track_found)-1))
      {
        for(idy_track in (idx_track+1):length(vec_track_found) )
        {
          df_infos_focus_idx_idy <- df_infos_focus[,c("Cheval",vec_track_found[idx_track],vec_track_found[idy_track])]
          if(sum(complete.cases(df_infos_focus_idx_idy))>0){
            df_infos_focus_idx_idy <- df_infos_focus_idx_idy[complete.cases(df_infos_focus_idx_idy),]
            if(nrow(df_infos_focus_idx_idy)>=3){
              if(length(unlist(strsplit(vec_track_found[idx_track],"_")))==4){
                if(unlist(strsplit(vec_track_found[idx_track],"_"))[1]!=unlist(strsplit(vec_track_found[idy_track],"_"))[1]){
                  if(unlist(strsplit(vec_track_found[idx_track],"_"))[2]==unlist(strsplit(vec_track_found[idy_track],"_"))[2]){
                    if(unlist(strsplit(vec_track_found[idx_track],"_"))[3]==unlist(strsplit(vec_track_found[idy_track],"_"))[3]){
                      if(unlist(strsplit(vec_track_found[idx_track],"_"))[4]==unlist(strsplit(vec_track_found[idy_track],"_"))[4]){
                        if(abs(as.numeric(unlist(strsplit(vec_track_found[idx_track],"_"))[3]) - as.numeric(unlist(strsplit(vec_track_found[idy_track],"_"))[3])) == 0){
                          res_t_test_tracks <- t.test(df_infos_focus_idx_idy[,2],df_infos_focus_idx_idy[,3],paired  = TRUE ,alternative = "two.sided")
                          df_mean_diff_test_tracks_compiled_current <- data.frame(Source = vec_track_found[idx_track], Target = vec_track_found[idy_track] , Significance = res_t_test_tracks$p.value, Statistic= res_t_test_tracks$estimate, Source_Reduction = median(df_infos_focus_idx_idy[,2]) , Target_Reduction = median(df_infos_focus_idx_idy[,3]), Size = nrow(df_infos_focus_idx_idy) , Discipline = current_discipline )
                          df_mean_diff_test_tracks_compiled_current$Detla_Reduction <- df_mean_diff_test_tracks_compiled_current$Source_Reduction-df_mean_diff_test_tracks_compiled_current$Target_Reduction
                          rownames(df_mean_diff_test_tracks_compiled_current) <- NULL
                          df_mean_diff_test_tracks_compiled <- rbind.fill(df_mean_diff_test_tracks_compiled,df_mean_diff_test_tracks_compiled_current)
                        }
                      }
                    }
                  }
                }
              }
              
              if(length(unlist(strsplit(vec_track_found[idx_track],"_")))==3){
                if(unlist(strsplit(vec_track_found[idx_track],"_"))[1]!=unlist(strsplit(vec_track_found[idy_track],"_"))[1]){
                  if(unlist(strsplit(vec_track_found[idx_track],"_"))[2]==unlist(strsplit(vec_track_found[idy_track],"_"))[2]){
                    if(unlist(strsplit(vec_track_found[idx_track],"_"))[2]==unlist(strsplit(vec_track_found[idy_track],"_"))[2]){
                      if(abs(as.numeric(unlist(strsplit(vec_track_found[idx_track],"_"))[3]) - as.numeric(unlist(strsplit(vec_track_found[idy_track],"_"))[3])) == 0){
                        res_t_test_tracks <- t.test(df_infos_focus_idx_idy[,2],df_infos_focus_idx_idy[,3],paired  = TRUE ,alternative = "two.sided")
                        df_mean_diff_test_tracks_compiled_current <- data.frame(Source = vec_track_found[idx_track], Target = vec_track_found[idy_track] , Significance = res_t_test_tracks$p.value, Statistic= res_t_test_tracks$estimate, Source_Reduction = median(df_infos_focus_idx_idy[,2]) , Target_Reduction = median(df_infos_focus_idx_idy[,3]), Size = nrow(df_infos_focus_idx_idy) , Discipline = current_discipline)
                        df_mean_diff_test_tracks_compiled_current$Detla_Reduction <- df_mean_diff_test_tracks_compiled_current$Source_Reduction-df_mean_diff_test_tracks_compiled_current$Target_Reduction
                        rownames(df_mean_diff_test_tracks_compiled_current) <- NULL
                        df_mean_diff_test_tracks_compiled <- rbind.fill(df_mean_diff_test_tracks_compiled,df_mean_diff_test_tracks_compiled_current)
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  if(!is.null(df_mean_diff_test_tracks_compiled)){
    if(nrow(df_mean_diff_test_tracks_compiled)>0){
      for(idx in 1:nrow(df_mean_diff_test_tracks_compiled))
      {
        if(df_mean_diff_test_tracks_compiled[idx,"Significance"] <= 0.1 & df_mean_diff_test_tracks_compiled[idx,"Statistic"] >0){
          df_mean_diff_test_tracks_compiled_formated_current <- data.frame(from = df_mean_diff_test_tracks_compiled[idx,"Source"], to = df_mean_diff_test_tracks_compiled[idx,"Target"],Discipline = df_mean_diff_test_tracks_compiled[idx,"Discipline"])
          df_mean_diff_test_tracks_compiled_formated <- rbind.fill(df_mean_diff_test_tracks_compiled_formated,df_mean_diff_test_tracks_compiled_formated_current)
        }
        if(df_mean_diff_test_tracks_compiled[idx,"Significance"] <= 0.1 & df_mean_diff_test_tracks_compiled[idx,"Statistic"] < 0){
          df_mean_diff_test_tracks_compiled_formated_current <- data.frame(from = df_mean_diff_test_tracks_compiled[idx,"Target"], to = df_mean_diff_test_tracks_compiled[idx,"Source"],Discipline = df_mean_diff_test_tracks_compiled[idx,"Discipline"])
          df_mean_diff_test_tracks_compiled_formated <- rbind.fill(df_mean_diff_test_tracks_compiled_formated,df_mean_diff_test_tracks_compiled_formated_current)
        }
      }
    }
  }

  return(list(Details = df_mean_diff_test_tracks_compiled , Formated = df_mean_diff_test_tracks_compiled_formated))
}
