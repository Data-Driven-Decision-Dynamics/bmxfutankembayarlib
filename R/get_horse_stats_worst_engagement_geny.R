# get_horse_stats_worst_engagement_geny <- function(df_infos_target_race_geny = NULL,
#                                                   path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
#                                                   path_stats_numero_autostart = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_stats_number_autostart.rds",
#                                                   number_days_back = 500){
#   
#   if(!exists("df_historical_races_geny")) {
#     if(file.exists(path_df_historical_races_geny)){
#       df_historical_races_geny <- readRDS(path_df_historical_races_geny)
#     }
#   }
#   
#   vec_label_race_recul <- unique(df_historical_races_geny[df_historical_races_geny$Recul >0,"Race"])
#   if(length(vec_label_race_recul)>0){
#     df_historical_races_focus <- df_historical_races_geny[df_historical_races_geny$Race %in% vec_label_race_recul,c("Date","Race","Details","Numero","Cheval","Lieu","Discipline","Depart","Distance","Longueur","Recul","Corde","Place") ]
#     df_historical_races_focus <- unique(df_historical_races_focus)
#     df_historical_races_focus %>%
#       filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
#       filter(Corde %in% unique(df_infos_target_race_geny$Corde)) %>%
#       filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
#       filter(Place <= 3) %>%
#       select(Cheval,Lieu,Longueur,Recul,Place) %>%
#       as.data.frame()
#       
#   }
#   
#   
#   
#   
#   
# }
# 
# 
# 
# 
# 
#     
#     
#     
#     
#     
#     
#     df_historical_races_focus <- df_historical_races_geny[df_historical_races_geny$Depart == "Auto-Start", c("Date","Numero","Cheval","Lieu","Depart","Distance","Place")]
#   df_historical_races_focus <- unique(df_historical_races_focus)
#   df_historical_races_focus$ID <- paste(df_historical_races_focus$Lieu,df_historical_races_focus$Distance,sep="-")
#   if(sum(is.na(df_historical_races_focus$Date))>0){
#     df_historical_races_focus <- df_historical_races_focus[!is.na(df_historical_races_focus$Date),]
#   }
#   
#   if(sum(is.na(df_historical_races_focus$Distance))>0){
#     df_historical_races_focus <- df_historical_races_focus[!is.na(df_historical_races_focus$Distance),]
#   }
#   
#   df_stats_numero_autostart_number <- df_historical_races_focus %>%
#     mutate(Numero = as.numeric(Numero)) %>%
#     mutate(Place = as.numeric(Place)) %>%
#     distinct() %>%
#     group_by(ID,Numero) %>%
#     dplyr::summarise(Size = n(),
#                      Podium = get_number_top_five(Place),
#                      Chance = round(Podium/Size,2)) %>%
#     as.data.frame()
#   
#   saveRDS(df_stats_numero_autostart_number,path_stats_numero_autostart)
#   
#   return(NULL)
# }
# 
# 
# 
# 
# 
