graph_compiled_identidy_cards_geny <- function (path_individual_id_cards = "/mnt/Master-Data/Futanke-Mbayar/France/report/2020-10-17/Caen/8ème-course--Prix-de-la-Ville-de-Caen/ID") {
  
  vec_indiv_files <- dir(path_individual_id_cards,pattern=".tiff")
  
  if(length(vec_indiv_files)==6)
  {
    vec.mfrow = c(2,3)
  } else if (length(vec_indiv_files)==8) {
    vec.mfrow = c(2,4)
  } else if(length(vec_indiv_files)==9) {
    vec.mfrow = c(3,3)
  } else if (length(vec_indiv_files)==10) {
    vec.mfrow = c(2,5)
  } else if (length(vec_indiv_files)==12) {
    vec.mfrow = c(3,4)
  } else if (length(vec_indiv_files)==16) {
    vec.mfrow = c(4,4)
  } else if (length(vec_indiv_files)==17) {
    vec.mfrow = c(3,6)
  } else if (length(vec_indiv_files)==18) {
    vec.mfrow = c(3,6)
  } else if (length(vec_indiv_files)==19) {
    vec.mfrow = c(4,5)
  } else if (length(vec_indiv_files)==20) {
    vec.mfrow = c(4,5)
  } else if (length(vec_indiv_files)==13) {
    vec.mfrow = c(3,5)
  } else if (length(vec_indiv_files)==14) {
    vec.mfrow = c(3,5)
  } else if (length(vec_indiv_files)==15) {
    vec.mfrow = c(3,5)
  } else if (length(vec_indiv_files)==11) {
    vec.mfrow = c(3,4)
  } else if (length(vec_indiv_files)==4) {
    vec.mfrow = c(2,2)
  } else if (length(vec_indiv_files)==5) {
    vec.mfrow = c(2,3)
  } else if(length(vec_indiv_files)==7) {
    vec.mfrow = c(2,4)
  } else if(length(vec_indiv_files)==3) {
    vec.mfrow = c(2,2)
  }
  
  par(mfrow=vec.mfrow)
  par(mar=c(0,0,0,0))

  for(id_card_file in 1:length(vec_indiv_files)){
    if(length(grep(paste("^",paste(id_card_file,"-",sep=""),sep=""),vec_indiv_files,value=TRUE))>0) {
      current_id_card <- paste(path_individual_id_cards,grep(paste("^",paste(id_card_file,"-",sep=""),sep=""),vec_indiv_files,value=TRUE),sep="/")
      image_current_id_card <- tiff::readTIFF(current_id_card, native=TRUE)
      label_current_id_card_converted <- sub(".tiff",".jpeg",current_id_card)
      jpeg::writeJPEG(image_current_id_card, target = label_current_id_card_converted, quality = 1)
      plot(imager::load.image(label_current_id_card_converted),xaxt="n",yaxt="n",axes=FALSE)
    }
  }
} 