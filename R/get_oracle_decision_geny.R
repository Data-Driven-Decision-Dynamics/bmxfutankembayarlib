get_oracle_decision_geny <- function(df_decision_criteria = NULL,
                                     df_decision_penalties = NULL,
                                     path_input_decision_system = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/input/Final-Decision-System-Mame-Lolly.xlsx") {
  
  
  message("Loading reference of historical performance data")
  if(!exists("df_historical_races_geny")) {
    df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    if(sum(is.na(df_historical_races_geny$Place))>0) {
      df_historical_races_geny$Place[is.na(df_historical_races_geny$Place)] <- 100
    }
  }
  message("Loading reference of historical performance data")
  
  message("Reading decision system details")
  if(file.exists(path_input_decision_system)){
    df_decision_system_betting <- xlsx::read.xlsx(path_input_decision_system,sheetIndex = 1)
    df_decision_system_betting <- as.data.frame(df_decision_system_betting)
  }
  message("Reading decision system details")

  df_decision_criteria_recode <- df_decision_criteria
  for(criteria in colnames(df_decision_criteria)[2:6])
  {
    df_decision_criteria_recode[,criteria] <- "Normal"
    vec_val_class <- unique(sort(df_decision_criteria[ ,"Class"],decreasing = TRUE))
    threshold_high <- vec_val_class[3]
    if(length(vec_val_class)>6){
      threshold_moderate <- vec_val_class[7]
    } else {
      threshold_moderate <- vec_val_class[length(vec_val_class)]
    }
    
    idx_top_scored <- df_decision_criteria[,criteria] >= threshold_high
    if(length(idx_top_scored==TRUE)>0){
      df_decision_criteria_recode[which(idx_top_scored ==TRUE),criteria] <- "High"
    }
    idx_good_scored <- df_decision_criteria[,criteria] < threshold_high &  df_decision_criteria[,criteria] >= threshold_moderate
    if(length(idx_good_scored==TRUE)>0){
      df_decision_criteria_recode[which(idx_good_scored==TRUE),criteria] <- "Good"
    }
  }
  
  df_decision_criteria_recode$Decision <- NA
  for(current_horse in unique(df_decision_criteria_recode$Cheval)){
    class_current_horse <- df_decision_criteria_recode[df_decision_criteria_recode$Cheval == current_horse ,"Class"]
    engagement_current_horse <- df_decision_criteria_recode[df_decision_criteria_recode$Cheval == current_horse ,"Engagement"]
    aptitude_current_horse <- df_decision_criteria_recode[df_decision_criteria_recode$Cheval == current_horse ,"Aptitude"]
    intention_current_horse <- df_decision_criteria_recode[df_decision_criteria_recode$Cheval == current_horse ,"Intention"]
    fitness_current_horse <- df_decision_criteria_recode[df_decision_criteria_recode$Cheval == current_horse ,"Form"]
    df_decision_criteria_recode[df_decision_criteria_recode$Cheval == current_horse ,"Decision"] <- df_decision_system_betting %>%
      filter(Class == class_current_horse ) %>%
      filter(Engagement == engagement_current_horse ) %>%
      filter(Aptitude == aptitude_current_horse ) %>%
      filter(Intention == intention_current_horse ) %>%
      filter(Form == fitness_current_horse ) %>%
      dplyr::select(Decision) %>%
      unlist() %>%
      as.vector()
  }
  message("Compiling the various signals for final decision")
  
  message("Testing if there is penalties")
  if(!is.null(df_decision_penalties)){
    if(max(df_decision_penalties$Penalty,na.rm = TRUE)>0){
      vec_candidates_penalties <- df_decision_penalties[df_decision_penalties$Penalty == 1,"Cheval"]
      df_decision_criteria_recode[df_decision_criteria_recode$Cheval %in% vec_candidates_penalties ,"Decision"] <- "Penalties"
    }
  }
  message("Testing if there is penalties")

  return(df_decision_criteria_recode)
}