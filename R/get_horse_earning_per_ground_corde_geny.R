get_horse_earning_per_ground_corde_geny <- function(df_infos_target_race_geny = NULL, df_historical_races_competitors_geny = NULL, number_days_back = 366) {
  
  message("Initialize final output")
  df_mean_earning_ground_corde <- NULL
  df_max_winning_travel_ground_corde <- NULL
  infos_corde_ground <- NULL
  
  message("Reading trainers geolocalization file")
  if(!exists("df_trainer_geolocalization")) {
    if(file.exists(path_trainer_geolocalization)) {
      df_trainer_geolocalization <- read.xlsx(path_trainer_geolocalization,sheetIndex  = 1) %>%
        as.data.frame()
    }
  }
  
  message("Reading geolocalization file")
  if(!exists("df_track_geolocalization")) {
    if(file.exists(path_track_geolocalization)) {
      df_track_geolocalization <- read.xlsx(path_track_geolocalization,sheetIndex = 1) %>%
        as.data.frame()
      df_track_geolocalization <- df_track_geolocalization[complete.cases(df_track_geolocalization),]
    }
  }
  
  message("Reading trainers lookup geolocalization file")
  if(!exists("df_trainer_lookup_address")) {
    if(file.exists(path_output_trainer_lookup_address)) {
      df_trainer_lookup_address <- read.xlsx(path_output_trainer_lookup_address,sheetIndex = 1) %>%
        as.data.frame()
      df_trainer_lookup_address <- df_trainer_lookup_address[df_trainer_lookup_address$Distance == 0 , ]
    }
  }
  
  message("Extract race date and convert in the right format if need")
  current_race_date  <- as.Date(unique(df_infos_target_race_geny$Date))
  
  if(!is.null(df_infos_target_race_geny)){
    df_infos_target_race_geny$Epreuve <- gsub("FALSE","F",df_infos_target_race_geny$Epreuve)
  }
  
  message("Extract prize of current candidates")
  current_race_prize <- as.numeric(gsub("\n","",unique(df_infos_target_race_geny$Prix)))
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
  
  message("Extract category of current race")
  current_race_category <- gsub("\n","",unique(df_infos_target_race_geny$Epreuve))
  
  message("Extract distance of current race")
  current_race_distance <- min(as.numeric(gsub("\n","",unique(df_infos_target_race_geny$Distance))))
  
  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
  
  message("Extract hippodrome of current race")
  current_race_location <- as.vector(unique(df_infos_target_race_geny$Lieu))
  
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
  current_race_terrain <- sub("Machefer","Cendrée",current_race_terrain)
  current_race_terrain <- sub("Mâchefer","Cendrée",current_race_terrain)
  
  message("Extract terrain of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
  
  message("Extract type od depart")
  if(current_race_discipline=="Trot Attelé"){
    message("Extract depart of current race")
    current_depart <- as.vector(unique(df_infos_target_race_geny$Depart))
  }
  
  current_race_going <- NA
  message("Getting Going for galop races")
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Plat","Steeplechase","Cross","Haies")){
    message("Getting depart of current race")
    if("ETAT_TERRAIN" %in% colnames(df_infos_target_race_geny)){
      current_race_going <- as.vector(unique(df_infos_target_race_geny$ETAT_TERRAIN))
      message("Quick cleaning of going")
      if(!is.null(current_race_terrain)){
        if(!is.na(current_race_terrain)){
          if(current_race_terrain == "Sable Fibré") {
            current_race_going <- "Standard"
          }
        }
      }
    }
  }
  
  rownames(df_infos_target_race_geny) <- as.vector(df_infos_target_race_geny$Cheval)
  
  if(current_race_discipline %in% c("Plat","Trot Monté","Haies","Steeplechase","Cross")){
    df_infos_focus_ground_corde <- df_historical_races_competitors_geny %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(current_race_date-Date<=number_days_back) %>%
      filter(Date<current_race_date) %>%
      filter(Discipline==current_race_discipline) %>%
      filter(Gains>0) %>%
      dplyr::select(Cheval,Gains,Corde,Terrain,Date,Poids,Lieu,Entraineur,Prix,Speed) %>%
      distinct() %>%
      drop_na() %>%
      as.data.frame()
  } else {
    df_infos_focus_ground_corde <- df_historical_races_competitors_geny %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(current_race_date-Date<=number_days_back) %>%
      filter(Date<current_race_date) %>%
      filter(Discipline==current_race_discipline) %>%
      filter(Gains>0) %>%
      dplyr::select(Cheval,Gains,Corde,Terrain,Date,Lieu,Entraineur,Prix,Speed) %>%
      distinct() %>%
      drop_na() %>%
      as.data.frame()
  }

  if(nrow(df_infos_focus_ground_corde)>0){
    df_infos_focus_ground_corde$Terrain <- gsub("Mâchefer","Cendrée",df_infos_focus_ground_corde$Terrain)
    df_infos_focus_ground_corde$Terrain <- gsub("Machefer","Cendrée",df_infos_focus_ground_corde$Terrain)
    idx_config <- df_infos_focus_ground_corde$Corde == current_race_corde & df_infos_focus_ground_corde$Terrain == current_race_terrain 
    idx_corde <- df_infos_focus_ground_corde$Corde == current_race_corde & df_infos_focus_ground_corde$Terrain != current_race_terrain
    idx_terrain <- df_infos_focus_ground_corde$Corde != current_race_corde & df_infos_focus_ground_corde$Terrain == current_race_terrain
    idx_else <- df_infos_focus_ground_corde$Corde != current_race_corde & df_infos_focus_ground_corde$Terrain != idx_terrain
    if(is.na(current_race_terrain)){
      idx_corde <- df_infos_focus_ground_corde$Corde == current_race_corde
      idx_else <- df_infos_focus_ground_corde$Corde != current_race_corde
    }
    if(is.na(current_race_corde)){
      idx_terrain <- df_infos_focus_ground_corde$Terrain == idx_terrain
      idx_else <- df_infos_focus_ground_corde$Terrain != idx_terrain
    }
    df_infos_focus_ground_corde$Class <- NA
    
    if(sum(idx_config,na.rm=TRUE)>0){
      df_infos_focus_ground_corde[which(idx_config==TRUE),"Class"] <-"Config"
    }
    if(sum(idx_corde,na.rm=TRUE)>0){
      df_infos_focus_ground_corde[which(idx_corde==TRUE),"Class"] <-"Turn"
    }
    if(sum(idx_else,na.rm=TRUE)>0){
      df_infos_focus_ground_corde[which(idx_else==TRUE),"Class"] <-"Others"
    }
    if(sum(idx_terrain,na.rm=TRUE)>0){
      df_infos_focus_ground_corde[which(idx_terrain==TRUE),"Class"] <-"Ground"
    }
  }

  if(current_race_discipline %in% c("Plat","Haies","Steeplechase","Cross")){
    if(nrow(df_infos_focus_ground_corde)>0){
      df_mean_earning_ground_corde <- df_infos_focus_ground_corde %>%
        filter(Gains>0) %>%
        group_by(Cheval,Class) %>%
        top_n(1,Gains) %>%
        as.data.frame()
    }
  }
  
  if(current_race_discipline %in% c("Trot Monté","Trot Attelé")){
    if(nrow(df_infos_focus_ground_corde)>0){
      df_mean_earning_ground_corde <- df_infos_focus_ground_corde [,c("Cheval","Class","Gains")] %>%
        mutate(Gains=get_percentile_values(Gains)) %>%
        pivot_wider(
          names_from = Class,
          values_from = Gains,
          values_fn = max
        ) %>%
        as.data.frame()
      
      colnames(df_mean_earning_ground_corde) <- gsub(" ","_",colnames(df_mean_earning_ground_corde))
      colnames(df_mean_earning_ground_corde) <- gsub("-","_",colnames(df_mean_earning_ground_corde))
      colnames(df_mean_earning_ground_corde) <- gsub("é","e",colnames(df_mean_earning_ground_corde))
      colnames(df_mean_earning_ground_corde) <- tolower(gsub("â","a",colnames(df_mean_earning_ground_corde)))
      colnames(df_mean_earning_ground_corde) <- stringi::stri_trans_totitle(sub("cheval","Cheval",colnames(df_mean_earning_ground_corde)))
      
      if(nrow(df_mean_earning_ground_corde)<nrow(df_infos_target_race_geny)){
        df_mean_earning_ground_corde <- merge(df_mean_earning_ground_corde,df_infos_target_race_geny[,c("Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE) 
      }
      
      if(!is.null(df_mean_earning_ground_corde)){
        if(nrow(df_mean_earning_ground_corde)>0){
          colnames(df_mean_earning_ground_corde) <- stringi::stri_trans_totitle(colnames(df_mean_earning_ground_corde))
          df_mean_earning_ground_corde <- df_mean_earning_ground_corde[,intersect(c("Cheval","Others","Ground","Turn","Config","Going"),colnames(df_mean_earning_ground_corde))]
          df_mean_earning_ground_corde[,-1] <- round(df_mean_earning_ground_corde[,-1],2)
        }
      }
    }
    
    if(nrow(df_mean_earning_ground_corde)==0) {
      df_mean_earning_ground_corde <- NULL
    } 
    
    df_infos_focus_ground_corde <- merge(df_infos_focus_ground_corde,df_trainer_lookup_address[,c("Entraineur","Lookup")],by.x="Entraineur",by.y="Entraineur",all.x = TRUE)
    df_infos_focus_ground_corde <- merge(df_infos_focus_ground_corde,df_trainer_geolocalization,by.x="Lookup",by.y="Entraineur",all.x=TRUE)
    
    if(nrow(df_infos_focus_ground_corde)>0){
      list_infos_focus_ground_corde <- split.data.frame(df_infos_focus_ground_corde,df_infos_focus_ground_corde$Cheval)
      list_infos_focus_ground_corde_travel <- lapply(list_infos_focus_ground_corde,get_mean_distance_home_visiting_location_geny)
      df_infos_focus_ground_corde_travel <- dplyr::bind_rows(list_infos_focus_ground_corde_travel) %>%
        as.data.frame()
      
      if(nrow(df_infos_focus_ground_corde_travel)>0){
        df_max_winning_travel_ground_corde <- df_infos_focus_ground_corde_travel %>%
          filter(Gains>0) %>%
          select(c("Cheval","Class","Travel")) %>%
          pivot_wider(
            names_from = Class,
            values_from = Travel,
            values_fn = max
          ) %>%
          as.data.frame()
      }
    }
  }

  infos_corde_ground <- list(Earning=df_mean_earning_ground_corde,Travel=df_max_winning_travel_ground_corde)
  infos_corde_ground <- infos_corde_ground[unlist(lapply(infos_corde_ground,length))>0]
  
  return(infos_corde_ground)
  
}
