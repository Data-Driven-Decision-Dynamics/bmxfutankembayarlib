get_imputed_standardized_performance_geny <- function(df_infos_missing_standardized  = NULL ,
                                                      path_df_standardized_times_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_standardized_times_geny.rds"){
  
  message("Initialization of final output")
  val_standardized_to_input <- NA
  message("Initialization of final output")
  
  message("Reading details of standardized times")
  if(!exists("df_standardized_times")){
    df_standardized_times <- readRDS(path_df_standardized_times_geny)
  }
  message("Reading details of standardized times")

  message("Extracting details of associated race")
  place_standardized_missing <- df_infos_missing_standardized$Place
  track_standardized_missing <- df_infos_missing_standardized$Track
  epreuve_standardized_missing <- df_infos_missing_standardized$Epreuve
  message("Extracting details of associated race")
  
  message("Running the estimation of the value to input in case of disqualification")
  if(!is.na(place_standardized_missing) & !is.na(track_standardized_missing) & !is.na(epreuve_standardized_missing)){
    if(place_standardized_missing == 100){
      val_standardized_to_input <- df_standardized_times %>%
        filter(Track == track_standardized_missing) %>%
        filter(Epreuve == epreuve_standardized_missing) %>%
        select(Cheval,Place,Standardized,Epreuve,Race) %>%
        drop_na() %>%
        group_by(Race) %>%
        top_n(1,Place) %>%
        as.data.frame() %>%
        select(Standardized) %>%
        unlist() %>%
        median()
      if(length(val_standardized_to_input)>0){
        if(!is.na(val_standardized_to_input)){
          val_standardized_to_input <- val_standardized_to_input - 10
          if(val_standardized_to_input>0){
            val_standardized_to_input <- 0
          }
        }
      }
    }
  }
  message("Running the estimation of the value to input in case of disqualification")
  
  message("Running the estimation of the value to input in case the horse finish the race")
  if(!is.na(place_standardized_missing) & !is.na(track_standardized_missing) & !is.na(epreuve_standardized_missing)){
    if(place_standardized_missing != 100){
      val_standardized_to_input <- df_standardized_times %>%
        filter(Track == track_standardized_missing) %>%
        filter(Epreuve == epreuve_standardized_missing) %>%
        filter(Place == place_standardized_missing) %>%
        select(Cheval,Place,Standardized,Epreuve,Race) %>%
        drop_na() %>%
        as.data.frame() %>%
        select(Standardized) %>%
        unlist() %>%
        median()
      if(length(val_standardized_to_input)>0){
        if(!is.na(val_standardized_to_input)){
          val_standardized_to_input <- val_standardized_to_input
        }
      }
    }
  }
  message("Running the estimation of the value to input in case the horse finish the race")

  return(val_standardized_to_input)
}
  