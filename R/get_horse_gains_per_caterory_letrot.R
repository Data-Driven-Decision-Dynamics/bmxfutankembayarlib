get_horse_gains_per_caterory_letrot <- function(df_infos_target_race = NULL,
                                                df_historical_races_competitors = NULL,
                                                path_cluster_distance_range_letrot = "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/input/mat_ranges_cluster_distance.rds",
                                                number_days_back = 366)
{
  
  df_date_gains_categories_specific <- NULL
  df_gains_categories_specific <- NULL
  df_gains_categories_others <- NULL
  
  message("Extract race date and convert in the right format if need")
  if(class(unique(df_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race$Date)
  }
  
  if(!is.null(df_infos_target_race)){
    df_infos_target_race$Epreuve <- gsub("FALSE","F",df_infos_target_race$Epreuve)
  }
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))
  
  message("Extract category of current race")
  current_race_category <- gsub("\n","",unique(df_infos_target_race$Epreuve))
  
  message("Extract distance of current race")
  current_race_distance <- min(as.numeric(gsub("\n","",unique(df_infos_target_race$Distance))))
  
  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race$Discipline)
  current_race_discipline <- paste("Trot",current_race_discipline)
  
  message("Extract hippodrome of current race")
  current_race_hippodrome <- as.vector(unique(df_infos_target_race$Lieu))
  
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race$Terrain))
  
  message("Extract corde of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race$Corde))
  
  message("Reading file with cluster distance definition")
  df_cluster_distance_range <- readRDS(path_cluster_distance_range)
  df_cluster_distance_range_discipline <- df_cluster_distance_range[df_cluster_distance_range$Discipline==current_race_discipline,]
  
  if(current_race_discipline=="Trot Attelé"){
    message("Extract depart of current race")
    current_race_depart <- as.vector(unique(df_infos_target_race$Depart))
  }
  
  idx_corde_terrain_current <- which(df_historical_races_competitors$Corde == current_race_corde | df_historical_races_competitors$Terrain == current_race_terrain)
  if(!is.na(current_race_corde) & !is.na(current_race_terrain)){
    message("Focusing on right racess for configuration similar to the one the current race")
    if(sum(current_race_horses %in% unique(df_historical_races_competitors$Cheval))>0) {
      df_historical_races_geny_focus <- df_historical_races_competitors[idx_corde_terrain_current,] %>%
        filter(Cheval %in% current_race_horses) %>%
        filter(Discipline == current_race_discipline) %>%
        filter(current_race_date-Date<=number_days_back) %>%
        filter(Date<current_race_date) %>%
        group_by(Cheval) %>%
        as.data.frame()
      message("Computing the earning per race classes for configuration similar to the one the current race")
      if(nrow(df_historical_races_geny_focus)>0){
        if(sum(!is.na(df_historical_races_geny_focus$Epreuve))>0){
          if(current_race_discipline %in% c("Trot Attelé","Trot Monté")){
            mat_categories <- data.frame(Category=c("I","II","III","A","B","C","D","E","F","G","R"),Values=seq(1:length(c("I","II","III","A","B","C","D","E","F","G","R"))))
          }
          mat_gains_focus <- merge(df_historical_races_geny_focus,mat_categories,by.x="Epreuve",by.y="Category")
          mat_gains_focus <- mat_gains_focus[,intersect(colnames(mat_gains_focus),c("Cheval","Gains","Epreuve","Date","Poids","Speed","Distance"))]
          
          df_gains_categories_specific <- mat_gains_focus[,c("Cheval","Gains","Epreuve")] %>%
            tidyr::pivot_wider(
              names_from = Epreuve,
              values_from = Gains,
              values_fn = max
            ) %>%
            as.data.frame()
          
          if(!is.null(df_gains_categories_specific)){
            if(nrow(df_gains_categories_specific)>0){
              vec_ordered_columns_gains_epreuves <- c("Cheval",rev(intersect(c("I","II","III","A","B","C","D","E","F","G","R"),colnames(df_gains_categories_specific))))
              df_gains_categories_specific <- df_gains_categories_specific[,vec_ordered_columns_gains_epreuves]
            }
          }
          
          if(nrow(df_gains_categories_specific)>0){
            if(nrow(df_gains_categories_specific)<nrow(df_infos_target_race)){
              df_gains_categories_specific <- merge(df_gains_categories_specific,df_infos_target_race[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
            }
          }
          
          df_date_gains_categories_specific <- mat_gains_focus[,c("Cheval","Date","Epreuve")]
          df_date_gains_categories_specific <- df_date_gains_categories_specific %>%
            tidyr::pivot_wider(
              names_from = Epreuve,
              values_from = Date,
              values_fn = max
            ) %>%
            as.data.frame()
          
          if(!is.null(df_date_gains_categories_specific)){
            if(nrow(df_date_gains_categories_specific)>0){
              vec_ordered_columns_gains_epreuves <- c("Cheval",rev(intersect(c("I","II","III","A","B","C","D","E","F","G","R"),colnames(df_date_gains_categories_specific))))
              df_date_gains_categories_specific <- df_date_gains_categories_specific[,vec_ordered_columns_gains_epreuves]
              df_gains_categories_specific [,-1] <- round(df_gains_categories_specific [,-1],2)
            }
          }
          
          if(nrow(df_date_gains_categories_specific)>0){
            if(nrow(df_date_gains_categories_specific)<nrow(df_infos_target_race)){
              df_date_gains_categories_specific <- merge(df_date_gains_categories_specific,df_infos_target_race[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
            }
          }
          
          if(nrow(df_date_gains_categories_specific)>0){
            if(nrow(df_date_gains_categories_specific)<nrow(df_infos_target_race)){
              df_date_gains_categories_specific <- merge(df_date_gains_categories_specific,df_infos_target_race[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
            }
          }
        }
      }
    }
    
    message("Focusing on right racess for configuration different from the one the current race")
    idx_corde_terrain_current <- which(df_historical_races_competitors$Corde == current_race_corde | df_historical_races_competitors$Terrain == current_race_terrain)
    if(sum(current_race_horses %in% unique(df_historical_races_competitors$Cheval))>0) {
      df_historical_races_geny_focus <- df_historical_races_competitors[-idx_corde_terrain_current,] %>%
        filter(Cheval %in% current_race_horses) %>%
        filter(Discipline == current_race_discipline) %>%
        filter(Date<current_race_date) %>%
        filter(current_race_date-Date<=number_days_back) %>%
        group_by(Cheval) %>%
        as.data.frame()
      message("Computing the earning per race classes for configuration similar to the one the current race")
      if(nrow(df_historical_races_geny_focus)>0){
        if(sum(!is.na(df_historical_races_geny_focus$Epreuve))>0){
          
          if(current_race_discipline %in% c("Trot Attelé","Trot Monté")){
            mat_categories <- data.frame(Category=c("I","II","III","A","B","C","D","E","F","G","R"),Values=seq(1:length(c("I","II","III","A","B","C","D","E","F","G","R"))))
          }
          
          mat_gains_focus <- merge(df_historical_races_geny_focus,mat_categories,by.x="Epreuve",by.y="Category")
          mat_gains_focus <- mat_gains_focus[,intersect(colnames(mat_gains_focus),c("Cheval","Gains","Epreuve","Date","Poids","Speed","Distance"))]
          
          df_gains_categories_others <- mat_gains_focus[,c("Cheval","Gains","Epreuve")] %>%
            tidyr::pivot_wider(
              names_from = Epreuve,
              values_from = Gains,
              values_fn = max
            ) %>%
            as.data.frame()
          
          # if(nrow(df_gains_categories_others)>0){
          #   df_gains_categories_others$Gains <- get_percentile_values(df_gains_categories_others$Gains)
          # }
          if(nrow(df_gains_categories_others)>0){
            if(nrow(df_gains_categories_others)<nrow(df_infos_target_race)){
              df_gains_categories_others <- merge(df_gains_categories_others,df_infos_target_race[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
            }
          }
          
          if(!is.null(df_gains_categories_others)){
            if(nrow(df_gains_categories_others)>0){
              vec_ordered_columns_gains_epreuves <- c("Cheval",rev(intersect(c("I","II","III","A","B","C","D","E","F","G","R"),colnames(df_gains_categories_others))))
              df_gains_categories_others <- df_gains_categories_others[,vec_ordered_columns_gains_epreuves]
            }
          }
          
          if(nrow(df_gains_categories_others)>0){
            if(nrow(df_gains_categories_others)<nrow(df_infos_target_race)){
              df_gains_categories_others <- merge(df_gains_categories_others,df_infos_target_race[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
            }
          }
        }
      }
    }
  }
  
  return(list(Specific=df_gains_categories_specific,Others=df_gains_categories_others,Date=df_date_gains_categories_specific))
}