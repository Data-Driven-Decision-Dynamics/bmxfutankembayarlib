get_min_max_normalization <- function(x)
{
  return((x- min(x,na.rm = TRUE)) /(max(x,na.rm = TRUE)-min(x,na.rm = TRUE)))
}

