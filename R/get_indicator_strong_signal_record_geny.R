get_indicator_strong_signal_record_geny <- function(df_infos_records = NULL, number_top_values = 3){
  
  df_selected_record <- NULL
  df_selected_horse_record_config <- NULL
  df_selected_horse_record_turn <- NULL
  df_selected_horse_record_ground <- NULL
  df_selected_horse_record_others <- NULL
  df_selected_horse_record_global <- NULL
  
  message("Processing Record Data To Identify Candidates With Nice Profile")
  if(!is.null(df_infos_records)){
    
    if("Class" %in% colnames(df_infos_records) & "Cheval" %in% colnames(df_infos_records) & "Score" %in% colnames(df_infos_records)){
      df_infos_records_formated <- df_infos_records %>%
        dplyr::select(Cheval,Class,Score) %>%
        tidyr::pivot_wider(
          names_from = Class,
          values_from = Score,
          values_fn = mean
        ) %>%
        as.data.frame()
      if(nrow(df_infos_records_formated)>0){
        if(ncol(df_infos_records_formated)>1){
          colnames(df_infos_records_formated)[-1] <- toupper(colnames(df_infos_records_formated)[-1])
        }
      }
    }
    
    idx_top_values_config <- NULL
    if("CONFIG" %in% colnames(df_infos_records_formated)){
      if(sum(!is.na(df_infos_records_formated[,"CONFIG"]),na.rm=TRUE)>=ceiling(nrow(df_infos_records_formated)/2)){
        df_infos_records_formated <- df_infos_records_formated[order(df_infos_records_formated[,"CONFIG"],decreasing = TRUE),]
        idx_top_values_config <- df_infos_records_formated[1:number_top_values,"Cheval"]
      }
    }
    
    idx_top_values_turn <- NULL
    if("TURN" %in% colnames(df_infos_records_formated)){
      if(sum(!is.na(df_infos_records_formated[,"TURN"]),na.rm=TRUE)>=ceiling(nrow(df_infos_records_formated)/2)){
        df_infos_records_formated <- df_infos_records_formated[order(df_infos_records_formated[,"TURN"],decreasing = TRUE),]
        idx_top_values_turn <- df_infos_records_formated[1:number_top_values,"Cheval"]
      }
    }
    
    idx_top_values_ground <- NULL
    idx_top_values_ground_from_config <- NULL
    idx_top_values_ground_from_turn <- NULL
    if(!is.null(idx_top_values_config)){
      val_max_record_config <- max(df_infos_records_formated[,"CONFIG"],na.rm=TRUE)
      if("GROUND" %in% colnames(df_infos_records_formated)){
        test_idx_top_values_config <- df_infos_records_formated[,"GROUND"] >= val_max_record_config
        if(sum(test_idx_top_values_config,na.rm = TRUE)>0){
          idx_top_values_ground_from_config <- df_infos_records_formated[test_idx_top_values_config,"Cheval"]
        }
      }
    }
    if(!is.null(idx_top_values_turn)){
      val_max_record_turn <- max(df_infos_records_formated[,"TURN"],na.rm=TRUE)
      if("GROUND" %in% colnames(df_infos_records_formated)){
        test_idx_top_values_turn <- df_infos_records_formated[,"GROUND"] >= val_max_record_turn
        if(sum(test_idx_top_values_turn,na.rm = TRUE)>0){
          idx_top_values_ground_from_turn <- df_infos_records_formated[test_idx_top_values_turn,"Cheval"]
        }
      }
    }
    idx_top_values_ground <- unique(c(idx_top_values_ground_from_config,idx_top_values_ground_from_turn))
    if(!is.null(idx_top_values_ground)){
      if(sum(!is.na(idx_top_values_ground),na.rm=TRUE)>0){
        if(sum(is.na(idx_top_values_ground),na.rm=TRUE)>0){
          idx_top_values_ground <- idx_top_values_ground[!is.na(idx_top_values_ground)]
        }
      }
    }
    
    idx_top_values_others <- NULL
    idx_top_values_others_from_config <- NULL
    idx_top_values_ground_from_turn <- NULL
    if(!is.null(idx_top_values_config)){
      val_max_record_config <- max(df_infos_records_formated[,"CONFIG"],na.rm=TRUE)
      if("OTHERS" %in% colnames(df_infos_records_formated)){
        test_idx_top_values_config <- df_infos_records_formated[,"OTHERS"] >= val_max_record_config
        if(sum(test_idx_top_values_config,na.rm = TRUE)>0){
          idx_top_values_others_from_config <- df_infos_records_formated[test_idx_top_values_config,"Cheval"]
        }
      }
    }
    if(!is.null(idx_top_values_turn)){
      val_max_record_turn <- max(df_infos_records_formated[,"TURN"],na.rm=TRUE)
      if("OTHERS" %in% colnames(df_infos_records_formated)){
        test_idx_top_values_turn <- df_infos_records_formated[,"OTHERS"] >= val_max_record_turn
        if(sum(test_idx_top_values_turn,na.rm = TRUE)>0){
          idx_top_values_ground_from_turn <- df_infos_records_formated[test_idx_top_values_turn,"Cheval"]
        }
      }
    }
    idx_top_values_others <- unique(c(idx_top_values_others_from_config,idx_top_values_ground_from_turn))
    
    idx_top_values_majority_cases <- NULL
    for(idx_item in 1:nrow(df_infos_records_formated)){
      test_number_non_missing_record <- sum(!is.na(unlist(df_infos_records_formated[idx_item,-1])),na.rm=TRUE)
      if(test_number_non_missing_record >=2){
        val_median_scores_all_records <- median(unlist(df_infos_records_formated[,-1]),na.rm=TRUE)
        vec_items_records_candidates <- unlist(df_infos_records_formated[idx_item,-1])>=val_median_scores_all_records
        vec_items_records_candidates <- colnames(df_infos_records_formated[,-1])[vec_items_records_candidates]
        if(sum(!is.na(vec_items_records_candidates))>0){
          if(sum(is.na(vec_items_records_candidates))>0){
            vec_items_records_candidates <- vec_items_records_candidates[!is.na(vec_items_records_candidates)]
          }
        }
        if(length(vec_items_records_candidates)>0){
          if(length(intersect(vec_items_records_candidates,c("TURN","CONFIG")))==2){
            id_top_values_majority_cases <- df_infos_records_formated$Cheval[idx_item]
            idx_top_values_majority_cases <- c(idx_top_values_majority_cases,id_top_values_majority_cases)
          }
        }
      }
    }
  }
  
  if(length(idx_top_values_config)>0){
    df_selected_horse_record_config <- data.frame(Cheval = idx_top_values_config, Family = "Aptitude" , Group = "Record" , Entity ="Config")
    df_selected_record <- rbind(df_selected_record,df_selected_horse_record_config)
  }
  
  if(length(idx_top_values_turn)>0){
    df_selected_horse_record_turn <- data.frame(Cheval = idx_top_values_turn, Family = "Aptitude" , Group = "Record" , Entity ="Turn")
    df_selected_record <- rbind(df_selected_record,df_selected_horse_record_turn)
  }
  
  if(length(idx_top_values_ground)>0){
    df_selected_horse_record_ground <- data.frame(Cheval = idx_top_values_ground, Family = "Aptitude" , Group = "Record" , Entity ="Ground")
    df_selected_record <- rbind(df_selected_record,df_selected_horse_record_ground)
  }
  
  if(length(idx_top_values_others)>0){
    df_selected_horse_record_others <- data.frame(Cheval = idx_top_values_others, Family = "Aptitude" , Group = "Record" , Entity ="Others")
    df_selected_record <- rbind(df_selected_record,df_selected_horse_record_others)
  }
  
  if(length(setdiff(idx_top_values_majority_cases,unique(df_selected_record$Cheval)))>0){
    df_selected_horse_record_global <- data.frame(Cheval = setdiff(idx_top_values_majority_cases,unique(df_selected_record$Cheval)), Family = "Aptitude" , Group = "Record" , Entity ="Global")
    df_selected_record <- rbind(df_selected_record,df_selected_horse_record_global)
  }
  
  return(df_selected_record)
}
