#' @return Compute engagement indicator for each candidate of a given race
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_trend_ferrage_letrot(df_infos_target_race_geny = NULL)
#' @export
verify_handcraft_signal_change_ferrage_geny <- function (df_infos_target_race_geny = NULL,
                                                         path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                         number_days_back = 366){
  
  message("Initialization of output")
  df_candidates_signal_change_ferrage <- data.frame(Cheval = df_infos_target_race_geny$Cheval, INTENTION_SIGNAL_TREND_FERRAGE = 0)
  message("Initialization of output")
  
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Trot Attelé","Trot Monté")){
    
    message("Reading historical race infos file")
    if(!exists("df_historical_races_geny")) {
      if(file.exists(path_df_historical_races_geny)) {
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    }
    
    message("Extract race date and convert in the right format if need")
    if(class(df_infos_target_race_geny$Date) [1] !="Date") {
      df_infos_target_race_geny$Date  <- as.Date(df_infos_target_race_geny$Date)
      current_race_date  <- unique(df_infos_target_race_geny$Date)
    } else {
      current_race_date <- unique(df_infos_target_race_geny$Date)
    }
    message("Extract race date and convert in the right format if need")
    
    message("Extract name of current candidates")
    current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
    message("Extract name of current candidates")
    
    message("Extract discipline of current race")
    current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
    message("Extract discipline of current race")
    
    message("Extract races of to focus on")
    df_historical_races_competitors_focus <- df_historical_races_geny %>%
      filter(Discipline == current_race_discipline) %>%
      filter(Cheval %in% current_race_horses) %>%
      filter(Date<current_race_date) %>%
      filter(current_race_date-Date<=number_days_back) %>%
      dplyr::select(Cheval,Date,Ferrage) %>%
      mutate(Pivot = paste(Cheval,Ferrage,sep="-")) %>%
      distinct() %>%
      drop_na() %>%
      group_by(Cheval) %>%
      top_n(3,Date) %>%
      as.data.frame()
    if(nrow(df_historical_races_competitors_focus)>0){
      for(current_horse in df_infos_target_race_geny$Cheval)
      {
        if(current_horse %in% df_historical_races_competitors_focus$Cheval){
          ferrage_current_horse <- df_infos_target_race_geny[df_infos_target_race_geny$Cheval == current_horse,"Ferrage"]
          ferrage_current_horse <- sub("D0",0,ferrage_current_horse)
          ferrage_current_horse <- sub("DA",2,ferrage_current_horse)
          ferrage_current_horse <- sub("DP",2,ferrage_current_horse)
          ferrage_current_horse <- as.numeric(sub("D4",4,ferrage_current_horse))
          df_infos_ferrage <- df_historical_races_competitors_focus[df_historical_races_competitors_focus$Cheval == current_horse,]
          ferrage_others_horse <- df_infos_ferrage$Ferrage
          ferrage_others_horse <- gsub("D0",0,ferrage_others_horse)
          ferrage_others_horse <- gsub("DA",2,ferrage_others_horse)
          ferrage_others_horse <- gsub("DP",2,ferrage_others_horse)
          ferrage_others_horse <- as.numeric(gsub("D4",4,ferrage_others_horse))
          vec_val_diff_ferrage <- ferrage_current_horse-ferrage_others_horse
          if(sum(vec_val_diff_ferrage==2) == length(vec_val_diff_ferrage)){
            df_candidates_signal_change_ferrage[df_candidates_signal_change_ferrage$Cheval == current_horse, "INTENTION_SIGNAL_TREND_FERRAGE"] <- 2
          }
          if(sum(vec_val_diff_ferrage>2) == length(vec_val_diff_ferrage)){
            df_candidates_signal_change_ferrage[df_candidates_signal_change_ferrage$Cheval == current_horse, "INTENTION_SIGNAL_TREND_FERRAGE"] <- 4
          }
          if(sum(vec_val_diff_ferrage== -2) == length(vec_val_diff_ferrage)){
            df_candidates_signal_change_ferrage[df_candidates_signal_change_ferrage$Cheval == current_horse, "INTENTION_SIGNAL_TREND_FERRAGE"] <- -2
          }
          if(sum(vec_val_diff_ferrage == -4) == length(vec_val_diff_ferrage)){
            df_candidates_signal_change_ferrage[df_candidates_signal_change_ferrage$Cheval == current_horse, "INTENTION_SIGNAL_TREND_FERRAGE"] <- -4
          }
        }
      }
    }
  }
  message("Extract races of to focus on")
  
  return(df_candidates_signal_change_ferrage)
}
