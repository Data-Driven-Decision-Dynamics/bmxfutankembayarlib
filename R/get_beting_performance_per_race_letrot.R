#' @return Compute various beting performance criteria
#' @importFrom magrittr %>%
#' @importFrom readxl read_excel
#' @importFrom dplyr top_n
#' @importFrom dplyr top_n
#' @importFrom dplyr summarise
#' @importFrom dplyr mutate
#' @examples
#' get_beting_performance_per_race_letrot(mat_infos_target_race = NULL)
#' @export
get_beting_performance_per_race_letrot <- function (df_prediction_details = NULL, path_beting_config = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/input/beting-types-ranges.xlsx") {
  
  df_beting_types_perf_compiled <- NULL
  
  if(!file.exists(path_beting_config)) {
    stop("Configuration file for beting range is mandatory")
  } else {
    df_beting_config <- read_excel(path_beting_config,sheet = 1) %>%
      as.data.frame()
    
    message("Check that the prediction details is not empty")
    if(is.null(df_prediction_details)){
      stop("Predicted parameters are needed")
    } else {
      df_prediction_details <- df_prediction_details[order(df_prediction_details$Lift,decreasing = TRUE),]
      for(idx_bet in 1:nrow(df_beting_config))
      {
        bet_type <- df_beting_config[idx_bet,"Bet"]
        is_order <- df_beting_config[idx_bet,"Order"]
        number_winners_bet_type <- df_beting_config[idx_bet,"Size"]
        range_nb_items <- as.vector(df_beting_config[df_beting_config$Bet==bet_type,c("Lower","Upper")])
        
        ranks_winning_start <- unique(unlist(df_beting_config[df_beting_config$Bet==bet_type,c("First")]))
        ranks_winning_end   <- unique(unlist(df_beting_config[df_beting_config$Bet==bet_type,c("Last")]))
        
        rank_winners <- unique(ranks_winning_start:ranks_winning_end)
        
        for(num_items in unlist((range_nb_items[1])):unlist((range_nb_items[2]))){
          df_beting_types_perf <- data.frame(Race=unlist(strsplit(unique(df_prediction_details$Identifier),"_"))[2],Beting=df_beting_config$Bet,Issue=NA)
          test_win_lost <- match(rank_winners,df_prediction_details[1:num_items,"Rank"])
          test_win_lost <- test_win_lost[!is.na(test_win_lost)]
          if(is_order==0) {
            df_beting_types_perf[df_beting_types_perf$Beting==bet_type,"Issue"] <- ifelse(sum(!is.na(test_win_lost))>=number_winners_bet_type,1,0)
          } else {
            tes_win_lost_final <- intersect(intersect(df_prediction_details[1:num_items,"Rank"],rank_winners),rank_winners)
            if(length(tes_win_lost_final)==length(rank_winners) & sum(diff(tes_win_lost_final)!=1)==0){
              df_beting_types_perf[df_beting_types_perf$Beting==bet_type,"Issue"] <- 1
            } else {
              df_beting_types_perf[df_beting_types_perf$Beting==bet_type,"Issue"] <- 0
            }
          }
          df_beting_types_perf$Size <- num_items
          df_beting_types_perf_compiled <- rbind(df_beting_types_perf,df_beting_types_perf_compiled)
          }
        }
      }
    }
  df_beting_types_perf_compiled <- df_beting_types_perf_compiled[!is.na(df_beting_types_perf_compiled$Issue),]
  return(df_beting_types_perf_compiled)
}


