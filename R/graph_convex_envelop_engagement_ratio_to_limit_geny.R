# graph_convex_envelop_engagement_ratio_to_limit_geny <- function(df_infos_engagement_limt_to_ration = NULL,
#                                                                 df_infos_target_race_geny = NULL,
#                                                                 path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
#                                                                 number_days_back = 720,
#                                                                 draw_plot=TRUE){
#   
#   df_infos_engagement_limt_to_ration_winning <- NULL
#   df_infos_engagement_limt_to_ration_loosing <- NULL
#   
#   if(!is.null(df_infos_target_race_geny)){
#     df_infos_target_race_geny <- get_race_maximum_limit_participation_geny(df_infos_target_race_geny)
#     df_infos_target_race_geny$Numero <- as.numeric(df_infos_target_race_geny$Numero)
#     df_infos_target_race_geny <- df_infos_target_race_geny[order(df_infos_target_race_geny$Numero),]
#     if (unique(df_infos_target_race_geny$Discipline) != "Trot Attelé") {
#       message("Extract race date and convert in the right format if need")
#       if (class(unique(df_infos_target_race_geny$Date)) != "Date") {
#         current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
#       } else {
#         current_race_date <- unique(df_infos_target_race_geny$Date)
#       }
#       
#       message("Extract name of current candidates")
#       current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
#       
#       message("Extract discipline of current race")
#       current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
#       
#       message("Extract terrain of current race")
#       current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
#       
#       message("Extract corde of current race")
#       current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
#     }
#   }
#   
#   message("Reading historical race infos file")
#   if (!exists("df_historical_races_geny")) {
#     if (file.exists(path_df_historical_races_geny)) {
#       df_historical_races_geny <- readRDS(path_df_historical_races_geny)
#     }
#   }
#   
#   if(!is.null(df_infos_engagement_limt_to_ration)){
#     if(nrow(df_infos_engagement_limt_to_ration)>0){
#       df_infos_engagement_limt_to_ration <- df_infos_engagement_limt_to_ration %>%
#         filter(current_race_date-Date<=number_days_back) %>%
#         as.data.frame()
#     }
#     idx_complete.cases <- !is.na(df_infos_engagement_limt_to_ration$Engagement) & !is.na(df_infos_engagement_limt_to_ration$Closeness_Limit)
#     if(sum(idx_complete.cases,na.rm = TRUE)>0){
#       df_infos_engagement_limt_to_ration <- df_infos_engagement_limt_to_ration[idx_complete.cases,]
#     }
#   }
#   
#   if(!is.null(df_infos_engagement_limt_to_ration)){
#     if(nrow(df_infos_engagement_limt_to_ration)>0){
#       df_infos_engagement_limt_to_ration$Terrain <- gsub("Mâchefer","Cendrée",df_infos_engagement_limt_to_ration$Terrain)
#       df_infos_engagement_limt_to_ration$Terrain <- gsub("Machefer","Cendrée",df_infos_engagement_limt_to_ration$Terrain)
#       idx_config <- df_infos_engagement_limt_to_ration$Corde == current_race_corde & df_infos_engagement_limt_to_ration$Terrain == current_race_terrain 
#       idx_corde <- df_infos_engagement_limt_to_ration$Corde == current_race_corde & df_infos_engagement_limt_to_ration$Terrain != current_race_terrain
#       idx_terrain <- df_infos_engagement_limt_to_ration$Corde != current_race_corde & df_infos_engagement_limt_to_ration$Terrain == current_race_terrain
#       idx_else <- df_infos_engagement_limt_to_ration$Corde != current_race_corde & df_infos_engagement_limt_to_ration$Terrain != current_race_terrain
#       df_infos_engagement_limt_to_ration$Class = NA
#       if(sum(idx_config,na.rm = TRUE)>0){
#         df_infos_engagement_limt_to_ration[which(idx_config==TRUE),"Class"] <-"Config"
#       }
#       if(sum(idx_corde,na.rm = TRUE)>0){
#         df_infos_engagement_limt_to_ration[which(idx_corde==TRUE),"Class"] <-"Turn"
#       }
#       if(sum(idx_else,na.rm = TRUE)>0){
#         df_infos_engagement_limt_to_ration[which(idx_else==TRUE),"Class"] <-"Others"
#       }
#       if(sum(idx_terrain,na.rm = TRUE)>0){
#         df_infos_engagement_limt_to_ration[which(idx_terrain==TRUE),"Class"] <-"Ground"
#       }
#     }
#   }
#   
#   if(!is.null(df_infos_engagement_limt_to_ration)){
#     if(nrow(df_infos_engagement_limt_to_ration)>0)
#     {
#       if(sum(df_infos_engagement_limt_to_ration$Gains>0,na.rm = TRUE)>0){
#         df_infos_engagement_limt_to_ration_winning <- df_infos_engagement_limt_to_ration[df_infos_engagement_limt_to_ration$Gains>0,]
#       }
#       if(sum(df_infos_engagement_limt_to_ration$Gains==0,na.rm = TRUE)>0){
#         df_infos_engagement_limt_to_ration_loosing <- df_infos_engagement_limt_to_ration[df_infos_engagement_limt_to_ration$Gains==0,]
#       }
#     }
#   }
#   
#   if(!is.null(df_infos_engagement_limt_to_ration_winning)){
#     if(nrow(df_infos_engagement_limt_to_ration_winning)>0){
#       vec_earnings_per_engagement_winning <- unique(df_infos_engagement_limt_to_ration_winning$Gains)
#       vec_earnings_per_engagement_winning <- vec_earnings_per_engagement_winning[vec_earnings_per_engagement_winning > 0]
#       vec_earnings_per_engagement_winning_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_earnings_per_engagement_winning))[rank(vec_earnings_per_engagement_winning)]
#       names(vec_earnings_per_engagement_winning_color) <- vec_earnings_per_engagement_winning
#     }
#   }
#   
#   if(!is.null(df_infos_engagement_limt_to_ration_winning)){
#     if(nrow(df_infos_engagement_limt_to_ration_winning)>0){
#       vec_earnings_per_engagement_winning <- unique(df_infos_engagement_limt_to_ration_winning$Gains)
#       vec_earnings_per_engagement_winning <- vec_earnings_per_engagement_winning[vec_earnings_per_engagement_winning > 0]
#       vec_earnings_per_engagement_winning_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_earnings_per_engagement_winning))[rank(vec_earnings_per_engagement_winning)]
#       names(vec_earnings_per_engagement_winning_color) <- vec_earnings_per_engagement_winning
#     }
#   }
#   
#   par(mar=c(3,3,2,2))
#   if(length(current_race_horses)==6)
#   {
#     vec.mfrow = c(2,3)
#   } else if (length(current_race_horses)==8) {
#     vec.mfrow = c(2,4)
#   } else if(length(current_race_horses)==9) {
#     vec.mfrow = c(3,3)
#   } else if (length(current_race_horses)==10) {
#     vec.mfrow = c(2,5)
#   } else if (length(current_race_horses)==12) {
#     vec.mfrow = c(3,4)
#   } else if (length(current_race_horses)==16) {
#     vec.mfrow = c(4,4)
#   } else if (length(current_race_horses)==17) {
#     vec.mfrow = c(3,6)
#   } else if (length(current_race_horses)==18) {
#     vec.mfrow = c(3,6)
#   } else if (length(current_race_horses)==19) {
#     vec.mfrow = c(4,5)
#   } else if (length(current_race_horses)==20) {
#     vec.mfrow = c(4,5)
#   } else if (length(current_race_horses)==13) {
#     vec.mfrow = c(3,5)
#   } else if (length(current_race_horses)==14) {
#     vec.mfrow = c(3,5)
#   } else if (length(current_race_horses)==15) {
#     vec.mfrow = c(3,5)
#   } else if (length(current_race_horses)==11) {
#     vec.mfrow = c(3,4)
#   } else if (length(current_race_horses)==4) {
#     vec.mfrow = c(2,2)
#   } else if (length(current_race_horses)==5) {
#     vec.mfrow = c(2,3)
#   } else if(length(current_race_horses)==7) {
#     vec.mfrow = c(2,4)
#   } else if(length(current_race_horses)==3) {
#     vec.mfrow = c(2,2)
#   }
# 
#     
#     if(!is.null(df_infos_engagement_limt_to_ration_winning) | !is.null(df_infos_engagement_limt_to_ration_loosing)){
#       if(nrow(df_infos_engagement_limt_to_ration_winning)>0 | nrow(df_infos_engagement_limt_to_ration_loosing)>0){
#         # par(mfrow=vec.mfrow)
#         for(horse in "Fuchsia Pierji")
#         {
#           engagement_reference_horse <-  as.numeric(subset(df_infos_target_race_geny,Cheval ==horse)[,"Engagement"])
#           engagement_closeness_limit_horse <-  as.numeric(subset(df_infos_target_race_geny,Cheval ==horse)[,"Closeness_Limit"])
#           numero_reference   <-  as.numeric(subset(df_infos_target_race_geny,Cheval ==horse)[,"Numero"])
#           
#           xlim_engagement <- range(c(unique(df_infos_target_race_geny$Engagement),df_infos_engagement_limt_to_ration[,c("Engagement")]),na.rm=TRUE)
#           xlim_closeness_limit <- range(c(unique(df_infos_target_race_geny$Closeness_Limit),df_infos_engagement_limt_to_ration[,c("Closeness_Limit")]),na.rm=TRUE)
#           
#           plot(df_infos_engagement_limt_to_ration_winning[,c("Engagement","Closeness_Limit")],xlab="",ylab="",type="n",main=paste(numero_reference,horse,sep="-"),xlim=xlim_engagement,ylim=xlim_closeness_limit)
#           rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = brewer.pal(11,"RdGy")[11])
#           
#           df_infos_engagement_limt_to_ration_winning_current_horse <- df_infos_engagement_limt_to_ration_winning[df_infos_engagement_limt_to_ration_winning$Cheval == horse,]
#           hpts <- chull(df_infos_engagement_limt_to_ration_winning_current_horse[complete.cases(df_infos_engagement_limt_to_ration_winning_current_horse[,c("Engagement","Closeness_Limit")]),c("Engagement","Closeness_Limit")])
#           hpts <- c(hpts, hpts[1])
#           val_earnings_per_engagement_winning_current_horse <- df_infos_engagement_limt_to_ration_winning_current_horse$Gains
#           col_earnings_per_engagement_winning_current_horse <- vec_earnings_per_engagement_winning_color[as.character(val_earnings_per_engagement_winning_current_horse)]
#           col_earnings_per_engagement_winning_current_horse[is.na(col_earnings_per_engagement_winning_current_horse)] <- "#808080 "
#           points(df_infos_engagement_limt_to_ration_winning_current_horse[,c("Engagement","Closeness_Limit")],pch=22,cex=2,col="white",bg=col_earnings_per_engagement_winning_current_horse)
#           points(engagement_reference_horse,engagement_closeness_limit_horse,pch=11,col="green",cex=1.5)
#           abline(v=engagement_reference_horse,lty="dashed",col="orange")
#           abline(h=engagement_closeness_limit_horse,lty="dashed",col="orange")
#           # lines(df_infos_engagement_limt_to_ration_winning_current_horse[,c("Engagement","Closeness_Limit")][hpts, ],col="limegreen")
#           
#           df_infos_engagement_limt_to_ration_loosing_current_horse <- df_infos_engagement_limt_to_ration_loosing[df_infos_engagement_limt_to_ration_loosing$Cheval == horse,]
#           hpts <- chull(df_infos_engagement_limt_to_ration_loosing_current_horse[complete.cases(df_infos_engagement_limt_to_ration_loosing_current_horse[,c("Engagement","Closeness_Limit")]),c("Engagement","Closeness_Limit")])
#           hpts <- c(hpts, hpts[1])
#           val_earnings_per_engagement_winning_current_horse <- df_infos_engagement_limt_to_ration_loosing_current_horse$Gains
#           col_earnings_per_engagement_winning_current_horse <- vec_earnings_per_engagement_winning_color[as.character(val_earnings_per_engagement_winning_current_horse)]
#           col_earnings_per_engagement_winning_current_horse[is.na(col_earnings_per_engagement_winning_current_horse)] <- "#FFFFFF"
#           points(df_infos_engagement_limt_to_ration_loosing_current_horse[,c("Engagement","Closeness_Limit")],pch=13,cex=2,col="white",bg="grey")
#           points(engagement_reference_horse,engagement_closeness_limit_horse,pch=11,col="green",cex=1.5)
#           abline(v=engagement_reference_horse,lty="dashed",col="orange")
#           abline(h=engagement_closeness_limit_horse,lty="dashed",col="orange")
#           # lines(df_infos_engagement_limt_to_ration_loosing_current_horse[,c("Engagement","Closeness_Limit")][hpts, ],col="red")
#         }
#       }
#     }
#     
# 
#     
#     
#     if(nrow(mat_weight_distance_speed) > 0) {
#       mat_weight_distance_speed <- merge(mat_weight_distance_speed,df_infos_target_race_geny[,c("Cheval","Numero")],by.x = "Cheval",by.y = "Cheval",all.y = TRUE)
#       mat_weight_distance_speed <- mat_weight_distance_speed[order(mat_weight_distance_speed$Numero),]
#       mat_weight_distance_speed <- mat_weight_distance_speed[complete.cases(mat_weight_distance_speed),]
#       df_infos_target_race_geny_focus <- df_infos_target_race_geny[,c("Cheval","Poids","Distance","Prix")]
#       colnames(df_infos_target_race_geny_focus) <- gsub("Poids","Weight",colnames(df_infos_target_race_geny_focus))
#       colnames(df_infos_target_race_geny_focus) <- gsub("Distance","Length",colnames(df_infos_target_race_geny_focus))
#       colnames(df_infos_target_race_geny_focus) <- gsub("Prix","Prize",colnames(df_infos_target_race_geny_focus))
#       mat_weight_distance_speed <- merge(mat_weight_distance_speed,df_infos_target_race_geny_focus,by.x = "Cheval",by.y = "Cheval",all = TRUE)
#       mat_weight_distance_speed <- transform(mat_weight_distance_speed,DelataWeight = round(((Poids - Weight)/Weight)*100,2))
#       mat_weight_distance_speed <- transform(mat_weight_distance_speed,DelataLength = round(((Distance - Length)/Length)*100,2))
#       mat_weight_distance_speed <- transform(mat_weight_distance_speed,DelataPrize = round(((Prix - Prize)/Prize)*100,2))
#       
#       vec_earnings <- unique(mat_weight_distance_speed$Gains)
#       vec_earnings <- vec_earnings[vec_earnings > 0]
#       vec_earnings_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_earnings))[rank(vec_earnings)]
#       names(vec_earnings_color) <- vec_earnings
#       
#       vec_prix <- unique(mat_weight_distance_speed$Prix)
#       vec_prix <- vec_prix[vec_prix>0]
#       vec_prix_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_prix))[rank(vec_prix)]
#       names(vec_prix_color) <- vec_prix
#       
#       vec_date <- unique(mat_weight_distance_speed$Date)
#       vec_date <- vec_date-Sys.Date()
#       vec_date_color <- colorRampPalette(rev(brewer.pal(9,"RdYlBu")))(length(vec_date))[rank(vec_date)]
#       names(vec_date_color) <- vec_date
#       
#       mat_weight_distance_speed <- mat_weight_distance_speed[complete.cases(mat_weight_distance_speed[,c("Distance","Poids")]),]
#       mat_weight_distance_speed$First <- ifelse(mat_weight_distance_speed$Distance>=mat_weight_distance_speed$Length,1,0)
#       mat_weight_distance_speed$Second <- ifelse(mat_weight_distance_speed$Poids>=mat_weight_distance_speed$Weight,1,0)
#       mat_weight_distance_speed$Code <- paste(mat_weight_distance_speed$First,mat_weight_distance_speed$Second,sep="-")
#       
#       mat_weight_distance_speed$Wins <- get_percentile_values(mat_weight_distance_speed$Gains)
#       mat_weight_distance_speed$WeightScore   <- get_percentile_values(mat_weight_distance_speed$DelataWeight)
#       mat_weight_distance_speed$LengthScore   <- get_percentile_values(mat_weight_distance_speed$DelataLength)
#       mat_weight_distance_speed$PrizeScore   <- get_percentile_values(mat_weight_distance_speed$DelataPrize)
#       
#       df_weight_distance_speed_config <- mat_weight_distance_speed %>%
#         mutate(Score=(Wins+WeightScore+LengthScore+PrizeScore)/4) %>%
#         select(Cheval,Code,Score) %>%
#         pivot_wider(
#           names_from = Code,
#           values_from = Score,
#           values_fn = list(Score = mean)) %>%
#         as.data.frame()
#       
#       if(nrow(df_weight_distance_speed_config)>0){
#         if(ncol(df_weight_distance_speed_config)>2){
#           df_weight_distance_speed_config[,-1] <- round(df_weight_distance_speed_config[,-1],2)
#           df_weight_distance_speed_config <- df_weight_distance_speed_config[,]
#           df_weight_distance_speed_config <- df_weight_distance_speed_config[,intersect(c("Cheval","0-0","0-1","1-0","1-1"),colnames(df_weight_distance_speed_config))]
#         }
#         
#         if(nrow(df_weight_distance_speed_config)<nrow(df_infos_target_race_geny)){
#           df_weight_distance_speed_config <- merge(df_weight_distance_speed_config,df_infos_target_race_geny,by.x="Cheval",by.y="Cheval",all.y = TRUE)
#         }
#       }
#       
#       if(draw_plot==TRUE){
# 
#         
#         file_name_output <- unique(df_infos_target_race_geny$Course)
#         file_name_output <- gsub("é","e",file_name_output)
#         file_name_output <- gsub("è","e",file_name_output)
#         file_name_output <- gsub(" -","-",file_name_output)
#         file_name_output <- gsub(" ","-",file_name_output)
#         file_name_output <- gsub("\\(","",file_name_output)
#         file_name_output <- gsub("\\)","",file_name_output)
#         file_name_output <- gsub("'","",file_name_output)
#         file_name_output <- unlist(strsplit(file_name_output,";"))[1]
#         file_name_output <- paste(file_name_output,"bounding-box",sep="_")
#         file_name_output <- paste(file_name_output,'.tiff',sep="")
#         # tiff(file_name_output, width = 1200, height = 800)
# 
#         # dev.off()
#       }
#     }
#   }
#   return(df_weight_distance_speed_config)
# }
