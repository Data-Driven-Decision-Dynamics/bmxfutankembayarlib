#' @return Compute engagement indicator for each candidate of a given race
#' @importFrom xml2 read_html
#' @importFrom curl curl
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_engagement_level_letrot(df_infos_target_race = NULL)
#' @export
get_horse_signal_driver_among_ecuries <- function(df_infos_target_race = NULL,
                                                  path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                  number_days_back=720){
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  vec_history_to_remove <- c("Depart","EUROPEAN","Criterium","Inedit","SA","Musique","Etat","Penetrometre","Reclamer","RECLAMER","INTERNATIONAL","Listed","Maiden","Amateur","AMATEUR","Apprenti","Conditions","Decharge","Valeur","Ecart")
  df_historical_races_geny <- df_historical_races_geny[,intersect(colnames(df_historical_races_geny),setdiff(colnames(df_historical_races_geny),vec_history_to_remove))]
  
  message("Initilization of the final output")
  df_perfs_driver <- NULL
  
  if(max(table(df_infos_target_race$Entraineur))>1){
    
    current_race_discipline <- unique(df_infos_target_race$Discipline)
    
    if(current_race_discipline == "Trot Attelé"){
      current_race_drivers <- unique(df_infos_target_race$Driver)
    } else {
      current_race_drivers <- unique(df_infos_target_race$Jockey)
    }

    if(class(unique(df_infos_target_race$Date))!="Date") {
      current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
    } else {
      current_race_date <- unique(df_infos_target_race$Date)
    }
    
    message("Reading historical race infos file")
    if(!exists("df_historical_races_geny")) {
      if(file.exists(path_df_historical_races_geny)){
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    }

    if(current_race_discipline == "Trot Attelé"){
      df_perf_race_target_drivers <- df_historical_races_geny[df_historical_races_geny$Discipline == current_race_discipline & df_historical_races_geny$Driver %in% current_race_drivers & df_historical_races_geny$Date<current_race_date,]
      if(nrow(df_perf_race_target_drivers)>0){
        df_perfs_driver <- df_perf_race_target_drivers %>%
          filter(current_race_date-Date<=number_days_back) %>%
          group_by(Driver) %>%
          dplyr::summarise(GLOBAL_NUMBER_RACES=n(),GLOBAL_DRIVER_NUMBER_PLACE=get_number_podium(Place),GLOBAL_DRIVER_PERCENT_SUCCESS=round((GLOBAL_DRIVER_NUMBER_PLACE/GLOBAL_NUMBER_RACES)*1,2)) %>%
          as.data.frame()
        if(nrow(df_perfs_driver)>0){
          df_perfs_driver <- merge(df_infos_target_race[,c("Cheval","Driver","Entraineur")],df_perfs_driver,by.x="Driver",by.y="Driver",all=TRUE)
          df_perfs_driver$SIGNAL_DRIVER_SELECT_ECURIE_RACE <- NA
          vec_freq_trainers <- table(df_infos_target_race$Entraineur)
          vec_trainers_ecuries <- names(vec_freq_trainers)[vec_freq_trainers>1]
          if(length(intersect(vec_trainers_ecuries,df_perfs_driver$Entraineur))>0){
            vec_trainers_ecuries <- intersect(vec_trainers_ecuries,df_perfs_driver$Entraineur)
            for(trainer in vec_trainers_ecuries){
              vec_horses_trainer <- df_perfs_driver[df_perfs_driver$Entraineur == trainer,"Cheval"]
              for(horse_trainer in vec_horses_trainer){
                percent_success_driver_horse <- df_perfs_driver[df_perfs_driver$Cheval == horse_trainer,"GLOBAL_DRIVER_PERCENT_SUCCESS"]
                percent_success_driver_ecurie_horse <- df_perfs_driver[df_perfs_driver$Cheval %in% setdiff(vec_horses_trainer,horse_trainer),"GLOBAL_DRIVER_PERCENT_SUCCESS"]
                if(!is.na(percent_success_driver_horse) & sum(!is.na(percent_success_driver_ecurie_horse))>0){
                  df_perfs_driver[df_perfs_driver$Cheval == horse_trainer,"SIGNAL_DRIVER_SELECT_ECURIE_RACE"] <- round((percent_success_driver_horse-mean(percent_success_driver_ecurie_horse[!is.na(percent_success_driver_ecurie_horse)]))/percent_success_driver_horse,2)
                }
              }
            }
          }
        }
      }
      df_perfs_driver <- df_perfs_driver[,intersect(colnames(df_perfs_driver),c("Cheval","SIGNAL_DRIVER_SELECT_ECURIE_RACE"))]
    }
    
    if(current_race_discipline %in% c("Plat","Trot Monté","Steeplechase","Cross","Haies")){
      df_perf_race_target_drivers <- df_historical_races_geny[df_historical_races_geny$Discipline == current_race_discipline & df_historical_races_geny$Jockey %in% current_race_drivers & df_historical_races_geny$Date<current_race_date,]
      if(nrow(df_perf_race_target_drivers)>0){
        df_perfs_driver <- df_perf_race_target_drivers %>%
          filter(current_race_date-Date<=number_days_back) %>%
          group_by(Jockey) %>%
          dplyr::summarise(GLOBAL_NUMBER_RACES=n(),GLOBAL_DRIVER_NUMBER_PLACE=get_number_podium(Place),GLOBAL_DRIVER_PERCENT_SUCCESS=round((GLOBAL_DRIVER_NUMBER_PLACE/GLOBAL_NUMBER_RACES)*1,2)) %>%
          as.data.frame()
        if(nrow(df_perfs_driver)>0){
          df_perfs_driver <- merge(df_infos_target_race[,c("Cheval","Jockey","Entraineur")],df_perfs_driver,by.x="Jockey",by.y="Jockey",all=TRUE)
          df_perfs_driver$SIGNAL_DRIVER_SELECT_ECURIE_RACE <- NA
          vec_freq_trainers <- table(df_infos_target_race$Entraineur)
          vec_trainers_ecuries <- names(vec_freq_trainers)[vec_freq_trainers>1]
          if(length(intersect(vec_trainers_ecuries,df_perfs_driver$Entraineur))>0){
            vec_trainers_ecuries <- intersect(vec_trainers_ecuries,df_perfs_driver$Entraineur)
            for(trainer in vec_trainers_ecuries){
              vec_horses_trainer <- df_perfs_driver[df_perfs_driver$Entraineur == trainer,"Cheval"]
              for(horse_trainer in vec_horses_trainer){
                percent_success_driver_horse <- df_perfs_driver[df_perfs_driver$Cheval == horse_trainer,"GLOBAL_DRIVER_PERCENT_SUCCESS"]
                percent_success_driver_ecurie_horse <- df_perfs_driver[df_perfs_driver$Cheval %in% setdiff(vec_horses_trainer,horse_trainer),"GLOBAL_DRIVER_PERCENT_SUCCESS"]
                if(!is.na(percent_success_driver_horse) & sum(!is.na(percent_success_driver_ecurie_horse))>0){
                  df_perfs_driver[df_perfs_driver$Cheval == horse_trainer,"SIGNAL_DRIVER_SELECT_ECURIE_RACE"] <- round((percent_success_driver_horse-mean(percent_success_driver_ecurie_horse[!is.na(percent_success_driver_ecurie_horse)]))/percent_success_driver_horse,2)
                }
              }
            }
          }
        }
      }
      df_perfs_driver <- df_perfs_driver[,intersect(colnames(df_perfs_driver),c("Cheval","SIGNAL_DRIVER_SELECT_ECURIE_RACE"))]
    }
  }
 
  return(df_perfs_driver)
}
