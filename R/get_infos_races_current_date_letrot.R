#' @return Extracts infos for races of current date
#' @importFrom bmxFutankeMbayar get_links_race_target_day_letrot
#' @importFrom bmxFutankeMbayar get_details_races_reunion_letrot
#' @importFrom bmxFutankeMbayar get_details_target_race_letrot
#' @importFrom bmxFutankeMbayar get_details_target_race_letrot
#' @importFrom dplyr bind_rows
#' @examples
#' get_horse_percent_win_pairwise_comparison_letrot(mat_infos_race_input = NULL)
#' @export
get_infos_races_current_date_letrot <- function(target_date=Sys.Date()) {
  vec_url_date_scrape_letrot  <- get_links_schedule_track_target_day_letrot(paste(rev(unlist(strsplit(as.character(Sys.Date()),"-"))),collapse ="-"))
  vec_url_date_scrape_letrot  <- gsub("courses/programme","fiche-course",vec_url_date_scrape_letrot)
  vec_races_current_date_letrot <- unique(unlist(lapply(vec_url_date_scrape_letrot,get_details_races_reunion_letrot)))
  mat_infos_races_current_day_letrot <- bind_rows(lapply(vec_races_current_date_letrot,get_details_target_race_letrot))
  list_infos_races_current_day_letrot <- split.data.frame(mat_infos_races_current_day_letrot,mat_infos_races_current_day_letrot$Course)
  return(list_infos_races_current_day_letrot)
}
