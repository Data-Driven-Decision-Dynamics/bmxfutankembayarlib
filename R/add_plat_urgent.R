# if(ncol(df_share_earning)>1){
#   vec_lab_xaxis <- colnames(df_share_earning)[-1]
#   if(ncol(df_share_earning)>1){
#     par(mar=c(4,1,2,0))
#     par(las=1)
#     plot(0,0,xlim=c(0,length(vec_lab_xaxis)+1),ylim=c(0,length(current_race_horses)),xaxt="n",yaxt="n",type="n",xlab="",ylab="")
#     rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = "white")
#     title('Competitiveness')
#     axis(1, 1:((length(vec_lab_xaxis))),labels=FALSE,tick = FALSE)
#     text(1:((length(vec_lab_xaxis))), par("usr")[3] - 0.2, labels = vec_lab_xaxis, pos = 1, xpd = TRUE,offset = 1.2,cex=0.7,srt=45)
#     for(id_col in 1:length(vec_lab_xaxis))    {
#       df_share_earning_current <- df_share_earning[,c("Cheval",vec_lab_xaxis[id_col]),drop=FALSE]
#       df_relative_weight_current <- df_relative_weight[,c("Cheval",vec_lab_xaxis[id_col]),drop=FALSE]
#       df_relative_prize_current <- df_relative_prize[,c("Cheval",vec_lab_xaxis[id_col]),drop=FALSE]
#       df_relative_distance_current <- df_relative_distance[,c("Cheval",vec_lab_xaxis[id_col]),drop=FALSE]
#       for(id_horse in 1:length(vec_lab_yaxis))
#       {
#         val_share_earning_horse <- NA
#         if(sum(df_share_earning_current$Cheval==unlist(str_split(vec_lab_yaxis[id_horse],"-",n=2))[2])>0){
#           val_share_earning_horse <- df_share_earning_current[df_share_earning_current$Cheval==unlist(str_split(vec_lab_yaxis[id_horse],"-",n=2))[2],vec_lab_xaxis[id_col]]
#           val_share_earning_horse_col <- vec_share_earning_values_color[as.character(val_share_earning_horse)]
#         }
#         
#         val_relative_weight_horse <- NA
#         if(sum(df_relative_weight_current$Cheval==unlist(str_split(vec_lab_yaxis[id_horse],"-",n=2))[2])>0){
#           val_relative_weight_horse <- df_relative_weight_current[df_relative_weight_current$Cheval==unlist(str_split(vec_lab_yaxis[id_horse],"-",n=2))[2],vec_lab_xaxis[id_col]]
#           val_relative_weight_horse_col <- vec_relative_weight_values_color[as.character(val_relative_weight_horse)]
#         }
#         
#         val_relative_prize_horse <- NA
#         if(sum(df_relative_prize_current$Cheval==unlist(str_split(vec_lab_yaxis[id_horse],"-",n=2))[2])>0){
#           val_relative_prize_horse <- df_relative_prize_current[df_relative_prize_current$Cheval==unlist(str_split(vec_lab_yaxis[id_horse],"-",n=2))[2],vec_lab_xaxis[id_col]]
#           val_relative_prize_horse_col <- vec_relative_prize_values_color[as.character(val_relative_prize_horse)]
#         }
#         
#         val_relative_distance_horse <- NA
#         if(sum(df_relative_distance_current$Cheval==unlist(str_split(vec_lab_yaxis[id_horse],"-",n=2))[2])>0){
#           val_relative_distance_horse <- df_relative_distance_current[df_relative_distance_current$Cheval==unlist(str_split(vec_lab_yaxis[id_horse],"-",n=2))[2],vec_lab_xaxis[id_col]]
#           val_relative_distance_horse_col <- vec_relative_distance_values_color[as.character(val_relative_distance_horse)]
#         }
#         
#         if(!is.na(val_relative_prize_horse) & !is.na(val_relative_distance_horse)){
#           points(grep(vec_lab_xaxis[id_col],vec_lab_xaxis),id_horse,pch=22,bg=val_relative_prize_horse_col,cex=4,col=val_relative_distance_horse_col)
#         }
#         
#         if(!is.na(val_share_earning_horse) & !is.na(val_relative_weight_horse)){
#           points(grep(vec_lab_xaxis[id_col],vec_lab_xaxis),id_horse,pch=22,bg=val_share_earning_horse_col,cex=2,col=val_relative_weight_horse_col)
#         }
#       }
#     }
#   }
# }
#         
