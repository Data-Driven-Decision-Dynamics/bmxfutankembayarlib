graph_network_aggregated_scores_geny <- function (target_race = "5ème-course-Prix-de-René-Daniel-Traiteur",
                                                  path_output_dashboard = "/mnt/Master-Data/Futanke-Mbayar/France/report/geny",
                                                  path_compiled_results = "/mnt/Master-Data/Futanke-Mbayar/France/report/geny",
                                                  path_daily_infos = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/raw/daily_performances") {
  
  network_scores <- NULL
  df_edges <- NULL
  
  require(xlsx)
  
  message("Start setting the information for reading the file with the details of the criteria")  
  target_date <- sort(dir(path_output_dashboard),decreasing = TRUE)[1]
  path_file_data_current_date <- paste(path_daily_infos,paste(target_date,".rds",sep=""),sep="/")
  if(file.exists(path_file_data_current_date)){
    infos_perfs_race_current_competitors <- readRDS(path_file_data_current_date)
    df_combined_infos_race_current_date <- infos_perfs_race_current_competitors$Infos
  }
  vec_details_reunions <- NULL
  if(dir.exists(paste(path_compiled_results,as.character(target_date),sep="/"))){
    vec_details_reunions <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),pattern = ".xlsx")
    vec_details_reunions <- setdiff(vec_details_reunions,c("Infos.xlsx","Bettable.xlsx","Drives.xlsx",".RData"))
    path_file_infos <- dir(paste(path_compiled_results,as.character(target_date),sep="/"),pattern = "Infos.xlsx")
  }
  if(length(path_file_infos)==1){
    df_infos_races <- read.xlsx(paste(paste(path_compiled_results,as.character(target_date),sep="/"),path_file_infos,sep="/"),sheetIndex = 1) %>%
      as.data.frame()
  }
  if(length(vec_details_reunions)==1){
    df_details_races <- read.xlsx(paste(paste(path_compiled_results,as.character(target_date),sep="/"),vec_details_reunions,sep="/"),sheetIndex = 1) %>%
      as.data.frame()
    df_details_races <- unique(df_details_races)
    df_details_races <- df_details_races[order(df_details_races$Heure,decreasing = FALSE),]
    df_details_races$Course <- gsub(" ","-",df_details_races$Race)
    df_details_races$Course <- gsub("/","-",df_details_races$Course)
    df_details_races$Course <- gsub("'","",df_details_races$Course)
    df_details_races$Course <- gsub("\\(","",df_details_races$Course)
    df_details_races$Course <- gsub(")","",df_details_races$Course)
    df_details_races$Course <- gsub("--Genybet-","-Genybet",df_details_races$Course)
    df_details_races$Course <- gsub("---","-",df_details_races$Course)
  }
  df_details_race_current <- df_details_races [df_details_races$Course == gsub("--Genybet-","-Genybet",target_race), ]
  number_race_current_item <- substr(unlist(strsplit(target_race,'-'))[1],1,1)
  path_current_reunion <- paste(path_compiled_results,target_date,df_details_race_current$Location,sep="/")
  path_current_reunion_race <- paste(path_compiled_results,target_date,df_details_race_current$Location,target_race,sep="/")
  path_current_reunion_race <-gsub("//","/",path_current_reunion_race)
  path_file_pronostics_current_race <- dir(path_current_reunion_race,pattern = ".xlsx")
  path_file_pronostics_current_race <- unique(setdiff(path_file_pronostics_current_race,c("Complexity.xlsx","Diakarloo.xlsx","Penalties.xlsx","Remontaders.xlsx","Decision.rds","Share.rds","Cluster.xlsx","Clusters.xlsx","Grounds.xlsx","Ground.xlsx","RFM.xlsx","Sky.xlsx","Rocket.xlsx","Trend.xlsx","Complexity.xlsx","Improvement.xlsx","Behind.xlsx","Performs.xlsx","Temples.xlsx","Elite.xlsx","Amateurisme.xlsx","Rank.xlsx","Complexities.xlsx","Distance.xlsx","Checks.xlsx","Groups.xlsx","Open-Region.xlsx","Rare.xlsx","Details.xlsx","Apparent.xlsx","Form.xlsx","Bases.xlsx","Engagement.xlsx","Categories.xlsx","Confirmation.xlsx","Wins.xlsx","Innovation.rds","Max.xlsx","Remontada.xlsx","Participation.xlsx","Probabilities.xlsx","TETF.xlsx","Distances.xlsx","Mague.xlsx","Apprentis.xlsx","Apprentices.xlsx","Year.xlsx","Signal.xlsx","SAME-RACE-H.xlsx","SAME-RACE-D.xlsx","SAME-RACE-T.xlsx","Carreer.xlsx","Amateurs.xlsx","TV.xlsx","TVA.xlsx","GNA.xlsx","Manual.xlsx","Trophee-Vert.xlsx","Grand-National-Trot.xlsx","Open-Regions.xlsx","Criterium.xlsx","Tracking.rds","Profile.xlsx","Trot.xlsx","Galop.xlsx","Ward.xlsx","Draw.xlsx","Compare.xlsx","Remontada.xlsx","AutoStart.xlsx","Auto-Start.xlsx","Aptitude.xlsx","Gore.xlsx","RTF.xlsx","Benchmark.xlsx","Advantage.xlsx","Sensitivity.xlsx","Limit.xlsx","Consecutive.xlsx","Consolidated.xlsx","Polygon.xlsx","Tankunem.xlsx","Doley.xlsx","Khaliss.xlsx","Trainers.xlsx","Horses.xlsx","Drivers.xlsx","Associations.xlsx","Year.xlsx","GNT.xlsx","Cadre.xlsx","Orderer.xlsx","Config.xlsx","International.xlsx","Placing.xlsx","Toughest.xlsx","Going.xlsx","Non-Partants.xlsx","Disqualified.xlsx","Epreuve.xlsx","Beaten.xlsx","Niveau.xlsx","Level.xlsx","Participation.xlsx","Yenn.xlsx","Handicap.xlsx","Weight.xlsx","Travel.xlsx","Infos.xlsx","Tandems.xlsx","Ecuries.xlsx","Participation.xlsx","Earning.xlsx","Ranking.xlsx","Records.xlsx","Track.xlsx","Issues.xlsx","Intention.xlsx","Spell.xlsx","Pairs.xlsx","Ferrage.xlsx","Success.xlsx","Engagement.xlsx","Fitness.xlsx","Odds.xlsx")))
  if(length(path_file_pronostics_current_race)==1){
    df_infos_pronostics_current_race <- read.xlsx(paste(path_current_reunion_race,path_file_pronostics_current_race,sep="/"),sheetIndex = 1) %>%
      as.data.frame
    df_infos_pronostics_current_race <- base::merge(df_infos_pronostics_current_race,df_infos_races[,intersect(c("Cheval","Numero","Gender","Age","Discipline","Poids","Ferrage","Corde","Terrain","Date","Epreuve"),colnames(df_infos_races))],by.x="Cheval",by.y="Cheval",all.x=TRUE)
    df_infos_pronostics_current_race <- df_infos_pronostics_current_race [df_infos_pronostics_current_race$Discipline == names(which.max(table(df_infos_pronostics_current_race$Discipline))) ,]
    df_infos_focus_id <- df_infos_pronostics_current_race[,c("Numero","Cheval")]
  }
  message("Start setting the information for reading the file with the details of the criteria")
  
  message("Start reading the file with the details of the criteria")
  vec_file_details_decision_criteria_current_race <- dir(path_current_reunion,recursive = TRUE,full.names = TRUE,pattern="Decision")
  if(length(vec_file_details_decision_criteria_current_race)>0){
    file_details_decision_criteria <- grep(target_race,vec_file_details_decision_criteria_current_race,value=TRUE)
    if(length(file_details_decision_criteria)==1){
      df_infos_target_criteria <- readRDS(file_details_decision_criteria)
      df_scores_details <- df_infos_target_criteria$Global
    }
  }
  message("Finish reading the file with the details of the criteria")
  
  
  if(!is.null(df_scores_details)) {
    df_aggregated_scores <- merge(df_scores_details,df_infos_focus_id[,c("Numero","Cheval"),drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
    df_aggregated_scores <- df_aggregated_scores[,intersect(colnames(df_aggregated_scores),c("Cheval","Class","Aptitude","Form","Intention","Engagement","Final"))]
    for(idx_score in c("Class","Aptitude","Form","Intention","Engagement"))
    {
      
      df_aggregated_scores_current <- df_aggregated_scores[order(df_aggregated_scores[,idx_score],decreasing = TRUE),]
      if(sum(!is.na(df_aggregated_scores[,idx_score]))>ceiling(nrow(df_aggregated_scores)/4)){
        df_aggregated_scores_current <- df_aggregated_scores_current[!is.na(df_aggregated_scores[,idx_score]),]
        vec_top_horses_score <- df_aggregated_scores_current$Cheval[1:ceiling(nrow(df_aggregated_scores)/4)]
        # vec_value_scores_top_horses_score <- df_aggregated_scores_current[df_aggregated_scores_current[,idx_score]>0,idx_score]
        vec_value_scores_top_horses_score <- df_aggregated_scores_current[,idx_score][1:ceiling(nrow(df_aggregated_scores)/4)]
        df_edges_current_score <- data.frame(from = vec_top_horses_score, to = idx_score , value = vec_value_scores_top_horses_score)
        if(nrow(df_edges_current_score)>0){
          df_edges <- rbind(df_edges,df_edges_current_score)
        }
      }
    }
  }
  
  if(sum(is.na(df_edges))>0){
    df_edges <- df_edges[complete.cases(df_edges),]
  }
  
  df_nodes_competitors <- data.frame(id= df_aggregated_scores$Cheval , label = df_aggregated_scores$Cheval)
  df_nodes_competitors$shape <- "ellipse"
  vec_colors_competitors_values <- df_aggregated_scores$Final
  df_nodes_competitors$color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_colors_competitors_values))[rank(vec_colors_competitors_values)]
  if(sum(is.na(vec_colors_competitors_values))>0){
    df_nodes_competitors$color[is.na(vec_colors_competitors_values)] <- "#696969"
  }
  
  df_nodes_scores <- data.frame(id = c("Class","Aptitude","Form","Intention","Engagement") , label = c("Class","Aptitude","Form","Intention","Engagement"))
  df_nodes_scores$shape <- "box"
  df_nodes_scores$color <- RColorBrewer::brewer.pal(9,"PuRd")[5]
  
  df_nodes <- rbind(df_nodes_competitors,df_nodes_scores)
  
  if(!is.null(df_edges)){
    vec_colors_scores_values <- df_edges$value
    df_edges$col <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_colors_scores_values))[rank(vec_colors_scores_values)]
  }
    
  if(!is.null(df_edges)){
    if(length(setdiff(df_infos_focus_id$Cheval,unique(df_edges$from)))>0){
      df_edges_missing <- data.frame(from= setdiff(df_infos_focus_id$Cheval,unique(df_edges$from)), to = setdiff(df_infos_focus_id$Cheval,unique(df_edges$from)), value=NA,col="white")
      df_edges <- rbind(df_edges,df_edges_missing)
    }
  }
  
  df_edges$id <- df_edges$from
  for(idx in 1:nrow(df_edges))
  {
    df_edges[idx,"id"] <- paste(df_infos_focus_id[df_infos_focus_id$Cheval ==df_edges[idx,"from"],"Numero"],df_edges[idx,"from"],sep="-")
    if(df_edges[idx,"to"] %in% df_infos_focus_id$Cheval ){
      df_edges[idx,"to"] <- paste(df_infos_focus_id[df_infos_focus_id$Cheval ==df_edges[idx,"from"],"Numero"],df_edges[idx,"to"],sep="-")
    }
  }
  
  df_edges$from <- df_edges$id
  df_edges <- df_edges[,setdiff(colnames(df_edges),"id")]
  
  for(idx in 1:nrow(df_nodes))
  {
    if(df_nodes[idx,"id"] %in% df_infos_focus_id$Cheval ){
      df_nodes[idx,"id"] <- paste(df_infos_focus_id[df_infos_focus_id$Cheval == df_nodes[idx,"id"],"Numero"],df_nodes[idx,"id"],sep="-")
      df_nodes[idx,"label"] <- paste(df_infos_focus_id[df_infos_focus_id$Cheval == df_nodes[idx,"label"],"Numero"],df_nodes[idx,"label"],sep="-")
    }
  }

  network_scores <- visNetwork::visNetwork(df_nodes,df_edges) %>%
    visNodes(color = list(border = "white"))

  return(list(edges = df_edges , nodes = df_nodes))
  
}

# df_nodes$shape <- gsub("ellipse","sphere",df_nodes$shape)
# df_nodes$shape <- gsub("box","square",df_nodes$shape)
# df_nodes$color <- gsub("#DF65B0","violet",df_nodes$color)
# graph_network <- graph_from_data_frame(d = df_edges, vertices = df_nodes, directed=TRUE)
# graph_network <- simplify(graph_network)
# # V(graph_network)$shape[V(graph_network)$name %in% df_aggregated_scores$Cheval] <-'sphere'
# # V(graph_network)$shape[V(graph_network)$name %in% c("Class","Aptitude","Fitness","Intention")] <-'square'
# V(graph_network)$label.cex <- 0.5
# V(graph_network)$label.cex [V(graph_network)$name %in% c("Class","Aptitude","Fitness","Intention") ] <- 0.45
# V(graph_network)$label [V(graph_network)$label %in% c("Class","Aptitude","Fitness","Intention") ] <- toupper(V(graph_network)$label [V(graph_network)$label %in% c("Class","Aptitude","Fitness","Intention") ])
# V(graph_network)$size <- 20
# E(graph_network)$width <- abs(df_edges$value[1:length(E(graph_network)$width)])
# E(graph_network)$color <- df_edges$color[1:length(E(graph_network)$width)]
# V(graph_network)$frame.color <- "white"



