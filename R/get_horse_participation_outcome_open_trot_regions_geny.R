get_horse_participation_outcome_open_trot_regions_geny <- function(regex_open_regions = "LeTROT Open des Régions",
                                                                   number_point_rank = c(15,10,8,6,5,4,3),
                                                                   path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                                   path_df_participation_open_regions_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_participation_outcome_letrot_open_regions_geny.rds") {
  
  require(bmxFutankeMbayar)
  require(plyr)
  require(dplyr)
  require(lubridate)
  
  df_races_good_open_trot <- NULL

  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  id_items_open_regions <- grep(regex_open_regions,df_historical_races_geny$Details,ignore.case = TRUE)
  if(length(id_items_open_regions)>0){
    df_races_good_open_regions <- df_historical_races_geny[id_items_open_regions,c("Date","Cheval","Cumul_Gains","Discipline","Lieu","Corde","Terrain","Age","Driver","Entraineur","Place","Gains","Course","Details","Prix","Epreuve")]
    df_races_good_open_regions <- distinct(df_races_good_open_regions)
    if(sum(id_items_open_regions,na.rm = TRUE)>0){
      df_races_good_open_regions$Region <- NA
      df_races_good_open_regions$ID_TROT_OPEN <- NA
      df_races_good_open_regions$Year <- lubridate::year(df_races_good_open_regions$Date)
      for(idx_row in 1:nrow(df_races_good_open_regions)){
        df_infos_open_regions_current <- get_open_regions_identifier_geny(df_races_good_open_regions[idx_row,"Details"])
        if(!is.null(df_infos_open_regions_current)){
          df_races_good_open_regions[idx_row,"Region"] <- df_infos_open_regions_current$Region
          df_races_good_open_regions[idx_row,"ID_TROT_OPEN"] <- df_infos_open_regions_current$ID
        }
      }
    }
    df_races_good_open_regions$Points <- NA
    for(idx_rank in 1:length(number_point_rank))
    {
      idx_item_current_rank <- df_races_good_open_regions$Place == idx_rank
      if(sum(idx_item_current_rank,na.rm = TRUE)>0){
        df_races_good_open_regions[idx_item_current_rank,"Points"] <- number_point_rank[idx_rank]
      }
    }
    if(sum(is.na(df_races_good_open_regions$Points))>0){
      df_races_good_open_regions[is.na(df_races_good_open_regions$Points),"Points"] <- 1
    }
  }
  
  df_races_good_open_regions$Item <- paste("LeTROT Open des Régions",df_races_good_open_regions$Age)

  if(!is.null(df_races_good_open_regions)){
    saveRDS(df_races_good_open_regions,path_df_participation_open_regions_geny)
  }
  
  return(df_races_good_open_regions)
}
