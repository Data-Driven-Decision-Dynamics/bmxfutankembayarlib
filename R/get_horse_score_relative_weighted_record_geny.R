get_horse_score_relative_weighted_record_geny <- function(df_infos_target_race_geny = NULL, 
                                                          number_items = 5,
                                                          path_records_perfs_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_records_all_tracks_geny.rds",
                                                          path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                          number_days_back=7000){
  
  df_score_records_all_tracks <- NULL
  
  message("Loading historical performance data")
  if(!exists("df_historical_races_geny")){
    df_historical_races_geny <- readRDS(path_df_historical_races_geny)
  }
  
  if(class(df_historical_races_geny$Date)!="Date"){
    df_historical_races_geny$Date <- as.Date(df_historical_races_geny$Date)
  }
  
  if(!is.null(df_infos_target_race_geny)) {
    
    message("Extraction date of the race")
    if(class(unique(df_infos_target_race_geny$Date))!="Date") {
      current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
    } else {
      current_race_date <- unique(df_infos_target_race_geny$Date)
    }
    
    message("Getting name of current candidates")
    current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
    
    message("Getting terrain of current race")
    current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
    current_race_terrain <- sub("Machefer","Cendrée",current_race_terrain)
    current_race_terrain <- sub("Mâchefer","Cendrée",current_race_terrain)
    
    message("Getting corde of current race")
    current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
    
    message("Reading historical race infos file")
    if(!exists("df_records_geny")) {
      if(file.exists(path_records_perfs_geny)) {
        df_records_geny <- readRDS(path_records_perfs_geny)
      }
    }
    
    if(class(df_records_geny$Date)!="Date"){
      df_records_geny$Date <- as.Date(df_records_geny$Date)
    }
    
    message("Extracting horse names")
    current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
    
    message("Extracting race date")
    current_race_date     <- as.Date(as.factor(unique(df_infos_target_race_geny$Date)))
    
    message("Extracting race type")
    current_race_discipline <- gsub("\n","",unique(df_infos_target_race_geny$Discipline))
    
    message("Extracting hippodrome")
    current_race_hippodrome <- gsub("\n","",unique(df_infos_target_race_geny$Lieu))
    
    message("Extracting race distance")
    current_race_distance <- min(df_infos_target_race_geny$Distance)
    
    message("Focusing on relevant races")
    df_score_records_all_tracks <- df_records_geny %>%
      select(Cheval,Id,Speed,Corde,Terrain,Date,Discipline,Score) %>%
      filter(Cheval %in% current_race_horses) %>%
      distinct() %>%
      mutate(Delay= as.numeric(current_race_date-Date)) %>%
      filter(Date<current_race_date) %>%
      filter(Delay<=number_days_back) %>%
      filter(Discipline %in% current_race_discipline) %>%
      as.data.frame()
    
    number_competitors_all_track_records <- df_records_geny %>%
      filter(Id %in% unique(df_score_records_all_tracks$Id)) %>%
      select(Cheval) %>%
      unlist() %>%
      unique()%>%
      length()
    
    df_number_competitors_track_records <- df_records_geny %>%
      filter(Id %in% unique(df_score_records_all_tracks$Id)) %>%
      group_by(Id) %>%
      dplyr::summarise(NUMBER_COMPETITORS = length(Cheval)) %>%
      # mutate(NUMBER_COMPETITORS = round(NUMBER_COMPETITORS/number_competitors_all_track_records,5)) %>%
      as.data.frame()
    
    if(nrow(df_score_records_all_tracks)>0){
      
      df_score_records_all_tracks$Delay <-  as.numeric(Sys.Date()-df_score_records_all_tracks$Date)
      df_score_records_all_tracks <- merge(df_score_records_all_tracks,df_number_competitors_track_records,by.x="Id",by.y="Id")
      # df_score_records_all_tracks$Score <- round(get_percentile_values(df_score_records_all_tracks$Score),2)

      if(sum(!is.na(df_score_records_all_tracks$Terrain) | !is.na(df_score_records_all_tracks$Corde),na.rm=TRUE)>0){
        
        df_score_records_all_tracks <- df_score_records_all_tracks[!is.na(df_score_records_all_tracks$Terrain) | !is.na(df_score_records_all_tracks$Corde),]
        df_score_records_all_tracks <- df_score_records_all_tracks[rownames(unique(df_score_records_all_tracks[,c("Cheval","Id","Date")])),]
        idx_config <- df_score_records_all_tracks$Corde == current_race_corde & df_score_records_all_tracks$Terrain ==current_race_terrain
        idx_corde <- (df_score_records_all_tracks$Corde == current_race_corde & df_score_records_all_tracks$Terrain !=current_race_terrain) | (is.na(df_score_records_all_tracks$Terrain) & df_score_records_all_tracks$Corde == current_race_corde)
        idx_terrain <- df_score_records_all_tracks$Corde != current_race_corde & df_score_records_all_tracks$Terrain ==current_race_terrain & !is.na(df_score_records_all_tracks$Terrain)
        df_score_records_all_tracks$Class = NA
        
        if(sum(is.na(idx_config))>0){
          idx_config[is.na(idx_config)] <- FALSE
        }
        
        if(sum(idx_config,na.rm = TRUE)>0){
          df_score_records_all_tracks[idx_config,"Class"] <-"Config"
        }
        
        if(sum(is.na(idx_corde))>0){
          idx_corde[is.na(idx_corde)] <- FALSE
        }
        
        if(sum(idx_corde,na.rm = TRUE)>0){
          df_score_records_all_tracks[idx_corde,"Class"] <-"Turn"
        }

        if(sum(is.na(idx_terrain))>0){
          idx_terrain[is.na(idx_terrain)] <- FALSE
        }
        
        if(sum(idx_terrain,na.rm = TRUE)>0){
          df_score_records_all_tracks[idx_terrain,"Class"] <-"Ground"
        }
        
        idx_else <- is.na(df_score_records_all_tracks$Class)
        if(sum(idx_else,na.rm = TRUE)>0){
          df_score_records_all_tracks[idx_else,"Class"] <-"Others"
        }
      }
      
      df_score_records_all_tracks$Distance <- as.numeric(unlist(lapply(df_score_records_all_tracks$Id,function(x) {unlist(strsplit(x,"_"))[3]})))
      
      if(current_race_discipline!="Trot Attelé"){
        df_score_records_all_tracks$Poids <- as.numeric(unlist(lapply(df_score_records_all_tracks$Id,function(x) {unlist(strsplit(x,"_"))[4]})))
      }
      
      df_score_records_all_tracks <- df_score_records_all_tracks %>%
        group_by(Cheval,Class) %>%
        top_n(number_items,Score) %>%
        as.data.frame()

      df_infos_weight_add <- df_infos_target_race_geny [,intersect(colnames(df_infos_target_race_geny),c("Cheval","Poids","Distance"))]
      colnames(df_infos_weight_add) <- sub("Poids","Weight", colnames(df_infos_weight_add))
      colnames(df_infos_weight_add) <- sub("Distance","Length", colnames(df_infos_weight_add))
      df_score_records_all_tracks <- merge(df_score_records_all_tracks,df_infos_weight_add,by.x="Cheval",by.y="Cheval",all.y=TRUE)
      
      if(current_race_discipline!="Trot Attelé"){
        df_score_records_all_tracks$DeltaWeight <- 100*((df_score_records_all_tracks$Poids-df_score_records_all_tracks$Weight)/df_score_records_all_tracks$Weight)
        df_score_records_all_tracks$DeltaWeight <- round(df_score_records_all_tracks$DeltaWeight,2)
      }
      df_score_records_all_tracks$DeltaDistance <-  100*((df_score_records_all_tracks$Distance-min(df_infos_target_race_geny$Distance))/min(df_infos_target_race_geny$Distance))
      df_score_records_all_tracks$DeltaDistance <- round(df_score_records_all_tracks$DeltaDistance,2)
    }
   }
  
  return(df_score_records_all_tracks)  
}

