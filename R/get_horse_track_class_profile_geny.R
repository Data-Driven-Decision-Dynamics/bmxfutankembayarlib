get_horse_track_class_profile_geny <- function(df_infos_target_race_geny = NULL,
                                               path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                               path_df_ranking_completed_tracks = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_ranking_tracks_completed_geny.xlsx",
                                               path_df_standardized_times_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_standardized_times_geny.rds",
                                               number_days_back = 20000) {


  message("Initialization of the final output")
  df_earning_track_profile <- NULL
  df_speed_track_profile <- NULL
  df_recency_track_profile <- NULL
  df_length_track_profile <- NULL
  message("Initialization of the final output")

  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Reading historical performance file")

  message("Reading rcompletd ranking file")
  if(!exists("df_ranking_tracks_completed")){
    df_ranking_tracks_completed <- xlsx::read.xlsx(path_df_ranking_completed_tracks,sheetIndex = 1)
  }
  message("Reading rcompletd ranking file")
  
  message("Extracting best performance per track for each competitor")
  if(unique(df_infos_target_race_geny$Discipline) == c("Trot Attelé")){
    df_infos_focus <- df_historical_races_geny %>%
      filter(Cheval %in% df_infos_target_race_geny$Cheval) %>%
      filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
      filter(as.numeric(Sys.Date()-Date) <= number_days_back) %>%
      select(Cheval,Lieu,Terrain,Longueur,Depart,Gains,Ecart,Place,Date) %>%
      tidyr::drop_na() %>%
      as.data.frame()
    df_infos_focus$Reduction <- unlist(lapply(df_infos_focus$Ecart,get_numeric_values_reduc_ecart_geny))
    df_infos_focus <- df_infos_focus %>%
      mutate(Speed = 1000/Reduction) %>%
      mutate(ID = paste(Lieu,Terrain,Longueur,Depart,sep="_")) %>%
      as.data.frame()
  }
  
  if(unique(df_infos_target_race_geny$Discipline) == c("Trot Monté")){
    df_infos_focus <- df_historical_races_geny %>%
      filter(Cheval %in% df_infos_target_race_geny$Cheval) %>%
      filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
      filter(as.numeric(Sys.Date()-Date) <= number_days_back) %>%
      select(Cheval,Lieu,Terrain,Longueur,Gains,Ecart,Place,Date) %>%
      tidyr::drop_na() %>%
      as.data.frame()
    df_infos_focus$Reduction <- unlist(lapply(df_infos_focus$Ecart,get_numeric_values_reduc_ecart_geny))
    df_infos_focus <- df_infos_focus %>%
      mutate(Speed = 1000/Reduction) %>%
      mutate(ID = paste(Lieu,Terrain,Longueur,sep="_")) %>%
      as.data.frame()
  } 
  
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Plat","Haies","Steeplechase","Cross")){
    df_infos_focus <- df_historical_races_geny %>%
      filter(Cheval %in% df_infos_target_race_geny$Cheval) %>%
      filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
      filter(as.numeric(Sys.Date()-Date) <= number_days_back) %>%
      select(Cheval,Lieu,Terrain,Longueur,Gains,Speed,Place,Date) %>%
      tidyr::drop_na() %>%
      mutate(ID = paste(Lieu,Terrain,Longueur,sep="_")) %>%
      as.data.frame()
  }
  
  if(nrow(df_infos_focus)>0){
    df_infos_focus <- merge(df_infos_focus,df_ranking_tracks_completed[,c("Track","Class")],by.x="ID",by.y="Track",all.x=TRUE)
    if(sum(is.na(df_infos_focus$Class))>0){
      df_infos_focus$Class [is.na(df_infos_focus$Class)] <- "Regular"
    }
    
    df_earning_track_profile <- df_infos_focus %>%
      group_by(Cheval) %>%
      select(Cheval,Class,Gains) %>%
      pivot_wider(
        names_from = Class,
        values_from = Gains,
        values_fn = sum
      ) %>%
      as.data.frame()
    
    if(!is.null(df_earning_track_profile)){
      if(nrow(df_earning_track_profile)>0){
        vec_order_prfile_track <-  intersect(c("Cheval","Complex","Hard","Difficult","Regular","Easy"),colnames(df_earning_track_profile))
        df_earning_track_profile <- df_earning_track_profile[,vec_order_prfile_track]
      }
    }
    
    df_speed_track_profile <- df_infos_focus %>%
      group_by(Cheval) %>%
      select(Cheval,Class,Speed) %>%
      pivot_wider(
        names_from = Class,
        values_from = Speed,
        values_fn = max
      ) %>%
      as.data.frame()
    
    if(!is.null(df_speed_track_profile)){
      if(nrow(df_speed_track_profile)>0){
        vec_order_prfile_track <-  intersect(c("Cheval","Complex","Hard","Difficult","Regular","Easy"),colnames(df_speed_track_profile))
        df_speed_track_profile <- df_speed_track_profile[,vec_order_prfile_track]
      }
    }
    
    df_recency_track_profile <- df_infos_focus %>%
      filter(Place <= 5) %>%
      mutate(Delay = as.numeric(unique(df_infos_target_race_geny$Date) - Date )) %>%
      group_by(Cheval) %>%
      select(Cheval,Class,Delay) %>%
      pivot_wider(
        names_from = Class,
        values_from = Delay,
        values_fn = min
      ) %>%
      as.data.frame()
    
    if(!is.null(df_recency_track_profile)){
      if(nrow(df_recency_track_profile)>0){
        vec_order_prfile_track <-  intersect(c("Cheval","Complex","Hard","Difficult","Regular","Easy"),colnames(df_recency_track_profile))
        df_recency_track_profile <- df_recency_track_profile[,vec_order_prfile_track]
      }
    }
    
    df_length_track_profile <- df_infos_focus %>%
      group_by(Cheval) %>%
      select(Cheval,Class,Longueur) %>%
      pivot_wider(
        names_from = Class,
        values_from = Longueur,
        values_fn = mean
      ) %>%
      as.data.frame()
    
    if(!is.null(df_length_track_profile)){
      if(nrow(df_length_track_profile)>0){
        vec_order_prfile_track <-  intersect(c("Cheval","Complex","Hard","Difficult","Regular","Easy"),colnames(df_recency_track_profile))
        df_length_track_profile <- df_length_track_profile[,vec_order_prfile_track]
      }
    }
  }
   
   return(list(Earning = df_earning_track_profile , Speed = df_speed_track_profile , Recency = df_recency_track_profile , Length = df_length_track_profile))
}
