get_stats_percent_disqualification_track_geny <- function(path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                          path_stats_disqualifications = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_stats_disqualification_track.rds",
                                                          number_days_back = 1000){
  
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  df_historical_races_focus <- df_historical_races_geny[df_historical_races_geny$Discipline == "Trot Attelé",]
  
  if(length(which(df_historical_races_focus$Discipline == "Trot Attelé" & df_historical_races_focus$Depart != "Auto-Start")) >0){
    df_historical_races_focus$Depart[which(df_historical_races_focus$Discipline == "Trot Attelé" & df_historical_races_focus$Depart != "Auto-Start")] <- "Volte"
  }
  
  if(sum(is.na(df_historical_races_focus$Date))>0){
    df_historical_races_focus <- df_historical_races_focus[!is.na(df_historical_races_focus$Date),]
  }
  
  if(sum(is.na(df_historical_races_focus$Distance))>0){
    df_historical_races_focus <- df_historical_races_focus[!is.na(df_historical_races_focus$Distance),]
  }
  
  df_historical_races_focus <- df_historical_races_focus %>%
    mutate(Delay = as.numeric(Sys.Date()-Date)) %>%
    filter(Delay <= number_days_back) %>%
    as.data.frame()
  
  df_historical_races_focus <- df_historical_races_focus[, c("Date","Numero","Cheval","Lieu","Depart","Distance","Longueur","Place","Discipline","Details","Race", "Status")]
  
  df_races_names_utils <- unique(df_historical_races_focus[,c("Race","Details","Discipline")])
  df_races_names_utils <- df_races_names_utils %>%
    add_epreuve_geny() %>%
    as.data.frame()
  
  df_epreuve <- data.frame(Epreuve=c("I","II","III","A","B","C","D","E","F","G","H","R","M"),Level=length(c("I","II","III","A","B","C","D","E","F","G","H","R","M")):1)
  df_historical_races_focus <- merge(df_historical_races_focus,df_races_names_utils[,c("Race","Epreuve")],by.x="Race",by.y="Race")
  df_historical_races_focus <- merge(df_historical_races_focus,df_epreuve,by.x="Epreuve",by.y="Epreuve")

  df_historical_races_focus <- unique(df_historical_races_focus)
  if(length(which(is.na(df_historical_races_focus$Depart))) >0){
    df_historical_races_focus$Depart[which(is.na(df_historical_races_focus$Depart))] <- "Volte"
  }
  
  df_historical_races_focus$ID <- paste(df_historical_races_focus$Lieu,df_historical_races_focus$Discipline,df_historical_races_focus$Longueur,df_historical_races_focus$Depart,sep="-")

  df_stats_percent_disqualification_track  <- df_historical_races_focus %>%
    mutate(Place = as.numeric(Place)) %>%
    distinct() %>%
    group_by(ID,Epreuve) %>%
    dplyr::summarise(Size = n(),
                     Number = get_number_disqualification(Place),
                     Percent = round(Number/Size,2)) %>%
    filter(Size >= 150) %>%
    as.data.frame()
  
  saveRDS(df_stats_percent_disqualification_track ,path_stats_disqualifications)
  
  return(NULL)
}


# 
# if(length(which(df_historical_races_geny$Discipline == "Trot Monté")) >0){
#   df_historical_races_geny$Depart[which(df_historical_races_geny$Discipline == "Trot Monté")] <- "Volte"
# }
# 
# if(length(which(df_historical_races_geny$Discipline %in% c("Plat","Steeplechase","Cross","Haies")))>0){
#   df_historical_races_geny$Depart[which(df_historical_races_geny$Discipline %in% c("Plat","Steeplechase","Cross","Haies"))] <- "Standard"
# }
# 


