# df_infos_mean_earning_turn = df_infos_stats_apparent
# df_infos_total_earning_turn = infos_shares_turns$Earning
# df_infos_number_races_turn = infos_shares_turns$Number
# df_infos_score_distance = df_cluster_distance_score$A
# df_infos_score_ground = df_ground_score$A
# df_infos_target_race_geny = df_infos_target_race_geny

get_ultimate_score_aptitude_geny_new <- function(df_infos_mean_earning_turn = NULL,
                                             df_infos_score_distance = NULL,
                                             df_infos_score_ground = NULL,
                                             df_infos_qc_index = NULL,
                                             df_infos_target_race_geny = NULL){

  df_score_aptitude <- NULL
  df_score_aptitude_details <- NULL
  df_score_aptitude_distance <- NULL
  df_score_aptitude_ground  <- NULL

  if(!is.null(df_infos_mean_earning_turn)){

    if(!is.null(df_infos_mean_earning_turn)) {
      df_infos_mean_earning <- df_infos_mean_earning_turn
      df_infos_mean_earning$MAX_TURN <- round(get_percentile_values(df_infos_mean_earning$RFM_SCORE_TURN),2)
      df_infos_mean_earning$MEAN_TURN <- round(get_percentile_values(df_infos_mean_earning$MEAN_POSITIVE_EARNING_TURN),2)
      df_infos_mean_earning$APTITUDE_TURN <- round(apply(df_infos_mean_earning[,c("MAX_TURN","MEAN_TURN")],1,function(x){mean(x,na.rm=TRUE)}),2)
      df_infos_mean_earning$MAX_REVERSE <- round(get_percentile_values(df_infos_mean_earning$RFM_SCORE_REVERSE),2)
      df_infos_mean_earning$MEAN_REVERSE <- round(get_percentile_values(df_infos_mean_earning$MEAN_POSITIVE_EARNING_REVERSE),2)
      df_infos_mean_earning$APTITUDE_REVERSE<- round(apply(df_infos_mean_earning[,c("MAX_REVERSE","MEAN_REVERSE")],1,function(x){mean(x,na.rm=TRUE)}),2)
      df_infos_mean_earning <- df_infos_mean_earning[,c("Cheval","APTITUDE_TURN","APTITUDE_REVERSE")]
      if("APTITUDE_TURN" %in% colnames(df_infos_mean_earning)){
        if(sum(is.na(df_infos_mean_earning$APTITUDE_TURN))>0){
          df_infos_mean_earning[is.na(df_infos_mean_earning$APTITUDE_TURN),"APTITUDE_TURN"] <- 0
        }
      }
      if("APTITUDE_REVERSE" %in% colnames(df_infos_mean_earning)){
        if(sum(is.na(df_infos_mean_earning$APTITUDE_REVERSE))>0){
          df_infos_mean_earning[is.na(df_infos_mean_earning$APTITUDE_REVERSE),"APTITUDE_REVERSE"] <- 0
        }
      }
      df_infos_mean_earning$APTITUDE <- (0.7 * df_infos_mean_earning$APTITUDE_TURN) + (0.3 * df_infos_mean_earning$APTITUDE_REVERSE)
      df_infos_mean_earning <- df_infos_mean_earning[,c("Cheval","APTITUDE")]
      df_infos_mean_earning$APTITUDE <- round(get_percentile_values(df_infos_mean_earning$APTITUDE),2)
      colnames(df_infos_mean_earning)[2] <- "APTITUDE_TURN"
      if(nrow(df_infos_mean_earning)<nrow(df_infos_target_race_geny)){
        df_infos_mean_earning <- merge(df_infos_mean_earning,df_infos_target_race_geny[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
      }
      if(sum(!is.na(df_infos_mean_earning$APTITUDE_TURN))>0){
        df_infos_mean_earning[is.na(df_infos_mean_earning$APTITUDE_TURN),"APTITUDE_TURN"] <- 0
      }
    }

    if(!is.null(df_infos_score_distance)){
      current_cluster_distance <- sort(unique(df_infos_target_race_geny$Cluster_Distance))[1]
      idx_current_cluster_distance <- df_infos_score_distance$Cluster_Distance == current_cluster_distance
      df_infos_score_distance$Class <- "Others"
      if(sum(idx_current_cluster_distance,na.rm = TRUE)>0){
        df_infos_score_distance[which(idx_current_cluster_distance==TRUE),"Class"] <- "Current"
      }
      df_score_aptitude_distance <- df_infos_score_distance [,c("Cheval","Class","RFM_SCORE")] %>%
        pivot_wider(
          names_from = Class,
          values_from = RFM_SCORE,
          values_fn = max
        ) %>%
        as.data.frame()
      if("Current" %in% colnames(df_score_aptitude_distance)){
        df_score_aptitude_distance$Current <- get_percentile_values(df_score_aptitude_distance$Current)
      }
      if("Others" %in% colnames(df_score_aptitude_distance)){
        df_score_aptitude_distance$Others <- get_percentile_values(df_score_aptitude_distance$Others)
      }
      if(length(intersect(colnames(df_score_aptitude_distance),"Current"))==0){
        df_score_aptitude_distance$Current <- 0
      }
      if(length(intersect(colnames(df_score_aptitude_distance),"Others"))==0){
        df_score_aptitude_distance$Others <- 0
      }
      df_score_aptitude_distance$APTITUDE_DISTANCE <- (0.7 * df_score_aptitude_distance$Current) + (0.3 * df_score_aptitude_distance$Others )
      df_score_aptitude_distance$APTITUDE_DISTANCE <- round(get_percentile_values(df_score_aptitude_distance$APTITUDE_DISTANCE),2)
      df_score_aptitude_distance <- df_score_aptitude_distance[,c("Cheval","APTITUDE_DISTANCE")]
      
      if(sum(!is.na(df_score_aptitude_distance$APTITUDE_DISTANCE))>0){
        df_score_aptitude_distance[is.na(df_score_aptitude_distance$APTITUDE_DISTANCE),"APTITUDE_DISTANCE"] <- 0
      }
      if(nrow(df_score_aptitude_distance)<nrow(df_infos_target_race_geny)){
        df_score_aptitude_distance <- merge(df_score_aptitude_distance,df_infos_target_race_geny[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
      }
      if(sum(!is.na(df_score_aptitude_distance$APTITUDE_DISTANCE))>0){
        df_score_aptitude_distance[is.na(df_score_aptitude_distance$APTITUDE_DISTANCE),"APTITUDE_DISTANCE"] <- 0
      }
    }
    
    if(!is.null(df_infos_score_ground)){
      current_race_ground <- sort(unique(df_infos_target_race_geny$Terrain))[1]
      idx_current_race_ground <- df_infos_score_ground$Terrain == current_race_ground
      df_infos_score_ground$Class <- "Others"
      if(sum(idx_current_race_ground,na.rm = TRUE)>0){
        df_infos_score_ground[which(idx_current_race_ground==TRUE),"Class"] <- "Current"
      }
      df_score_aptitude_ground <- df_infos_score_ground [,c("Cheval","Class","RFM_SCORE")] %>%
        pivot_wider(
          names_from = Class,
          values_from = RFM_SCORE,
          values_fn = max
        ) %>%
        as.data.frame()
      if("Current" %in% colnames(df_score_aptitude_ground)){
        df_score_aptitude_ground$Current <- get_percentile_values(df_score_aptitude_ground$Current)
      }
      if("Others" %in% colnames(df_score_aptitude_ground)){
        df_score_aptitude_ground$Others <- get_percentile_values(df_score_aptitude_ground$Others)
      }
      if(length(intersect(colnames(df_score_aptitude_ground),"Current"))==0){
        df_score_aptitude_ground$Current <- 0
      }
      if(length(intersect(colnames(df_score_aptitude_ground),"Others"))==0){
        df_score_aptitude_ground$Others <- 0
      }
      df_score_aptitude_ground$APTITUDE_GROUND <- (0.7 * df_score_aptitude_ground$Current) + (0.3 * df_score_aptitude_ground$Others )
      df_score_aptitude_ground$APTITUDE_GROUND <- round(get_percentile_values(df_score_aptitude_ground$APTITUDE_GROUND),2)
      df_score_aptitude_ground <- df_score_aptitude_ground[,c("Cheval","APTITUDE_GROUND")]
      
      if(sum(!is.na(df_score_aptitude_ground$APTITUDE_GROUND))>0){
        df_score_aptitude_ground[is.na(df_score_aptitude_ground$APTITUDE_GROUND),"APTITUDE_GROUND"] <- 0
      }
      if(nrow(df_score_aptitude_ground)<nrow(df_infos_target_race_geny)){
        df_score_aptitude_ground <- merge(df_score_aptitude_ground,df_infos_target_race_geny[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
      }
      if(sum(!is.na(df_score_aptitude_ground$APTITUDE_GROUND))>0){
        df_score_aptitude_ground[is.na(df_score_aptitude_ground$APTITUDE_GROUND),"APTITUDE_GROUND"] <- 0
      }
    }

    list_combined_aptitude_criteria <- list(A = df_infos_mean_earning, B = df_score_aptitude_distance, C = df_score_aptitude_ground )
    list_combined_aptitude_criteria <- list_combined_aptitude_criteria[lengths(list_combined_aptitude_criteria) != 0]

    if(length(list_combined_aptitude_criteria)>0){
      df_combined_aptitude_criteria <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_combined_aptitude_criteria)
      df_score_aptitude_details <- df_combined_aptitude_criteria[,setdiff(colnames(df_combined_aptitude_criteria),"Aptitude")]
      df_combined_aptitude_criteria$Aptitude <- round(apply(df_combined_aptitude_criteria[,-1],1,function(x){mean(x,na.rm=TRUE)}),2)
      df_score_aptitude <- df_combined_aptitude_criteria[,c("Cheval","Aptitude"),]
      df_score_aptitude$Aptitude <- round(get_percentile_values(df_score_aptitude$Aptitude),2)
      if(nrow(df_score_aptitude)<nrow(df_infos_target_race_geny)){
        df_score_aptitude <- merge(df_score_aptitude,df_infos_target_race_geny[,"Cheval",drop=FALSE],by.x="Cheval",by.y="Cheval",all.y=TRUE)
      }
    }
  }

  return(list(Global = df_score_aptitude, Details = df_score_aptitude_details, Quality=df_infos_qc_index))
}
