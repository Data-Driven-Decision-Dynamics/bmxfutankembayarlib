graph_yearly_earning_config_geny <- function(current_path_race = "http://www.geny.com/partants-pmu/2020-10-11-agen-le-passage-pmu-grand-prix-baron-d-ardeuil-aoc-buzet_c1177992;jsessionid=EED9D19514FFB42EB7974A3A3CC32999",
                                             target_horse = NULL,
                                             path_mat_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds") {
  
  mat_gains_yearly_config <- NULL
  
  df_infos_target_race <- mat_races_current_date[mat_races_current_date$URL==current_path_race,]
  
  message("Extract race date and convert in the right format if need")
  if(class(unique(df_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race$Date)
  }
  
  if(!is.null(df_infos_target_race)){
    df_infos_target_race$Epreuve <- gsub("FALSE","F",df_infos_target_race$Epreuve)
  }

  message("Extract category of current race")
  current_race_category <- gsub("\n","",unique(df_infos_target_race$Epreuve))
  
  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race$Discipline)

  message("Extract corde of current race")
  current_race_corde <- as.vector(unique(df_infos_target_race$Corde))
  
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race$Terrain))
  
  message("Extract terrain of current race")
  current_race_terrain <- as.vector(unique(df_infos_target_race$Terrain))
  
  message("Reading historical race infos file")
  if (!exists("mat_historical_races_geny")) {
    if (file.exists(path_mat_historical_races_geny)) {
      mat_historical_races_geny <- readRDS(path_mat_historical_races_geny)
    }
  }
  
  if(class(mat_historical_races_geny$Date)!="Date"){
    mat_historical_races_geny$Date <- as.Date(mat_historical_races_geny$Date)
  }
  
  mat_historical_races_geny_focus <- mat_historical_races_geny %>%
    mutate(Age=as.numeric(Age)) %>%
    filter(Cheval %in% current_race_horses) %>%
    filter(Discipline == current_race_discipline) %>%
    filter(Date<current_race_date) %>%
    filter(Age<=as.numeric(max(df_infos_target_race$Age))) %>%
    mutate(Year = year(Date) ) %>%
    select(Cheval,Gains,Year,Corde,Terrain) %>%
    group_by(Corde,Terrain,Year) %>%
    mutate(Gains = round(get_percentile_values(Gains),2)) %>%
    filter(Cheval == target_horse) %>%
    as.data.frame()

  idx_all <- mat_historical_races_geny_focus$Corde == gsub("è","e",gsub("â","a",gsub("é","e",current_race_corde))) & mat_historical_races_geny_focus$Terrain == gsub("è","e",gsub("â","a",gsub("é","e",current_race_terrain)))
  idx_corde <- mat_historical_races_geny_focus$Corde == gsub("è","e",gsub("â","a",gsub("é","e",current_race_corde))) & mat_historical_races_geny_focus$Terrain != gsub("è","e",gsub("â","a",gsub("é","e",current_race_terrain)))
  idx_terrain <- mat_historical_races_geny_focus$Corde != gsub("è","e",gsub("â","a",gsub("é","e",current_race_corde))) & mat_historical_races_geny_focus$Terrain == gsub("è","e",gsub("â","a",gsub("é","e",current_race_terrain)))
  idx_none <- mat_historical_races_geny_focus$Corde != gsub("è","e",gsub("â","a",gsub("é","e",current_race_corde))) & mat_historical_races_geny_focus$Terrain != gsub("è","e",gsub("â","a",gsub("é","e",current_race_terrain)))
  
  mat_historical_races_geny_focus$Class = NA
  if(sum(idx_all>0)){
    mat_historical_races_geny_focus[idx_all,"Class"] <-"Config"
  }
  
  if(sum(idx_corde>0)){
    mat_historical_races_geny_focus[idx_corde,"Class"] <-"Corde"
  }
  
  if(sum(idx_none>0)){
    mat_historical_races_geny_focus[idx_none,"Class"] <-"Others"
  }
  
  if(sum(idx_terrain>0)){
    mat_historical_races_geny_focus[idx_terrain,"Class"] <-"Terrain"
  }
 
  if(nrow(mat_historical_races_geny_focus)>0){
    mat_gains_yearly_config <- mat_historical_races_geny_focus[,c("Year","Gains","Class")] %>%
      tidyr::pivot_wider(
        names_from = Class,
        values_from = Gains,
        values_fn = mean
      ) %>%
      as.data.frame()
    
    if(nrow(mat_gains_yearly_config)>0){
      if(ncol(mat_gains_yearly_config)>1){
        rownames(mat_gains_yearly_config) <- mat_gains_yearly_config$Year
        mat_gains_yearly_config <- mat_gains_yearly_config[,-1]
        vec_columns_ordered <- intersect(c("Others","Terrain","Corde","Config"),colnames(mat_gains_yearly_config))
        mat_gains_yearly_config <- mat_gains_yearly_config[sort(rownames(mat_gains_yearly_config)),vec_columns_ordered]
      }
      
      if(sum(is.na(mat_gains_yearly_config))){
        mat_gains_yearly_config[is.na(mat_gains_yearly_config)] <- 0
      }
    }
    vec_earnings_year_config <- unlist(mat_gains_yearly_config)
    vec_earnings_year_config <- vec_earnings_year_config[!is.na(vec_earnings_year_config)]
    vec_earnings_year_config <- vec_earnings_year_config[vec_earnings_year_config > 0]
    vec_earnings_year_config_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_earnings_year_config))[rank(vec_earnings_year_config)]
    names(vec_earnings_year_config_color) <- vec_earnings_year_config
    
    vec_lab_xaxis <- rownames(mat_gains_yearly_config)
    vec_lab_yaxis <- colnames(mat_gains_yearly_config)
    
    plot(0,0,xlim=c(0,length(vec_lab_xaxis))+0.5,ylim=c(0,length(vec_lab_yaxis)+0.5),xaxt="n",yaxt="n",type="n",xlab="",ylab="")
    rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = brewer.pal(11,"RdGy")[11])
    axis(1, 1:((length(vec_lab_xaxis))),labels=FALSE,tick = FALSE)
    text(1:((length(vec_lab_xaxis))), par("usr")[3] - 0.2, labels = vec_lab_xaxis,pos = 1, xpd = TRUE,offset = .2,cex=0.7)
    par(las=2)
    axis(2, 1:length(vec_lab_yaxis), labels = vec_lab_yaxis,cex.axis=0.85)
    for(idx_time in vec_lab_xaxis )
    {
      for(idx_level in vec_lab_yaxis ){
        val_time_level <- mat_gains_yearly_config[idx_time,idx_level] 
        points(grep(idx_time,vec_lab_xaxis),grep(idx_level,vec_lab_yaxis),pch=15,cex=3,col=vec_earnings_year_config_color[as.character(val_time_level)])
      }
    }
  }

return(mat_gains_yearly_config)

}





