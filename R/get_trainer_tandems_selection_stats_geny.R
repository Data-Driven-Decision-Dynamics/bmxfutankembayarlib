get_trainer_tandems_selection_stats_geny <- function(target_trainer = NULL,
                                                     df_infos_target_race_geny = NULL,
                                                     path_df_historical_races = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                     number_days_back=720){
  
  message("Initilization of output")
  df_number_tandem_trainer_driver <- NULL
  df_mean_earning_tandem_location <- NULL
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  if(sum(is.na(df_historical_races_geny$Cheval))>0){
    df_historical_races_geny <- df_historical_races_geny[!is.na(df_historical_races_geny$Cheval),]
  }
  
  if(class(df_historical_races_geny$Date)!="Date"){
    df_historical_races_geny$Date <- as.Date(df_historical_races_geny$Date)
  }

  if(unique(df_infos_target_race_geny$Discipline) == "Trot Attelé"){
    df_mean_earning_tandem_location <- df_historical_races_geny %>%
      select(Date,Cheval,Lieu,Driver,Entraineur,Gains) %>%
      filter(Lieu ==unique(df_infos_target_race_geny$Lieu)) %>%
      filter(Entraineur == target_trainer) %>%
      filter(Driver %in%  df_infos_target_race_geny[df_infos_target_race_geny$Entraineur == target_trainer,"Driver"]) %>%
      filter(unique(df_infos_target_race_geny$Date)-Date<=number_days_back) %>%
      mutate(Tandem_Location = paste(Driver,Lieu,sep="_")) %>%
      distinct() %>%
      group_by(Tandem_Location) %>%
      dplyr::summarise(MEAN_EARNING_TANDEM_LOCATION = mean(Gains)) %>%
      as.data.frame()
    
    if(nrow(df_mean_earning_tandem_location)>0){
      df_mean_earning_tandem_location$Driver <- unlist(lapply(df_mean_earning_tandem_location$Tandem_Location,function(x) {unlist(strsplit(x,"_"))[1]}))
      df_mean_earning_tandem_location <- merge(df_mean_earning_tandem_location,df_infos_target_race_geny[,c("Driver","Cheval","Entraineur")],by.x="Driver",by.y="Driver",all.x=TRUE)
      df_mean_earning_tandem_location <- df_mean_earning_tandem_location[,c("Cheval","MEAN_EARNING_TANDEM_LOCATION")]
    }
  }
  
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Plat","Steeplechase","Cross","Haies","Trot Monté")){
    df_mean_earning_tandem_location <- df_historical_races_geny %>%
      select(Date,Cheval,Lieu,Jockey,Entraineur,Gains) %>%
      filter(Lieu ==unique(df_infos_target_race_geny$Lieu)) %>%
      filter(Entraineur == target_trainer) %>%
      filter(Jockey %in%  df_infos_target_race_geny[df_infos_target_race_geny$Entraineur == target_trainer,"Jockey"]) %>%
      filter(unique(df_infos_target_race_geny$Date)-Date<=number_days_back) %>%
      mutate(Tandem_Location = paste(Jockey,Lieu,sep="_")) %>%
      distinct() %>%
      group_by(Tandem_Location) %>%
      dplyr::summarise(MEAN_EARNING_TANDEM_LOCATION = mean(Gains)) %>%
      as.data.frame()
    
    if(nrow(df_mean_earning_tandem_location)>0){
      df_mean_earning_tandem_location$Jockey <- unlist(lapply(df_mean_earning_tandem_location$Tandem_Location,function(x) {unlist(strsplit(x,"_"))[1]}))
      df_mean_earning_tandem_location <- merge(df_mean_earning_tandem_location,df_infos_target_race_geny[,c("Jockey","Cheval","Entraineur")],by.x="Jockey",by.y="Jockey",all.x=TRUE)
      df_mean_earning_tandem_location <- df_mean_earning_tandem_location[,c("Cheval","MEAN_EARNING_TANDEM_LOCATION")]
    }
  }
  
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Plat","Steeplechase","Cross","Haies","Trot Monté")){

    vec_current_tandems <- df_infos_target_race_geny %>%
      filter(Entraineur == target_trainer) %>%
      select(Entraineur,Jockey) %>%
      mutate(Tandem= paste(Entraineur,Jockey,sep="-")) %>%
      filter() %>%
      select(Tandem) %>%
      unlist() %>%
      unique()
    
    df_number_tandem_trainer_driver <- df_historical_races_geny %>%
      select(Date,Cheval,Lieu,Driver,Jockey,Entraineur,Gains) %>%
      filter(Lieu == unique(df_infos_target_race_geny$Lieu)) %>%
      filter(Entraineur %in% unique(df_infos_target_race_geny$Entraineur)) %>%
      filter(unique(df_infos_target_race_geny$Date)-Date<=number_days_back) %>%
      mutate(Tandem = paste(Entraineur,Jockey,sep="-")) %>%
      filter(Tandem %in% vec_current_tandems) %>%
      distinct() %>%
      group_by(Tandem) %>%
      dplyr::summarise(NUMBER_TIME_TANDEM = n()) %>%
      as.data.frame()
    
    if(nrow(df_number_tandem_trainer_driver)>0) {
      df_number_tandem_trainer_driver$Jockey <- unlist(lapply(df_number_tandem_trainer_driver$Tandem,function(x){unlist(strsplit(x,"-"))[2]}))
      df_number_tandem_trainer_driver <- merge(df_number_tandem_trainer_driver,df_infos_target_race_geny[,c("Cheval","Jockey")],by.x="Jockey",by.y='Jockey',all.x=TRUE)
      df_number_tandem_trainer_driver <- df_number_tandem_trainer_driver[,c("Cheval","NUMBER_TIME_TANDEM")] 
    }
  }

  if(unique(df_infos_target_race_geny$Discipline) %in% c("Trot Attelé")){
    
    vec_current_tandems <- df_infos_target_race_geny %>%
      filter(Entraineur == target_trainer) %>%
      select(Entraineur,Driver) %>%
      mutate(Tandem= paste(Entraineur,Driver,sep="-")) %>%
      filter() %>%
      select(Tandem) %>%
      unlist() %>%
      unique()
    
    df_number_tandem_trainer_driver <- df_historical_races_geny %>%
      select(Date,Cheval,Lieu,Driver,Entraineur,Gains) %>%
      filter(Entraineur %in% unique(df_infos_target_race_geny$Entraineur)) %>%
      filter(unique(df_infos_target_race_geny$Date)-Date<=number_days_back) %>%
      mutate(Tandem = paste(Entraineur,Driver,sep="-")) %>%
      filter(Tandem %in% vec_current_tandems) %>%
      distinct() %>%
      group_by(Tandem) %>%
      dplyr::summarise(NUMBER_TIME_TANDEM = n()) %>%
      as.data.frame()
    
    if(nrow(df_number_tandem_trainer_driver)>0) {
      df_number_tandem_trainer_driver$Driver <- unlist(lapply(df_number_tandem_trainer_driver$Tandem,function(x){unlist(strsplit(x,"-"))[2]}))
      df_number_tandem_trainer_driver <- merge(df_number_tandem_trainer_driver,df_infos_target_race_geny[,c("Cheval","Driver")],by.x="Driver",by.y='Driver',all.x=TRUE)
      df_number_tandem_trainer_driver <- df_number_tandem_trainer_driver[,c("Cheval","NUMBER_TIME_TANDEM")] 
    }
  }

  if(!is.null(df_mean_earning_tandem_location)){
    if(!is.null(df_number_tandem_trainer_driver)){
      if(nrow(df_mean_earning_tandem_location)>0){
        if(nrow(df_number_tandem_trainer_driver)>0){
          df_mean_earning_tandem_location <- merge(df_mean_earning_tandem_location,df_number_tandem_trainer_driver,by.x="Cheval",by.y="Cheval",by.all=TRUE)
        }
      }
    }
  }
  
  if(nrow(df_mean_earning_tandem_location)==0){
    df_mean_earning_tandem_location <- NULL
  }
  
  return(df_mean_earning_tandem_location)
  
}
