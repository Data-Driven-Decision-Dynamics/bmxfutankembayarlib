get_horse_total_earning_epreuve_config_geny <- function (df_infos_target_race_geny = NULL,
                                                         path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                         number_days_back = 366){
  
  df_stats_epreuve_config <- NULL
  df_utils_sort <- NULL
  res <- NULL

  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  message("Preparing categories levels mapping data frame")
  df_epreuve <- data.frame(Epreuve=c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"),Values=seq(1:length(c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"))))
  
  message("Extract category of current race")
  if(!is.null(df_infos_target_race_geny)){
    message("Extract name of current candidates")
    current_race_horses <- gsub("\n","",unique(df_infos_target_race_geny$Cheval))
    message("Extract discipline of current race")
    current_race_discipline <- unique(df_infos_target_race_geny$Discipline)
    message("Extract terrain of current race")
    current_race_terrain <- as.vector(unique(df_infos_target_race_geny$Terrain))
    current_race_terrain <- sub("Machefer","Cendrée",current_race_terrain)
    current_race_terrain <- sub("Mâchefer","Cendrée",current_race_terrain)
    message("Extract terrain of current race")
    current_race_corde <- as.vector(unique(df_infos_target_race_geny$Corde))
    message("Extract terrain of current race")
    current_race_date  <- unique(df_infos_target_race_geny$Date)
    current_race_category <- get_race_category_geny (df_infos_target_race_geny)
    
    if(as.vector(unique(df_infos_target_race_geny$Discipline)) %in% c("Trot Attelé","Trot Monté")){
      df_performance_details <- df_historical_races_geny %>%
        filter(Cheval %in% current_race_horses ) %>%
        filter(Discipline == current_race_discipline ) %>%
        filter(current_race_date-Date<=number_days_back) %>%
        select(Cheval,Date,Discipline,Corde,Terrain,Gains,Epreuve,Details) %>%
        unique() %>%
        as.data.frame()
    }
    
    if(as.vector(unique(df_infos_target_race_geny$Discipline)) %in% c("Trot Attelé","Trot Monté")){
      df_performance_details <- df_historical_races_geny %>%
        filter(Cheval %in% current_race_horses ) %>%
        filter(Discipline == current_race_discipline ) %>%
        filter(current_race_date-Date<=number_days_back) %>%
        select(Cheval,Date,Discipline,Corde,Terrain,Gains,Epreuve,Details) %>%
        unique() %>%
        as.data.frame()
    }
    
    if(as.vector(unique(df_infos_target_race_geny$Discipline)) %in% c("Plat","Cross","Haies","Steeplechase")){
      df_performance_details <- df_historical_races_geny %>%
        filter(Cheval %in% current_race_horses ) %>%
        filter(Discipline == current_race_discipline ) %>%
        filter(current_race_date-Date<=number_days_back) %>%
        select(Cheval,Date,Discipline,Corde,Terrain,Gains,Epreuve,Details,Distance,Poids) %>%
        unique() %>%
        as.data.frame()
    }
    
    if(as.vector(unique(df_infos_target_race_geny$Discipline)) %in% c("Trot Attelé","Trot Monté")){
      for(idx_row in 1:nrow(df_performance_details))
      {
        df_performance_details_current_row <- df_performance_details[idx_row,,drop=FALSE]
        val_detected_category <- get_race_category_geny(df_performance_details_current_row)
        if(!is.null(val_detected_category)){
          if(!is.na(val_detected_category)){
            df_performance_details[idx_row,"Epreuve"] <- val_detected_category
          }
        }
      }
      for(idx_row in 1:nrow(df_performance_details))
      {
        if(!is.na(df_performance_details[idx_row,"Epreuve"])){
          if(df_performance_details[idx_row,"Epreuve"] == "QUALIFICATIVE"){
            val_detected_category <- get_race_category_geny(df_performance_details[idx_row,])
            if(!is.null(current_race_category)){
              if(!is.na(current_race_category)){
                df_performance_details[idx_row,"Epreuve"] <- val_detected_category
              }
            }
          }
        }
      }
    }
    
    if(as.vector(unique(df_infos_target_race_geny$Discipline)) %in% c("Plat","Cross","Haies","Steeplechase")){
      df_performance_details$Epreuve <- gsub("Listed","L",df_performance_details$Epreuve)
      for(idx_row in 1:nrow(df_performance_details))
      {
        df_performance_details_current_row <- df_performance_details[idx_row,,drop=FALSE]
        val_detected_category <- get_race_category_geny(df_performance_details_current_row)
        if(!is.null(val_detected_category)){
          if(!is.na(val_detected_category)){
            df_performance_details[idx_row,"Epreuve"] <- val_detected_category
          }
        }
      }
      df_performance_details <- df_performance_details %>%
        filter(Epreuve %in% unique(df_epreuve$Epreuve)) %>%
        as.data.frame()
    } else {
      df_performance_details <- df_performance_details %>%
        filter(Cheval %in% current_race_horses ) %>%
        filter(Discipline == current_race_discipline ) %>%
        filter(Epreuve %in% unique(df_epreuve$Epreuve)) %>%
        as.data.frame()
    }

    if(nrow(df_performance_details)>0){
      if(!is.na(current_race_category)){
        df_performance_details <- df_performance_details[rownames(unique(df_performance_details[,c("Cheval","Date","Discipline")])),]
        df_performance_details$Reference <- df_epreuve[df_epreuve$Epreuve==current_race_category,"Values"]
        df_performance_details <- merge(df_performance_details,df_epreuve,by.x="Epreuve",by.y="Epreuve",all.x=TRUE)
        df_performance_details$Delta <- df_performance_details$Reference - df_performance_details$Values
        if(nrow(df_performance_details)>0){
          df_performance_details$Terrain <- gsub("Mâchefer","Cendrée",df_performance_details$Terrain)
          df_performance_details$Terrain <- gsub("Machefer","Cendrée",df_performance_details$Terrain)
          test_missing_corde <- is.na(df_performance_details$Corde)
          test_missing_terrain <- is.na(df_performance_details$Terrain)
          if(sum(test_missing_corde,na.rm = TRUE)>0){
            vec_lieu_missing_corde <- unique(df_performance_details[test_missing_corde,"Lieu"])
            vec_discipline_missing_corde <- unique(df_performance_details[test_missing_corde,"Discipline"])
            if(length(vec_lieu_missing_corde)>0){
              for(idx_missing_corde in vec_lieu_missing_corde ){
                for(idx_missing_discipline in vec_discipline_missing_corde ){
                  df_test_missing_corde_location <- unique(df_historical_races_geny[df_historical_races_geny$Discipline == idx_missing_discipline & df_historical_races_geny$Lieu == idx_missing_corde,c("Terrain","Corde")])
                  df_test_missing_corde_location <- df_test_missing_corde_location[complete.cases(df_test_missing_corde_location),]
                  if(nrow(df_test_missing_corde_location)>1){
                    df_test_missing_corde_location <- df_test_missing_corde_location[1,]
                  }
                  if(nrow(df_test_missing_corde_location)>0){
                    val_good_corde <- df_test_missing_corde_location$Corde
                    df_performance_details[is.na(df_performance_details$Corde) & df_performance_details$Lieu == vec_lieu_missing_corde,"Corde"] <- val_good_corde
                  }
                }
              }
            }
          }
          if(sum(test_missing_terrain,na.rm = TRUE)>0){
            vec_lieu_missing_terrain <- unique(df_performance_details[test_missing_terrain,"Lieu"])
            vec_discipline_missing_terrain <- unique(df_performance_details[test_missing_terrain,"Discipline"])
            if(length(vec_lieu_missing_terrain)>0){
              for(idx_missing_terrain in vec_lieu_missing_terrain ){
                for(idx_missing_discipline in vec_discipline_missing_terrain ){
                  df_test_missing_terrain_location <- unique(df_historical_races_geny[df_historical_races_geny$Discipline == idx_missing_discipline & df_historical_races_geny$Lieu == idx_missing_terrain,c("Terrain","Corde")])
                  df_test_missing_terrain_location <- df_test_missing_terrain_location[complete.cases(df_test_missing_terrain_location),]
                  if(nrow(df_test_missing_terrain_location)>1){
                    df_test_missing_terrain_location <- df_test_missing_terrain_location[1,]
                  }
                  if(nrow(df_test_missing_terrain_location)>0){
                    val_good_terrain <- df_test_missing_terrain_location$Terrain
                    df_performance_details[is.na(df_performance_details$Terrain) & df_performance_details$Lieu == vec_lieu_missing_terrain,"Terrain"] <- val_good_terrain
                  }
                }
              }
            }
          }
          
          if(nrow(df_performance_details)>0){
            idx_final_filled_corde_terrain <- !is.na(df_performance_details$Corde) & !is.na(df_performance_details$Terrain)
            if(sum(idx_final_filled_corde_terrain,na.rm = TRUE)>0){
              idx_config <- df_performance_details$Corde == unique(df_infos_target_race_geny$Corde) & df_performance_details$Terrain == unique(df_infos_target_race_geny$Terrain) 
              idx_corde <- df_performance_details$Corde == unique(df_infos_target_race_geny$Corde) & df_performance_details$Terrain != unique(df_infos_target_race_geny$Terrain)
              idx_terrain <- df_performance_details$Corde != unique(df_infos_target_race_geny$Corde) & df_performance_details$Terrain == unique(df_infos_target_race_geny$Terrain)
              idx_else <- df_performance_details$Corde != unique(df_infos_target_race_geny$Corde) & df_performance_details$Terrain != unique(df_infos_target_race_geny$Terrain)
              df_performance_details$Class <- NA
              if(sum(idx_config,na.rm = TRUE)>0){
                df_performance_details[which(idx_config==TRUE),"Class"] <-"Config"
              }
              if(sum(idx_corde,na.rm = TRUE)>0){
                df_performance_details[which(idx_corde==TRUE),"Class"] <-"Turn"
              }
              if(sum(idx_else,na.rm = TRUE)>0){
                df_performance_details[which(idx_else==TRUE),"Class"] <-"Others"
              }
              if(sum(idx_terrain,na.rm = TRUE)>0){
                df_performance_details[which(idx_terrain==TRUE),"Class"] <-"Ground"
              }
              if(sum(is.na(df_performance_details$Class),na.rm=TRUE)>0){
                idx_turn_filled <- is.na(df_performance_details$Class) & !is.na(df_performance_details$Corde) 
                if(sum(idx_turn_filled,na.rm=TRUE)>0){
                  df_performance_details[which(idx_turn_filled==TRUE),"Class"] <-"Turn"
                }
                idx_ground_filled <- is.na(df_performance_details$Class) & !is.na(df_performance_details$Terrain) 
                if(sum(idx_ground_filled,na.rm=TRUE)>0){
                  df_performance_details[which(idx_ground_filled==TRUE),"Class"] <-"Ground"
                }
                idx_turn_ground_filled <- is.na(df_performance_details$Class) & is.na(df_performance_details$Terrain) & is.na(df_performance_details$Corde)
                if(sum(idx_turn_ground_filled,na.rm=TRUE)>0){
                  df_performance_details[which(idx_turn_ground_filled==TRUE),"Class"] <-"Others"
                }
              }
            }
          }
        }
      }
    }
  }

  if(nrow(df_performance_details)==0){
    df_performance_details <- NULL
  }
  
  
  if(!is.na(current_race_category)){
    if(!is.null(df_performance_details)){
      df_performance_details$Epreuve <- gsub("Listed","L",df_performance_details$Epreuve)
      df_stats_epreuve_config <- df_performance_details %>%
        select(Cheval,Gains,Class,Epreuve) %>%
        filter(Gains>0) %>%
        group_by(Cheval,Class,Epreuve) %>%
        summarise(TOTAL_EARNING_LEVEL = sum(Gains,na.rm = TRUE)) %>%
        as.data.frame()
      
      if(!is.na(current_race_category)){
        df_utils_sort <- unique(df_performance_details[,c("Epreuve","Delta")])
        df_utils_sort <- df_utils_sort %>%
          group_by(Epreuve) %>%
          top_n(1,Delta) %>%
          as.data.frame()
        df_utils_sort <- df_utils_sort[order(df_utils_sort$Delta,decreasing = FALSE),]
        df_utils_sort <- unique(df_utils_sort[,1,drop=FALSE])
      }
    }
  }
  
  if(as.vector(unique(df_infos_target_race_geny$Discipline)) %in% c("Plat","Cross","Haies","Steeplechase")){
    df_df_infos_target_race_focus <- df_infos_target_race_geny[,c("Cheval","Poids","Distance")]
    colnames(df_df_infos_target_race_focus) <- sub("Poids","Weight",colnames(df_df_infos_target_race_focus))
    colnames(df_df_infos_target_race_focus) <- sub("Distance","Length",colnames(df_df_infos_target_race_focus))
    df_performance_details <- merge(df_performance_details,df_df_infos_target_race_focus,by.x="Cheval",by.y="Cheval",all.y=TRUE)
    res <- list(Infos=df_performance_details,Stats=df_stats_epreuve_config,Order=df_utils_sort)
  } else {
    res <- list(Stats=df_stats_epreuve_config,Order=df_utils_sort,Infos=df_performance_details)
  }
 
  return(res)
}

