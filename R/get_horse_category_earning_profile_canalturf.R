#' @return Compute the earnings par race category
#' @importFrom magrittr %>%
#' @importFrom reshape2 melt
#' @importFrom reshape2 dcast
#' @examples
#' get_horse_category_earning_profile_letrot(mat_perf_horse =NULL)
#' @export
get_horse_category_earning_profile_canalturf <- function (mat_infos_target_race = NULL,
                                                          path_mat_historical_races_canalturf = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/output/02-processed/mat_historical_races_canalturf.rds",
                                                          path_cluster_distance_range = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/input/mat_ranges_cluster_distance.rds",
                                                          number_days_back=366){

  mat_max_earnings_levels <- NULL
  mat_sum_earnings_levels <- NULL
  mat_earning_levels <- NULL
  
  message("Extraction date of the race")
  if(class(unique(mat_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(mat_infos_target_race$Date)))
  } else {
    current_race_date <- unique(mat_infos_target_race$Date)
  }
  
  message("Reading historical race infos file")
  if(!exists("mat_historical_races_canalturf")) {
    if(file.exists(path_mat_historical_races_canalturf)) {
      mat_historical_races_canalturf <- readRDS(path_mat_historical_races_canalturf)
    }
  }
  
  message("Reading file with cluster distance definition")
  mat_cluster_distance_range <- readRDS(path_cluster_distance_range)
  
  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(mat_infos_target_race$Cheval))
  
  message("Extract category of current race")
  current_race_category <- gsub("\n","",unique(mat_infos_target_race$Epreuve))
  
  message("Extract discipline of current race")
  current_discipline <- unique(mat_infos_target_race$Discipline)
  
  message("Focusing on relevant races")
  mat_performances_races_focus <- mat_historical_races_canalturf %>%
    mutate(Delay=as.numeric(current_race_date-Date)) %>%
    filter(Delay<=number_days_back) %>%
    filter(Cheval %in% current_race_horses) %>%
    filter(Discipline==current_discipline) %>%
    filter(Date<current_race_date) %>%
    select(Cheval,Epreuve,Gains,Distance)%>%
    as.data.frame()
 
  if(nrow(mat_performances_races_focus)>0) {
    mat_performances_races_focus <- mat_performances_races_focus[complete.cases(mat_performances_races_focus),]
    if(nrow(mat_performances_races_focus)>0){
      message("Focusing definition of cluster distance of current race")
      mat_cluster_distance_range_discipline <- mat_cluster_distance_range[gsub("É","E",toupper(mat_cluster_distance_range$Discipline))==unique(mat_infos_target_race$Discipline),]
      
      mat_performances_races_focus$Cluster_Distance <- NA
      for(i in 1:nrow(mat_cluster_distance_range_discipline))
      {
        idx_cluster_distance_group <- mat_cluster_distance_range_discipline[i,"Lower"] <= unlist(mat_performances_races_focus[,"Distance"]) & mat_cluster_distance_range_discipline[i,"Upper"] >= unlist(mat_performances_races_focus[,"Distance"])
        mat_performances_races_focus[idx_cluster_distance_group==TRUE,"Cluster_Distance"] <- paste(mat_cluster_distance_range_discipline[i,c("Lower","Upper")],collapse = "-")
      }
      
      mat_categories <- data.frame(Category=c("I","II","III","A","B","C","D","E","F","G","R"),Values=seq(1:length(c("I","II","III","A","B","C","D","E","F","G","R"))))
      
      mat_performances_races_focus <- merge(mat_performances_races_focus,mat_categories,by.x="Epreuve",by.y="Category")
      mat_performances_races_focus$Reference <- mat_categories[mat_categories$Category==current_race_category,"Values"]
      mat_performances_races_focus$Delta <- mat_performances_races_focus$Reference - mat_performances_races_focus$Values
      mat_performances_races_focus$RELATIVE_LEVEL <- NULL
      
      id_lower_levels <-  mat_performances_races_focus$Delta<=(-2)
      if(length(id_lower_levels)>0) {
        mat_performances_races_focus$RELATIVE_LEVEL[id_lower_levels] <- paste("EARNING_LOWER_LEVEL",mat_performances_races_focus$Cluster_Distance[id_lower_levels],sep="_")
      }
      
      id_current_level <- mat_performances_races_focus$Delta >-2 & mat_performances_races_focus$Delta<2
      if(length(id_current_level)>0) {
        mat_performances_races_focus$RELATIVE_LEVEL[id_current_level] <- paste("EARNING_CURRENT_LEVEL",mat_performances_races_focus$Cluster_Distance[id_current_level],sep="_")
      }
      
      id_upper_levels <- mat_performances_races_focus$Delta>=2
      if(length(id_upper_levels)>0) {
        mat_performances_races_focus$RELATIVE_LEVEL[id_upper_levels] <-  paste("EARNING_UPPER_LEVEL",mat_performances_races_focus$Cluster_Distance[id_upper_levels],sep="_")
      }
      
      mat_sum_earnings_levels <- mat_performances_races_focus %>%
        select(Cheval,RELATIVE_LEVEL,Gains) %>%
        pivot_wider(
          names_from = RELATIVE_LEVEL,
          values_from = Gains,
          values_fn = list(Gains = sum)
        )
      
      
      if(!is.null(mat_sum_earnings_levels)) {
        if(sum(is.na(mat_sum_earnings_levels))>0) {
          mat_sum_earnings_levels[is.na(mat_sum_earnings_levels)] <- 0
        }
        
        mat_sum_earnings_levels[,-1] <- round(mat_sum_earnings_levels[,-1,drop=FALSE]/sum(mat_sum_earnings_levels[,-1,drop=FALSE]),2)
        
        max_earnings_levels <- apply(mat_sum_earnings_levels[,-1,drop=FALSE],2,max)
        mat_sum_earnings_levels <- mat_sum_earnings_levels[,c("Cheval",names(max_earnings_levels)[max_earnings_levels>0])]
        colnames(mat_sum_earnings_levels) <- gsub("EARNING","EARNING_SUM",colnames(mat_sum_earnings_levels))
      }
      
      mat_max_earnings_levels <- mat_performances_races_focus %>%
        select(Cheval,RELATIVE_LEVEL,Gains) %>%
        pivot_wider(
          names_from = RELATIVE_LEVEL,
          values_from = Gains,
          values_fn = list(Gains = max)
        )
      
      if(!is.null(mat_max_earnings_levels)) {
        if(sum(is.na(mat_max_earnings_levels))>0) {
          mat_max_earnings_levels[is.na(mat_max_earnings_levels)] <- 0
        }
        
        mat_max_earnings_levels[,-1] <- round(mat_max_earnings_levels[,-1,drop=FALSE]/max(mat_max_earnings_levels[,-1,drop=FALSE]),2)
        
        max_earnings_levels <- apply(mat_max_earnings_levels[,-1,drop=FALSE],2,max)
        mat_max_earnings_levels <- mat_max_earnings_levels[,c("Cheval",names(max_earnings_levels)[max_earnings_levels>0])]
        colnames(mat_max_earnings_levels) <- gsub("EARNING","EARNING_MAX",colnames(mat_max_earnings_levels))
      }
      
      list_infos_earning_levels <- list(mat_sum_earnings_levels=mat_sum_earnings_levels,mat_max_earnings_levels=mat_max_earnings_levels)
      list_infos_earning_levels <- list_infos_earning_levels[lapply(list_infos_earning_levels,length)>0]
      
      if(length(list_infos_earning_levels)>0) {
        mat_earning_levels <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_infos_earning_levels)
      }
      
      if(sum(is.na(mat_earning_levels)>0)){
        mat_earning_levels [is.na(mat_earning_levels)] <- 0
      }
     }
  }
  return(mat_earning_levels)
}
