get_horse_best_comparable_speed_geny <- function(df_infos_target_race_geny = NULL , number_days_back = 720) {
  
  df_horses_records_per_track <- NULL
  df_horses_records_current_track <- NULL
  df_horses_records <- NULL
  
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  
  val_current_track <- unique(paste(df_infos_target_race_geny$Lieu,df_infos_target_race_geny$Discipline,df_infos_target_race_geny$Longueur,sep="_"))
  
  df_horses_records_per_track <- df_historical_races_geny %>%
    filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
    filter(Discipline == unique(df_infos_target_race_geny$Discipline) ) %>%
    filter(Date <= unique(df_infos_target_race_geny$Date) ) %>%
    filter(unique(df_infos_target_race_geny$Date)-Date<=number_days_back)  %>%
    select(Cheval,Numero,Discipline,Lieu,Longueur,Speed,Place) %>%
    drop_na() %>%
    mutate(Track = paste(Lieu,Discipline,Longueur,sep="_")) %>%
    select(Cheval,Track,Speed) %>%
    pivot_wider(
      names_from = Track,
      values_from = Speed,
      values_fn = max
    ) %>% 
    as.data.frame()
  
  if(nrow(df_horses_records_per_track)>0){
    vec_best_speed_horses <- apply(df_horses_records_per_track[,-1],1,function(x){max(x,na.rm=TRUE)})
    df_overall_records <- data.frame(Cheval = df_horses_records_per_track$Cheval , OVERALL_RECORD = vec_best_speed_horses)
  }
  
  if(val_current_track %in% colnames(df_horses_records_per_track)){
    if(sum(!is.na(df_horses_records_per_track[,val_current_track])) == nrow(df_horses_records_per_track)){
      df_horses_records_current_track <- df_horses_records_per_track[,c("Cheval",val_current_track)]
    }
  }
  
  if(!is.null(df_horses_records_current_track)){
    df_horses_records <- df_horses_records_current_track
    colnames(df_horses_records)[2] <- "SPEED"
    df_horses_records$Track <- 1
  } else {
    df_horses_records <- df_overall_records
    colnames(df_horses_records)[2] <- "SPEED"
    df_horses_records$Track <- 0
  }
  
  return(df_horses_records)
  
}