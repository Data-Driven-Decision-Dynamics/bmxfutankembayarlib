get_feature_improvement_driver_geny <- function(df_infos_target_race_geny = NULL,
                                                path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"){
  
  message("Initialization of final output")
  df_stats_success_driver_compiled <- NULL
  message("Initialization of final output")
  
  message("Computation of statistics on driver")
  for(target_horse in df_infos_target_race_geny$Cheval)
  {
    df_stats_success_driver <- get_signal_change_driver_previous_current_geny(df_infos_target_race_geny[df_infos_target_race_geny$Cheval == target_horse ,])
    df_stats_success_driver <- df_stats_success_driver[,c("Cheval",grep("PREVIOUS_DRIVER_PERCENT_SUCCESS|CURRENT_DRIVER_PERCENT_SUCCESS",colnames(df_stats_success_driver),value=TRUE))]
    if(!is.null(df_stats_success_driver)){
      df_stats_success_driver_compiled <- rbind.fill(df_stats_success_driver_compiled,df_stats_success_driver)
    }
  }
  
  if(!is.null(df_stats_success_driver_compiled)){
    if(nrow(df_stats_success_driver_compiled)>0){
      df_stats_success_driver_compiled$INTENTION_SCORE_IMPROVEMENT_DRIVER <- df_stats_success_driver_compiled$CURRENT_DRIVER_PERCENT_SUCCESS - df_stats_success_driver_compiled$PREVIOUS_DRIVER_PERCENT_SUCCESS
    }
    df_stats_success_driver_compiled <- df_stats_success_driver_compiled[,c("Cheval","INTENTION_SCORE_IMPROVEMENT_DRIVER")]
  }
  
  return(df_stats_success_driver_compiled)
  
}
