get_horse_earning_per_categories_geny <- function(df_infos_target_race_geny = NULL,
                                                  path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"){
  
  message("Initialization of final output")
  df_stats_earnings <- NULL
  message("Initialization of final output")
  
  message("Reading historical performance file")
  if(!exists("df_historical_races_geny")) {
    if(file.exists(path_df_historical_races_geny)){
      df_historical_races_geny <- readRDS(path_df_historical_races_geny)
    }
  }
  message("Reading historical performance file")
  
  message("Converting categories to number for easy comparison")
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Trot Attelé","Trot Monté")){
    df_epreuve <- data.frame(Epreuve=c("I","II","III","A","B","C","D","E","F","G","H","R","M"),Values=length(c("I","II","III","A","B","C","D","E","F","G","H","R","M")):1)
  } else {
    df_epreuve <- data.frame(Epreuve=c("I","II","III","L","A","B","C","D","E","F","G","H","R","M"),Values=length(c("I","II","III","L","A","B","C","D","E","F","G","H","R","M")):1)
  }
  id_groupes <- c("I","II","III","L")
  id_ab <- c("A","B")
  id_cd <- c("C","D")
  id_ef <- c("E","F")
  id_gh <- c("G","H","R","M")
  df_epreuve$Class <- NA
  df_epreuve[df_epreuve$Epreuve %in% id_groupes, "Class"] <- "I-I"
  df_epreuve[df_epreuve$Epreuve %in% id_ab, "Class"] <- "B-A"
  df_epreuve[df_epreuve$Epreuve %in% id_cd, "Class"] <- "D-C"
  df_epreuve[df_epreuve$Epreuve %in% id_ef, "Class"] <- "E-F"
  df_epreuve[df_epreuve$Epreuve %in% id_gh, "Class"] <- "G-H"
  message("Converting categories to number for easy comparison")
  
  message("Start computing total earning per categories")
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Trot Attelé","Trot Monté")){
    df_infos_earnings <- df_historical_races_geny %>%
      filter(Cheval %in% df_infos_target_race_geny$Cheval) %>%
      filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
      select(Cheval,Epreuve,Gains,Date,Engagement,Place) %>%
      drop_na() %>%
      as.data.frame()
    if(nrow(df_infos_earnings)>0){
      if(length(names(table(df_infos_earnings$Epreuve)))>5){
        df_infos_earnings <- merge(df_infos_earnings,df_epreuve,by.x="Epreuve",by.y="Epreuve",all.x=TRUE)
        df_stats_earnings <- df_infos_earnings %>%
          group_by(Cheval,Class) %>%
          mutate(Winner = ifelse(Place == 1 , 1, 0)) %>%
          dplyr::summarise(LAST_PARTICIPATION = max(Date),
                           SUM_EARNING = sum(Gains,na.rm=TRUE),
                           NUMBER_WINS = sum(Winner),
                           LIMITS = max(Engagement)) %>%
          mutate(Delay = as.numeric(unique(as.Date(df_infos_target_race_geny$Date))-LAST_PARTICIPATION)) %>%
          as.data.frame()
      } else {
        df_stats_earnings <- df_infos_earnings %>%
          group_by(Cheval,Epreuve) %>%
          mutate(Winner = ifelse(Place == 1 , 1, 0)) %>%
          dplyr::summarise(LAST_PARTICIPATION = max(Date),
                           SUM_EARNING = sum(Gains,na.rm=TRUE),
                           NUMBER_WINS = sum(Winner),
                           LIMITS = max(Engagement)) %>%
          mutate(Delay = as.numeric(unique(as.Date(df_infos_target_race_geny$Date))-LAST_PARTICIPATION)) %>%
          as.data.frame()
      }
    } 
  }
  
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Plat","Cross","Haies","Steeplechase")){
    df_infos_earnings <- df_historical_races_geny %>%
      filter(Cheval %in% df_infos_target_race_geny$Cheval) %>%
      filter(Discipline %in% unique(df_infos_target_race_geny$Discipline)) %>%
      select(Cheval,Epreuve,Gains,Date,Place) %>%
      drop_na() %>%
      as.data.frame()
    if(nrow(df_infos_earnings)>0){
      if(length(names(table(df_infos_earnings$Epreuve)))>5){
        df_infos_earnings <- merge(df_infos_earnings,df_epreuve,by.x="Epreuve",by.y="Epreuve",all.x=TRUE)
        df_stats_earnings <- df_infos_earnings %>%
          group_by(Cheval,Class) %>%
          mutate(Winner = ifelse(Place == 1 , 1, 0)) %>%
          dplyr::summarise(LAST_PARTICIPATION = max(Date),
                           SUM_EARNING = sum(Gains,na.rm=TRUE),
                           NUMBER_WINS = sum(Winner)) %>%
          mutate(Delay = as.numeric(unique(as.Date(df_infos_target_race_geny$Date))-LAST_PARTICIPATION)) %>%
          as.data.frame()
      } else {
        df_stats_earnings <- df_infos_earnings %>%
          group_by(Cheval,Epreuve) %>%
          mutate(Winner = ifelse(Place == 1 , 1, 0)) %>%
          dplyr::summarise(LAST_PARTICIPATION = max(Date),
                           SUM_EARNING = sum(Gains,na.rm=TRUE),
                           NUMBER_WINS = sum(Winner)) %>%
          mutate(Delay = as.numeric(unique(as.Date(df_infos_target_race_geny$Date))-LAST_PARTICIPATION)) %>%
          as.data.frame()
      }
    }
  }
  message("Start computing total earning per categories")

  return(df_stats_earnings)
}





