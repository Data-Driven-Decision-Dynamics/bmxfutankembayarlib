get_horse_profile_engagement_galop_geny <- function (df_perf_one_horse  = NULL , 
                                                     df_infos_target_race_geny = NULL,
                                                     path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds",
                                                     path_res_ranking = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/candidates_ranking",
                                                     number_days_back=366){
  
  message("Initialization of final output")
  df_profile_engagement <- NULL
  
  if(unique(df_infos_target_race_geny$Discipline) %in% c("Plat","Steeplechase","Cross","Haies")){
    
    message("Reading historical race infos file")
    if(!exists("df_historical_races_geny")) {
      if(file.exists(path_df_historical_races_geny)) {
        df_historical_races_geny <- readRDS(path_df_historical_races_geny)
      }
    }
    
    message("Extracting race date and convert in the right format if need")
    if(class(unique(df_infos_target_race_geny$Date))!="Date") {
      current_race_date  <- as.Date(as.vector(unique(df_infos_target_race_geny$Date)))
    } else {
      current_race_date <- unique(df_infos_target_race_geny$Date)
    }
    
    df_perf_one_horse <- df_perf_one_horse[complete.cases(df_perf_one_horse[,c("Corde","Terrain")]),]
    df_perf_one_horse <- df_perf_one_horse %>%
      mutate(Race = paste(Date,Heure,Lieu,Course,sep="_")) %>%
      as.data.frame()
    
    vec_potential_columns <- c("Cheval","Age","Date","Gender","Course","Race","Speed","Lieu","Heure","Corde","Distance","Terrain","Cumul_Gains","Details","Gains","Place","Date","Discipline","Recul","Poids","Prix",rev(grep("Cot",colnames(df_perf_one_horse),value=TRUE))[1])
    vec_potential_columns <- intersect(vec_potential_columns,colnames(df_perf_one_horse))
    
    df_perf_one_horse <- df_perf_one_horse[,vec_potential_columns] %>%
      distinct()
    
    df_perf_one_horse <- df_perf_one_horse %>%
      dplyr::filter(Discipline == unique(df_infos_target_race_geny$Discipline)) %>%
      dplyr::filter(Date<unique(df_infos_target_race_geny$Date)) %>%
      dplyr::filter(current_race_date-Date<=number_days_back) %>%
      top_n(5,Date) %>%
      as.data.frame()

    if(nrow(df_perf_one_horse)>0){
      if(length(grep("Cot",colnames(df_perf_one_horse),value=TRUE))>0){
        colnames(df_perf_one_horse) <- sub(grep("Cot",colnames(df_perf_one_horse),value=TRUE),"ODDS",colnames(df_perf_one_horse))
      }
      
      if(length(intersect("Cumul_Gains",colnames(df_infos_target_race_geny))) == 0){
        df_cumul_gains <- df_historical_races_geny %>%
          filter(Cheval %in% unique(df_infos_target_race_geny$Cheval)) %>%
          dplyr::group_by(Cheval) %>%
          dplyr::summarise(Cumul_Gains = sum(Gains)) %>%
          as.data.frame()
        if(nrow(df_cumul_gains)>0){
          df_infos_target_race_geny <- merge(df_infos_target_race_geny,df_cumul_gains,by.x="Cheval",by.y="Cheval",all=TRUE)
        }
      }
      
      df_infos_target_race_geny <- df_infos_target_race_geny %>%
        mutate(Race = paste(Date,Heure,Lieu,Course,sep="_")) %>%
        as.data.frame()
      
      df_infos_target_race_horse <- df_infos_target_race_geny[df_infos_target_race_geny$Cheval == unique(df_perf_one_horse$Cheval) ,c("Cheval","Age","Distance","Gender","Course","Race","Lieu","Heure","Corde","Terrain","Cumul_Gains","Details","Date","Discipline","Recul","Poids","Prix",rev(grep("Cot",colnames(df_infos_target_race_geny),value=TRUE))[1])]
      df_infos_target_race_horse$Gains <- NA
      df_infos_target_race_horse$Place <- NA
      df_infos_target_race_horse$Speed <- NA
      
      if(length(grep("Cot",colnames(df_infos_target_race_horse),value=TRUE))>0){
        colnames(df_infos_target_race_horse) <- sub(grep("Cot",colnames(df_infos_target_race_horse),value=TRUE),"ODDS",colnames(df_infos_target_race_horse))
      }
      
      df_perf_one_horse <- rbind(df_perf_one_horse,df_infos_target_race_horse)
      df_perf_one_horse <- df_perf_one_horse[order(df_perf_one_horse$Date,decreasing = FALSE),]
      if(sum(!duplicated(df_perf_one_horse[,c("Date","Course")]))>0){
        df_perf_one_horse <- df_perf_one_horse[!duplicated(df_perf_one_horse[,c("Date","Course")]),]
      }

      for(idx in 1:nrow(df_perf_one_horse))
      {
        horse_current_race_label <- df_perf_one_horse[idx,"Race"]
        
        df_perf_one_horse_current <- df_historical_races_geny %>%
          select(Cheval,Date,Lieu,Heure,Course, Gains,Discipline,Prix,Speed,Corde,Terrain,Distance,Race,Place,Cumul_Gains,Poids) %>%
          filter(Race == horse_current_race_label) %>%
          distinct() %>%
          select(Cheval,Date,Lieu,Heure,Course, Gains,Discipline,Prix,Speed,Corde,Terrain,Distance,Race,Place,Cumul_Gains,Poids) %>%
          as.data.frame()
        
        if(nrow(df_perf_one_horse_current)>0){
          current_horse_race_competitors <- as.vector(unique(df_perf_one_horse_current$Cheval))
          current_horse_race_discipline <- as.vector(unique(df_perf_one_horse_current$Discipline))
          current_horse_race_discipline <- tolower(current_horse_race_discipline)
          current_horse_race_discipline <- gsub("é","e",current_horse_race_discipline)
          current_horse_race_discipline <- tolower(gsub(" ","-",current_horse_race_discipline))
          vec_files_res_ranking <- dir(path_res_ranking,pattern="mat_ranking_monthly_temporal_glicko_global_")
          vec_files_res_ranking <- grep(gsub("-","_",current_horse_race_discipline),vec_files_res_ranking,value=TRUE)
          df_ranking_current <- readRDS(paste(path_res_ranking,vec_files_res_ranking,sep="/"))
          df_ranking_current_competitors <- df_ranking_current[df_ranking_current$Cheval %in% current_horse_race_competitors,]
          vec_candidate_dates <- as.Date(colnames(df_ranking_current_competitors)[-1])
          most_recent_date <- as.character(max(vec_candidate_dates[vec_candidate_dates<min(df_perf_one_horse_current$Date)]))
          df_ranking_current_competitors_focus <- df_ranking_current_competitors[,c("Cheval",as.character(most_recent_date))]
          df_ranking_current_competitors_focus <- df_ranking_current_competitors_focus[setdiff(df_ranking_current_competitors_focus$Cheval,unique(df_perf_one_horse$Cheval)),]
          df_ranking_current_competitors_focus <- df_ranking_current_competitors_focus[order(df_ranking_current_competitors_focus[,2],decreasing = TRUE),]
          mean_top_rank_opponents <- round(mean(df_ranking_current_competitors_focus[1:4,2],na.rm=TRUE),2)
        }
        
        df_perf_one_horse_current_cumul_earning <- df_perf_one_horse[idx,c("Cheval","Cumul_Gains")]
        horse_current_race_location <- df_perf_one_horse[idx,"Lieu"]
        horse_current_race_date <- df_perf_one_horse[idx,"Date"]
        horse_current_race_label <- df_perf_one_horse[idx,"Course"]
        horse_current_race_earning <- df_perf_one_horse[idx,"Gains"]
        horse_current_race_corde <- df_perf_one_horse[idx,"Corde"]
        horse_current_race_terrain <- df_perf_one_horse[idx,"Terrain"]
        horse_current_race_recul <- NA
        
        if("Recul" %in% colnames(df_perf_one_horse)){
          horse_current_race_recul <- df_perf_one_horse[idx,"Recul"]
        }
        
        horse_current_race_poids <- NA
        
        if("Poids" %in% colnames(df_perf_one_horse)){
          horse_current_race_poids <- df_perf_one_horse[idx,"Poids"]
        }
        
        horse_current_race_prize<- df_perf_one_horse[idx,"Prix"]
        horse_current_race_odds <- df_perf_one_horse[idx,"ODDS"]
        horse_current_race_config <- NA

        if(!is.na(horse_current_race_terrain) & !is.na(horse_current_race_corde)){
          if(horse_current_race_corde == unique(df_infos_target_race_geny$Corde) & horse_current_race_terrain != unique(df_infos_target_race_geny$Terrain)){
            horse_current_race_config <- "Turn"
          }
        }
        
        if(is.na(horse_current_race_terrain) & !is.na(horse_current_race_corde)){
          if(horse_current_race_corde == unique(df_infos_target_race_geny$Corde)){
            horse_current_race_config <- "Turn"
          }
        }
        
        if(!is.na(horse_current_race_terrain) & !is.na(horse_current_race_corde)){
          if(horse_current_race_corde == unique(df_infos_target_race_geny$Corde) & horse_current_race_terrain == unique(df_infos_target_race_geny$Terrain)){
            horse_current_race_config <- "Config"
          }
        }
        
        if(!is.na(horse_current_race_terrain) & !is.na(horse_current_race_corde)){
          if(horse_current_race_corde != unique(df_infos_target_race_geny$Corde) & horse_current_race_terrain == unique(df_infos_target_race_geny$Terrain)){
            horse_current_race_config <- "Ground"
          }
        }
        
        if(!is.na(horse_current_race_terrain) & is.na(horse_current_race_corde)){
          if(horse_current_race_terrain == unique(df_infos_target_race_geny$Terrain)){
            horse_current_race_config <- "Ground"
          }
        }
        
        if(!is.na(horse_current_race_terrain) & !is.na(horse_current_race_corde)){
          if(horse_current_race_corde != unique(df_infos_target_race_geny$Corde) & horse_current_race_terrain != unique(df_infos_target_race_geny$Terrain)){
            horse_current_race_config <- "Others"
          }
        }

        df_profile_engagement_current <- data.frame(Cheval = df_infos_target_race_horse$Cheval, Course = horse_current_race_label, Date = horse_current_race_date, Location = horse_current_race_location,   Corde = horse_current_race_corde, Terrain = horse_current_race_terrain, Configuration = horse_current_race_config, Prix = horse_current_race_prize,Earning = horse_current_race_earning, Odds = horse_current_race_odds, Poids= horse_current_race_poids , MEAN_RANKING_TOP_OPPONENTS = mean_top_rank_opponents)
        df_profile_engagement <- rbind.fill(df_profile_engagement,df_profile_engagement_current)
      }
    }
    
    if(!is.null(df_profile_engagement)){
      df_profile_engagement$Odds <- as.numeric(df_profile_engagement$Odds)
      if(sum(!is.na(df_profile_engagement$Odds))>0){
        df_profile_engagement$Odds <- round(100 * (1/df_profile_engagement$Odds),2)
      }
    }
  }

  return(df_profile_engagement) 
  
}




