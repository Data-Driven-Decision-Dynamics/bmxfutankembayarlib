#' @return Computes the trend in terms of category between all races over the last year
#' @importFrom magrittr %>%
#' @examples
#' get_horse_trend_category_letrot(df_infos_target_race=NULL)
#' @export
get_horse_progress_time_between_levels_geny <- function (df_infos_target_race=NULL,
                                                         path_mat_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/processed/historical_performances/mat_historical_races_geny.rds",
                                                         min_number_items=3,
                                                         number_days_back=366){
  
  mat_timing_progress_levels <- NULL
  
  if(!is.null(df_infos_target_race)){
    df_infos_target_race$Epreuve <- gsub("FALSE","F",df_infos_target_race$Epreuve)
  }

  message("Extraction date of the race")
  if(class(unique(df_infos_target_race$Date))!="Date") {
    current_race_date  <- as.Date(as.vector(unique(df_infos_target_race$Date)))
  } else {
    current_race_date <- unique(df_infos_target_race$Date)
  }
  
  message("Reading historical race infos file")
  if(!exists("mat_historical_races_geny")) {
    if(file.exists(path_mat_historical_races_geny)) {
      mat_historical_races_geny <- readRDS(path_mat_historical_races_geny)
    }
  }

  message("Extract name of current candidates")
  current_race_horses <- gsub("\n","",unique(df_infos_target_race$Cheval))
  
  message("Extract category of current race")
  current_race_category <- gsub("\n","",unique(df_infos_target_race$Epreuve))
  current_race_category <- sub("FALSE","F",current_race_category)
  
  message("Extract discipline of current race")
  current_race_discipline <- unique(df_infos_target_race$Discipline)
  
  if(!is.na(current_race_category)) {
    message("Selecting races over the last year for each candidate")
    mat_infos_first_try_level <- mat_historical_races_geny %>%
      mutate(Delay=as.numeric(current_race_date-Date)) %>%
      filter(Delay<=number_days_back) %>%
      filter(Cheval %in% current_race_horses) %>%
      group_by(Cheval,Epreuve) %>%
      filter(Discipline==current_race_discipline) %>%
      filter(Date<current_race_date) %>%
      filter(Gains>0) %>%
      top_n(-1,Date) %>%
      dplyr::select(Cheval,Epreuve,Distance,Gains,Delay,Date) %>%
      drop_na() %>%
      as.data.frame()
    
    if(nrow(mat_infos_first_try_level)>0) {
      mat_infos_first_try_level$Epreuve <- gsub("FALSE","F",mat_infos_first_try_level$Epreuve) 
    }
    
    mat_categories <- data.frame(Epreuve=c("I","II","III","A","B","C","D","E","F","G","R"),Values=seq(1:length(c("I","II","III","A","B","C","D","E","F","G","R"))))
    
    if(nrow(mat_infos_first_try_level)>0) {
      mat_infos_first_try_level <- merge(mat_infos_first_try_level,mat_categories,by.x="Epreuve",by.y="Epreuve")
      mat_infos_first_try_level$Reference <- mat_categories[mat_categories$Epreuve==current_race_category,"Values"]
      mat_infos_first_try_level$Delta <- mat_infos_first_try_level$Reference - mat_infos_first_try_level$Values
      
      mat_infos_first_try_level$RELATIVE_LEVEL <- NULL
      
      id_lower_levels <- mat_infos_first_try_level$Delta<=(-2)
      if(length(id_lower_levels)>0) {
        mat_infos_first_try_level$RELATIVE_LEVEL[id_lower_levels] <- "TIMING_FIRST_MONEY_LOWER_LEVEL"
      }
      
      id_current_level <- mat_infos_first_try_level$Delta >-2 & mat_infos_first_try_level$Delta<2
      if(length(id_current_level)>0) {
        mat_infos_first_try_level$RELATIVE_LEVEL[id_current_level] <- "TIMING_FIRST_MONEY_CURRENT_LEVEL"
      }
      
      id_upper_levels <- mat_infos_first_try_level$Delta>=2
      if(length(id_upper_levels)>0) {
        mat_infos_first_try_level$RELATIVE_LEVEL[id_upper_levels] <- "TIMING_FIRST_MONEY_UPPER_LEVEL"
      }
      
      mat_timing_progress_levels <- mat_infos_first_try_level %>%
        dplyr::select(Cheval,RELATIVE_LEVEL,Delay) %>%
        pivot_wider(
          names_from = RELATIVE_LEVEL,
          values_from = Delay,
          values_fn = list(Delay = min)) %>%
        as.data.frame()
      
      mat_num_horses_with_values <- apply(mat_timing_progress_levels,2,function(x){sum(!is.na(x))})
      vec_columns_progress_keep <- names(mat_num_horses_with_values)[mat_num_horses_with_values>=min_number_items]
      
      if(length(vec_columns_progress_keep)>1) {
        mat_timing_progress_levels <- mat_timing_progress_levels[,vec_columns_progress_keep]
        mat_timing_progress_levels <- merge(df_infos_target_race[,"Cheval",drop=FALSE],mat_timing_progress_levels,by.x="Cheval",by.y="Cheval",all=TRUE)
        for(col in 2:length(vec_columns_progress_keep))
        {
          if(sum(is.na(mat_timing_progress_levels[,col]))>0){
            mat_timing_progress_levels[is.na(mat_timing_progress_levels[,col]),col] <- median(mat_timing_progress_levels[!is.na(mat_timing_progress_levels[,col]),col])
          }
        }
      }
      
      for(col in 2:length(mat_timing_progress_levels))
      {
        mat_timing_progress_levels[,col] <- round(get_percentile_values(mat_timing_progress_levels[,col]),3)
      }
    }
  }

  return(mat_timing_progress_levels)
}











