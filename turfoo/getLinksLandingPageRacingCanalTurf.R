#' @return mat.offres a matrix with innfos on offers
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' getLinksLandingPageRacingCanalTurf(url.path='http://www.canalturf.com/courses_archives.php?date=2017-08-19')
#' @export
getLinksLandingPageRacingCanalTurf <- function(url.path='http://www.canalturf.com/courses_archives.php?date=2005-11-26')
{
  page.race <- tryCatch(read_html(url.path),error=function(e){cat("ERROR :",conditionMessage(e), "\n")})
  if(!is.null(page.race))
  {
    xpath.course <- "//*[@class=\"col-xs-8 text-overflow\"]"
    courses <- str_trim(page.race %>% html_nodes(xpath=xpath.course)  %>% html_nodes(xpath="strong") %>% html_text())
    infos.course <- str_trim(page.race %>% html_nodes(xpath=xpath.course) %>% html_text())
    for(course.label in courses)
    {
      infos.course <- gsub(course.label,"",infos.course,fixed = TRUE)
    }
    length.infos.course <- unlist(lapply(infos.course,function(x){return(length(unlist(strsplit(x,'-'))))}))
    infos.course <- infos.course[length.infos.course==3]
    infos.course <- unlist(strsplit(infos.course,"-"))
    disciplines  <- str_trim(infos.course[seq(1,length(infos.course),by=3)])
    distances    <- str_trim(infos.course[seq(2,length(infos.course),by=3)])
    distances    <- as.numeric(as.vector(sub('m','',distances)))
    prix    <- str_trim(infos.course[seq(3,length(infos.course),by=3)])
    prix    <- gsub('€','',prix)
    prix    <- gsub('\\.00','',prix)
    prix    <- gsub(' ','',prix)
    infos.courses <- data.frame ( Course=courses[length.infos.course==3], Discipline=disciplines, Distance=distances,Prix=prix)
    page=read_html("http://www.yelp.com/search?find_loc=New+York,+NY,+USA")
    xpath.links.programs.resultats <- "//*[@class=\"btn-group-vertical mar-rgt\"]"
    programs.resultats <- page.race  %>% html_nodes(xpath=xpath.links.programs.resultats) %>% html_nodes(xpath='a') %>% html_attr('href')
    programs <- programs.resultats[seq(1,length(programs.resultats),by=2)]
    programs <- programs [length.infos.course==3]
    resultats <- programs.resultats[seq(2,length(programs.resultats),by=2)]
    resultats <- resultats [length.infos.course==3]
    programs.resultats <- data.frame(Programmes=programs,Resultats=resultats)
    infos.courses <- cbind(infos.courses,programs.resultats)
  }
return(infos.courses)
}