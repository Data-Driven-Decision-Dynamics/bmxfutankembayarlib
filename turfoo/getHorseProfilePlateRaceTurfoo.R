getHorseProfilePlateRaceTurfoo <- function (path.race.data="/home/rd.loreal/payem/Futanke-Mbayar/input/matrix-performances-all-races.csv",
                                            url.path.new.race='http://www.turfoo.fr/programmes-courses/170911/reunion1-maisons-laffitte/course1-prix-du-brionnais/',
                                            race.reference.select.type = 'mixed',
                                            number.distances.frequent  = 3,
                                            number.distances.similar   = 2,
                                            date.race.min ="01/01/2016",
                                            delta.acceptable.distance = 100
                                   )
{
  mat.perf.all.races <- as.data.frame(read_csv(path.race.data))
  mat.perf.all.races <- mat.perf.all.races [!is.na(mat.perf.all.races$Distance),]
  mat.perf.all.races$RACE_TYPE <- NA
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)<=1200] <-'Sprint' 
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)>1200 & as.numeric(mat.perf.all.races$Distance)<=1600 ] <-'Flyer'
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)>1600 & as.numeric(mat.perf.all.races$Distance)<=2000 ] <-'Miler'
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)>2000 & as.numeric(mat.perf.all.races$Distance)<=2400 ] <-'Classique'
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)>2400] <-'Marathon'
  infos.horse.new.race <- getDetailsGivenRacingTurfo(url.path = url.path.new.race)
  race.horses <- unique(as.vector(infos.horse.new.race$Cheval))
  if(length(race.horses)<2)
  {
    stop('This function requires at 2 least 2 horses')
  }
  nb.horses.unknow <- length(setdiff(race.horses,unique(mat.perf.all.races$Cheval)))
  if(nb.horses.unknow>0)
  {
    horses.unknow <- setdiff(race.horses,unique(mat.perf.all.races$Cheval))
    message(paste(paste(setdiff(race.horses,unique(mat.perf.all.races$Cheval)),collapse = ",")," not present in knowledgebase"))
  }
  
  race.distance <- unique(infos.horse.new.race[,'Distance'])
  race.distance <- str_trim(gsub('mètres','',race.distance))
  race.distance <- as.numeric(race.distance)
  race.distance <- min(race.distance)
  
  if(class(race.distance)!='numeric')
  {
    stop('This function requires that the race distace must be given and should be numeric')
  }
  
  race.discipline <- as.vector(unique(infos.horse.new.race[,'Discipline']))
  
  mat.perf.races.current.horse <- subset(mat.perf.all.races,Cheval %in% race.horses)
  mat.perf.races.current.horse <- subset(mat.perf.races.current.horse,Discipline==race.discipline)
  mat.perf.races.current.horse <- subset(mat.perf.races.current.horse, Date >=as.Date(date.race.min,format = "%d/%m/%Y"))
  mat.perf.races.current.horse <- subset(mat.perf.races.current.horse, Distance <=race.distance+delta.acceptable.distance & Distance >=race.distance-delta.acceptable.distance)
  
  vec.races.curent.horses <- unique(mat.perf.races.current.horse$Race)
  mat.perf.races.complete.current.horse <- subset(mat.perf.all.races,Race %in% vec.races.curent.horses)
  list.mat.perf.races.complete.current.horse <- lapply(split.data.frame(mat.perf.races.complete.current.horse,mat.perf.races.complete.current.horse$Race),getReductionBasedEcartPlatTurfoo)
  mat.perf.races.complete.current.horse <- rbindlist(list.mat.perf.races.complete.current.horse,fill = TRUE)
  mat.perf.races.complete.current.horse <- as.data.frame(mat.perf.races.complete.current.horse)
  # list.mat.perf.races.complete.current.horse <- lapply(split.data.frame(mat.perf.races.complete.current.horse,mat.perf.races.complete.current.horse$Race),getRealDistanceBasedWeightTurfoo)
  # mat.perf.races.complete.current.horse <- rbindlist(list.mat.perf.races.complete.current.horse,fill = TRUE)
  # mat.perf.races.complete.current.horse <- as.data.frame(mat.perf.races.complete.current.horse)
  mat.perf.races.complete.current.horse<- subset(mat.perf.races.complete.current.horse,Cheval %in% race.horses)
  mat.perf.races.complete.current.horse <- transform(mat.perf.races.complete.current.horse,Time=EcartFormat)
  mat.perf.races.complete.current.horse <- transform(mat.perf.races.complete.current.horse,Speed=Distance/Time)
  mat.perf.races.current.horse <- mat.perf.races.complete.current.horse
  
  infos.horse.new.race.joint <- infos.horse.new.race[,c('Numero','Cheval','Distance','Poids')]
  
  ########## Tentative Ranking Horses based on previous races 
  mat.horses.direct.ranking <- NULL
  vec.races.curent.horses <- unique(mat.perf.races.current.horse$Race)
  mat.perf.races.complete.current.horse <- subset(mat.perf.all.races,Race %in% setdiff(vec.races.curent.horses,"01/02/2016-Cagnes Sur Mer-Prix De Grimaud"))
  list.mat.perf.races.complete.current.horse <- lapply(split.data.frame(mat.perf.races.complete.current.horse,mat.perf.races.complete.current.horse$Race),getReductionBasedEcartPlatTurfoo)
  mat.perf.races.complete.current.horse <- rbindlist(list.mat.perf.races.complete.current.horse,fill = TRUE)
  mat.perf.races.complete.current.horse <- as.data.frame(mat.perf.races.complete.current.horse)
  vec.num.horses.race <- table(mat.perf.races.current.horse$Race)
  vec.candidates.races.ranking <- names(vec.num.horses.race)[vec.num.horses.race>1] 
  for(race in vec.candidates.races.ranking)
  {
    mat.horses.direct.ranking.current.race <- NULL
    mat.perf.races.complete.current.horse.candidate.race <- subset(mat.perf.races.complete.current.horse,Race == race & Cheval %in% race.horses)
    mat.perf.races.complete.current.horse.candidate.race <- mat.perf.races.complete.current.horse.candidate.race[order(mat.perf.races.complete.current.horse.candidate.race[,'Place']),]
    for(id.horse in 1:(nrow(mat.perf.races.complete.current.horse.candidate.race)-1))
    {
      mat.horses.direct.ranking.current.race.current.horse <- NULL
      id.horse.compare <- (id.horse+1): nrow(mat.perf.races.complete.current.horse.candidate.race) 
      for(id.horse.next in id.horse.compare)
      {
        if(!is.na(mat.perf.races.complete.current.horse.candidate.race[id.horse,'EcartFormat']) & !is.na(mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'EcartFormat']))
        {
          delta.weight.compared.horses <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Poids']-mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'Poids']
          delta.time.compared.horses   <- mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'EcartFormat']-mat.perf.races.complete.current.horse.candidate.race[id.horse,'EcartFormat']
          if(delta.weight.compared.horses >=0 | delta.weight.compared.horses < 0 & delta.time.compared.horses >=(-delta.weight.compared.horses)*0.2)
          {
            race.name <- race
            date.name <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Date']
            distance <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Distance']
            prix <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Prix']
            terrain <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Terrain']
            corde <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Corde']
            winner <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Cheval']
            looser <- mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'Cheval']
            poids.winner <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Poids']
            poids.looser <- mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'Poids']
            temps.winner <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'EcartFormat']
            temps.looser <- mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'EcartFormat']
            mat.horses.direct.ranking.current.race.current.horse.item <- data.frame(Race=race.name,Date=date.name,Distance=distance,Prix=prix,Terrain=terrain,Corde=corde,Winner=winner,Looser=looser,
                                                                                    PoidsWinner=poids.winner,PoidsLooser=poids.looser,TempsWinner=temps.winner,TempsLooser=temps.looser)
            print(mat.horses.direct.ranking.current.race.current.horse.item)
          }
          mat.horses.direct.ranking.current.race.current.horse <- rbind(mat.horses.direct.ranking.current.race.current.horse,mat.horses.direct.ranking.current.race.current.horse.item)
        }
        mat.horses.direct.ranking.current.race <- rbind(mat.horses.direct.ranking.current.race.current.horse,mat.horses.direct.ranking.current.race) 
      }
    }
    
    mat.horses.direct.ranking <- rbind(mat.horses.direct.ranking,mat.horses.direct.ranking.current.race)
  }
  mat.horses.direct.ranking  <- mat.horses.direct.ranking[as.vector(mat.horses.direct.ranking$Winner)!=as.vector(mat.horses.direct.ranking$Looser),] 
  mat.horses.direct.ranking  <- transform(mat.horses.direct.ranking,DeltaTemps=TempsWinner-TempsLooser)
  mat.horses.direct.ranking  <- transform(mat.horses.direct.ranking,DeltaPoids=PoidsWinner-PoidsLooser)
  mat.horses.direct.ranking  <- unique(mat.horses.direct.ranking)
  mat.horses.direct.ranking  <- transform(mat.horses.direct.ranking,Delay=Sys.Date()-Date)
}


mat.winner.looser.direct.ranking <- matrix (0,ncol=length(as.vector(infos.horse.new.race$Cheval)),nrow=length(as.vector(infos.horse.new.race$Cheval)))
colnames(mat.winner.looser.direct.ranking) <- as.vector(infos.horse.new.race$Cheval)
rownames(mat.winner.looser.direct.ranking) <- as.vector(infos.horse.new.race$Cheval)
for(horse in rownames(mat.winner.looser.direct.ranking) )
{
  mat.horses.direct.ranking.tmp <- subset(mat.horses.direct.ranking,Winner==horse)
  if(nrow(mat.horses.direct.ranking.tmp)>0)
  {
    label.looser.freq <- table(as.vector(mat.horses.direct.ranking.tmp[,'Looser']))
    label.looser <- names(label.looser.freq)
    for(looser in label.looser)
    {
      delta.poids.past    <- max(subset(mat.horses.direct.ranking.tmp,Looser==looser)[,"DeltaPoids"])
      delta.poids.current <- subset(infos.horse.new.race.joint,Cheval==horse)[,'Poids'] - subset(infos.horse.new.race.joint,Cheval==looser)[,'Poids']
      if(delta.poids.past >= 0 & delta.poids.current<=delta.poids.past)
      {
        mat.winner.looser.direct.ranking[horse,looser] <- label.looser.freq[looser]
      } else if (delta.poids.past < 0 & delta.poids.current>=delta.poids.past) {
        mat.winner.looser.direct.ranking[horse,looser] <- label.looser.freq[looser]
      }
    }
    
  }
}







mat.winner.looser.direct.ranking <- table(mat.horses.direct.ranking$Winner,mat.horses.direct.ranking$Looser)
col.names<- colnames(mat.winner.looser.direct.ranking)
row.names <- rownames(mat.winner.looser.direct.ranking)
mat.winner.looser.direct.ranking <- as.matrix.data.frame(mat.winner.looser.direct.ranking)
colnames(mat.winner.looser.direct.ranking) <- col.names
rownames(mat.winner.looser.direct.ranking) <- row.names

getNodesEdgesListFromMatrix (network.data=mat.winner.looser.direct.ranking)




########## Tentative Ranking Horses based on previous races


########## Vitesse Estimation

  list.mat.perf.races.current.horse <- lapply(split.data.frame(mat.perf.races.current.horse,mat.perf.races.current.horse$Cheval),getAverageSpeedHorse)
  mat.average.speed.horse <- rbindlist(list.mat.perf.races.current.horse,fill = TRUE)
  mat.average.speed.horse <- as.data.frame(mat.average.speed.horse)
  infos.horse.new.race.joint <- infos.horse.new.race[,c('Numero','Cheval','Distance','Poids')]
  mat.average.speed.horse <- merge(mat.average.speed.horse,infos.horse.new.race.joint,by.x='Cheval',by.y='Cheval',all.x=TRUE ,all.y=TRUE)
  colnames(mat.average.speed.horse) <- sub('Distance.x','MeanDistance',colnames(mat.average.speed.horse))
  colnames(mat.average.speed.horse) <- sub('Distance.y','Distance',colnames(mat.average.speed.horse))
  mat.average.speed.horse <- getRealDistanceBasedWeightTurfoo(mat.average.speed.horse)
  mat.average.speed.horse <- transform(mat.average.speed.horse,Time= Distance/AverageSpeed)
  mat.average.speed.horse$Cheval <- paste(mat.average.speed.horse$Numero,mat.average.speed.horse$Cheval,sep="-")
  mat.average.speed.horse <- mat.average.speed.horse[order(mat.average.speed.horse$Time),]
  
  mat.average.speed.horse <- mat.average.speed.horse[rownames(unique(mat.average.speed.horse[,c('Cheval','Time','Distance')])),]
  
  par(mar=c(2,2,2,2))
  par(mfrow=c(2,2))
  xlim=c(min(mat.average.speed.horse$MeanDistance)-50,max(mat.average.speed.horse$MeanDistance)+50)
  par(las=2)
  plot(mat.average.speed.horse[,'MeanDistance'],mat.average.speed.horse[,'Speed'],ylab='Average Speed',xlab='Average Distance',type='n',xlim=xlim)
  text(mat.average.speed.horse[,'MeanDistance'],mat.average.speed.horse[,'Speed'],mat.average.speed.horse$Cheval,cex=0.75)
  abline(v=race.distance,col="orange",lty=2)
  par(las=2)
  plot(mat.average.speed.horse[,'MeanDistance'],mat.average.speed.horse[,'Time'],ylab='Estimated Time',xlab='Average Distance',type='n',xlim=xlim)
  text(mat.average.speed.horse[,'MeanDistance'],mat.average.speed.horse[,'Time'],mat.average.speed.horse$Cheval,cex=0.75)
  abline(v=race.distance,col="orange",lty=2)
  par(las=2)
  plot(mat.average.speed.horse[,'Speed'],mat.average.speed.horse[,'Time'],ylab='Estimated Time',xlab='Average Speed',type='n')
  text(mat.average.speed.horse[,'Speed'],mat.average.speed.horse[,'Time'],mat.average.speed.horse$Cheval,cex=0.75)
  abline(v=race.distance,col="orange",lty=2)
  ########## Vitesse Estimation
  
  
  
  ########## Global Indicators
  
  mat.perf.races.complete.current.horse.dt <- as.data.table(mat.perf.races.complete.current.horse)
  infos.horse.new.race.joint <- infos.horse.new.race[,c('Cheval','Poids','Distance')]
  colnames(infos.horse.new.race.joint) <- c('Cheval','PoidsCurrent','DistanceCurrent')
  
  par(mar=c(4,4,3,3))
  par(mfrow=c(2,2))
  mat.infos.best.ecart.current.horse  <- mat.perf.races.complete.current.horse.dt[,.SD[which.min(EcartFormat)],by=Cheval]
  mat.infos.best.ecart.current.horse  <- mat.infos.best.ecart.current.horse[,c('Cheval','Date','Distance','Reference','Poids','EcartFormat','Place')]
  mat.infos.best.ecart.current.horse.joint <- mat.infos.best.ecart.current.horse[,c('Cheval','Distance','Poids','EcartFormat')]
  mat.infos.best.ecart.current.horse.joint <- merge(infos.horse.new.race.joint,mat.infos.best.ecart.current.horse.joint,by.x='Cheval',by.y='Cheval',all.x=TRUE,all.y=TRUE)
  mat.infos.best.ecart.current.horse.joint <- transform(mat.infos.best.ecart.current.horse.joint,DeltaPoids=Poids-PoidsCurrent)
  mat.infos.best.ecart.current.horse.joint <- transform(mat.infos.best.ecart.current.horse.joint,Vitesse=Distance/EcartFormat)
  xlimit.abs <- max(abs(range(mat.infos.best.ecart.current.horse.joint$DeltaPoids,na.rm = TRUE)))
  plot(mat.infos.best.ecart.current.horse.joint$DeltaPoids,mat.infos.best.ecart.current.horse.joint$Vitesse,xlab='Weight Advatange',ylab='Speed',xlim=c(-xlimit.abs,xlimit.abs),type='n',main="Best Race Speed")
  text(mat.infos.best.ecart.current.horse.joint$DeltaPoids,mat.infos.best.ecart.current.horse.joint$Vitesse,paste(mat.infos.best.ecart.current.horse.joint$Cheval,mat.infos.best.ecart.current.horse.joint$Poids,sep='-'),cex=0.75)
  grid(10,10)
  
  
  mat.infos.heaviest.current.horse <- mat.perf.races.complete.current.horse.dt[,.SD[which.max(Poids)],by=Cheval]
  mat.infos.heaviest.current.horse  <- mat.infos.heaviest.current.horse[,c('Cheval','Date','Distance','Reference','Poids','EcartFormat','Place')]
  mat.infos.heaviest.current.horse.joint <- mat.infos.heaviest.current.horse[,c('Cheval','Distance','Poids','EcartFormat')]
  mat.infos.heaviest.current.horse.joint <- merge(infos.horse.new.race.joint,mat.infos.heaviest.current.horse,by.x='Cheval',by.y='Cheval',all.x=TRUE,all.y=TRUE)
  mat.infos.heaviest.current.horse.joint <- transform(mat.infos.heaviest.current.horse.joint,DeltaPoids=Poids-PoidsCurrent)
  mat.infos.heaviest.current.horse.joint <- transform(mat.infos.heaviest.current.horse.joint,Vitesse=Distance/EcartFormat)
  xlimit.abs <- max(abs(range(mat.infos.heaviest.current.horse.joint$DeltaPoids,na.rm = TRUE)))
  plot(mat.infos.heaviest.current.horse.joint$DeltaPoids,mat.infos.heaviest.current.horse.joint$Vitesse,xlab='Weight Advatange',ylab='Speed',xlim=c(-xlimit.abs,xlimit.abs),type='n',main="Heaviest Weight Race")
  text(mat.infos.heaviest.current.horse.joint$DeltaPoids,mat.infos.heaviest.current.horse.joint$Vitesse,paste(mat.infos.heaviest.current.horse.joint$Cheval,mat.infos.heaviest.current.horse.joint$Poids,sep='-'),cex=0.75)
  grid(10,10)
  
  
  mat.infos.lowest.reference.current.horse  <- mat.perf.races.complete.current.horse.dt[,.SD[which.min(Reference)],by=Cheval]
  mat.infos.lowest.reference.current.horse  <- mat.infos.lowest.reference.current.horse[,c('Cheval','Date','Distance','Reference','Poids','EcartFormat','Place')]
  mat.infos.lowest.reference.current.horse.joint <- mat.infos.lowest.reference.current.horse[,c('Cheval','Distance','Poids','EcartFormat')]
  mat.infos.lowest.reference.current.horse.joint <- merge(infos.horse.new.race.joint,mat.infos.lowest.reference.current.horse,by.x='Cheval',by.y='Cheval',all.x=TRUE,all.y=TRUE)
  mat.infos.lowest.reference.current.horse.joint <- transform(mat.infos.lowest.reference.current.horse.joint,DeltaPoids=Poids-PoidsCurrent)
  mat.infos.lowest.reference.current.horse.joint <- transform(mat.infos.lowest.reference.current.horse.joint,Vitesse=Distance/EcartFormat)
  xlimit.abs <- max(abs(range(mat.infos.lowest.reference.current.horse.joint$DeltaPoids,na.rm = TRUE)))
  plot(mat.infos.lowest.reference.current.horse.joint$DeltaPoids,mat.infos.lowest.reference.current.horse.joint$Vitesse,xlab='Weight Advatange',ylab='Speed',xlim=c(-xlimit.abs,xlimit.abs),type='n',main="Lowest Reference Race")
  text(mat.infos.lowest.reference.current.horse.joint$DeltaPoids,mat.infos.lowest.reference.current.horse.joint$Vitesse,paste(mat.infos.lowest.reference.current.horse.joint$Cheval,mat.infos.lowest.reference.current.horse.joint$Poids,sep='-'),cex=0.75)
  grid(10,10)
  
  
  mat.infos.most.recent.ecart.current.horse <- mat.perf.races.complete.current.horse.dt[,.SD[which.max(Date)],by=Cheval]
  mat.infos.most.recent.ecart.current.horse  <- mat.infos.most.recent.ecart.current.horse[,c('Cheval','Date','Distance','Reference','Poids','EcartFormat','Place')]
  mat.infos.most.recent.ecart.current.horse.joint <- mat.infos.most.recent.ecart.current.horse[,c('Cheval','Distance','Poids','EcartFormat')]
  mat.infos.most.recent.ecart.current.horse.joint <- merge(infos.horse.new.race.joint,mat.infos.most.recent.ecart.current.horse.joint,by.x='Cheval',by.y='Cheval',all.x=TRUE,all.y=TRUE)
  mat.infos.most.recent.ecart.current.horse.joint <- transform(mat.infos.most.recent.ecart.current.horse.joint,DeltaPoids=Poids-PoidsCurrent)
  mat.infos.most.recent.ecart.current.horse.joint <- transform(mat.infos.most.recent.ecart.current.horse.joint,Vitesse=Distance/EcartFormat)
  xlimit.abs <- max(abs(range(mat.infos.most.recent.ecart.current.horse.joint$DeltaPoids,na.rm = TRUE)))
  plot(mat.infos.most.recent.ecart.current.horse.joint$DeltaPoids,mat.infos.most.recent.ecart.current.horse.joint$Vitesse,xlab='Weight Advatange',ylab='Speed',xlim=c(-xlimit.abs,xlimit.abs),type='n',main="Most Recent Race")
  text(mat.infos.most.recent.ecart.current.horse.joint$DeltaPoids,mat.infos.most.recent.ecart.current.horse.joint$Vitesse,paste(mat.infos.most.recent.ecart.current.horse.joint$Cheval,mat.infos.most.recent.ecart.current.horse.joint$Poids,sep='-'),cex=0.75)
  grid(10,10)
  ########## Global Indicators
  
  
  ###### Impact Weight
  mat.perf.current.horse.weight.group <- group_by(subset(mat.perf.races.complete.current.horse,Distance==1900),Cheval,Poids)
  mat.perf.current.horse.weight.melt <- melt(mat.perf.current.horse.weight.group, id=c("Cheval", "Poids"),measure.vars='EcartFormat',na.rm=TRUE)
  mat.perf.current.horse.weight <- cast(mat.perf.current.horse.weight.melt, Cheval ~ Poids ~ variable,mean)
  mat.perf.current.horse.weight <- as.data.frame(mat.perf.current.horse.weight)
  colnames(mat.perf.current.horse.weight) <- gsub('.EcartFormat','',colnames(mat.perf.current.horse.weight))
  
  
  par(mfrow=c(3,4))
  for( i in 1:nrow(a))
  {
    plot(a[i,],main=rownames(a)[i])
  }
  ###### Impact Weight
  
  
  ########## Speciality Analysis 
  # mat.perf.current.horse.race.type.group <- group_by(mat.perf.races.current.horse ,Cheval,RACE_TYPE)
  # mat.perf.current.horse.gains.race.type.melt <- melt(mat.perf.current.horse.race.type.group, id=c("Cheval", "RACE_TYPE"),measure.vars='Gains',na.rm=TRUE)
  # mat.perf.current.horse.gains.race.type <- cast(mat.perf.current.horse.gains.race.type.melt, Cheval ~ RACE_TYPE ~ variable,sum)
  # mat.perf.current.horse.gains.race.type <- as.data.frame(mat.perf.current.horse.gains.race.type)
  # colnames(mat.perf.current.horse.gains.race.type) <- gsub('.Gains','',colnames(mat.perf.current.horse.gains.race.type))
  # mat.perf.current.horse.gains.race.type <- mat.perf.current.horse.gains.race.type/apply(mat.perf.current.horse.gains.race.type,1,sum)
  # mds <- cmdscale(1-cor(t(mat.perf.current.horse.gains.race.type)))
  # plot(mds,type='n',xlab='',ylab='')
  # text(mds,rownames(mds),cex=0.75)
  # grid(20,20)
  ########## Speciality Analysis 
  
  
  ########## Tentative Distance Affinity Estimation 
  # mat.perf.current.horse.distances.group <- group_by(mat.perf.races.current.horse ,Cheval,Distance)
  # mat.perf.current.horse.gains.distances.melt <- melt(mat.perf.current.horse.distances.group, id=c("Cheval", "Distance"),measure.vars='Gains',na.rm=TRUE)
  # mat.perf.current.horse.gains.distances <- cast(mat.perf.current.horse.gains.distances.melt, Cheval ~ Distance ~ variable,sum)
  # mat.perf.current.horse.gains.distances <- as.data.frame(mat.perf.current.horse.gains.distances)
  # colnames(mat.perf.current.horse.gains.distances) <- gsub('.Gains','',colnames(mat.perf.current.horse.gains.distances))
  # mat.perf.current.horse.gains.distances <- mat.perf.current.horse.gains.distances[apply(mat.perf.current.horse.gains.distances,1,max)>0,apply(mat.perf.current.horse.gains.distances,2,max)>0]
  # mat.perf.current.horse.gains.distances <- t(mat.perf.current.horse.gains.distances/ apply(mat.perf.current.horse.gains.distances,1,sum))
  # if(race.reference.select.type=='mixed')
  # {
  #   vec.distances.most.frequent <- as.numeric(names(sort(table(mat.perf.races.current.horse$Distance),decreasing = TRUE)))[1:number.distances.frequent]
  #   vec.distances.available <-sort(as.numeric(names(sort(table(mat.perf.races.current.horse$Distance),decreasing = TRUE))))
  #   id.distance.race <- grep(race.distance,vec.distances.available)
  #   id.distance.race.range <- (id.distance.race-number.distances.similar):(id.distance.race+number.distances.similar)
  #   id.distance.race.range <- intersect(id.distance.race.range,1:length(vec.distances.available))
  #   vec.distances.most.similar <- vec.distances.available[id.distance.race.range]
  #   vec.distances.most.similar <- intersect(vec.distances.most.similar,rownames(mat.perf.current.horse.gains.distances))
  #   vec.distances.selected <- unique(intersect(as.character(vec.distances.most.similar),as.character(vec.distances.most.frequent)))
  #   if(length(vec.distances.selected)>1) {
  #     weight.selected.distances <- apply(mat.perf.current.horse.gains.distances[vec.distances.selected,],2,sum)
  #     affinity.selected.distance.horses <- weight.selected.distances[sort(names(weight.selected.distances))]
  #   } else if(length(vec.distances.selected)==1) {
  #     affinity.selected.distance.horses <- mat.perf.current.horse.gains.distances[vec.distances.selected,]
  #     affinity.selected.distance.horses <- affinity.selected.distance.horses[sort(names(affinity.selected.distance.horses))]
  #   } else {
  #     
  #     vec.candidates.distances <- unique(c(as.character(vec.distances.most.similar),as.character(vec.distances.most.frequent)))
  #     vec.delta.distances <- abs(race.distance-as.numeric(vec.candidates.distances))
  #     vec.distances.selected <- vec.candidates.distances[vec.delta.distances<=delta.acceptable.distance]
  #     if(length(vec.distances.selected)>1) {
  #       weight.selected.distances <- apply(mat.perf.current.horse.gains.distances[vec.distances.selected,],2,sum)
  #       affinity.selected.distance.horses <- weight.selected.distances
  #       affinity.selected.distance.horses <- affinity.selected.distance.horses[sort(names(affinity.selected.distance.horses))]
  #     } else if (length(vec.distances.selected) ==1) {
  #       affinity.selected.distance.horses <- mat.perf.current.horse.gains.distances[vec.distances.selected,]
  #       affinity.selected.distance.horses <- affinity.selected.distance.horses[sort(names(affinity.selected.distance.horses))]
  #     }
  #   }
  # }
  ########## Tentative Distance Affinity Estimation
  
    
  
########## Advantage Weight
# mat.horses.direct.ranking.filtered <- subset(mat.horses.direct.ranking,Distance ==1600)
# mat.mean.weight.direct.ranking <- group_by(mat.horses.direct.ranking.filtered,Winner)
# mat.mean.weight.direct.ranking <- summarise(mat.mean.weight.direct.ranking,
#                                           PoidsMeanRanking=mean(PoidsWinner,na.rm = TRUE)
# )
#                                           
#                                           
# for()
# {
#   
# }
# 
# 
# 
# 
# 
# 
#   mat.perf.races.complete.current.horse <- subset(mat.perf.races.complete.current.horse,Cheval %in% race.horses)
#   mat.perf.races.complete.current.horse.group <- group_by(mat.perf.races.complete.current.horse,Cheval)
#   mat.indicators.current.horse <- summarise(mat.perf.races.complete.current.horse.group,
#                                             Ecart=mean(EcartFormat,na.rm = TRUE),
#                                             Distance=mean(Distance,na.rm = TRUE),
#                                             Poids=mean(Poids,na.rm = TRUE),
#                                             Vitesse =Distance/(Ecart*(Distance/1000)),
#                                             Decision= Vitesse*(Poids/10)
#   )
#   
#   plot(mat.indicators.current.horse$Distance,mat.indicators.current.horse$Vitesse,type='n',xlab='Distance',ylab='Affinity')
#   text(mat.indicators.current.horse$Distance,mat.indicators.current.horse$Vitesse,mat.indicators.current.horse$Cheval)
#   for(distance in as.character(vec.distances.selected))
#   {
#     abline(v=as.numeric(distance),col='orange',lty=2)
#   }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  ###
  # mat.perf.all.races.current.discipline <- subset(mat.perf.all.races,Discipline==race.discipline)
  # mat.perf.all.races.current.discipline.group <- group_by(mat.perf.all.races.current.discipline,Distance)
  # matrix.records.discipline <- summarise(mat.perf.all.races.current.discipline.group,
  #                                        Record=min(ReductionFormat,na.rm = TRUE)
  # )
  # matrix.records.discipline <- as.data.frame(matrix.records.discipline)
  # matrix.records.discipline <- matrix.records.discipline[complete.cases(matrix.records.discipline),]
  # matrix.records.discipline$Distance <- as.character(matrix.records.discipline$Distance)
  # rownames(matrix.records.discipline) <- as.character(matrix.records.discipline$Distance)
  # mat.score.gap.record.each.distance <- matrix(NA,ncol=ncol(mat.best.perf.distance.horses),nrow=nrow(mat.best.perf.distance.horses)) 
  # colnames(mat.score.gap.record.each.distance) <- colnames(mat.best.perf.distance.horses)
  # rownames(mat.score.gap.record.each.distance) <- rownames(mat.best.perf.distance.horses)
  # 
  # mat.perf.races.current.horse <- merge(mat.perf.races.current.horse,matrix.records.discipline,by.x='Distance',by.y='Distance')
  # mat.perf.races.current.horse <- transform(mat.perf.races.current.horse,Gap=Record-ReductionFormat)
  # 
  # mat.horse.distance.infos <- table(mat.perf.races.current.horse$Cheval,mat.perf.races.current.horse$Distance)
  # mat.horse.distance <- as.matrix.data.frame(mat.horse.distance.infos)
  # colnames(mat.horse.distance) <- colnames(mat.horse.distance.infos)
  # rownames(mat.horse.distance) <- rownames(mat.horse.distance.infos)
  # mat.horse.distance.score <- matrix(NA,nrow=nrow(mat.horse.distance),ncol=1)
  # colnames(mat.horse.distance.score) <- 'Affinity'
  # rownames(mat.horse.distance.score) <- rownames(mat.horse.distance)
  # 
  # for(horse in rownames(mat.score.gap.record.each.distance))
  # {
  #   distance.found <- colnames(mat.horse.distance)[mat.horse.distance[horse,]>0]
  #   distance.found <- distance.found[!is.na(distance.found)]
  #   number.races.distance <- mat.horse.distance[horse,distance.found]
  #   normalizer.distance <- as.numeric(distance.found)/race.distance
  #   normalizer.distance <- 1/(normalizer.distance/sum(normalizer.distance))
  #   mat.horse.distance.score[horse,1] <- weighted.mean(as.numeric(distance.found),normalizer.distance)
  #   distance.found <- colnames(mat.best.perf.distance.horses)[!is.infinite(t(mat.best.perf.distance.horses[horse,])[,1])]
  #   distance.found <- distance.found[!is.na(distance.found)]
  #   for(distance in distance.found)
  #   {
  #     reduction.horse.distance <- mat.best.perf.distance.horses[horse,distance]
  #     record.distance <- matrix.records.discipline[distance,'Record']
  #     delta.reduction.distance <- reduction.horse.distance-record.distance
  #     mat.score.gap.record.each.distance[horse,distance] <- delta.reduction.distance
  #   }
  # }
  
  ###
  
  
  

  
  
  # mat.infos.best.ecart.current.horse$Vitesse <- mat.infos.best.ecart.current.horse$Distance/mat.infos.best.ecart.current.horse$EcartFormat
  # mat.infos.heaviest.current.horse$Vitesse   <- mat.infos.heaviest.current.horse$Distance/mat.infos.heaviest.current.horse$EcartFormat
  # 
  # 
  # 
  # 
  # 
  # 
  # plot(Decision~Distance, data=mat.indicators.current.horse,type='n')
  # text(mat.indicators.current.horse$Distance,mat.indicators.current.horse$Decision,mat.indicators.current.horse$Cheval,cex=0.75)
  # abline(v=mean(mat.indicators.current.horse$Distance),col='orange',lty=2)
  # abline(v=2500,col='red',lty=2)
  # abline(v=2700,col='red',lty=2)
  # 
  # write.xlsx(mat.infos.best.ecart.current.horse,file="mat.infos.best.ecart.current.horse.xlsx")
  # write.xlsx(mat.infos.heaviest.current.horse,file="mat.infos.heaviest.current.horse.xlsx")
  # 
  # 
  # 
  # colnames(mat.best.perf.distance.horses) <- as.numeric(colnames(mat.best.perf.distance.horses))
  # mat.best.perf.distance.horses[is.infinite(as.matrix(mat.best.perf.distance.horses))] = 0
  # mat.best.perf.distance.horses[is.nan(as.matrix(mat.best.perf.distance.horses))] = 0
  
  

  
  
    
 
  
  
  
  
  
  # mat.perf.races.current.horse <- transform(mat.perf.races.current.horse,Vitesse = Distance/Reduction)
  # colnames(infos.horse.new.race) <- sub('Distance.x','Distance',colnames(infos.horse.new.race))

  # mat.perf.races.current.horse <- transform(mat.perf.races.current.horse,Vitesse = Distance/Reduction)
  # 
  # 
  # race.number.horses <- unique(infos.horse.new.race[,'Chevaux'])
  # if(race.number.horses <0)
  # {
  #   stop('This function requires that at least one horse name to be filled')
  # }
  # vec.known.horses <- as.vector(unique(mat.perf.all.races$Cheval))
  # number.race.horses.known <- intersect(race.horses,vec.known.horses)
  # if(length(number.race.horses.known)!=length(race.horses))
  # {
  #   message (paste('No information found for these horses',setdiff(race.horses,vec.known.horses)))
  # }
  # if(length(number.race.horses.known)<1)
  # {
  #   stop('This function requires that at least one horse for which there is historical data')
  # }
  # 
  # if(race.discipline =="Trot Attele")
  # {
  # }
  # 
  # 
  # mat.final.choix <- merge(mat.perf.races.current.horse,matrix.records.discipline,by.x = 'Distance',by.y='Distance')
  # mat.final.choix <- mat.final.choix[!is.na(mat.final.choix$Reduction),]
  # mat.final.choix <- transform(mat.final.choix,Gap=Record-Reduction)
  # mat.final.choix.group <- group_by(mat.final.choix,Cheval)
  # mat.final.choix.score <- summarise(mat.final.choix.group,
  #                                    ReductionMean = mean(Reduction,na.rm = TRUE)
  # )
  # mat.final.choix.score <- merge(infos.horse.new.race[,c('Position','Cheval','DistanceReal')],mat.final.choix.score,by.x='Cheval',by.y='Cheval')
  # mat.final.choix.score <- transform(mat.final.choix.score,Vitesse=ReductionMean/1000)
  # mat.final.choix.score <- transform(mat.final.choix.score,Arrivee=Vitesse*DistanceReal)
  # 
  # 
  # mat.perf.current.horse.races.group <- group_by(mat.perf.races.current.horse,Cheval)
  # mat.global.indicators.horse.races <- summarise(mat.perf.current.horse.races.group,
  #                                                GainMean = mean(Gains,na.rm = TRUE),
  #                                                DistanceMean = mean(Distance,na.rm = TRUE),
  #                                                ReductionMean = mean(Reduction,na.rm = TRUE),
  #                                                MostRecentRace = max(Date,na.rm = TRUE)
  # )
  # mat.global.indicators.horse.races <- as.data.frame(mat.global.indicators.horse.races)
  # rownames(mat.global.indicators.horse.races) <-as.vector(mat.global.indicators.horse.races$Cheval)
  # mat.global.indicators.horse.races <- transform(mat.global.indicators.horse.races,DelayLastRace=Sys.Date()-MostRecentRace)
  # mat.global.indicators.horse.races$DelayLastRace <- as.numeric(mat.global.indicators.horse.races$DelayLastRace)
  # mat.global.indicators.horse.races <- mat.global.indicators.horse.races[,-c(1,5)]
  # 
  # getProjectiondisplay(mat.global.indicators.horse.races)
  # 
  # 
  # 
  # mds.global.indicators = cmdscale(1-cor(t(mat.global.indicators.horse.races)))
  # plot(mds.global.indicators,type='n',xlab='',ylab='')
  # text(mds.global.indicators,rownames(mds.global.indicators),cex=0.8)
  # 
  # getProjectiondisplay <- function(mat.data=mat.score.gap.record.each.distance)
  # {
  #   cor.values <- cor(t(mat.data),use="pairwise.complete.obs") 
  #   id.remove.keep <- apply(cor.values,1,function(x){sum(is.na(x))}) != nrow(cor.values)
  #   cor.values <- cor.values[id.remove.keep,id.remove.keep]
  #   pca.res <- pca(cor.values,method='svdImpute')
  #   cor.values <- pca.res@completeObs
  #   mds.values = cmdscale(1-cor.values)
  #   xlimits <- max(abs(range(mds.values[,1])))
  #   ylimits <- max(abs(range(mds.values[,2])))
  #   
  #   plot(mds.values,type='n',xlab='',ylab='',xlim=c(-xlimits,xlimits),ylim=c(-ylimits,ylimits),cex.axis=0.75)
  #   text(mds.values,rownames(mds.values),cex=0.8,col=brewer.pal(8,'Set1')[1:4][as.factor(kmeans(mds.values,4)$cluster)])
  #   abline(v=0,lty=2,col='black')
  #   abline(h=0,lty=2,col='black')
  #   grid(15,15)
  # }
  # 
  # 
  # minimum = apply(mat.score.gap.record.each.distance,1,function(x){return(min(x,na.rm = TRUE))})
  # moyenne = apply(mat.score.gap.record.each.distance,1,function(x){return(mean(x,na.rm = TRUE))})
  # mediane = apply(mat.score.gap.record.each.distance,1,function(x){return(median(x,na.rm = TRUE))})
  # mat.score.gap.record.each.distance.average <- data.frame(Minimum=minimum,Moyenne=moyenne,Mediane=mediane)
  # mat.score.gap.record.each.distance.average$Cheval <- as.vector(rownames(mat.score.gap.record.each.distance.average))
  # mat.score.gap.record.each.distance.average <- merge(infos.horse.new.race[,c('Position','Cheval')],mat.score.gap.record.each.distance.average,by.x='Cheval',by.y='Cheval')
  # 
  # 
  # mat.perf.races.current.horse.year.melt <- melt(mat.perf.races.current.horse, id=c("Cheval", "Year"),measure.vars='Reduction',na.rm=TRUE)
  # mat.best.perf.distance.horses.year <- cast(mat.perf.races.current.horse.year.melt, Cheval ~ Year ~ variable,min)
  # mat.best.perf.distance.horses.year <- as.data.frame(mat.best.perf.distance.horses.year)
  # colnames(mat.best.perf.distance.horses.year) <- gsub('.Reduction','',colnames(mat.best.perf.distance.horses.year))
  # mat.best.perf.distance.horses.year[mat.best.perf.distance.horses.year==Inf] <-NA
  # number.missing.values <- apply(aa,1,function(x){return(sum(is.na(x)))})
  # aa <- aa[number.missing.values<10,number.missing.values<10]
  # mds <- cmdscale(1-aa)
  
  
  # 
  # getProjectiondisplay(mat.score.gap.record.each.distance)
  # getProjectiondisplay(mat.global.indicators.horse.races)
  # 
  
  
  
  # a = apply(mat.score.gap.record.each.distance,1,function(x){mean(x,na.rm = TRUE)})
  # b =mat.horse.distance.score[,1]
  # plot(a,b)
  # text(a,b,names(b))
  # mat.horse.distance.group <- group_by(mat.perf.races.current.horse,Cheval,Distance)
  # matrix.records.discipline <- summarise(mat.horse.distance.group,
  #                                        Record =min(Reduction,na.rm = TRUE)
  # sort(apply(mat.score.gap.record.each.distance,1,function(x){sum(x,na.rm =TRUE)}))
  
  
  # getDistanceAffinity <- function(mat.aff=mat.score.gap.record.each.distance,dist.race=2850)
  # {
  #   horse.label <- rownames(mat.aff)
  #   mat.res <- matrix(0,ncol=1,nrow=length(horse.label))
  #   colnames(mat.res) <- c("Affinity")
  #   rownames(mat.res) <- horse.label
  #   for(horse in horse.label )
  #   {
  #     vec.val.gap.horse   <- NULL
  #     distance.found <- as.numeric(colnames(mat.aff)[!is.na(mat.aff[horse,])])
  #     distance.found <- distance.found[distance.found >= dist.race-200 & distance.found <= dist.race+200] 
  #     distance.found <- as.character(distance.found)
  #     if(length(distance.found)>0)
  #     {
  #       for(distance in distance.found )
  #       {
  #         if( sum(!is.na(mat.aff[,distance]))>4)
  #         {
  #           vec.val.gap.horse   <- c(vec.val.gap.horse,mat.aff[horse,distance])
  #         }
  #       }
  #       mat.res[horse,'Affinity'] <- mean(vec.val.gap.horse)
  #     }
  #   }
  # }
  # 

  # mat.wins.races.current.horse.melt <- melt(mat.perf.races.current.horse, id=c("Cheval", "Distance"),measure.vars='Gains',na.rm=TRUE)
  # mat.wins.distance.horses <- cast(mat.wins.races.current.horse.melt, Cheval ~ Distance ~ variable,sum)
  # mat.wins.distance.horses <- as.data.frame(mat.wins.distance.horses)
  # colnames(mat.wins.distance.horses) <- gsub('.Gains','',colnames(mat.wins.distance.horses))
  # mat.percent.wins.distance <- mat.wins.distance.horses
  # for(row in rownames(mat.wins.distance.horses))
  # {
  #   mat.percent.wins.distance[row,] <- mat.wins.distance.horses[row,]/sum(mat.wins.distance.horses[row,])
  # }
  
  
  
# table.distances.horses <- table(mat.perf.races.current.horse$Distance,mat.perf.races.current.horse$Cheval)
# mat.distances.horses <- as.matrix.data.frame(table.distances.horses)
# colnames(mat.distances.horses) <- colnames(table.distances.horses)
# rownames(mat.distances.horses) <- rownames(table.distances.horses)
# num.distances.horses <- sort(apply(mat.distances.horses,1,function(x) {sum(x>0)}),decreasing = TRUE)
# if((num.distances.horses[1]/length(race.horses))>0.79)
# {
#   first.candidate.distance <- names(num.distances.horses)[1]
#   mat.perf.races.complete.first.candidate.distance.current.horse<-subset(mat.perf.races.complete.current.horse,Distance==first.candidate.distance)
#   mat.perf.races.complete.first.candidate.distance.current.horse.dt <- as.data.table(mat.perf.races.complete.first.candidate.distance.current.horse)
#   mat.best.perf.first.candidate.distance.horse  <- mat.perf.races.complete.first.candidate.distance.current.horse.dt[,.SD[which.min(EcartFormat)],by=Cheval]
#   
# }

