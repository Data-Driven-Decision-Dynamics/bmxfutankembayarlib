path_output_final_stats_position_recul_compiled <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_position_recul.rds"
path_output_final_stats_position_autostart_compiled <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_stats_number_autostart.rds"


df_stats_position <- readRDS(path_output_final_stats_position_recul_compiled)


target_track = "Vincennes-Trot-Attelé-2850"
target_level = "D"

df_stats_position_track <- df_stats_position %>%
  # filter(ID == target_track) %>%
  # filter(Epreuve == target_level) %>%
  mutate(Podium = ifelse(Place<4,1,0)) %>%
  as.data.frame()

df_epreuve <- data.frame(Epreuve=c("I","II","III","A","B","C","D","E","F","G","H","R","M"),Level=length(c("I","II","III","A","B","C","D","E","F","G","H","R","M")):1)

df_success_stats_position_track <- df_stats_position_track %>%
  group_by(ID,Recul,Epreuve) %>%
  dplyr::summarise(NUMBER_ITEMS = n(),
                   NUMBER_PODIUM = get_number_podium(Place) ,
                   PERCENT_SUCCESS = NUMBER_PODIUM/NUMBER_ITEMS,
                   PERCENT_SUCCESS = round(PERCENT_SUCCESS,2),
                   SCORE_TURN_RECENT = get_mean(TURN_RECENT),
                   SCORE_TURN_CARREER = get_mean(TURN_CARREER),
                   SCORE_GLOBAL_RECENT = get_mean(GLOBAL_RECENT),
                   SCORE_GLOBAL_CARREER = get_mean(GLOBAL_CARREER)) %>%
  as.data.frame()

df_success_stats_position_track <- merge(df_success_stats_position_track,df_epreuve,by.x="Epreuve",by.y="Epreuve")






df_stats_position_track = df_stats_position[df_stats_position$ID == target_track,  ]


file_stats_position_recul <- dir(path=path_files_stats_position_recul,pattern = ".xlsx")
setwd(path_files_stats_position_recul)
vec_steps <- seq(1,length(file_stats_position_recul),by=1000)
vec_steps[length(vec_steps)] <- length(file_stats_position_recul)

for (step in 1:(length(vec_steps)-1)) {
  setwd(path_files_stats_position_recul)
  df_stats_position_recul_current <- NULL
  file_stats_position_recul_current <- file_stats_position_recul[vec_steps[step]:(vec_steps[step+1])] 
  for(current_file in file_stats_position_recul_current)
  {
    df_one_race_stats_position_recul <- read.xlsx(current_file,sheetIndex = 1)
    if(nrow(df_one_race_stats_position_recul)>0){
      df_stats_position_recul_current <- rbind.fill(df_stats_position_recul_current,df_one_race_stats_position_recul)
    }
  }
  setwd(path_files_stats_position_recul_compiled)
  saveRDS(df_stats_position_recul_current,paste(step,".rds",sep=""))
  print(step)
}

setwd(path_files_stats_position_recul_compiled)
df_stats_position_recul_compiled <- list.files(path=path_files_stats_position_recul_compiled, full.names = TRUE) %>%
  lapply(readRDS) %>%
  bind_rows %>%
  as.data.frame()
df_stats_position_recul_compiled <- df_stats_position_recul_compiled %>% distinct() %>% as.data.frame()
saveRDS(df_stats_position_recul_compiled,path_output_final_stats_position_recul_compiled)

# list_stats_postion_recul <- lapply(list_historical_races_focus_recent,get_horse_stats_position_recul_geny)
# list_stats_postion_recul <- list_stats_postion_recul[lengths(list_stats_postion_recul) != 0]
# if(length(list_stats_postion_recul)>0){
#   df_stats_position_recul <- rbind.fill(list_stats_postion_recul)
#   if("TURN_RECENT_CONF" %in% colnames(df_stats_position_recul)){
#     df_success_stats_position_recul_turn_recent <- df_stats_position_recul %>%
#       mutate(Place = as.numeric(Place)) %>%
#       group_by(ID,TURN_RECENT_CONF) %>%
#       summarise(Number = n(),
#                 Success = get_number_podium_geny(Place),
#                 Chance = round(Success/Number,2)) %>%
#       drop_na() %>%
#       as.data.frame()
#     if(nrow(df_success_stats_position_recul_turn_recent)>0){
#       if(sum(colnames(df_success_stats_position_recul_turn_recent) == "TURN_RECENT_CONF")>0){
#         colnames(df_success_stats_position_recul_turn_recent)[colnames(df_success_stats_position_recul_turn_recent) == "TURN_RECENT_CONF"] <- "Conf"
#         df_success_stats_position_recul_turn_recent$Class <- "TURN_RECENT"
#       }
#     }
#   }
#   
#   if("TURN_CARREER_CONF" %in% colnames(df_stats_position_recul)){
#     df_success_stats_position_recul_turn_carreer <- df_stats_position_recul %>%
#       mutate(Place = as.numeric(Place)) %>%
#       group_by(ID,TURN_CARREER_CONF) %>%
#       summarise(Number = n(),
#                 Success = get_number_podium_geny(Place),
#                 Chance = round(Success/Number,2)) %>%
#       drop_na() %>%
#       as.data.frame()
#     if(nrow(df_success_stats_position_recul_turn_carreer)>0){
#       if(sum(colnames(df_success_stats_position_recul_turn_carreer) == "TURN_CARREER_CONF")>0){
#         colnames(df_success_stats_position_recul_turn_carreer)[colnames(df_success_stats_position_recul_turn_carreer) == "TURN_CARREER_CONF"] <- "Conf"
#         df_success_stats_position_recul_turn_carreer$Class <- "TURN_CARREER"
#       }
#     }
#   }
#   
#   if("GLOBAL_RECENT_CONF" %in% colnames(df_stats_position_recul)){
#     df_success_stats_position_recul_global_recent <- df_stats_position_recul %>%
#       mutate(Place = as.numeric(Place)) %>%
#       group_by(ID,GLOBAL_RECENT_CONF) %>%
#       summarise(Number = n(),
#                 Success = get_number_podium_geny(Place),
#                 Chance = round(Success/Number,2)) %>%
#       drop_na() %>%
#       as.data.frame()
#     if(nrow(df_success_stats_position_recul_global_recent)>0){
#       if(sum(colnames(df_success_stats_position_recul_global_recent) == "GLOBAL_RECENT_CONF")>0){
#         colnames(df_success_stats_position_recul_global_recent)[colnames(df_success_stats_position_recul_global_recent) == "GLOBAL_RECENT_CONF"] <- "Conf"
#         df_success_stats_position_recul_global_recent$Class <- "GLOBAL_RECENT"
#       }
#     }
#   }
#   
#   if("GLOBAL_CARREER_CONF" %in% colnames(df_stats_position_recul)){
#     df_success_stats_position_recul_global_carreer <- df_stats_position_recul %>%
#       mutate(Place = as.numeric(Place)) %>%
#       group_by(ID,GLOBAL_CARREER_CONF) %>%
#       summarise(Number = n(),
#                 Success = get_number_podium_geny(Place),
#                 Chance = round(Success/Number,2)) %>%
#       drop_na() %>%
#       as.data.frame()
#     if(nrow(df_success_stats_position_recul_global_carreer)>0){
#       if(sum(colnames(df_success_stats_position_recul_global_carreer) == "GLOBAL_CARREER_CONF")>0){
#         colnames(df_success_stats_position_recul_global_carreer)[colnames(df_success_stats_position_recul_global_carreer) == "GLOBAL_CARREER_CONF"] <- "Conf"
#         df_success_stats_position_recul_global_carreer$Class <- "GLOBAL_CARREER"
#       }
#     }
#   }
#   
#   list_success_stats_position_recul <- list(A = df_success_stats_position_recul_turn_recent, B = df_success_stats_position_recul_turn_carreer, C = df_success_stats_position_recul_global_recent, D = df_success_stats_position_recul_global_carreer)
#   list_success_stats_position_recul <- list_success_stats_position_recul[lengths(list_success_stats_position_recul) != 0]
#   df_success_stats_position_recul <- do.call("rbind", list_success_stats_position_recul)
#   saveRDS(df_success_stats_position_recul,path_stats_position_recul)
#   
# }
