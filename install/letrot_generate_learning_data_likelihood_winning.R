######################################## Loading required Packages ########################################
require(readr)
require(dplyr)
require(plyr)
require(magrittr)
require(doSNOW)
require(doParallel)
require(foreach)
require(parallel)
######################################## Loading required Packages ########################################
 
################################ Setting Output Path ############################
path_input_data  <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/01-raw/models"
path_output_training_data <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/train"
path_output_testing <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/test"
################################### Setting Output Path ############################

################################ Getting list of files to process ############################
vec_file_train_test   <- dir(path_input_data)
################################ Getting list of files to process ############################

################################ Defining parallelization scheme ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- parallel::makeCluster(ceiling(host_nb_cores*0.5), outfile="")
registerDoSNOW(nb_cores_use)
################################ Defining parallelization scheme ############################

################################ Compiling data on training and test set ############################
setwd(path_input_data)
mat_features_compiled <- ldply(vec_file_train_test, read_csv,.parallel=TRUE) %>%
  distinct() %>%
  as.data.frame()
mat_features_compiled <- transform(mat_features_compiled,Model=paste(mat_features_compiled$HIPPODROME,mat_features_compiled$DISCIPLINE,ifelse(mat_features_compiled$DISTANCE<=2400,"Classic","Marathon"),sep="_"))
mat_features_compiled <- transform(mat_features_compiled,Model=paste(mat_features_compiled$HIPPODROME,mat_features_compiled$DISCIPLINE,mat_features_compiled$DISTANCE,sep="_"))
colnames(mat_features_compiled) <- gsub("\\.","_",colnames(mat_features_compiled))
# test_missing_values <- apply(mat_features_compiled, 2, function(x){sum(is.na(x))})
# vec_features_with_missing <- names(test_missing_values)[test_missing_values>0]
# if(length(vec_features_with_missing)>0){
#   for(feature_with_missing in vec_features_with_missing )
#   {
#     if(sum(unique(mat_features_compiled[,feature_with_missing][!is.na(mat_features_compiled[,feature_with_missing])]) %in% c(0,1)==FALSE)==0) {
#       mat_features_compiled[,feature_with_missing][is.na(mat_features_compiled[,feature_with_missing])] <- 0
#     } else{
#       mat_features_compiled[,feature_with_missing][is.na(mat_features_compiled[,feature_with_missing])] <- median(mat_features_compiled[,feature_with_missing][!is.na(mat_features_compiled[,feature_with_missing])],na.rm=TRUE)
#     }
#   }
# }
saveRDS(mat_features_compiled,file=paste(path_output_training_data,paste(paste("mat_training_","consolided_compiled",sep=""),".rds",sep=""),sep="/"))
if(!file.exists("/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/input/Master-Feature-Description-Futanke-Mbayar.xlsx"))
{
  mat_features_infos <- data.frame(Feature=colnames(mat_features_compiled),Group=NA,Role=NA,Minimum=NA,Maximum=NA,Domain=NA,Class=NA)
  mat_features_infos$Feature <- as.vector(mat_features_infos$Feature)
  
  for(feature in mat_features_infos$Feature)
  {
    mat_features_infos[mat_features_infos$Feature==feature,"Domain"] <- length(unique(mat_features_compiled[,feature]))
    
    if(is.numeric(mat_features_compiled[,feature])) {
      mat_features_infos[mat_features_infos$Feature==feature,"Minimum"] <- min(mat_features_compiled[,feature],na.rm = TRUE)
      mat_features_infos[mat_features_infos$Feature==feature,"Maximum"] <- max(mat_features_compiled[,feature],na.rm = TRUE)
    }
    
    if(feature=="DATE") {
      mat_features_infos[mat_features_infos$Feature==feature,"Minimum"] <-  as.Date(min(mat_features_compiled[,feature],na.rm = TRUE),origin="1970-01-01")
      mat_features_infos[mat_features_infos$Feature==feature,"Maximum"] <-  as.Date(max(mat_features_compiled[,feature],na.rm = TRUE),origin="1970-01-01")
    }
  }
  
  write.xlsx(mat_features_infos,"/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/input/Master-Feature-Description-Futanke-Mbayar.xlsx")
  
}

mat_features_compiled$Class <- NA
mat_features_compiled$Class [mat_features_compiled$Place<=5] <- "High"
mat_features_compiled$Class [mat_features_compiled$Place>5]  <- "Low"
mat_features_compiled$Class <- as.factor(mat_features_compiled$Class)
mat_features_compiled <- mat_features_compiled[,c(setdiff(colnames(mat_features_compiled),"Class"),"Class")]
colnames(mat_features_compiled)=gsub("\\+","_",colnames(mat_features_compiled))
colnames(mat_features_compiled)=gsub("\\.","_",colnames(mat_features_compiled))
mat_features_compiled$Class <- as.factor(mat_features_compiled$Class)
mat_features_compiled <- mat_features_compiled[,c(setdiff(colnames(mat_features_compiled),"Class"),"Class")]
colnames(mat_features_compiled) <- toupper(colnames(mat_features_compiled))
colnames(mat_features_compiled)[ncol(mat_features_compiled)] <-"Class"
################################ Compiling data on training and test set ############################

################################ Preparing training data ###########################
# vec_hippodrome_already_processed <- unique(unlist(lapply(dir(path_output_training_data),function(x){unlist(strsplit(x,"_"))[3]})))
# vec_hippodrome_available <- tolower(unique(mat_features_compiled$HIPPODROME))
# setdiff(vec_hippodrome_available,vec_hippodrome_already_processed)
# vec_number_items_race_class <- sort(table(mat_features_compiled [mat_features_compiled$HIPPODROME=="MARSEILLE (A VIVAUX)",]$MODEL),decreasing = TRUE)
# mat_features_compiled <- mat_features_compiled[mat_features_compiled$DISCIPLINE =="Monté",]
vec_number_items_race_class <- sort(table(as.vector(mat_features_compiled$MODEL)),decreasing = TRUE)
vec_race_class_trainable <- names(vec_number_items_race_class)
# vec_race_class_trainable <- names(vec_number_items_race_class)[grep("ARGENTAN",names(vec_number_items_race_class))]

for(current_race_class in vec_race_class_trainable)
{
  current_model_name <- tolower(gsub(" ","-",gsub("è","e",gsub("é","e",current_race_class))))
  mat_features_compiled_current <- mat_features_compiled[mat_features_compiled$MODEL==current_race_class,]
  vec_missing_number <- apply(mat_features_compiled_current,2,function(x){return(sum(is.na(x)))})
  vec_features_remove <- names(vec_missing_number) [vec_missing_number==nrow(mat_features_compiled_current)]
  mat_features_compiled_current <- mat_features_compiled_current[,setdiff(colnames(mat_features_compiled_current),vec_features_remove)]
  
  vec_dates_train_test <- sort(unique(mat_features_compiled_current$DATE),decreasing = TRUE)
  vec_dates_test <- vec_dates_train_test[1:ceiling(length(vec_dates_train_test)*0.2)]

  vec_dates_train <- as.Date(setdiff(vec_dates_train_test,vec_dates_test),origin="1970-01-01")

  mat_training_current_race_class    <- mat_features_compiled_current[mat_features_compiled_current$DATE %in% vec_dates_train,]
  mat_testing_current_race_class     <- mat_features_compiled_current[mat_features_compiled_current$DATE %in% vec_dates_test,]

  colnames(mat_training_current_race_class) [ncol(mat_training_current_race_class)] <-"Class"
  mat_training_current_race_class <- mat_training_current_race_class[,setdiff(colnames(mat_training_current_race_class),vec_features_remove)]
  
  colnames(mat_testing_current_race_class) [ncol(mat_testing_current_race_class)] <-"Class"
  mat_testing_current_race_class <- mat_testing_current_race_class[,setdiff(colnames(mat_testing_current_race_class),vec_features_remove)]
  
  saveRDS(mat_training_current_race_class,file=paste(path_output_training_data,paste(paste("mat_training_",current_model_name,sep=""),".rds",sep=""),sep="/"))
  saveRDS(mat_testing_current_race_class,file=paste(path_output_testing,paste(paste("mat_testing_",current_model_name,sep=""),".rds",sep=""),sep="/"))
}
################################ Preparing training data ###########################