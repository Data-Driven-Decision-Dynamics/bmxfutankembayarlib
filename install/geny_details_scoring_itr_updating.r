######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
require( magrittr)
require(parallel)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_mat_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
path_input_details_itr_current   = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_compilation_details_itr.rds"
path_output_itr = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
path_output_monitoring <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/monitoring"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- ceiling(host_nb_cores*0.75)
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Reading Current ITR Details ############################
mat_compilation_details_itr_current <- readRDS(path_input_details_itr_current)
################################ Reading Current ITR Details ############################

################################ Data Extraction and storage ############################
mat_historical_races <- readRDS(path_mat_historical_races)
most_recent_date_historical_races   <- max(mat_historical_races$Date,na.rm = TRUE)
vec_engaged_horses_found <- dir(path_output_monitoring,pattern = "engaged-horses")
vec_dates_found <- as.Date(unlist(lapply(vec_engaged_horses_found,function(x){unlist(strsplit(x,"_"))[1]})))
last_date_race <- max(vec_dates_found)
mat_horses_to_update <- read.delim(paste(path_output_monitoring,paste(last_date_race,"engaged-horses",sep="_"),sep="/"),header = FALSE)
vec_horses_to_update <- as.vector(mat_horses_to_update$V1)
vec_horses_to_update <- vec_horses_to_update[!is.na(vec_horses_to_update)]
vec_horses_not_to_update <- setdiff(as.vector(unique(mat_compilation_details_itr_current$Cheval)),vec_horses_to_update)
mat_compilation_details_itr_current_not_to_update <- mat_compilation_details_itr_current[mat_compilation_details_itr_current$Cheval %in%vec_horses_not_to_update,]
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
mat_compilation_details_itr_new <- NULL
for (i in 1:length(vec_horses_to_update))
  {
    mat_historical_races_current <- mat_historical_races[mat_historical_races$Cheval==vec_horses_to_update[i],]
    mat_historical_races_current <- mat_historical_races_current[!is.na(mat_historical_races_current$Distance),]
    mat_historical_races_current <- subset(mat_historical_races_current,Gains>=0)
    if(nrow(mat_historical_races_current)>0 & max(mat_historical_races_current$Date)>=as.Date("2015-01-01")) {
      mat_compilation_details_itr_new_current_horse <- get_high_level_global_kpi_geny(mat_perf_horse=mat_historical_races_current)
      mat_compilation_details_itr_new <- rbind(mat_compilation_details_itr_new,mat_compilation_details_itr_new_current_horse)
  }
}
################################ Generate and store data ############################

################################ Compilation of ITR Details ############################
mat_compilation_details_itr_updated <- rbind(mat_compilation_details_itr_current,mat_compilation_details_itr_new)
mat_compilation_details_itr_updated$Gender <- gsub("FALSE","F",mat_compilation_details_itr_updated$Gender)
mat_compilation_details_itr_updated <- mat_compilation_details_itr_updated %>% distinct() %>% as.data.frame()
################################ Compilation of ITR Details ############################

################################ Saving ITR Details Calculation ############################
saveRDS(mat_compilation_details_itr_updated,paste(path_output_itr,"mat_compilation_details_itr.rds",sep="/"))
################################ Saving ITR Details Calculation ############################
