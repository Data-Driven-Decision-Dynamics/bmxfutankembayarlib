################################ Loading required packages ################################
require(fst)
require(readr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(data.table)
require(parallel)
require(magrittr)
require(bmxFutankeMbayar)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_input_data  <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/raw/issues_profile_delay"
path_output_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_issues_profile_delay.rds"
path_output_data_start <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/issues_profile_delay_tmp"
################################ Setting Output Path ############################

################################ Performance files compilation ############################
vec_availables_files <- dir(path_input_data)
vec_steps <- seq(1,length(vec_availables_files),by=100)
vec_steps[length(vec_steps)] <- length(vec_availables_files)

for (step in 1:(length(vec_steps)-1)) {
  setwd(path_input_data)
  mat_historical_races_current <- NULL
  vec_availables_files_current <- vec_availables_files[vec_steps[step]:(vec_steps[step+1])] 
  for(current_file in vec_availables_files_current)
  {
    mat_one_race <- read.csv(current_file)
    mat_historical_races_current <- rbind(mat_historical_races_current,mat_one_race)
  }
  setwd(path_output_data_start)
  saveRDS(mat_historical_races_current,paste(step,".rds",sep=""))
}

setwd(path_output_data_start)
mat_historical_races <- list.files(path=path_output_data_start, full.names = TRUE) %>%
  lapply(readRDS) %>%
  bind_rows %>%
  distinct() %>%
  as.data.frame()

df_issues_profile_delay_compiled <- mat_historical_races %>%
  group_by(Config,Discipline) %>%
  dplyr::summarise(Size = n(),
                   Success = get_number_top_five(Place),
                   Percent = Success/Size,
                   Percent = round(Percent,2)) %>%
  as.data.frame()

saveRDS(df_issues_profile_delay_compiled,path_output_data)



saveRDS(mat_historical_races,path_output_data)
################################ Performance files compilation ############################

################################ Parallelization Management ############################
# host_os  <- as.vector(Sys.info()['sysname'])
# host_nb_cores <- parallel::detectCores()
# nb_cores_use <- parallel::makeCluster(ceiling(host_nb_cores*0.9), outfile="")
# registerDoMC(cores=ceiling(host_nb_cores*0.9))
################################ Parallelization Management ############################