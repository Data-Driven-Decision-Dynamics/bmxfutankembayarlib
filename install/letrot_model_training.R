######################################## Loading required Packages ########################################
require(readr)
require(dplyr)
require(plyr)
require(e1071)
require(foreach)
require(doSNOW)
require(doParallel)
require(data.table)
require(magrittr)
require(WriteXLS)
require(forcats)
require(stringi)
require(stringr)
require(foreach)
require(parallel)
require(caret)
require(lime)
require(SuperLearner)
require(xlsx)
require(randomForest)
require(ROCR)
require(OptimalCutpoints)
require(ranger)
require(readr)
require(dplyr)
require(plyr)
require(C50)
require(rpart)
require(magrittr)
require(rattle)
######################################## Loading required Packages ########################################
 
################################ Setting Output Path ############################
path_input_training_data <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/train"
path_input_validation <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/validation"
path_input_testing <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/test"
path_output_perf <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/perfs"
path_output_models <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/models"
path_output_dashboard <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/dashboard"
################################### Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Model building for each location ############################
vec_training_file <- dir(path_input_training_data)
seq_nodesize <- seq(5,50,by=5)
seq_ntree=1000

for(training_file in vec_training_file)
{

  mat_agg_perf_type_jeux_train_compile <- NULL
  mat_agg_perf_type_jeux_validation_compile <- NULL
  
  mat_training_current_race_class <- readRDS(paste(path_input_training_data,training_file,sep="/"))
  mat_training_current_race_class$IDENTIFIER <- paste(mat_training_current_race_class$CHEVAL,mat_training_current_race_class$RACE,sep="_")
  rownames(mat_training_current_race_class)  <- mat_training_current_race_class$IDENTIFIER 
  mat_training_current_race_class_utils <- mat_training_current_race_class[,c("IDENTIFIER","RACE","DATE","HIPPODROME","PLACE")]
  mat_training_current_race_class <- mat_training_current_race_class %>%
    select(-(HIPPODROME:PLACE)) %>%
    select(-(MODEL)) %>%
    select(-(IDENTIFIER)) %>%
    select(-(CHEVAL)) %>%
    as.data.frame()

  if(nrow(mat_training_current_race_class)>75) {

    print(paste("processing ",training_file, "which contains ", nrow(mat_training_current_race_class),"lines"))
    
    seq_mtry <- seq(ceiling(sqrt(ncol(mat_training_current_race_class))),ncol(mat_training_current_race_class)/2,by=5)
    mat_tune_grid <- expand.grid(mtry=seq_mtry,nodesize=seq_nodesize,ntree=seq_ntree)

    for(idx_config in 1:nrow(mat_tune_grid))
    {
     
      message(paste("Launching training for",training_file,"and number", idx_config,"configuration"))
      model_current_race_class <- ranger(Class~.,data=mat_training_current_race_class,
                                         mtry=mat_tune_grid[idx_config,"mtry"],
                                         min.node.size=mat_tune_grid[idx_config,"nodesize"],
                                         num.trees=mat_tune_grid[idx_config,"ntree"],
                                         classification=TRUE,
                                         num.threads=ceiling(host_nb_cores*0.75),
                                         probability=TRUE,
                                         importance="impurity")
      
      message(paste("Compute prediction on training samples for",paste(training_file,"and number", idx_config,"configuration")))
      predicted_probabilities_current_race_class_training   <- model_current_race_class$predictions
      rownames(predicted_probabilities_current_race_class_training) <- rownames(mat_training_current_race_class)
      predicted_probabilities_current_race_class_training <- cbind(predicted_probabilities_current_race_class_training,mat_training_current_race_class_utils[rownames(predicted_probabilities_current_race_class_training),c("PLACE","RACE"),drop=FALSE])
      predicted_probabilities_current_race_class_training <- predicted_probabilities_current_race_class_training[order(predicted_probabilities_current_race_class_training$High,decreasing = TRUE),]

      message(paste("Initialization on matrix with training samples performances for",paste(training_file,"and number", idx_config,"configuration")))
      vec_races_train <- unique(mat_training_current_race_class_utils$RACE)
      mat_perf_type_jeux_train <- as.data.frame(matrix(NA,ncol=7,nrow=length(vec_races_train)))
      colnames(mat_perf_type_jeux_train) <- c("Simple","Couple","Trio","Tierce","Quarte","Quinte","Multi")
      rownames(mat_perf_type_jeux_train) <- vec_races_train
      
      message(paste("Extract validation samples for",paste(sub("training","validation",training_file),"and number", idx_config,"configuration")))
      mat_validation_current_race_class <- readRDS(paste(path_input_validation,sub("training","validation",training_file),sep="/"))
      mat_validation_current_race_class$IDENTIFIER <- paste(mat_validation_current_race_class$CHEVAL,mat_validation_current_race_class$RACE,sep="_")
      rownames(mat_validation_current_race_class)  <- mat_validation_current_race_class$IDENTIFIER 
      vec_races_validation <- unique(mat_validation_current_race_class$RACE)
      
      message(paste("Compute prediction on validation samples for",paste(sub("training","validation",training_file),"and number", idx_config,"configuration")))
      predicted_probabilities_current_race_class_validation   <- predict(model_current_race_class,mat_validation_current_race_class)$prediction
      rownames(predicted_probabilities_current_race_class_validation) <- rownames(mat_validation_current_race_class)
      predicted_probabilities_current_race_class_validation <- cbind(predicted_probabilities_current_race_class_validation,mat_validation_current_race_class[rownames(predicted_probabilities_current_race_class_validation),c("PLACE","RACE"),drop=FALSE])
      predicted_probabilities_current_race_class_validation <- predicted_probabilities_current_race_class_validation[order(predicted_probabilities_current_race_class_validation$High,decreasing = TRUE),]
      
      message(paste("Initialization on matrix with training samples performances for",paste(sub("training","validation",training_file),"and number", idx_config,"configuration")))
      mat_perf_type_jeux_validation <- as.data.frame(matrix(NA,ncol=7,nrow=length(vec_races_validation)))
      colnames(mat_perf_type_jeux_validation) <- c("Simple","Couple","Trio","Tierce","Quarte","Quinte","Multi")
      rownames(mat_perf_type_jeux_validation) <- vec_races_validation
      
      message(paste("Computing performance in terms of type of bet for",paste(training_file,"and number", idx_config,"configuration")))
      for(race_train in vec_races_train)
      {
        predicted_probabilities_current_race_class_training_current <- subset(predicted_probabilities_current_race_class_training,RACE==race_train)
        test_multi_7 <- match(1:4,predicted_probabilities_current_race_class_training_current[1:7,"PLACE"])
        test_quinte <- match(1:5,predicted_probabilities_current_race_class_training_current[1:7,"PLACE"])
        test_quarte <- match(1:4,predicted_probabilities_current_race_class_training_current[1:7,"PLACE"])
        test_tierce <- match(1:3,predicted_probabilities_current_race_class_training_current[1:5,"PLACE"])
        test_trio <- match(1:3,predicted_probabilities_current_race_class_training_current[1:5,"PLACE"])
        test_couple <- match(1:2,predicted_probabilities_current_race_class_training_current[1:4,"PLACE"])
        test_simple <- match(1,predicted_probabilities_current_race_class_training_current[1:3,"PLACE"])
        mat_perf_type_jeux_train[race_train,"Simple"] <- ifelse(sum(is.na(test_simple))==0,1,0)
        mat_perf_type_jeux_train[race_train,"Couple"] <- ifelse(sum(is.na(test_couple))==0,1,0)
        mat_perf_type_jeux_train[race_train,"Trio"] <- ifelse(sum(is.na(test_trio))==0,1,0)
        mat_perf_type_jeux_train[race_train,"Tierce"] <- ifelse(sum(is.na(test_tierce))==0,1,0)
        mat_perf_type_jeux_train[race_train,"Quarte"] <- ifelse(sum(is.na(test_quarte))==0,1,0)
        mat_perf_type_jeux_train[race_train,"Multi"] <- ifelse(sum(is.na(test_multi_7))==0,1,0)
        mat_perf_type_jeux_train[race_train,"Quinte"] <- ifelse(sum(is.na(test_quinte))==0,1,0)
      }
      mat_agg_perf_type_jeux_train <- as.data.frame(t(as.data.frame(round((apply(mat_perf_type_jeux_train,2,sum)/nrow(mat_perf_type_jeux_train))*100,2))))
      colnames(mat_agg_perf_type_jeux_train) <- colnames(mat_perf_type_jeux_train)
      rownames(mat_agg_perf_type_jeux_train) <- idx_config
      mat_agg_perf_type_jeux_train$MTRY <- mat_tune_grid[idx_config,"mtry"]
      mat_agg_perf_type_jeux_train$NODESIZE <- mat_tune_grid[idx_config,"nodesize"]
      mat_agg_perf_type_jeux_train$NTREE <- mat_tune_grid[idx_config,"ntree"]
      mat_agg_perf_type_jeux_train_compile <- rbind(mat_agg_perf_type_jeux_train_compile,mat_agg_perf_type_jeux_train)
      
      for(race_validation in vec_races_validation)
      {
        predicted_probabilities_current_race_class_validation_current <- subset(predicted_probabilities_current_race_class_validation,RACE==race_validation)
        test_multi_7 <- match(1:4,predicted_probabilities_current_race_class_validation_current[1:7,"PLACE"])
        test_quinte <- match(1:5,predicted_probabilities_current_race_class_validation_current[1:7,"PLACE"])
        test_quarte <- match(1:4,predicted_probabilities_current_race_class_validation_current[1:7,"PLACE"])
        test_tierce <- match(1:3,predicted_probabilities_current_race_class_validation_current[1:5,"PLACE"])
        test_trio <- match(1:3,predicted_probabilities_current_race_class_validation_current[1:5,"PLACE"])
        test_couple <- match(1:2,predicted_probabilities_current_race_class_validation_current[1:4,"PLACE"])
        test_simple <- match(1,predicted_probabilities_current_race_class_validation_current[1:3,"PLACE"])
        mat_perf_type_jeux_validation[race_validation,"Simple"] <- ifelse(sum(is.na(test_simple))==0,1,0)
        mat_perf_type_jeux_validation[race_validation,"Couple"] <- ifelse(sum(is.na(test_couple))==0,1,0)
        mat_perf_type_jeux_validation[race_validation,"Trio"] <- ifelse(sum(is.na(test_trio))==0,1,0)
        mat_perf_type_jeux_validation[race_validation,"Tierce"] <- ifelse(sum(is.na(test_tierce))==0,1,0)
        mat_perf_type_jeux_validation[race_validation,"Quarte"] <- ifelse(sum(is.na(test_quarte))==0,1,0)
        mat_perf_type_jeux_validation[race_validation,"Multi"] <- ifelse(sum(is.na(test_multi_7))==0,1,0)
        mat_perf_type_jeux_validation[race_validation,"Quinte"] <- ifelse(sum(is.na(test_quinte))==0,1,0)
      }
      
      mat_agg_perf_type_jeux_validation <- as.data.frame(t(as.data.frame(round((apply(mat_perf_type_jeux_validation,2,sum)/nrow(mat_perf_type_jeux_validation))*100,2))))
      colnames(mat_agg_perf_type_jeux_validation) <- colnames(mat_perf_type_jeux_validation)
      rownames(mat_agg_perf_type_jeux_validation) <- idx_config
      mat_agg_perf_type_jeux_validation$MTRY <- mat_tune_grid[idx_config,"mtry"]
      mat_agg_perf_type_jeux_validation$NODESIZE <- mat_tune_grid[idx_config,"nodesize"]
      mat_agg_perf_type_jeux_validation$NTREE <- mat_tune_grid[idx_config,"ntree"]
      mat_agg_perf_type_jeux_validation_compile <- rbind(mat_agg_perf_type_jeux_validation_compile,mat_agg_perf_type_jeux_validation)
    }
  }
    
    mat_agg_perf_type_jeux_train_compile <- mat_agg_perf_type_jeux_train_compile[,c(c("MTRY","NODESIZE","NTREE"),setdiff(colnames(mat_agg_perf_type_jeux_train_compile),c("MTRY","NODESIZE","NTREE")))]
    mat_agg_perf_type_jeux_train_compile$Class<- "Train"
    if(!is.null(ncol(mat_agg_perf_type_jeux_train_compile))) {
      saveRDS(mat_agg_perf_type_jeux_train_compile,paste(path_output_perf,sub("mat_","", training_file),sep="/"))
    }
    
    mat_agg_perf_type_jeux_validation_compile <- mat_agg_perf_type_jeux_validation_compile[,c(c("MTRY","NODESIZE","NTREE"),setdiff(colnames(mat_agg_perf_type_jeux_validation_compile),c("MTRY","NODESIZE","NTREE")))]
    mat_agg_perf_type_jeux_validation_compile$Class<- "Validation"
    if(!is.null(ncol(mat_agg_perf_type_jeux_validation_compile))){
      saveRDS(mat_agg_perf_type_jeux_validation_compile,paste(path_output_perf,sub("mat_training","validation",training_file),sep="/"))
    }
  
  }
