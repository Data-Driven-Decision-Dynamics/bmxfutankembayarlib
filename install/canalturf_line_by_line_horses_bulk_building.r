######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
require(doMC)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_line_by_line_output  =  "/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/output/01-raw/ranking_horses"
path_mat_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/output/02-processed/mat_historical_races_canalturf.rds"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- parallel::makeCluster(ceiling(host_nb_cores*0.75), outfile="")
registerDoMC(cores=ceiling(host_nb_cores*0.75))
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races_canalturf <- readRDS(path_mat_historical_races)
mat_historical_races_canalturf$Cheval <- as.vector(mat_historical_races_canalturf$Cheval)
mat_historical_races_canalturf$Race <- as.vector(mat_historical_races_canalturf$Race)
mat_historical_races_canalturf$Date <- as.Date(mat_historical_races_canalturf$Date)
mat_historical_races_canalturf <- mat_historical_races_canalturf[Sys.Date()- mat_historical_races_canalturf$Date<(367*5),]
vec_discipline <- as.vector(unique(mat_historical_races_canalturf$Discipline))
vec_discipline <- vec_discipline[!is.na(vec_discipline)]
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
for(discipline in vec_discipline)
{
  mat_historical_races_canalturf_focus <- subset(mat_historical_races_canalturf,Discipline==discipline)
  mat_historical_races_canalturf_focus <- mat_historical_races_canalturf_focus[Sys.Date()- mat_historical_races_canalturf_focus$Date<(367*5),]
  vec_races <- as.vector(unique(mat_historical_races_canalturf_focus$Race))
  vec_races <- vec_races[!is.na(vec_races)]
  print(paste("Starting the computation of line by line results for :",discipline))
  foreach(i=1:length(vec_races),.errorhandling="pass") %dopar% {
  # for(i in 1:length(vec_races)){
    mat_historical_races_canalturf_focus_current <- mat_historical_races_canalturf_focus[mat_historical_races_canalturf_focus$Race==vec_races[i],]
    mat_historical_races_canalturf_focus_current <- mat_historical_races_canalturf_focus_current [!is.na(mat_historical_races_canalturf_focus_current$Race),]
    if(nrow(mat_historical_races_canalturf_focus_current)>1) {
      get_line_by_line_driver_jockey_race_canalturf (mat_perf_race_complete=mat_historical_races_canalturf_focus_current,target_item="Horse")
    }
  }
  print(paste("Finished the computation of line by line results for :",discipline))
}
################################ Generate and store data ############################
