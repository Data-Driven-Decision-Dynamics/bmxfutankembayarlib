################################ Loading required packages ################################
require(bmxFutankeMbayar)
require(readr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(data.table)
require(parallel)
require(gtools)
require(randomForest)
require(doMC)
require(bmxFutankeMbayar)
require(rvest)
require(stringr)
require(curl)
require(dplyr)
require(qdap)
require(magrittr)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_input_historical <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/"
################################ Setting Output Path ############################

################################ Reading current data ############################
mat_historical_races <- readRDS(paste(path_input_historical,"mat_historical_races_geny.rds",sep="/"))
if(class(mat_historical_races$Date)!="Date"){
  mat_historical_races$Date <- as.Date(mat_historical_races$Date)
}

if(sum(is.na(mat_historical_races$Date))>0){
  mat_historical_races <- mat_historical_races[!is.na(mat_historical_races$Date),]
}

if(nrow(mat_historical_races)>0){
  mat_historical_races$Terrain <- gsub("Mâchefer","Cendrée",mat_historical_races$Terrain)
  mat_historical_races$Terrain <- gsub("Machefer","Cendrée",mat_historical_races$Terrain)
}
################################ Reading current data ############################

################################ Generate and store data ############################
start_date = as.Date("2021-07-08")
end_date   = as.Date("2021-07-10")
mat_new_races_pmu <- NULL
if(start_date<Sys.Date()-1){
  seq_date <- seq(start_date, end_date, by = "day")
  vec_url_date_scrape_pmu   <- paste("http://www.geny.com/reunions-courses-pmu?date=",seq_date,sep="")
  vec_url_date_scrape_pmu <- rev(vec_url_date_scrape_pmu)
  mat_new_races_pmu <- NULL
  for(i in 1:length(vec_url_date_scrape_pmu)) {
    vec_race_current_date_pmu <- get_race_given_date_geny(vec_url_date_scrape_pmu[i])
    vec_race_current_date_pmu <- setdiff(vec_race_current_date_pmu,"http://www.geny.com")
    if(length(vec_race_current_date_pmu)>0) {
      for(j in 1:length(vec_race_current_date_pmu)) {
        mat_infos_race_pmu <- get_performances_details_race_geny(vec_race_current_date_pmu[j])
        if(nrow(mat_infos_race_pmu)>0){
          mat_new_races_pmu <- rbind.fill(mat_new_races_pmu,mat_infos_race_pmu)
          print(paste(i,length(vec_url_date_scrape_pmu)))
        }
      } 
    }
  }
  
  if(!is.null(mat_new_races_pmu)){
    if(nrow(mat_new_races_pmu)>0){
      mat_new_races_pmu$Race <- paste(mat_new_races_pmu$Date,mat_new_races_pmu$Heure,mat_new_races_pmu$Lieu,mat_new_races_pmu$Course,sep="_")
      mat_new_races_pmu$Gender  <- as.vector(gsub("FALSE","F",mat_new_races_pmu$Gender))
      mat_new_races_pmu$Epreuve <- as.vector(gsub("FALSE","F",mat_new_races_pmu$Epreuve))
      mat_new_races_pmu <- mat_new_races_pmu %>% distinct()
    }
  }
}

mat_new_races_pmh <- NULL
if(start_date<Sys.Date()-1){
  seq_date <- seq(start_date, end_date, by = "day")
  vec_url_date_scrape_pmh   <- paste("http://www.geny.com/reunions-pmh?date=",seq_date,sep="")
  vec_url_date_scrape_pmh <- rev(vec_url_date_scrape_pmh)
  mat_new_races_pmh <- NULL
  for(i in 1:length(vec_url_date_scrape_pmh)) {
    vec_race_current_date_pmh <- get_pmh_race_given_date_geny(vec_url_date_scrape_pmh[i])
    vec_race_current_date_pmh <- setdiff(vec_race_current_date_pmh,"http://www.geny.com")
    if(length(vec_race_current_date_pmh)>0) {
      for(j in 1:length(vec_race_current_date_pmh)) {
        mat_infos_race_pmh <- get_pmh_performances_details_race_geny(vec_race_current_date_pmh[j])
        if(nrow(mat_infos_race_pmh)>0){
          mat_new_races_pmh <- rbind.fill(mat_new_races_pmh,mat_infos_race_pmh)
          print(paste(i,length(vec_url_date_scrape_pmh)))
        }
      } 
    }
  }
  
  if(!is.null(mat_new_races_pmh)){
    if(nrow(mat_new_races_pmh)>0){
      mat_new_races_pmh$Race <- paste(mat_new_races_pmh$Date,mat_new_races_pmh$Heure,mat_new_races_pmh$Lieu,mat_new_races_pmh$Course,sep="_")
      mat_new_races_pmh$Gender  <- as.vector(gsub("FALSE","F",mat_new_races_pmh$Gender))
      mat_new_races_pmh$Epreuve <- as.vector(gsub("FALSE","F",mat_new_races_pmh$Epreuve))
      mat_new_races_pmh <- mat_new_races_pmh %>% distinct()
    }
  }
}

if(!is.null(mat_new_races_pmu) & !is.null(mat_new_races_pmh)){
  mat_new_races <- rbind.fill(mat_new_races_pmu,mat_new_races_pmh)
  mat_new_races <- mat_new_races %>%
    distinct()%>%
    as.data.frame()
  mat_historical_races <- rbind.fill(mat_historical_races,mat_new_races)
  mat_historical_races <- mat_historical_races %>% distinct()
  if(class(mat_historical_races$Date)!="Date"){
    mat_historical_races$Date <- as.Date(mat_historical_races$Date)
  }
  
  setwd(path_input_historical)
  mat_historical_races$Terrain <- gsub("Sable Rose","Sable",mat_historical_races$Terrain)
  mat_historical_races$Terrain <- gsub("Mâchefer","Cendrée",mat_historical_races$Terrain)
  mat_historical_races$Terrain <- gsub("Machefer","Cendrée",mat_historical_races$Terrain)
  mat_historical_races$Lieu <- str_trim(gsub("\\[Matinée]","",mat_historical_races$Lieu))
  mat_historical_races$Lieu <- str_trim(gsub("- Genybet","",mat_historical_races$Lieu))
  mat_historical_races$Lieu <- str_trim(gsub(" Soir","",mat_historical_races$Lieu))
  mat_historical_races$Lieu <- bracketX(mat_historical_races$Lieu)
  mat_historical_races$Lieu <- str_trim(gsub("\\[Etats Unis","",mat_historical_races$Lieu))
  mat_historical_races$Lieu <- str_trim(gsub("\\{Emirats Arabes]","",mat_historical_races$Lieu))
  mat_historical_races$Lieu <- str_trim(gsub("Chantilly Midi","Chantilly",mat_historical_races$Lieu)) 
  mat_historical_races$Lieu <- str_trim(gsub("Deauville Midi","Deauville",mat_historical_races$Lieu)) 
  mat_historical_races$Lieu <- str_trim(gsub("Enghien-Midi","Enghien",mat_historical_races$Lieu)) 
  mat_historical_races <- mat_historical_races %>%
    mutate(Race = paste(Date,Heure,Lieu,Course,sep="_")) %>%
    as.data.frame()
  if(sum(is.na(mat_historical_races$Cheval))>0){
    mat_historical_races <- mat_historical_races[!is.na(mat_historical_races$Cheval),]
  }
  if(class(mat_historical_races$Date)!="Date"){
    mat_historical_races$Date <- as.Date(mat_historical_races$Date)
  }
  saveRDS(mat_historical_races,"mat_historical_races_geny.rds")
}
################################ Generate and store data ############################
