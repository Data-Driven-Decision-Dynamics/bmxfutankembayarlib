######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
require(doMC)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_line_by_line_output_tmp  =  "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/raw/pairwise_comparisons"
path_df_historical_line_by_line_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/pairwise_comparisons/mat_line_by_line_rating_geny.rds"
path_df_historical_races_geny <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
# host_os  <- as.vector(Sys.info()['sysname'])
# host_nb_cores <- parallel::detectCores()
# nb_cores_use <- parallel::makeCluster(ceiling(host_nb_cores*0.9), outfile="")
# registerDoMC(cores=ceiling(host_nb_cores*0.9))
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
df_historical_races_geny <- readRDS(path_df_historical_races_geny)
df_historical_line_by_line_geny <- readRDS(path_df_historical_line_by_line_geny)

if(class(df_historical_line_by_line_geny$Date)!="Date"){
  df_historical_line_by_line_geny$Date <- as.Date(df_historical_line_by_line_geny$Date)
}

mat_recent_races <- df_historical_races_geny[df_historical_races_geny$Date>max(df_historical_line_by_line_geny$Date,na.rm = TRUE),]

rm(df_historical_races_geny)
gc()

mat_line_by_line_recent_races <- NULL
if(nrow(mat_recent_races)>0){
  vec_races <- as.vector(unique(mat_recent_races$Race))
  vec_races <- vec_races[!is.na(vec_races)]
  for(i in 1:length(vec_races)){
    mat_recent_races_current <- mat_recent_races[mat_recent_races$Race==vec_races[i],]
    mat_recent_races_current <- mat_recent_races_current [rownames(unique(mat_recent_races_current[,c("Cheval","Numero")])),]
    mat_recent_races_current <- mat_recent_races_current [!is.na(mat_recent_races_current$Race),]
    mat_recent_races_current <- unique(mat_recent_races_current[,setdiff(colnames(mat_recent_races_current),c("Speed","Time","Musique","Draw","Jockey","Ecart",grep("Cote",colnames(mat_recent_races_current),value=TRUE)))])
    mat_recent_races_current <- mat_recent_races_current[,setdiff(colnames(mat_recent_races_current),"Numero")]
    mat_recent_races_current <- unique(mat_recent_races_current)
    if(nrow(mat_recent_races_current)>1) {
      mat_line_by_line_recent_races_tmp <- get_line_by_line_driver_jockey_race_geny (df_perf_race_complete=mat_recent_races_current,target_item="Horse",path_line_by_line_output = path_line_by_line_output_tmp)
      if(class(mat_line_by_line_recent_races_tmp$Winner_Last_Race)=="numeric"){
        mat_line_by_line_recent_races_tmp$Winner_Last_Race <- as.Date(mat_line_by_line_recent_races_tmp$Winner_Last_Race,origin="1970-01-01")
      }
      mat_line_by_line_recent_races <- rbind(mat_line_by_line_recent_races,mat_line_by_line_recent_races_tmp)
      print(i)
    }
  }
}

if(!is.null(mat_line_by_line_recent_races)){
  if(nrow(mat_line_by_line_recent_races)>0){
    mat_line_by_line_recent_races <- as.data.frame(mat_line_by_line_recent_races)
    mat_line_by_line_recent_races$Winner <- as.vector(mat_line_by_line_recent_races$Winner)
    mat_line_by_line_recent_races$Looser <- as.vector(mat_line_by_line_recent_races$Looser)
    df_historical_line_by_line_geny <- rbind(df_historical_line_by_line_geny,mat_line_by_line_recent_races)
    df_historical_line_by_line_geny <- df_historical_line_by_line_geny [df_historical_line_by_line_geny$Winner!=df_historical_line_by_line_geny$Looser,]
    df_historical_line_by_line_geny <- df_historical_line_by_line_geny %>% distinct()
    saveRDS(df_historical_line_by_line_geny,path_df_historical_line_by_line_geny)
  }
}
################################ Data Extraction and storage ############################
