######################################## Loading required Packages ########################################
require(readr)
require(dplyr)
require(plyr)
require(foreach)
require(doSNOW)
require(doParallel)
require(data.table)
require(magrittr)
require(stringr)
require(foreach)
require(parallel)
require(xlsx)
require(RColorBrewer)
require(readxl)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_perf <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/perfs"
################################### Setting Output Path ############################

################################ Getting list of files to process ############################
vec_perfs_files   <- dir(path_input_perf,pattern="^beting")
################################ Getting list of files to process ############################

################################ Compiling Performance data for all available configurations ############################
setwd(path_input_perf)
mat_performances_compiled <- ldply(vec_perfs_files, read_excel,sheet=1,.parallel=TRUE) %>%
  distinct() %>%
  as.data.frame()
mat_performances_compiled <- mat_performances_compiled
################################ Compiling Performance data for all available configurations ############################



