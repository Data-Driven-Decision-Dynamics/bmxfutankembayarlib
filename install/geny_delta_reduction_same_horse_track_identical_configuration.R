require(dplyr)
require(tidyr)
require(magrittr)

path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"
path_df_similar_config_per_track = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_similar_track_ultimate_complexity.rds"
number_days_back = 10000

message("Initialization of the final output")
df_score_delta_reduction_similar_tracks <- NULL
message("Initialization of the final output")

message("Reading historical performance file")
if(!exists("df_historical_races_geny")) {
  if(file.exists(path_df_historical_races_geny)){
    df_historical_races_geny <- readRDS(path_df_historical_races_geny)
  }
}
message("Reading historical performance file")

message("Fill missing details  on tracks")
if(nrow(df_historical_races_geny)>0){
  if(sum(df_historical_races_geny$Lieu=="Solvalla")>0){
    df_historical_races_geny[df_historical_races_geny$Lieu=="Solvalla","Terrain"]="Sable"
  }
  if(sum(df_historical_races_geny$Lieu=="Eskilstuna")>0){
    df_historical_races_geny[df_historical_races_geny$Lieu=="Eskilstuna","Terrain"]="Sable"
  }
  if(sum(df_historical_races_geny$Lieu=="Jägersro")>0){
    df_historical_races_geny[df_historical_races_geny$Lieu=="Jägersro","Terrain"]="Sable"
  }
}
message("Fill missing details  on tracks")

message("Extracting available ground found")
vec_discipline_found <- sort(unique(df_historical_races_geny$Discipline))
message("Extracting available ground found")

idx_races_trot_attele <- df_historical_races_geny$Discipline == "Trot Attelé" & !is.na(df_historical_races_geny$Ecart) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Depart) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
idx_races_trot_monte <- df_historical_races_geny$Discipline == "Trot Monté" & !is.na(df_historical_races_geny$Ecart) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
idx_races_galop <- df_historical_races_geny$Discipline %in% c("Plat","Haies","Steeplechase","Cross") & !is.na(df_historical_races_geny$Speed) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
df_historical_races_geny$Track <- NA
df_historical_races_geny$Reduction <- NA
df_historical_races_geny[idx_races_trot_attele,"Track"] <- paste(df_historical_races_geny[idx_races_trot_attele,"Lieu"], df_historical_races_geny[idx_races_trot_attele,"Discipline"],df_historical_races_geny[idx_races_trot_attele,"Terrain"],df_historical_races_geny[idx_races_trot_attele,"Corde"],df_historical_races_geny[idx_races_trot_attele,"Longueur"],df_historical_races_geny[idx_races_trot_attele,"Depart"],sep="_")
df_historical_races_geny[idx_races_trot_attele,"Reduction"] <- unlist(lapply(df_historical_races_geny[idx_races_trot_attele,"Ecart"],get_numeric_values_reduc_ecart_geny))
df_historical_races_geny[idx_races_trot_attele,"Speed"] <- 1000/df_historical_races_geny[idx_races_trot_attele,"Reduction"]
df_historical_races_geny[idx_races_trot_monte,"Track"] <- paste(df_historical_races_geny[idx_races_trot_monte,"Lieu"], df_historical_races_geny[idx_races_trot_monte,"Discipline"],df_historical_races_geny[idx_races_trot_monte,"Terrain"],df_historical_races_geny[idx_races_trot_monte,"Corde"],df_historical_races_geny[idx_races_trot_monte,"Longueur"],sep="_")
df_historical_races_geny[idx_races_trot_monte,"Reduction"] <- unlist(lapply(df_historical_races_geny[idx_races_trot_monte,"Ecart"],get_numeric_values_reduc_ecart_geny))
df_historical_races_geny[idx_races_trot_monte,"Speed"] <- 1000/df_historical_races_geny[idx_races_trot_monte,"Reduction"]
df_historical_races_geny[idx_races_galop,"Track"] <- paste(df_historical_races_geny[idx_races_galop,"Lieu"], df_historical_races_geny[idx_races_galop,"Discipline"],df_historical_races_geny[idx_races_galop,"Terrain"],df_historical_races_geny[idx_races_galop,"Corde"],df_historical_races_geny[idx_races_galop,"Longueur"],sep="_")
df_historical_races_focus_track <- df_historical_races_geny[!is.na(df_historical_races_geny$Track),]
vec_discipline_found <- unique(df_historical_races_focus_track$Discipline)
vec_freq_tracks <- table(df_historical_races_focus_track$Track)
df_historical_races_focus_track <- df_historical_races_focus_track[df_historical_races_focus_track$Track %in% names(vec_freq_tracks)[vec_freq_tracks>1], ]
vec_freq_horses <- table(df_historical_races_focus_track$Cheval)
df_historical_races_focus_track <- df_historical_races_focus_track[df_historical_races_focus_track$Cheval %in% names(vec_freq_horses)[vec_freq_horses>1],]

for(current_discipline in vec_discipline_found) 
{
  df_infos_focus <- df_historical_races_focus_track %>%
    filter(Discipline == current_discipline ) %>%
    filter(as.numeric(Sys.Date()-Date) <= number_days_back) %>%
    select(Cheval,Track,Speed) %>%
    pivot_wider(
      names_from = Track,
      values_from = Speed,
      values_fn = max
    ) %>%
    as.data.frame()
  if(nrow(df_infos_focus)>0){
    vec_track_found <- colnames(df_infos_focus)[-1]
    for(idx_track in 1:(length(vec_track_found)-1))
    {
      for(idy_track in (idx_track+1):length(vec_track_found) )
      {
        df_infos_focus_idx_idy <- df_infos_focus[,c("Cheval",vec_track_found[idx_track],vec_track_found[idy_track])]
        if(sum(complete.cases(df_infos_focus_idx_idy))>0){
          df_infos_focus_idx_idy <- df_infos_focus_idx_idy[complete.cases(df_infos_focus_idx_idy),]
          df_infos_focus_idx_idy <- df_infos_focus_idx_idy[is.finite(df_infos_focus_idx_idy[,3]),]
          df_infos_focus_idx_idy <- df_infos_focus_idx_idy[is.finite(df_infos_focus_idx_idy[,2]),]
          if(nrow(df_infos_focus_idx_idy)>=3){
            if(length(unlist(strsplit(vec_track_found[idx_track],"_")))==6){
              if(unlist(strsplit(vec_track_found[idx_track],"_"))[1]!=unlist(strsplit(vec_track_found[idy_track],"_"))[1]){
                if(unlist(strsplit(vec_track_found[idx_track],"_"))[2]==unlist(strsplit(vec_track_found[idy_track],"_"))[2]){
                  if(unlist(strsplit(vec_track_found[idx_track],"_"))[3]==unlist(strsplit(vec_track_found[idy_track],"_"))[3]){
                    if(unlist(strsplit(vec_track_found[idx_track],"_"))[4]==unlist(strsplit(vec_track_found[idy_track],"_"))[4]){
                      if(unlist(strsplit(vec_track_found[idx_track],"_"))[5]==unlist(strsplit(vec_track_found[idy_track],"_"))[5]){
                        if(unlist(strsplit(vec_track_found[idx_track],"_"))[6]==unlist(strsplit(vec_track_found[idy_track],"_"))[6]){
                          if(abs(as.numeric(unlist(strsplit(vec_track_found[idx_track],"_"))[5]) - as.numeric(unlist(strsplit(vec_track_found[idy_track],"_"))[5])) == 0){
                            res_t_test_tracks <- t.test(df_infos_focus_idx_idy[,2],df_infos_focus_idx_idy[,3],paired  = TRUE ,alternative = "two.sided")
                            df_score_delta_reduction_similar_tracks_current <- data.frame(Source = vec_track_found[idx_track], Target = vec_track_found[idy_track] , Significance = res_t_test_tracks$p.value, Statistic= res_t_test_tracks$estimate, Source_Reduction = median(df_infos_focus_idx_idy[,2]) , Target_Reduction = median(df_infos_focus_idx_idy[,3]), Size = nrow(df_infos_focus_idx_idy) , Discipline = current_discipline )
                            df_score_delta_reduction_similar_tracks_current$Detla_Reduction <- df_score_delta_reduction_similar_tracks_current$Source_Reduction-df_score_delta_reduction_similar_tracks_current$Target_Reduction
                            rownames(df_score_delta_reduction_similar_tracks_current) <- NULL
                            df_score_delta_reduction_similar_tracks <- rbind.fill(df_score_delta_reduction_similar_tracks,df_score_delta_reduction_similar_tracks_current)
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            
            if(length(unlist(strsplit(vec_track_found[idx_track],"_")))==5){
              if(unlist(strsplit(vec_track_found[idx_track],"_"))[1]!=unlist(strsplit(vec_track_found[idy_track],"_"))[1]){
                if(unlist(strsplit(vec_track_found[idx_track],"_"))[2]==unlist(strsplit(vec_track_found[idy_track],"_"))[2]){
                  if(unlist(strsplit(vec_track_found[idx_track],"_"))[3]==unlist(strsplit(vec_track_found[idy_track],"_"))[3]){
                    if(unlist(strsplit(vec_track_found[idx_track],"_"))[4]==unlist(strsplit(vec_track_found[idy_track],"_"))[4]){
                      if(unlist(strsplit(vec_track_found[idx_track],"_"))[5]==unlist(strsplit(vec_track_found[idy_track],"_"))[5]){
                        if(abs(as.numeric(unlist(strsplit(vec_track_found[idx_track],"_"))[5]) - as.numeric(unlist(strsplit(vec_track_found[idy_track],"_"))[5])) == 0){
                          res_t_test_tracks <- t.test(df_infos_focus_idx_idy[,2],df_infos_focus_idx_idy[,3],paired  = TRUE ,alternative = "two.sided")
                          df_score_delta_reduction_similar_tracks_current <- data.frame(Source = vec_track_found[idx_track], Target = vec_track_found[idy_track] , Significance = res_t_test_tracks$p.value, Statistic= res_t_test_tracks$estimate, Source_Reduction = median(df_infos_focus_idx_idy[,2]) , Target_Reduction = median(df_infos_focus_idx_idy[,3]), Size = nrow(df_infos_focus_idx_idy) , Discipline = current_discipline)
                          df_score_delta_reduction_similar_tracks_current$Detla_Reduction <- df_score_delta_reduction_similar_tracks_current$Source_Reduction-df_score_delta_reduction_similar_tracks_current$Target_Reduction
                          rownames(df_score_delta_reduction_similar_tracks_current) <- NULL
                          df_score_delta_reduction_similar_tracks <- rbind.fill(df_score_delta_reduction_similar_tracks,df_score_delta_reduction_similar_tracks_current)
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

df_score_aggregated_combined <- NULL
for(discipline in unique(df_score_delta_reduction_similar_tracks$Discipline))
{
  vec_track_found_afterwards<- unique(unlist(df_score_delta_reduction_similar_tracks[df_score_delta_reduction_similar_tracks$Discipline ==discipline,c("Source","Target")]))
  for(track in vec_track_found_afterwards)
  {
    df_score_delta_reduction_similar_tracks_current <- df_score_delta_reduction_similar_tracks[df_score_delta_reduction_similar_tracks$Source== track | df_score_delta_reduction_similar_tracks$Target==track,]
    val_normalizer_track <- sum(df_score_delta_reduction_similar_tracks_current$Size)
    val_score_track <- sum(df_score_delta_reduction_similar_tracks_current$Detla_Reduction*df_score_delta_reduction_similar_tracks_current$Size)/val_normalizer_track
    df_score_aggregated_track <- data.frame(Track = track , SCORE_LIFT_SIMILAR_TRACK = val_score_track, Discipline = discipline )
    df_score_aggregated_combined <- rbind(df_score_aggregated_combined,df_score_aggregated_track)
  }
}

saveRDS(df_score_aggregated_combined,path_df_similar_config_per_track)
# if(!is.null(df_score_delta_reduction_similar_tracks)){
#   if(nrow(df_score_delta_reduction_similar_tracks)>0){
#     for(idx in 1:nrow(df_score_delta_reduction_similar_tracks))
#     {
#       if(df_score_delta_reduction_similar_tracks[idx,"Significance"] <= 0.1 & df_score_delta_reduction_similar_tracks[idx,"Statistic"] >0){
#         df_score_delta_reduction_similar_tracks_formated_current <- data.frame(from = df_score_delta_reduction_similar_tracks[idx,"Source"], to = df_score_delta_reduction_similar_tracks[idx,"Target"],Discipline = df_score_delta_reduction_similar_tracks[idx,"Discipline"])
#         df_score_delta_reduction_similar_tracks_formated <- rbind.fill(df_score_delta_reduction_similar_tracks_formated,df_score_delta_reduction_similar_tracks_formated_current)
#       }
#       if(df_score_delta_reduction_similar_tracks[idx,"Significance"] <= 0.1 & df_score_delta_reduction_similar_tracks[idx,"Statistic"] < 0){
#         df_score_delta_reduction_similar_tracks_formated_current <- data.frame(from = df_score_delta_reduction_similar_tracks[idx,"Target"], to = df_score_delta_reduction_similar_tracks[idx,"Source"],Discipline = df_score_delta_reduction_similar_tracks[idx,"Discipline"])
#         df_score_delta_reduction_similar_tracks_formated <- rbind.fill(df_score_delta_reduction_similar_tracks_formated,df_score_delta_reduction_similar_tracks_formated_current)
#       }
#     }
#   }
# }
