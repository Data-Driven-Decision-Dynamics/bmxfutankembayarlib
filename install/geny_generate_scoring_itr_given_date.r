######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(plyr)
require(dplyr)
require(stringr)
require(curl)
require(rvest)
require(reshape2)
require(readr)
require(parallel)
require(foreach)
require(doSNOW)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_output_scoring = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
path_output_res_scoring = "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/output/itr"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Extract races current day ############################
url_path_current_date <- paste("http://www.geny.com/reunions-courses-pmu/_d",as.character(Sys.Date()),"?",sep="")
vec_races_current_date <- get_race_given_date_geny(url_path_date=url_path_current_date)
vec_races_current_date <- gsub('arrivee-et-rapports-pmu','partants-pmu',vec_races_current_date)
################################ Extract races current day ############################

################################ Extract races current day ############################
list_infos_races_current_date <- lapply(vec_races_current_date, get_details_race_geny)
names(list_infos_races_current_date) <- vec_races_current_date
mat_infos_races_current_date <- rbind_list(list_infos_races_current_date)
mat_infos_races_current_date <- mat_infos_races_current_date %>% distinct() %>% as.data.frame()
vec_race_identifier <- paste(mat_infos_races_current_date$Date,mat_infos_races_current_date$Heure,mat_infos_races_current_date$Lieu,mat_infos_races_current_date$Course,sep="-")
mat_infos_races_current_date <- transform(mat_infos_races_current_date,Race =vec_race_identifier)
vec_column_add <- c("Numero","Cheval","Age","Gender","Distance","Poids","Jockey","Driver" ,"Ferrage","Musique",grep("cot",colnames(mat_infos_races_current_date),ignore.case = TRUE,value=TRUE))
vec_column_add <- intersect(colnames(mat_infos_races_current_date),vec_column_add)
mat_infos_races_current_date_joint <- mat_infos_races_current_date[,vec_column_add]
vec_races_current_date <- as.vector(unique(mat_infos_races_current_date$Race))
################################ Extract races current day ############################

################################ Extract races current day ############################
for(current_race in vec_races_current_date)
{
  current_candidates <- subset(mat_infos_races_current_date,Race==current_race)[,"Cheval"]
  current_discipline <- as.vector(unique(subset(mat_infos_races_current_date,Race==current_race)[,"Discipline"]))
  current_discipline <- tolower(current_discipline)
  current_discipline <- gsub(" ","-",current_discipline)
  current_discipline <- gsub("é","e",current_discipline)
  current_discipline <- gsub("è","e",current_discipline)
  current_scoring_file <- grep(current_discipline,dir(path_output_scoring,pattern = "scoring"),value=TRUE)
  if(length(current_scoring_file)>0) {
    mat_compilation_scoring <- readRDS(paste(path_output_scoring,current_scoring_file,sep="/"))
    mat_compilation_scoring_current_race <- subset(mat_compilation_scoring,Cheval %in% current_candidates)
    if(nrow(mat_compilation_scoring_current_race)>1)
    {
      mat_compilation_scoring_current_race <- merge(mat_infos_races_current_date_joint,mat_compilation_scoring_current_race,by.x="Cheval",by.y="Cheval")
      mat_compilation_scoring_current_race$ID <- paste(mat_compilation_scoring_current_race$Year,mat_compilation_scoring_current_race$Cluster_Distance,sep="_")
      mat_compilation_scoring_current_race_melt <- reshape2::melt(mat_compilation_scoring_current_race, id.vars=c("Cheval", "ID"), measure.vars="SCORE_ITR",na.rm=TRUE)

      mat_compilation_scoring_current_race_format <- acast(mat_compilation_scoring_current_race_melt, Cheval ~ ID,function(x){mean(x,na.rm = TRUE)})
      mat_compilation_scoring_current_race_format <- as.data.frame(mat_compilation_scoring_current_race_format)
      mat_compilation_scoring_current_race_format$Cheval <- rownames(mat_compilation_scoring_current_race_format)
      mat_compilation_scoring_current_race_format <- mat_compilation_scoring_current_race_format[,c("Cheval",colnames(mat_compilation_scoring_current_race_format)[-ncol(mat_compilation_scoring_current_race_format)])]
      mat_compilation_scoring_current_race_format <- merge(mat_infos_races_current_date_joint,mat_compilation_scoring_current_race_format,by.x="Cheval",by.y="Cheval")
      setwd(path_output_res_scoring)
      file_name_output <- gsub("é","e",current_race)
      file_name_output <- gsub("è","e",file_name_output)
      file_name_output <- gsub(" ","-",file_name_output)
      file_name_output <- gsub("\\(","",file_name_output)
      file_name_output <- gsub("\\)","",file_name_output)
      file_name_output <- paste(file_name_output,"itr",sep="-")
      file_name_output <- paste(file_name_output,".csv",sep="")
      write.csv(mat_compilation_scoring_current_race_format,file=file_name_output)
      # xlsx::write.xlsx(mat_compilation_scoring_current_race_format,file=file_name_output)
    }
  }
}
################################ Extract races current day ############################
