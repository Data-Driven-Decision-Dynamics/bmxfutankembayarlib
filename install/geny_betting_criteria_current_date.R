######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(dplyr)
require(data.table)
require(magrittr)
require(rvest)
require(stringr)
require(curl)
require(WriteXLS)
require(xlsx)
require(igraph)
require(visNetwork)
require(readxl)
require(RColorBrewer)
require(plyr)
require(lubridate)
require(gtools)
require(reshape)
require(DT)
require(formattable)
require(htmlwidgets)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_output_data <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/output/indicators/"
path_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
path_input_ranking_data = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_line_by_line_rating.rds"
path_cluster_distance_range = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/input/mat_ranges_cluster_distance.rds"
################################### Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Extract races current day ############################
target_date <- as.character(Sys.Date())
url_path_current_date <- paste("http://www.geny.com/reunions-courses-pmu/_d",target_date,"?",sep="")
vec_races_current_date <- rev(get_race_current_date_geny(url_path_date=url_path_current_date))
vec_races_current_date <- vec_races_current_date[grep("le-bouscat",vec_races_current_date)]
################################ Extract races current day ############################

################################ Reading Distance Cluster Information ############################
mat_cluster_distance_range <- readRDS(path_cluster_distance_range)
################################ Reading Distance Cluster Information ############################

################################ Details races current day ############################
mat_infos_races <- NULL
for(i in 1:length(vec_races_current_date))
  {
  infos_race_current_try <- tryCatch(get_details_race_geny(vec_races_current_date[i]),error = function(e) {print(paste("Some issues with", vec_races_current_date[i]));NULL})
  if(!is.null(infos_race_current_try)) {
    mat_infos_races_current <- infos_race_current_try
    mat_infos_races <- rbind.fill(mat_infos_races,mat_infos_races_current)
  } else {
    mat_infos_races_current <- NULL
    mat_infos_races <- rbind.fill(mat_infos_races,mat_infos_races_current)
  }
}

if(length(intersect("Cluster_Distance",colnames(mat_infos_races)))==0) {
  mat_infos_races$Cluster_Distance <-NULL
  for(i in 1:nrow(mat_infos_races))
  {
    mat_cluster_distance_range_discipline <- mat_cluster_distance_range[mat_cluster_distance_range$Discipline==as.vector(unique(mat_infos_races[i,"Discipline"])),]
    idx_cluster_distance_group <- mat_cluster_distance_range_discipline$Lower <= unlist(mat_infos_races[i,"Distance"]) & mat_cluster_distance_range_discipline$Upper >= unlist(mat_infos_races[i,"Distance"])
    mat_infos_races[i,"Cluster_Distance"] <- paste(mat_cluster_distance_range_discipline[idx_cluster_distance_group,c("Lower","Upper")],collapse = "-")
    print(i)
  }
}

mat_infos_races <- transform(mat_infos_races,Class=paste(mat_infos_races$Lieu,mat_infos_races$Discipline,mat_infos_races$Cluster_Distance,sep="-"))
vec_hyppodromes <- as.vector(unique(mat_infos_races$Lieu))
################################ Details races current day ############################

################################ Setting output directory ############################
setwd(path_output_data)
dir.create(target_date)
setwd(paste(path_output_data,target_date,sep="/"))
for(hyppo in vec_hyppodromes)
{
  dir.create(str_trim(unlist(strsplit(hyppo,"\\["))[1]))
}
################################ Setting output directory ############################

################################ Loading useful data ################################
print("Loading line by line analysis matrix")
if(!exists("mat_line_by_line")) {
  mat_line_by_line <- readRDS(path_input_ranking_data)
  mat_line_by_line$Date <- unlist(lapply(mat_line_by_line$Race,function(x){substr(x,1,10)}))
  mat_line_by_line$Date <- as.Date(mat_line_by_line$Date)
  mat_line_by_line <- mat_line_by_line[nchar(as.character(mat_line_by_line$Date))==10,]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Date),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Discipline),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Looser),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Looser_Distance),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Winner),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Winner_Distance),] 
}
print("Line by line analysis matrix loaded")

print("Loading historical performance matrix")

if(!exists("mat_historical_races")) {
  mat_historical_races <- readRDS(path_historical_races)
  vec_date_rebuilt <- unlist(lapply(mat_historical_races$Race,function(x){return(substr(x,1,10))}))
  mat_historical_races$Date <- vec_date_rebuilt
  mat_historical_races$Date <- as.Date(mat_historical_races$Date)
  mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
  mat_historical_races$Race <- as.vector(mat_historical_races$Race)
  mat_historical_races$Prix <- as.numeric(mat_historical_races$Prix)
  mat_historical_races$Gains <- as.numeric(mat_historical_races$Gains)
}
# print("Historical performance matrix loaded")
################################ Loading useful data ################################

################################ Calculate the features ############################
for(i in 1:length(vec_races_current_date)) {
  setwd(paste(path_output_data,target_date,sep="/"))
  mat_infos_target_race <- get_details_race_geny(vec_races_current_date[i])
  setwd(paste(getwd(),str_trim(unlist(strsplit(unique(mat_infos_target_race$Lieu),"\\["))[1]),sep="/"))
  id_race <- gsub('_.*',"",unlist(strsplit(vec_races_current_date[i],"pmu-"))[2])
  dir.create(id_race)
  setwd(paste(getwd(),id_race,sep="/"))
  list_infos_willingness_win <- get_horse_willingness_win_fitness_geny(vec_races_current_date[i])
  table_viz_willingness_win  <- list_infos_willingness_win$graph
  name_viz_willingness_win   <- list_infos_willingness_win$graph_label
  
  if(!is.null(table_viz_willingness_win)) {
    saveWidget(table_viz_willingness_win,paste(name_viz_willingness_win,".html",sep=""))
  }
  
  print(i)
}
