.libPaths(new = c(.libPaths(),"/home/futankembayar/R/x86_64-pc-linux-gnu-library/4.1"))
require(bmxFutankeMbayar)
require(curl)
require(plyr)
require(dplyr)

path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"
message("Reading historical performance file")
if(!exists("df_historical_races_geny")) {
  if(file.exists(path_df_historical_races_geny)){
    df_historical_races_geny <- readRDS(path_df_historical_races_geny)
  }
}
message("Reading historical performance file")

get_odds_evolution_races_current_date()