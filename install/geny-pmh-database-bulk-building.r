################################ Loading required packages ################################
require(bmxFutankeMbayar)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(doMC)
require(data.table)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_output_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmh/output/raw/historical_performances"
################################ Setting Output Path ############################

################################ Generate and store data ############################
seq_date <- seq(as.Date("2015-01-01"), Sys.Date()-1, by = "day")
vec_url_date_scrape   <- paste("http://www.geny.com/reunions-pmh?date=",seq_date,sep="")
vec_url_date_scrape <- rev(vec_url_date_scrape)
for(i in 1:length(vec_url_date_scrape))
{
  vec_race_current_date <- get_pmh_race_given_date_geny(vec_url_date_scrape[i])
  if(length(vec_race_current_date)>0){
    for(j in 1:length(vec_race_current_date)) {
      if(vec_race_current_date[j] != "http://www.geny.com"){
        mat_infos_race <- try(get_pmh_performances_details_race_geny(vec_race_current_date[j]))
        if(class(mat_infos_race)!= "try-error"){
          if(nrow(mat_infos_race)>0){
            mat_infos_race$Race <- paste(mat_infos_race$Date,mat_infos_race$Heure,mat_infos_race$Course,sep="_")
            setwd(path_output_data)
            file_name_output <- gsub("http://www.geny.com/arrivee-et-rapports-pmh/","infos-races-",vec_race_current_date[j])
            file_name_output <- paste(paste(i,j,sep ="-"),file_name_output,sep="_")
            file_name_output <- paste(file_name_output,".csv",sep="")
            file_name_output <- paste(path_output_data,file_name_output,sep="/")
            write.csv(mat_infos_race,file=file_name_output,row.names = FALSE)
          }
        }
      }
    }
  }
}
################################ Generate and store data ############################