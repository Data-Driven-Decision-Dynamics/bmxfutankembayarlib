path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"
path_df_standardized_times_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_standardized_times_geny.rds"

df_infos_time_combined_compiled <- NULL

message("Reading historical performance file")
if(!exists("df_historical_races_geny")) {
  if(file.exists(path_df_historical_races_geny)){
    df_historical_races_geny <- readRDS(path_df_historical_races_geny)
  }
}
message("Reading historical performance file")

message("Computing standard times per track and level")
vec_available_discipline <- unique(df_historical_races_geny$Discipline)
for(current_discipline in vec_available_discipline)
{
  if(current_discipline == "Trot Attelé" ){
    df_infos_time_combined <- df_historical_races_geny %>%
      filter(Discipline %in% current_discipline) %>%
      select(Cheval,Lieu,Longueur,Depart,Corde,Terrain,Ecart,Place,Date,Ferrage) %>%
      tidyr::drop_na() %>%
      as.data.frame()
    df_infos_time_combined$Reduction <- unlist(lapply(df_infos_time_combined$Ecart,get_numeric_values_reduc_ecart_geny))
    df_infos_time_combined$Time <- df_infos_time_combined$Reduction * (df_infos_time_combined$Longueur/1000)
    df_infos_time_combined <- df_infos_time_combined %>% 
      mutate(Track = paste(Lieu,Terrain,Longueur,Depart,sep="_")) %>%
      as.data.frame()
    df_record_track <- df_infos_time_combined %>%
      group_by(Track) %>%
      dplyr::summarise(Record = min(Time)) %>%
      as.data.frame()
    vec_tracks_found <- unique(df_record_track$Track)
    df_infos_delta_times <- as.data.frame(matrix(NA,ncol=3,nrow=length(vec_tracks_found)))
    colnames(df_infos_delta_times) <- c("Track","Size","Delta")
    df_infos_delta_times$Track <- vec_tracks_found
    rownames(df_infos_delta_times) <- NULL
    for(track in vec_tracks_found)
    {
      df_infos_times_track <- df_infos_time_combined[df_infos_time_combined$Track == track,]
      vec_times_values_track <- unique(df_infos_times_track$Time)
      vec_times_values_track <- sort(vec_times_values_track)
      df_infos_delta_times[df_infos_delta_times$Track == track ,"Size"] <- nrow(df_infos_times_track)
      df_infos_delta_times[df_infos_delta_times$Track == track ,"Delta"] <- median(diff(vec_times_values_track))
    }
    df_infos_time_combined <- merge(df_infos_time_combined,df_infos_delta_times,by.x="Track",by.y="Track",all.x = TRUE )
    df_infos_time_combined <- merge(df_infos_time_combined,df_record_track,by.x="Track",by.y="Track",all.x = TRUE )
    df_infos_time_combined$Gap <- df_infos_time_combined$Time-df_infos_time_combined$Record
    df_infos_time_combined$Ratio <- df_infos_time_combined$Gap / df_infos_time_combined$Delta
    df_infos_time_combined$Standardized <- 100 - df_infos_time_combined$Ratio
    df_infos_time_combined$Discipline = current_discipline
  } 
  
  if(current_discipline == "Trot Monté" ){
    df_infos_time_combined <- df_historical_races_geny %>%
      filter(Discipline %in% current_discipline) %>%
      select(Cheval,Lieu,Longueur,Corde,Terrain,Ecart,Place,Date,Ferrage) %>%
      tidyr::drop_na() %>%
      as.data.frame()
    df_infos_time_combined$Reduction <- unlist(lapply(df_infos_time_combined$Ecart,get_numeric_values_reduc_ecart_geny))
    df_infos_time_combined$Time <- df_infos_time_combined$Reduction * (df_infos_time_combined$Longueur/1000)
    df_infos_time_combined <- df_infos_time_combined %>% 
      mutate(Track = paste(Lieu,Terrain,Longueur,sep="_")) %>%
      as.data.frame()
    df_record_track <- df_infos_time_combined %>%
      group_by(Track) %>%
      dplyr::summarise(Record = min(Time)) %>%
      as.data.frame()
    vec_tracks_found <- unique(df_record_track$Track)
    df_infos_delta_times <- as.data.frame(matrix(NA,ncol=3,nrow=length(vec_tracks_found)))
    colnames(df_infos_delta_times) <- c("Track","Size","Delta")
    df_infos_delta_times$Track <- vec_tracks_found
    rownames(df_infos_delta_times) <- NULL
    for(track in vec_tracks_found)
    {
      df_infos_times_track <- df_infos_time_combined[df_infos_time_combined$Track == track,]
      vec_times_values_track <- unique(df_infos_times_track$Time)
      vec_times_values_track <- sort(vec_times_values_track)
      df_infos_delta_times[df_infos_delta_times$Track == track ,"Size"] <- nrow(df_infos_times_track)
      df_infos_delta_times[df_infos_delta_times$Track == track ,"Delta"] <- median(diff(vec_times_values_track))
    }
    df_infos_time_combined <- merge(df_infos_time_combined,df_infos_delta_times,by.x="Track",by.y="Track",all.x = TRUE )
    df_infos_time_combined <- merge(df_infos_time_combined,df_record_track,by.x="Track",by.y="Track",all.x = TRUE )
    df_infos_time_combined$Gap <- df_infos_time_combined$Time-df_infos_time_combined$Record
    df_infos_time_combined$Ratio <- df_infos_time_combined$Gap / df_infos_time_combined$Delta
    df_infos_time_combined$Standardized <- 100 - df_infos_time_combined$Ratio
    df_infos_time_combined$Discipline = current_discipline
  }
  
  if(current_discipline %in% c( "Plat","Haies","Steeplechase","Cross" ) ){
    df_infos_time_combined <- df_historical_races_geny %>%
      filter(Discipline %in% current_discipline) %>%
      select(Cheval,Lieu,Longueur,Corde,Terrain,Time,Place,Date,Ferrage) %>%
      tidyr::drop_na() %>%
      as.data.frame()
    df_infos_time_combined <- df_infos_time_combined %>% 
      mutate(Track = paste(Lieu,Terrain,Longueur,sep="_")) %>%
      as.data.frame()
    df_record_track <- df_infos_time_combined %>%
      group_by(Track) %>%
      dplyr::summarise(Record = min(Time)) %>%
      as.data.frame()
    vec_tracks_found <- unique(df_record_track$Track)
    df_infos_delta_times <- as.data.frame(matrix(NA,ncol=3,nrow=length(vec_tracks_found)))
    colnames(df_infos_delta_times) <- c("Track","Size","Delta")
    df_infos_delta_times$Track <- vec_tracks_found
    rownames(df_infos_delta_times) <- NULL
    for(track in vec_tracks_found)
    {
      df_infos_times_track <- df_infos_time_combined[df_infos_time_combined$Track == track,]
      vec_times_values_track <- unique(df_infos_times_track$Time)
      vec_times_values_track <- sort(vec_times_values_track)
      df_infos_delta_times[df_infos_delta_times$Track == track ,"Size"] <- nrow(df_infos_times_track)
      df_infos_delta_times[df_infos_delta_times$Track == track ,"Delta"] <- median(diff(vec_times_values_track))
    }
    df_infos_time_combined <- merge(df_infos_time_combined,df_infos_delta_times,by.x="Track",by.y="Track",all.x = TRUE )
    df_infos_time_combined <- merge(df_infos_time_combined,df_record_track,by.x="Track",by.y="Track",all.x = TRUE )
    df_infos_time_combined$Gap <- df_infos_time_combined$Time-df_infos_time_combined$Record
    df_infos_time_combined$Ratio <- df_infos_time_combined$Gap / df_infos_time_combined$Delta
    df_infos_time_combined$Standardized <- 100 - df_infos_time_combined$Ratio
    df_infos_time_combined$Discipline = current_discipline
  }
  df_infos_time_combined_compiled <- rbind.fill(df_infos_time_combined,df_infos_time_combined_compiled)
}
message("Computing standard times per track and level")

message("Saving standardized times")
if(!is.null(df_infos_time_combined_compiled)){
  if(nrow(df_infos_time_combined_compiled)>0){
    saveRDS(df_infos_time_combined_compiled,path_df_standardized_times_geny)
  }
}
message("Saving standardized times")
