path_output_data="/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/output/01-raw/models"
vec_races_already_processed <- dir(path_output_data)

path_mat_historical_line_by_line_canalturf = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/output/02-processed/mat_line_by_line_rating_canalturf.rds"
path_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/output/02-processed/mat_historical_races_canalturf.rds"
path_input_training_data <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/train"

if(!exists("mat_historical_races_canalturf")) {
  mat_historical_races_canalturf <- readRDS(path_historical_races)
  if(sum(is.na(mat_historical_races_canalturf$Place))>0) {
    mat_historical_races_canalturf$Place[is.na(mat_historical_races_canalturf$Place)] <- 99
  }
}

target_hippodrome <-  names(sort(table(mat_historical_races_canalturf$Lieu),decreasing = TRUE))
target_hippodrome <- unique(mat_historical_races_canalturf[mat_historical_races_canalturf$Discipline=="PLAT","Lieu"])
target_hippodrome <- "happy-valley-hong-kong"
target_discipline=c("PLAT")
target_min_date=as.Date("2016-01-01")
target_min_num_horses=10

mat_historical_races_canalturf_focus <- mat_historical_races_canalturf[mat_historical_races_canalturf$Lieu %in% target_hippodrome & mat_historical_races_canalturf$Discipline %in%  target_discipline & mat_historical_races_canalturf$Date >=target_min_date,]
mat_historical_races_canalturf_focus <- mat_historical_races_canalturf_focus[mat_historical_races_canalturf_focus$Status %in% c("Finish","Disqualifie"),]
vec_number_horses_races <- table(mat_historical_races_canalturf_focus$Race)
vec_candidates_races <- names(vec_number_horses_races) [vec_number_horses_races>target_min_num_horses]
vec_missing_races <- rev(setdiff(vec_candidates_races,vec_races_already_processed))

mat_infos_target_race = list_historical_races_letrot_focus[[vec_missing_races[1]]]


host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- parallel::makeCluster(ceiling(host_nb_cores*0.75), outfile="")
registerDoMC(cores=ceiling(host_nb_cores*0.75))

if(!exists("mat_line_by_line_canalturf")) {
  mat_line_by_line_canalturf <- readRDS(path_mat_historical_line_by_line_canalturf)
}

# if(length(vec_races_already_processed)>0) {
#   mat_historical_races_canalturf_focus <- mat_historical_races_canalturf[mat_historical_races_canalturf$Race %in% setdiff(mat_historical_races_canalturf$Race,vec_races_already_processed),]
# }

mat_historical_races_canalturf_focus <- mat_historical_races_canalturf[mat_historical_races_canalturf$Lieu %in% target_hippodrome & mat_historical_races_canalturf$Discipline %in%  target_discipline & mat_historical_races_canalturf$Date >=target_min_date,]
mat_historical_races_canalturf_focus <- mat_historical_races_canalturf_focus[mat_historical_races_canalturf_focus$Status %in% c("Finish","Disqualifie"),]
vec_number_horses_races <- table(mat_historical_races_canalturf_focus$Race)
vec_candidates_races <- names(vec_number_horses_races) [vec_number_horses_races>target_min_num_horses]
vec_candidates_races <- rev(setdiff(vec_candidates_races,vec_races_already_processed))
mat_historical_races_canalturf_focus <- mat_historical_races_canalturf_focus [mat_historical_races_canalturf_focus$Race %in% vec_candidates_races, ]
list_historical_races_letrot_focus <- split.data.frame(mat_historical_races_canalturf_focus,mat_historical_races_canalturf_focus$Race)

foreach(i=1:length(names(list_historical_races_letrot_focus)),.errorhandling='pass') %dopar% {
  require(doSNOW)
  require(doMC)
  require(doParallel)
  require(foreach)
  require(parallel)
  require(bmxFutankeMbayar)
  require(magrittr)
  require(dplyr)
  require(tidyr)
  require(stringr)
  if(!file.exists(paste(path_output_data,gsub("\n","",gsub("\\/"," ",gsub("\\("," ",names(list_historical_races_letrot_focus)[i]))),sep="/"))){
    df_predictive_features <- get_processed_numerical_data_predictive_model_canalturf(list_historical_races_letrot_focus[[i]])
  }
}
