######################################## Loading required Packages ########################################
require(pcaMethods)
require(caTools)
require(OneR)
require(bmxFutankeMbayar)
######################################## Loading required Packages ########################################

###################################################### Setting Output Path ##################################################
path_input_compiled_feature_data <- "/mnt/Master-Data/Futanke-Mbayar/France/models/le-trot/input/all/mat_training_consolided_compiled.rds"
path_input_feature_infos <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/input/Master-High-Level-Feature-Description-Futanke-Mbayar_Letrot.xlsx"
######################################################### Setting Output Path ##################################################

######################################################### Reading Data  ##################################################
mat_features_infos <- readxl::read_excel(path_input_feature_infos,sheet = 1)
mat_features_infos <- as.data.frame(mat_features_infos)
mat_features_consolidated <- readRDS(path_input_compiled_feature_data)
mat_features_consolidated$Class <- ifelse(mat_features_consolidated$Place<=7,"High","Low")
list_features_consolidated_per_model <- split.data.frame(mat_features_consolidated,mat_features_consolidated$Model)
mat_features_association_target <- NULL
for(model in rev(names(list_features_consolidated_per_model))) {
  mat_features_values <- mat_features_consolidated[mat_features_consolidated$Model == model,]
  mat_features_association_target_current <- get_feature_association_target(df_features_infos = mat_features_infos,df_features_values=mat_features_values)
  mat_features_association_target <- rbind(mat_features_association_target,mat_features_association_target_current)
}
# list_features_association_target <- lapply(list_features_consolidated_per_model,get_feature_association_target)
# mat_features_association_target <- bind_rows(list_features_association_target)
# df_features_infos  <- mat_features_infos
######################################################### Reading Data  ##################################################
