######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
require(doMC)
require(PlayerRatings)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_line_by_line_output  =  "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/pmh/output/raw/pairwise_comparisons"
path_mat_historical_races = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/pmh/output/processed/historical_performances/mat_historical_races_pmh_geny.rds"
################################ Setting Output Path ############################

################################ Data Extraction and storage ############################
mat_historical_races <- readRDS(path_mat_historical_races)
vec_discipline <- as.vector(unique(mat_historical_races$Discipline))
vec_discipline <- vec_discipline[!is.na(vec_discipline)]
# vec_discipline <- setdiff(vec_discipline,c("Plat"))
# vec_discipline <- setdiff(vec_discipline,c("Plat","Trot Attelé"))
# vec_discipline <- c("Plat","Trot Attelé","Trot Monté","Haies","Steeplechase","Cross") 
# vec_discipline <- setdiff(vec_discipline,c("Trot Monté","Plat","Trot Attelé"))
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
for(discipline in vec_discipline)
{
  mat_historical_races_focus <- subset(mat_historical_races,Discipline==discipline)
  mat_historical_races_focus <- mat_historical_races_focus[mat_historical_races_focus$Date>=as.Date("2018-01-01"),]
  vec_races <- as.vector(unique(mat_historical_races_focus$Race))
  vec_races <- vec_races[!is.na(vec_races)]
  print(paste("Starting the computation of line by line results for :",discipline))
  for(i in 1:length(vec_races)){
      mat_historical_races_current <- unique(mat_historical_races_focus[mat_historical_races_focus$Race==vec_races[i],])
      if(nrow(mat_historical_races_current)>1) {
        get_line_by_line_driver_jockey_race_geny (df_perf_race_complete=mat_historical_races_current,target_item="Horse",path_line_by_line_output =path_line_by_line_output)
      }
  }
  print(paste("Finished the computation of line by line results for :",discipline))
}
################################ Generate and store data ############################
