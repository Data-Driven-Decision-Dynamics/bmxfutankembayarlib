path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"
path_impact_distance_per_track = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_margial_distance_per_track_ultimate_complexity.rds"
number_days_back = 366

message("Initialization of the final output")
df_mean_diff_test_same_tracks_different_distance_compiled <- NULL
df_mean_diff_test_same_tracks_different_distance_compiled_formated <- NULL
message("Initialization of the final output")

message("Reading historical performance file")
if(!exists("df_historical_races_geny")) {
  if(file.exists(path_df_historical_races_geny)){
    df_historical_races_geny <- readRDS(path_df_historical_races_geny)
  }
}
message("Reading historical performance file")

message("Extracting available ground found")
vec_hippodrome_found <- sort(unique(df_historical_races_geny$Lieu))
message("Extracting available ground found")


for(hippodrome in vec_hippodrome_found)
{
  df_historical_races_geny_hippodrome <- df_historical_races_geny[df_historical_races_geny$Lieu == hippodrome,]
  vec_found_discipline_hippodrome <- unique(df_historical_races_geny_hippodrome$Discipline)
  for(discipline in vec_found_discipline_hippodrome)
  {
    if(discipline == "Trot Attelé")
    {
      df_historical_races_geny_hippodrome_discipline <- df_historical_races_geny_hippodrome[df_historical_races_geny_hippodrome$Discipline == discipline , ]
      vec_found_discipline_hippodrome_depart <- unique(df_historical_races_geny_hippodrome_discipline$Depart)
      if(sum(is.na(vec_found_discipline_hippodrome_depart))>0){
        vec_found_discipline_hippodrome_depart <- vec_found_discipline_hippodrome_depart[!is.na(vec_found_discipline_hippodrome_depart)]
      }
      for(depart in vec_found_discipline_hippodrome_depart)
      {
        df_historical_races_geny_hippodrome_discipline_depart <- df_historical_races_geny_hippodrome_discipline[df_historical_races_geny_hippodrome_discipline$Depart == depart , ]
        vec_found_discipline_hippodrome_depart_longueur <- sort(unique(df_historical_races_geny_hippodrome_discipline$Longueur))
        if(sum(is.na(vec_found_discipline_hippodrome_depart_longueur))>0){
          vec_found_discipline_hippodrome_depart_longueur <- vec_found_discipline_hippodrome_depart_longueur[!is.na(vec_found_discipline_hippodrome_depart_longueur)]
        }
        
        if(length(vec_found_discipline_hippodrome_depart_longueur)>1){
          df_historical_races_geny_hippodrome_discipline_depart_longueur <- df_historical_races_geny_hippodrome_discipline_depart %>%
            filter(as.numeric(Sys.Date()-Date) <= number_days_back) %>%
            select(Cheval,Longueur,Speed) %>%
            drop_na() %>%
            pivot_wider(
              names_from = Longueur,
              values_from = Speed,
              values_fn = max
            ) %>%
            as.data.frame()
          
          if(nrow(df_historical_races_geny_hippodrome_discipline_depart_longueur)>0){
            for(idx in 1:(length(vec_found_discipline_hippodrome_depart_longueur)-1))
            {
              for(idy in (idx+1):length(vec_found_discipline_hippodrome_depart_longueur))
              {
                if(vec_found_discipline_hippodrome_depart_longueur[idx] %in% colnames(df_historical_races_geny_hippodrome_discipline_depart_longueur) & vec_found_discipline_hippodrome_depart_longueur[idy] %in% colnames(df_historical_races_geny_hippodrome_discipline_depart_longueur)){
                  
                  df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy <- df_historical_races_geny_hippodrome_discipline_depart_longueur[,c("Cheval",vec_found_discipline_hippodrome_depart_longueur[idx],vec_found_discipline_hippodrome_depart_longueur[idy])]
                  if(sum(complete.cases(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy))>4){
                    df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy <- df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[complete.cases(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy),]
                    res_t_test_hippodrome_longueur <- t.test(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[,2],df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[,3],paired  = TRUE ,alternative = "two.sided")
                    df_mean_diff_test_same_tracks_different_distance_compiled_current <- data.frame(Hippodrome=hippodrome, Discipline = discipline , Depart = depart, Source = vec_found_discipline_hippodrome_depart_longueur[idx], Target = vec_found_discipline_hippodrome_depart_longueur[idy] , Significance = res_t_test_hippodrome_longueur$p.value, Statistic= res_t_test_hippodrome_longueur$estimate, Source_Reduction = median(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[,2]) , Target_Reduction = median(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[,3]), Size = nrow(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy))
                    df_mean_diff_test_same_tracks_different_distance_compiled_current$Detla_Reduction <- df_mean_diff_test_same_tracks_different_distance_compiled_current$Source_Reduction-df_mean_diff_test_same_tracks_different_distance_compiled_current$Target_Reduction
                    rownames(df_mean_diff_test_same_tracks_different_distance_compiled_current) <- NULL
                    df_mean_diff_test_same_tracks_different_distance_compiled <- rbind.fill(df_mean_diff_test_same_tracks_different_distance_compiled,df_mean_diff_test_same_tracks_different_distance_compiled_current)
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
 
df_mean_diff_test_same_tracks_different_distance_compiled$Delta_Distance <- df_mean_diff_test_same_tracks_different_distance_compiled$Source-df_mean_diff_test_same_tracks_different_distance_compiled$Target

idx_races_trot_attele <- df_historical_races_geny$Discipline == "Trot Attelé" & !is.na(df_historical_races_geny$Ecart) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Depart) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
idx_races_trot_monte <- df_historical_races_geny$Discipline == "Trot Monté" & !is.na(df_historical_races_geny$Ecart) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
idx_races_galop <- df_historical_races_geny$Discipline %in% c("Plat","Haies","Steeplechase","Cross") & !is.na(df_historical_races_geny$Speed) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
df_historical_races_geny$Track <- NA
df_historical_races_geny[idx_races_trot_attele,"Track"] <- paste(df_historical_races_geny[idx_races_trot_attele,"Lieu"], df_historical_races_geny[idx_races_trot_attele,"Discipline"],df_historical_races_geny[idx_races_trot_attele,"Terrain"],df_historical_races_geny[idx_races_trot_attele,"Corde"],df_historical_races_geny[idx_races_trot_attele,"Longueur"],df_historical_races_geny[idx_races_trot_attele,"Depart"],sep="_")
df_historical_races_geny[idx_races_trot_monte,"Track"] <- paste(df_historical_races_geny[idx_races_trot_monte,"Lieu"], df_historical_races_geny[idx_races_trot_monte,"Discipline"],df_historical_races_geny[idx_races_trot_monte,"Terrain"],df_historical_races_geny[idx_races_trot_monte,"Corde"],df_historical_races_geny[idx_races_trot_monte,"Longueur"],sep="_")
df_historical_races_geny[idx_races_galop,"Track"] <- paste(df_historical_races_geny[idx_races_galop,"Lieu"],df_historical_races_geny[idx_races_galop,"Discipline"], df_historical_races_geny[idx_races_galop,"Terrain"],df_historical_races_geny[idx_races_galop,"Corde"],df_historical_races_geny[idx_races_galop,"Longueur"],sep="_")
vec_track_posteriori <- unique(df_historical_races_geny$Track)
if(sum(is.na(vec_track_posteriori))>0){
  vec_track_posteriori <- vec_track_posteriori[!is.na(vec_track_posteriori)]
}
df_stats_margial_distance_impact <- data.frame(Track = vec_track_posteriori , SCORE_MARGINAL_DISTANCE_IMPACT = NA )

for(track in vec_track_posteriori)
{
  val_location <- unlist(strsplit(track,"_"))[1]
  val_discipline <- unlist(strsplit(track,"_"))[2]
  val_longueur <- unlist(strsplit(track,"_"))[5]
  df_mean_diff_test_same_tracks_different_distance_compiled_focus <- df_mean_diff_test_same_tracks_different_distance_compiled %>%
    filter(Size>=10) %>%
    filter(Hippodrome==val_location) %>%
    filter(Discipline==val_discipline) %>%
    as.data.frame()
  if(nrow(df_mean_diff_test_same_tracks_different_distance_compiled_focus)>0){
    if(sum(df_mean_diff_test_same_tracks_different_distance_compiled_focus$Source==val_longueur | df_mean_diff_test_same_tracks_different_distance_compiled_focus$Target==val_longueur)>0){
      df_mean_diff_test_same_tracks_different_distance_compiled_focus<- df_mean_diff_test_same_tracks_different_distance_compiled_focus[df_mean_diff_test_same_tracks_different_distance_compiled_focus$Source==val_longueur | df_mean_diff_test_same_tracks_different_distance_compiled_focus$Target==val_longueur,]
      df_mean_diff_test_same_tracks_different_distance_compiled_focus <- df_mean_diff_test_same_tracks_different_distance_compiled_focus[order(df_mean_diff_test_same_tracks_different_distance_compiled_focus$Size,decreasing = TRUE),]
      df_stats_margial_distance_impact[df_stats_margial_distance_impact$Track==track,"SCORE_MARGINAL_DISTANCE_IMPACT"] <- df_mean_diff_test_same_tracks_different_distance_compiled_focus[1,"Detla_Reduction"]/abs(df_mean_diff_test_same_tracks_different_distance_compiled_focus[1,"Delta_Distance"])
    }
  }
}
df_stats_margial_distance_impact <- df_stats_margial_distance_impact[complete.cases(df_stats_margial_distance_impact),]
saveRDS(df_stats_margial_distance_impact,path_impact_distance_per_track)

# df_turn_ground_tracks <- unique(df_historical_races_geny[,c("Lieu","Corde","Terrain","Discipline","Longueur")])
#   df_turn_ground_tracks <- df_turn_ground_tracks[complete.cases(df_turn_ground_tracks),]
#   
#   df_mean_diff_test_same_tracks_different_distance_compiled$Turn = NA
#   df_mean_diff_test_same_tracks_different_distance_compiled$Ground = NA
#   df_mean_diff_test_same_tracks_different_distance_compiled$Delta_Distance <- df_mean_diff_test_same_tracks_different_distance_compiled$Source-df_mean_diff_test_same_tracks_different_distance_compiled$Target
#   for(idx in 1:nrow(df_mean_diff_test_same_tracks_different_distance_compiled))
#   {
#     val_discipline_idx <- df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Discipline"]
#     val_location_idx <- df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Hippodrome"]
#     val_start_idx <- df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Depart"]
#     val_longueur_idx <- df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Source"]
#     val_found_turn <- df_turn_ground_tracks %>%
#       filter(Lieu==val_location_idx)%>%
#       filter(Discipline==val_discipline_idx)%>%
#       filter(Longueur==val_longueur_idx)%>%
#       select(Corde)%>%
#       as.vector()%>%
#       unlist()
#     val_found_ground <- df_turn_ground_tracks %>%
#       filter(Lieu==val_location_idx)%>%
#       filter(Discipline==val_discipline_idx)%>%
#       filter(Longueur==val_longueur_idx)%>%
#       select(Terrain)%>%
#       as.vector()%>%
#       unlist()
#     df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Turn"]<-val_found_turn[1]
#     df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Ground"]<-val_found_ground[1]
#   }
#   
#   
#   
#   
#   
#   
#   
#   
#   
#   
#   
#   
#   
#   
#   
#   
#   idx_races_trot_attele <- df_historical_races_geny$Discipline == "Trot Attelé" & !is.na(df_historical_races_geny$Ecart) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Depart) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
#   idx_races_trot_monte <- df_historical_races_geny$Discipline == "Trot Monté" & !is.na(df_historical_races_geny$Ecart) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
#   idx_races_galop <- df_historical_races_geny$Discipline %in% c("Plat","Haies","Steeplechase","Cross") & !is.na(df_historical_races_geny$Speed) & !is.na(df_historical_races_geny$Terrain) & !is.na(df_historical_races_geny$Longueur) & !is.na(df_historical_races_geny$Lieu)
#   df_historical_races_geny$Track <- NA
#   df_historical_races_geny$Reduction <- NA
#   
#   df_historical_races_geny[idx_races_trot_attele,"Track"] <- paste(df_historical_races_geny[idx_races_trot_attele,"Lieu"], df_historical_races_geny[idx_races_trot_attele,"Terrain"],df_historical_races_geny[idx_races_trot_attele,"Longueur"],df_historical_races_geny[idx_races_trot_attele,"Depart"],sep="_")
#   df_historical_races_geny[idx_races_trot_attele,"Reduction"] <- unlist(lapply(df_historical_races_geny[idx_races_trot_attele,"Ecart"],get_numeric_values_reduc_ecart_geny))
#   df_historical_races_geny[idx_races_trot_attele,"Speed"] <- 1000/df_historical_races_geny[idx_races_trot_attele,"Reduction"]
#   
#   df_historical_races_geny[idx_races_trot_monte,"Track"] <- paste(df_historical_races_geny[idx_races_trot_monte,"Lieu"], df_historical_races_geny[idx_races_trot_monte,"Terrain"],df_historical_races_geny[idx_races_trot_monte,"Longueur"],sep="_")
#   df_historical_races_geny[idx_races_trot_monte,"Reduction"] <- unlist(lapply(df_historical_races_geny[idx_races_trot_monte,"Ecart"],get_numeric_values_reduc_ecart_geny))
#   df_historical_races_geny[idx_races_trot_monte,"Speed"] <- 1000/df_historical_races_geny[idx_races_trot_monte,"Reduction"]
#   
#   df_historical_races_geny[idx_races_galop,"Track"] <- paste(df_historical_races_geny[idx_races_galop,"Lieu"], df_historical_races_geny[idx_races_galop,"Terrain"],df_historical_races_geny[idx_races_galop,"Longueur"],sep="_")
#   
#   df_historical_races_focus_track <- df_historical_races_geny[!is.na(df_historical_races_geny$Track),]
#   
#   vec_discipline_found <- unique(df_historical_races_focus_track$Discipline)
#   
#   vec_freq_tracks <- table(df_historical_races_focus_track$Track)
#   df_historical_races_focus_track <- df_historical_races_focus_track[df_historical_races_focus_track$Track %in% names(vec_freq_tracks)[vec_freq_tracks>1], ]
#   vec_freq_horses <- table(df_historical_races_focus_track$Cheval)
#   df_historical_races_focus_track <- df_historical_races_focus_track[df_historical_races_focus_track$Cheval %in% names(vec_freq_horses)[vec_freq_horses>1],]
#   
#   for(current_discipline in vec_discipline_found) 
#   {
#     df_infos_focus <- df_historical_races_focus_track %>%
#       filter(Discipline == current_discipline ) %>%
#       filter(as.numeric(Sys.Date()-Date) <= number_days_back) %>%
#       select(Cheval,Track,Speed) %>%
#       pivot_wider(
#         names_from = Track,
#         values_from = Speed,
#         values_fn = max
#       ) %>%
#       as.data.frame()
#     if(nrow(df_infos_focus)>0){
#       vec_track_found <- colnames(df_infos_focus)[-1]
#       for(idx_track in 1:(length(vec_track_found)-1))
#       {
#         for(idy_track in (idx_track+1):length(vec_track_found) )
#         {
#           df_infos_focus_idx_idy <- df_infos_focus[,c("Cheval",vec_track_found[idx_track],vec_track_found[idy_track])]
#           if(sum(complete.cases(df_infos_focus_idx_idy))>0){
#             df_infos_focus_idx_idy <- df_infos_focus_idx_idy[complete.cases(df_infos_focus_idx_idy),]
#             if(nrow(df_infos_focus_idx_idy)>=3){
#               if(length(unlist(strsplit(vec_track_found[idx_track],"_")))==4){
#                 if(unlist(strsplit(vec_track_found[idx_track],"_"))[1]!=unlist(strsplit(vec_track_found[idy_track],"_"))[1]){
#                   if(unlist(strsplit(vec_track_found[idx_track],"_"))[2]==unlist(strsplit(vec_track_found[idy_track],"_"))[2]){
#                     if(unlist(strsplit(vec_track_found[idx_track],"_"))[3]==unlist(strsplit(vec_track_found[idy_track],"_"))[3]){
#                       if(unlist(strsplit(vec_track_found[idx_track],"_"))[4]==unlist(strsplit(vec_track_found[idy_track],"_"))[4]){
#                         if(abs(as.numeric(unlist(strsplit(vec_track_found[idx_track],"_"))[3]) - as.numeric(unlist(strsplit(vec_track_found[idy_track],"_"))[3])) == 0){
#                           res_t_test_hippodrome_longueur <- t.test(df_infos_focus_idx_idy[,2],df_infos_focus_idx_idy[,3],paired  = TRUE ,alternative = "two.sided")
#                           df_mean_diff_test_same_tracks_different_distance_compiled_current <- data.frame(Source = vec_track_found[idx_track], Target = vec_track_found[idy_track] , Significance = res_t_test_hippodrome_longueur$p.value, Statistic= res_t_test_hippodrome_longueur$estimate, Source_Reduction = median(df_infos_focus_idx_idy[,2]) , Target_Reduction = median(df_infos_focus_idx_idy[,3]), Size = nrow(df_infos_focus_idx_idy) , Discipline = current_discipline )
#                           df_mean_diff_test_same_tracks_different_distance_compiled_current$Detla_Reduction <- df_mean_diff_test_same_tracks_different_distance_compiled_current$Source_Reduction-df_mean_diff_test_same_tracks_different_distance_compiled_current$Target_Reduction
#                           rownames(df_mean_diff_test_same_tracks_different_distance_compiled_current) <- NULL
#                           df_mean_diff_test_same_tracks_different_distance_compiled <- rbind.fill(df_mean_diff_test_same_tracks_different_distance_compiled,df_mean_diff_test_same_tracks_different_distance_compiled_current)
#                         }
#                       }
#                     }
#                   }
#                 }
#               }
#               
#               if(length(unlist(strsplit(vec_track_found[idx_track],"_")))==3){
#                 if(unlist(strsplit(vec_track_found[idx_track],"_"))[1]!=unlist(strsplit(vec_track_found[idy_track],"_"))[1]){
#                   if(unlist(strsplit(vec_track_found[idx_track],"_"))[2]==unlist(strsplit(vec_track_found[idy_track],"_"))[2]){
#                     if(unlist(strsplit(vec_track_found[idx_track],"_"))[2]==unlist(strsplit(vec_track_found[idy_track],"_"))[2]){
#                       if(abs(as.numeric(unlist(strsplit(vec_track_found[idx_track],"_"))[3]) - as.numeric(unlist(strsplit(vec_track_found[idy_track],"_"))[3])) == 0){
#                         res_t_test_hippodrome_longueur <- t.test(df_infos_focus_idx_idy[,2],df_infos_focus_idx_idy[,3],paired  = TRUE ,alternative = "two.sided")
#                         df_mean_diff_test_same_tracks_different_distance_compiled_current <- data.frame(Source = vec_track_found[idx_track], Target = vec_track_found[idy_track] , Significance = res_t_test_hippodrome_longueur$p.value, Statistic= res_t_test_hippodrome_longueur$estimate, Source_Reduction = median(df_infos_focus_idx_idy[,2]) , Target_Reduction = median(df_infos_focus_idx_idy[,3]), Size = nrow(df_infos_focus_idx_idy) , Discipline = current_discipline)
#                         df_mean_diff_test_same_tracks_different_distance_compiled_current$Detla_Reduction <- df_mean_diff_test_same_tracks_different_distance_compiled_current$Source_Reduction-df_mean_diff_test_same_tracks_different_distance_compiled_current$Target_Reduction
#                         rownames(df_mean_diff_test_same_tracks_different_distance_compiled_current) <- NULL
#                         df_mean_diff_test_same_tracks_different_distance_compiled <- rbind.fill(df_mean_diff_test_same_tracks_different_distance_compiled,df_mean_diff_test_same_tracks_different_distance_compiled_current)
#                       }
#                     }
#                   }
#                 }
#               }
#             }
#           }
#         }
#       }
#     }
#   }
#   
#   if(!is.null(df_mean_diff_test_same_tracks_different_distance_compiled)){
#     if(nrow(df_mean_diff_test_same_tracks_different_distance_compiled)>0){
#       for(idx in 1:nrow(df_mean_diff_test_same_tracks_different_distance_compiled))
#       {
#         if(df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Significance"] <= 0.1 & df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Statistic"] >0){
#           df_mean_diff_test_same_tracks_different_distance_compiled_formated_current <- data.frame(from = df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Source"], to = df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Target"],Discipline = df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Discipline"])
#           df_mean_diff_test_same_tracks_different_distance_compiled_formated <- rbind.fill(df_mean_diff_test_same_tracks_different_distance_compiled_formated,df_mean_diff_test_same_tracks_different_distance_compiled_formated_current)
#         }
#         if(df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Significance"] <= 0.1 & df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Statistic"] < 0){
#           df_mean_diff_test_same_tracks_different_distance_compiled_formated_current <- data.frame(from = df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Target"], to = df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Source"],Discipline = df_mean_diff_test_same_tracks_different_distance_compiled[idx,"Discipline"])
#           df_mean_diff_test_same_tracks_different_distance_compiled_formated <- rbind.fill(df_mean_diff_test_same_tracks_different_distance_compiled_formated,df_mean_diff_test_same_tracks_different_distance_compiled_formated_current)
#         }
#       }
#     }
#   }
#   
#   return(list(Details = df_mean_diff_test_same_tracks_different_distance_compiled , Formated = df_mean_diff_test_same_tracks_different_distance_compiled_formated))
# }

# if(discipline == "Trot Attelé")
# {
#   df_historical_races_geny_hippodrome_discipline <- df_historical_races_geny_hippodrome[df_historical_races_geny_hippodrome$Discipline == discipline , ]
#   vec_found_discipline_hippodrome_depart <- unique(df_historical_races_geny_hippodrome_discipline$Depart)
#   if(sum(is.na(vec_found_discipline_hippodrome_depart))>0){
#     vec_found_discipline_hippodrome_depart <- vec_found_discipline_hippodrome_depart[!is.na(vec_found_discipline_hippodrome_depart)]
#   }
#   for(depart in vec_found_discipline_hippodrome_depart)
#   {
#     df_historical_races_geny_hippodrome_discipline_depart <- df_historical_races_geny_hippodrome_discipline[df_historical_races_geny_hippodrome_discipline$Depart == depart , ]
#     vec_found_discipline_hippodrome_depart_longueur <- sort(unique(df_historical_races_geny_hippodrome_discipline$Longueur))
#     if(sum(is.na(vec_found_discipline_hippodrome_depart_longueur))>0){
#       vec_found_discipline_hippodrome_depart_longueur <- vec_found_discipline_hippodrome_depart_longueur[!is.na(vec_found_discipline_hippodrome_depart_longueur)]
#     }
#     
#     if(length(vec_found_discipline_hippodrome_depart_longueur)>1){
#       df_historical_races_geny_hippodrome_discipline_depart_longueur <- df_historical_races_geny_hippodrome_discipline_depart %>%
#         filter(as.numeric(Sys.Date()-Date) <= number_days_back) %>%
#         select(Cheval,Longueur,Speed) %>%
#         drop_na() %>%
#         pivot_wider(
#           names_from = Longueur,
#           values_from = Speed,
#           values_fn = max
#         ) %>%
#         as.data.frame()
#       
#       if(nrow(df_historical_races_geny_hippodrome_discipline_depart_longueur)>0){
#         for(idx in 1:(length(vec_found_discipline_hippodrome_depart_longueur)-1))
#         {
#           for(idy in (idx+1):length(vec_found_discipline_hippodrome_depart_longueur))
#           {
#             if(vec_found_discipline_hippodrome_depart_longueur[idx] %in% colnames(df_historical_races_geny_hippodrome_discipline_depart_longueur) & vec_found_discipline_hippodrome_depart_longueur[idy] %in% colnames(df_historical_races_geny_hippodrome_discipline_depart_longueur)){
#               
#               df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy <- df_historical_races_geny_hippodrome_discipline_depart_longueur[,c("Cheval",vec_found_discipline_hippodrome_depart_longueur[idx],vec_found_discipline_hippodrome_depart_longueur[idy])]
#               if(sum(complete.cases(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy))>4){
#                 df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy <- df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[complete.cases(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy),]
#                 res_t_test_hippodrome_longueur <- t.test(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[,2],df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[,3],paired  = TRUE ,alternative = "two.sided")
#                 df_mean_diff_test_same_tracks_different_distance_compiled_current <- data.frame(Hippodrome=hippodrome, Discipline = discipline , Depart = depart, Source = vec_found_discipline_hippodrome_depart_longueur[idx], Target = vec_found_discipline_hippodrome_depart_longueur[idy] , Significance = res_t_test_hippodrome_longueur$p.value, Statistic= res_t_test_hippodrome_longueur$estimate, Source_Reduction = median(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[,2]) , Target_Reduction = median(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy[,3]), Size = nrow(df_historical_races_geny_hippodrome_discipline_depart_longueur_idx_idy))
#                 df_mean_diff_test_same_tracks_different_distance_compiled_current$Detla_Reduction <- df_mean_diff_test_same_tracks_different_distance_compiled_current$Source_Reduction-df_mean_diff_test_same_tracks_different_distance_compiled_current$Target_Reduction
#                 rownames(df_mean_diff_test_same_tracks_different_distance_compiled_current) <- NULL
#                 df_mean_diff_test_same_tracks_different_distance_compiled <- rbind.fill(df_mean_diff_test_same_tracks_different_distance_compiled,df_mean_diff_test_same_tracks_different_distance_compiled_current)
#               }
#             }
#           }
#         }
#       }
#     }
#   }
# }
