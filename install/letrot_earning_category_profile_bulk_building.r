######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_earning_profile_output  =  "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/01-raw/earning"
path_earning_profile_output  =  "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/02-processed"
path_mat_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/02-processed/mat_historical_races_letrot.rds"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- ceiling(host_nb_cores*0.75)
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races <- readRDS(path_mat_historical_races)
mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
mat_historical_races$Race <- as.vector(mat_historical_races$Race)
mat_historical_races$Date <- as.Date(mat_historical_races$Date)
mat_historical_races <- transform(mat_historical_races,Delay=Sys.Date()-Date)
vec_discipline <- as.vector(unique(mat_historical_races$Discipline))
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
setwd(path_earning_profile_output)
for(discipline in vec_discipline)
{
  mat_historical_races_focus <- subset(mat_historical_races,Discipline==discipline)
  vec_horses <- as.vector(unique(mat_historical_races_focus$Cheval))
  vec_horses <- vec_horses[!is.na(vec_horses)]
  print(paste("starting :",discipline))
  mat_earning_catgory_profile_compiled <- foreach(i=1:length(vec_horses),.combine=rbind) %dopar% {
    require(bmxFutankeMbayar)
    mat_historical_races_current <- mat_historical_races_focus[mat_historical_races_focus$Cheval==vec_horses[i],]
    if(nrow(mat_historical_races_current)>0) {
      mat_earning_catgory_profile_current <- get_horse_category_distance_earning_profile_letrot(mat_historical_races_current)
    }
  }
  mat_earning_catgory_profile_compiled <- mat_earning_catgory_profile_compiled %>% 
    group_by(Age,Class) %>% 
    mutate(Mean_Earning_Percent=get_percentile_values(Mean_Earning),Total_Earning_Percent=get_percentile_values(Total_Earning))
  saveRDS(mat_earning_catgory_profile_compiled,file=gsub(" ","-",paste(paste("mat-earning-catogory-profile_",gsub(" ","-",tolower(discipline)),sep=""),".rds",sep="")))
  print(paste("finishing :",discipline))
}
################################ Generate and store data ############################
