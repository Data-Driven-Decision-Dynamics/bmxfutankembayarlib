# message("Start computing issues statistics")
# df_stat_races_issues <- NULL
# for(horse in current_race_horses)
# {
#   if(horse %in% df_historical_races_geny$Cheval){
#     df_stat_races_issues_current <- get_horse_stat_races_issues_geny(df_infos_target_race_geny ,horse, df_engagement_closeness_limit_participation)
#     if(nrow(df_stat_races_issues_current)>0){
#       df_stat_races_issues <- rbind.fill(df_stat_races_issues,df_stat_races_issues_current)
#     }
#   }
# }
# 
# if(nrow(df_stat_races_issues)>0){
#   if(sum(!is.na(df_stat_races_issues$MEAN_EARNING_POSITIVE))>0){
#     if("MEAN_EARNING_POSITIVE" %in% colnames(df_stat_races_issues)){
#       df_stat_races_issues$MEAN_EARNING_POSITIVE <- round(get_percentile_values(df_stat_races_issues$MEAN_EARNING_POSITIVE),2)
#     }
#     if("PERCENT_LONGEST_SEQUENCE_FINISH" %in% colnames(df_stat_races_issues)){
#       df_stat_races_issues$PERCENT_LONGEST_SEQUENCE_FINISH <- round(get_percentile_values(df_stat_races_issues$PERCENT_LONGEST_SEQUENCE_FINISH),2)
#     }
#     if("PERCENT_LONGEST_SEQUENCE_PODIUM" %in% colnames(df_stat_races_issues)){
#       df_stat_races_issues$PERCENT_LONGEST_SEQUENCE_PODIUM <- round(get_percentile_values(df_stat_races_issues$PERCENT_LONGEST_SEQUENCE_PODIUM),2)
#     }
#   }
# }
# 
# if(!is.null(df_stat_races_issues)) {
#   df_score_best_sequence <- df_stat_races_issues[,c("Cheval","PERCENT_LONGEST_SEQUENCE_FINISH","PERCENT_LONGEST_SEQUENCE_PODIUM")]
#   df_score_best_sequence$PERCENT_LONGEST_SEQUENCE_FINISH <- 0.4*df_score_best_sequence$PERCENT_LONGEST_SEQUENCE_FINISH
#   df_score_best_sequence$PERCENT_LONGEST_SEQUENCE_PODIUM <- 0.6*df_score_best_sequence$PERCENT_LONGEST_SEQUENCE_PODIUM
#   df_score_best_sequence$SEQUENCE <- apply(df_score_best_sequence[,-1],1,function(x){mean(x,na.rm=TRUE)})
#   df_score_best_sequence <- df_score_best_sequence[,c("Cheval","SEQUENCE")]
#   df_score_best_sequence$SEQUENCE <- round(get_percentile_values(df_score_best_sequence$SEQUENCE),2)
#   if(!is.null(df_stat_races_issues)){
#     writexl::write_xlsx(df_stat_races_issues,paste(path_start_output_dashboard_location_race,"Issues.xlsx",sep="/"))
#   }
# }
# message("Finish computing issues statistics")