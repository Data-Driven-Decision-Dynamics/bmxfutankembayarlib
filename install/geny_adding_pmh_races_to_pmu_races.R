################################ Setting Output Path ############################
path_input_pmh_data  <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmh/output/processed/historical_performances/mat_historical_races_pmh_geny.rds"
path_input_pmu_data  <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_pmu_geny.rds"
path_output_pmh_pmu_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"
################################ Setting Output Path ############################

################################ Combining Performance Data ############################
df_historical_races_pmh_geny <- readRDS(path_input_pmh_data)
df_historical_races_pmu_geny <- readRDS(path_input_pmu_data)
vec_url_pmh <- unique(df_historical_races_pmh_geny$URL)
vec_url_pmu <- unique(df_historical_races_pmu_geny$URL)
df_historical_races_pmh_pmu_geny <- plyr::rbind.fill(df_historical_races_pmh_geny,df_historical_races_pmu_geny)
df_historical_races_pmh_pmu_geny<- df_historical_races_pmh_pmu_geny[rownames(unique(df_historical_races_pmh_pmu_geny[,c("Cheval","Numero","Date","Discipline","Course")])),]
df_historical_races_pmh_pmu_geny$Lieu <- stringr::str_trim(gsub("\\[.*","",df_historical_races_pmh_pmu_geny$Lieu))
saveRDS(df_historical_races_pmh_pmu_geny,path_output_pmh_pmu_data)
################################ Combining Performance Data ############################


