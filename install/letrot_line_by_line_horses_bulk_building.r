######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_line_by_line_output  =  "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/raw/pairwise_comparisons"
path_mat_historical_races = "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/processed/historical_performances/mat_historical_races_letrot.rds"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
# host_os  <- as.vector(Sys.info()['sysname'])
# host_nb_cores <- parallel::detectCores()
# nb_cores_use <- ceiling(host_nb_cores*0.75)
# registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races_letrot <- readRDS(path_mat_historical_races)
mat_historical_races_letrot$Cheval <- as.vector(mat_historical_races_letrot$Cheval)
mat_historical_races_letrot$Race <- as.vector(mat_historical_races_letrot$Race)
mat_historical_races_letrot$Date <- as.Date(mat_historical_races_letrot$Date)
mat_historical_races_letrot <- mat_historical_races_letrot[Sys.Date()- mat_historical_races_letrot$Date<(367*5),]
vec_discipline <- as.vector(unique(mat_historical_races_letrot$Discipline))
vec_discipline <- vec_discipline[!is.na(vec_discipline)]
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
for(discipline in vec_discipline)
{
  mat_historical_races_letrot_focus <- subset(mat_historical_races_letrot,Discipline==discipline)
  mat_historical_races_letrot_focus <- mat_historical_races_letrot_focus[Sys.Date()- mat_historical_races_letrot_focus$Date<(367*5),]
  vec_races <- as.vector(unique(mat_historical_races_letrot_focus$Race))
  vec_races <- vec_races[!is.na(vec_races)]
  print(paste("Starting the computation of line by line results for :",discipline))
  # foreach(i=1:length(vec_races),.errorhandling="pass") %dopar% {
  for(i in 1:length(vec_races)){
    mat_historical_races_letrot_focus_current <- mat_historical_races_letrot_focus[mat_historical_races_letrot_focus$Race==vec_races[i],]
    mat_historical_races_letrot_focus_current <- mat_historical_races_letrot_focus_current [!is.na(mat_historical_races_letrot_focus_current$Race),]
    mat_historical_races_letrot_focus_current <- mat_historical_races_letrot_focus_current[rownames(unique(mat_historical_races_letrot_focus_current[,c("Cheval","Numero")])),]
    mat_historical_races_letrot_focus_current <- mat_historical_races_letrot_focus_current [!is.na(mat_historical_races_letrot_focus_current$Date),]
    if(nrow(mat_historical_races_letrot_focus_current)>1) {
      get_line_by_line_driver_jockey_race_letrot (mat_perf_race_complete=mat_historical_races_letrot_focus_current,target_item="Horse")
    }
  }
  print(paste("Finished the computation of line by line results for :",discipline))
}
################################ Generate and store data ############################
