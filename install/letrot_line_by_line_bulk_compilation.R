######################################## Loading required Packages ########################################
require(data.table)
require(plyr)
require(dplyr)
require(PlayerRatings)
require(parallel)
require(magrittr)
require(foreach)
require(doParallel)
require(readr)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data  <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/raw/pairwise_comparisons"
path_output_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/processed/pairwise_comparisons/mat_line_by_line_rating_letrot.rds"
path_output_data_start <- "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/output/processed/pairwise_comparisons_tmp"
path_characteristics_location = "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/input/Hyppodroms-Label-Characteristics-Corrected.xlsx"
path_historical_characteristics_location = "/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/input/mat_tracks_dates_characteristics.rds"
################################ Setting Output Path ############################

df_tracks_historical_characteristics <- readRDS("/mnt/Master-Data/Futanke-Mbayar/France/data/le-trot/input/mat_tracks_dates_characteristics.rds")
df_characteristics_location <- readxl::read_excel(path_characteristics_location,sheet = 2)
df_characteristics_location <- as.data.frame(df_characteristics_location)
df_characteristics_location$Corde <- stringr::str_to_title(df_characteristics_location$Corde)
df_characteristics_location$Terrain <- stringr::str_to_title(df_characteristics_location$Terrain)

################################ Setting Output Path ############################
files_line_by_line_rating <- dir(path=path_input_data,pattern = ".csv")
setwd(path_input_data)
vec_steps <- seq(1,length(files_line_by_line_rating),by=1000)
vec_steps[length(vec_steps)] <- length(files_line_by_line_rating)
for (step in 1:(length(vec_steps)-1)) {
  setwd(path_input_data)
  mat_line_by_line_rating_current <- NULL
  files_line_by_line_rating_current <- files_line_by_line_rating[vec_steps[step]:(vec_steps[step+1])] 
  for(current_file in files_line_by_line_rating_current)
  {
    mat_one_race <- read.csv(current_file)
    if(nrow(mat_one_race)>0){
      mat_line_by_line_rating_current <- rbind.fill(mat_line_by_line_rating_current,mat_one_race)
    }
  }
  setwd(path_output_data_start)
  saveRDS(mat_line_by_line_rating_current,paste(step,".rds",sep=""))
  print(step)
}
setwd(path_output_data_start)
mat_line_by_line_rating <- list.files(path=path_output_data_start, full.names = TRUE) %>%
  lapply(readRDS) %>%
  bind_rows %>%
  as.data.frame()
mat_line_by_line_rating <- mat_line_by_line_rating %>% distinct() %>% as.data.frame()
idx_missing_corde_terrain<- is.na(mat_line_by_line_rating$Corde) | is.na(mat_line_by_line_rating$Terrain)
if(sum(idx_missing_corde_terrain)>0){
  vec_location_missing_corde_terrain <- unique(mat_line_by_line_rating[idx_missing_corde_terrain,"Location"])
  if(length(intersect(vec_location_missing_corde_terrain,unique(df_tracks_historical_characteristics$Lieu)))>0){
    for(location_missing in vec_location_missing_corde_terrain)
    {
      if(location_missing %in% unique(df_tracks_historical_characteristics$Lieu)){
        df_tracks_historical_characteristics <- df_tracks_historical_characteristics[order(df_tracks_historical_characteristics$Date,decreasing = TRUE),]
        val_corde_update <- df_tracks_historical_characteristics[df_tracks_historical_characteristics$Lieu == location_missing,"Corde"][1]
        val_terrain_update <- df_tracks_historical_characteristics[df_tracks_historical_characteristics$Lieu == location_missing,"Terrain"][1]
        mat_line_by_line_rating[mat_line_by_line_rating$Location == location_missing & is.na(mat_line_by_line_rating$Corde),'Corde'] <- val_corde_update
        mat_line_by_line_rating[mat_line_by_line_rating$Location == location_missing & is.na(mat_line_by_line_rating$Terrain),'Terrain'] <- val_terrain_update
        print(location_missing)
      }
    }
  }
}

mat_line_by_line_rating$Terrain <- stringr::str_to_title(mat_line_by_line_rating$Terrain)
if(nrow(mat_line_by_line_rating)>0) {
  mat_line_by_line_rating$Terrain <- gsub("Cendrée", "Machefer",mat_line_by_line_rating$Terrain)
  mat_line_by_line_rating$Terrain <- gsub("Mâchefer","Machefer",mat_line_by_line_rating$Terrain)
  mat_line_by_line_rating$Terrain <- gsub("Sable Rose","Sable",mat_line_by_line_rating$Terrain)
}

if(nrow(mat_line_by_line_rating)>0) {
  mat_line_by_line_rating$CD <- paste(mat_line_by_line_rating$Corde,mat_line_by_line_rating$Terrain,sep="-")
}

saveRDS(mat_line_by_line_rating,path_output_data)
################################ Setting Output Path ############################