# require(bmxFutankeMbayar)
# require(dplyr)
# require(plyr)
# require(magrittr)
# 
# message("Setting up path of necessary files")
# path_df_historical_races_geny = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"
# path_df_issues_profile_delay = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/df_stats_issues_profile_delay.rds"
# path_df_issues_profile_delay_tmp = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/issues_profile_delay_tmp"
# 
# number_days_back = 1000
# message("Setting up path of necessary files")
# 
# df_infos_issues_profile_delay_compiled <- NULL
# df_issues_profile_delay_compiled <- NULL
# 
# message("Setting up path of necessary files")
# 
# message("Reading historical performance file")
# if(!exists("df_historical_races_geny")) {
#   if(file.exists(path_df_historical_races_geny)){
#     df_historical_races_geny <- readRDS(path_df_historical_races_geny)
#   }
# }
# message("Reading historical performance file")
# 
# message("Focusing on monte and attele races")
# vec_races_config_profile_delay <- unique(df_historical_races_geny[unique(max(df_historical_races_geny$Date)) - df_historical_races_geny$Date <= number_days_back, "Race"])
# df_historical_races_focus <- df_historical_races_geny %>%
#   filter(Race %in% vec_races_config_profile_delay) %>%
#   select(Cheval,Discipline,Lieu,Longueur,Place,Race,Date,Age,Numero) %>%
#   # drop_na() %>%
#   as.data.frame()
# list_historical_races_focus <- split.data.frame(df_historical_races_focus,df_historical_races_focus$Race)
# message("Focusing on monte and attele races")
# 
# setwd(path_df_issues_profile_delay_tmp)
# 
# for(race in rev(names(list_historical_races_focus)))
# {
#   if(min(as.numeric(list_historical_races_focus[[race]][,"Age"]))>=3){
#     df_infos_issues_profile_delay_current <- try(get_horse_successful_profile_delay_geny(list_historical_races_focus[[race]]))
#     if(class(df_infos_issues_profile_delay_current) == "data.frame"){
#       if(!is.null(df_infos_issues_profile_delay_current)){
#         write.csv(df_infos_issues_profile_delay_current,paste(paste(path_df_issues_profile_delay_tmp,gsub("\n","",gsub("\\/"," ",gsub("\\("," ",race))),sep="/"),".csv",sep=""))
#         # df_infos_issues_profile_delay_compiled <- rbind.fill(df_infos_issues_profile_delay_current,df_infos_issues_profile_delay_compiled)
#       }
#     }
#   }
# }




# df_issues_profile_delay_compiled <- df_infos_issues_profile_delay_compiled %>%
#   group_by(Config,Discipline) %>%
#   dplyr::summarise(Size = n(),
#                    Success = get_number_top_five(Place),
#                    Percent = Success/Size,
#                    Percent = round(Percent,2)) %>%
#   as.data.frame()
# 
# saveRDS(df_issues_profile_delay_compiled,path_df_issues_profile_delay)
