######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
require( magrittr)
require(parallel)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_mat_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
path_input_itr_per_horse   = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/itr"
path_output_itr = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- ceiling(host_nb_cores*0.75)
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races <- readRDS(path_mat_historical_races)
most_recent_date <- as.Date(max(mat_historical_races$Date,na.rm = TRUE))
vec_horses <- as.vector(unique(mat_historical_races$Cheval))
vec_horses <- vec_horses[!is.na(vec_horses)]
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
# mat_compilation_details_itr <- foreach(i=1:length(vec_horses),.combine='rbind') %dopar% {
#   mat_historical_races_current <- mat_historical_races[mat_historical_races$Cheval==vec_horses[i],]
#   mat_historical_races_current <- mat_historical_races_current[!is.na(mat_historical_races_current$Distance),]
#   mat_historical_races_current <- subset(mat_historical_races_current,Gains>=0)
#   if(nrow(mat_historical_races_current)>0 & max(mat_historical_races_current$Date)>=as.Date("2015-01-01")) {
#     get_high_level_global_kpi_geny(mat_perf_horse=mat_historical_races_current)
#   }
# }

for (i in 1:length(vec_horses))
  {
    mat_historical_races_current <- mat_historical_races[mat_historical_races$Cheval==vec_horses[i],]
    mat_historical_races_current <- mat_historical_races_current[!is.na(mat_historical_races_current$Distance),]
    mat_historical_races_current <- subset(mat_historical_races_current,Gains>=0)
    if(nrow(mat_historical_races_current)>0 & max(mat_historical_races_current$Date)>=as.Date("2015-01-01")) {
      get_high_level_global_kpi_geny(mat_perf_horse=mat_historical_races_current)
  }
}

################################ Generate and store data ############################

################################ Compilation of ITR Details ############################
vec_availables_files <- dir(path_input_itr_per_horse,full.names = TRUE)
setwd(path_input_itr_per_horse)
mat_compilation_details_itr <- ldply(vec_availables_files, read_csv,.parallel=TRUE)
mat_compilation_details_itr <- mat_compilation_details_itr %>% distinct() %>% as.data.frame()
mat_compilation_details_itr$Gender <- gsub("FALSE","F",mat_compilation_details_itr$Gender)
################################ Compilation of ITR Details ############################

################################ Saving ITR Details Calculation ############################
saveRDS(mat_compilation_details_itr,paste(path_output_itr,"mat_compilation_details_itr.rds",sep="/"))
################################ Saving ITR Details Calculation ############################
