###################################################### Loading Required Packages ##################################################
require(plspm)
require(plsgenomics)
require(CMA)
require(ROCR)
###################################################### Loading Required Packages ##################################################

###################################################### Setting Output Path ##################################################
path_input_features_desc <-"/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/input/Master-Feature-Description-Futanke-Mbayar.xlsx"
path_input_training_data <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/train"
path_input_testing <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/input/test"
path_output_perf <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/perfs"
path_output_models <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/models"
path_output_dashboard <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/dashboard"
path_output_importance <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/letrot/output/importances"
######################################################### Setting Output Path ##################################################

######################################################### Reading Feature Description File ##################################################
mat_features_infos <- readxl::read_excel(path_input_features_desc,sheet = 1)
mat_features_infos <- as.data.frame(mat_features_infos)
vec_features_fully_described <- mat_features_infos[mat_features_infos$Role =="Explainer","Feature"]
######################################################### Reading Feature Description File ##################################################

######################################################### Processing Each Data Set ##################################################
vec_training_file <- dir(path_input_training_data,pattern="*")
vec_training_file <- vec_training_file [-grep("attele",vec_training_file)]

for(training_file in rev(vec_training_file))
{
  vec_desc_races_add <- unlist(strsplit(sub(".rds","",sub("mat_training_","",training_file)),"_"))
  message(paste("Reading",training_file,"file"))
  mat_training_current_race_class <- readRDS(paste(path_input_training_data,training_file,sep="/"))
  mat_training_current_race_class$IDENTIFIER <- paste(mat_training_current_race_class$CHEVAL,mat_training_current_race_class$RACE,sep="_")
  rownames(mat_training_current_race_class)  <- mat_training_current_race_class$IDENTIFIER 
  mat_training_current_race_class_utils <- mat_training_current_race_class[,c("IDENTIFIER","RACE","DATE","HIPPODROME","PLACE")]
  message("Removing uncessary columns")
  mat_training_current_race_class <- mat_training_current_race_class[,setdiff(colnames(mat_training_current_race_class),c("_ID","CHEVAL","HIPPODROME","DISCIPLINE","DISTANCE_DAY","CATEGORY","PRIZE","RACE","DATE","MODEL","IDENTIFIER","PLACE","GENDER","AGE","RECUL","FERRAGE","AMATEURS","INTERNATIONAL",grep("^RACE",colnames(mat_training_current_race_class),value = TRUE)))]

  mat_testing_current_race_class <- readRDS(paste(path_input_testing,sub("mat_training_","mat_testing_",training_file),sep="/"))
  mat_testing_current_race_class$IDENTIFIER <- paste(mat_testing_current_race_class$CHEVAL,mat_testing_current_race_class$RACE,sep="_")
  rownames(mat_testing_current_race_class)  <- mat_testing_current_race_class$IDENTIFIER 
  
  vec_features_fully_described <- intersect(vec_features_fully_described,colnames(mat_training_current_race_class))

  mat_training_current_race_class [is.na(mat_training_current_race_class)] <- 0
  mat_testing_current_race_class [is.na(mat_testing_current_race_class)] <- 0
  
  
  model_pls_basic <- pls.lda(Xtrain=mat_training_current_race_class[,vec_features_fully_described],Ytrain=mat_training_current_race_class$Class,Xtest = mat_testing_current_race_class[,vec_features_fully_described],ncomp=20,nruncv=0)
  prediction_pls_basic <- as.data.frame(model_pls_basic$pred.lda.out$posterior)
  prediction_pls_basic$real <- mat_testing_current_race_class$Class
  
  pred_roc_pls_basic_current <- ROCR::prediction(prediction_pls_basic$High,mat_testing_current_race_class$Class,label.ordering=c("Low","High"))
  perf_roc_pls_basic_current <- performance(pred_roc_pls_basic_current, measure = "tpr", x.measure = "fpr")
  plot(perf_roc_pls_basic_current,colorize=TRUE)
  round(unlist(performance(pred_roc_pls_basic_current, measure = "auc")@y.values),2)
  
}
######################################################### Processing Each Data Set ##################################################


######################################################### Reading Feature Description File ##################################################
######################################################### Reading Feature Description File ##################################################


folds_perfs_measures <- createMultiFolds(mat_training_current_race_class$Class, k = 10, times = 10)
control_parameters   <- trainControl("repeatedcv", index = folds_perfs_measures, selectionFunction = "oneSE")
model_pls_caret <- train(Class ~ ., data = mat_training_current_race_class,method = "pls",metric = "Accuracy",tuneLength = 30,trControl = control_parameters)

model_pls_basic <-  pls.lda(Xtrain=mat_training_current_race_class[,vec_features_fully_described],Ytrain=mat_training_current_race_class$Class,Xtest=mat_testing_current_race_class[,vec_features_fully_described],ncomp=1:30,nruncv=20)
prediction_pls_basic <- as.data.frame(model_pls_basic$pred.lda.out$posterior)
prediction_pls_basic$real <- mat_testing_current_race_class$Class

pred_roc_pls_basic_current <- ROCR::prediction(prediction_pls_basic$High,mat_testing_current_race_class$Class,label.ordering=c("Low","High"))
perf_roc_pls_basic_current <- performance(pred_roc_pls_basic_current, measure = "tpr", x.measure = "fpr")
plot(perf_roc_pls_basic_current,colorize=TRUE)
round(unlist(performance(pred_roc_pls_basic_current, measure = "auc")@y.values),2)

vec_number_samples_class <- table(mat_training_current_race_class$Class)
mat_training_current_race_class_balanced <- NULL
for(current_mod_class in names(vec_number_samples_class))
{
  mat_training_current_race_class_balanced_current <- mat_training_current_race_class[mat_training_current_race_class$Class==current_mod_class,]
  mat_training_current_race_class_balanced_current <- mat_training_current_race_class_balanced_current[1:min(vec_number_samples_class),]
  mat_training_current_race_class_balanced <- rbind(mat_training_current_race_class_balanced,mat_training_current_race_class_balanced_current)
}






model_pls_rf <- pls_rfCMA(as.matrix(mat_training_current_race_class[,vec_features_fully_described]), c(0,1)[as.factor(mat_training_current_race_class$Class)], comp=15,models=TRUE,learnind=1:8657)
prediction_pls_rf <- predict(model_pls_rf@model,mat_testing_current_race_class[,vec_features_fully_described])
          
          
          
#           
# bayesian network
# stratification


# list_training_current_race_class <- list(x=as.matrix(mat_training_current_race_class[,vec_features_fully_described]),y=c(0,1)[as.factor(mat_training_current_race_class$Class)])
# fit_sgl_group_lasso <- SGL(list_training_current_race_class, mat_mapping_features$Group_ID, type = "logit")
# prediction_automl_current_group_lasso <- predictSGL(fit_sgl_group_lasso,as.matrix(mat_testing_current_race_class[,vec_features_fully_described]),8.201831e-04)

# model_group_lasso_gglasso <- gglasso(x=as.matrix(mat_training_current_race_class[,vec_features_fully_described]),y=c(-1,1)[as.factor(mat_training_current_race_class$Class[1:500])],group=mat_mapping_features$Group_ID,loss="logit")



