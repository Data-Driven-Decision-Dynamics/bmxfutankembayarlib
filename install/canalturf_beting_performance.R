######################################## Loading required Packages ########################################
require(xlsx)
require(h2o)
require(dplyr)
require(magrittr)
require(readxl)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_train <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/canalturf/input/train"
path_input_testing <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/canalturf/input/test"
path_input_pred <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/canalturf/output/predictions"
path_output_perf <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/canalturf/output/bet_perfs"
path_beting_config = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/input/beting-types-ranges.xlsx"
################################### Setting Output Path ############################

################################### COmputing Beting Performances ############################
df_beting_config <- read_excel(path_beting_config,sheet = 1) %>%
  as.data.frame()
vec_predictions_files <- dir(path_input_pred,pattern = "*")
for(prediction_file in vec_predictions_files)
{
  mat_prediction_details_current_model <- readRDS(paste(path_input_pred,prediction_file,sep="/"))
  list_prediction_details_current_model <- split.data.frame(mat_prediction_details_current_model,mat_prediction_details_current_model$Race)
  list_beting_performance_current_model <- lapply(list_prediction_details_current_model,get_beting_performance_per_race_canalturf)
  mat_beting_performance_current_model  <- dplyr::bind_rows(list_beting_performance_current_model)
  
  mat_summary_performance_beting_current_model <- mat_beting_performance_current_model %>%
    group_by(Beting,Size) %>%
    dplyr::summarise(Success=mean(Issue)) %>%
    as.data.frame() %>%
    pivot_wider(
      names_from = Size,
      values_from = Success,
      values_fn = list(Success = mean)
    ) %>%
    as.data.frame()
  
  mat_summary_performance_beting_current_model <- mat_summary_performance_beting_current_model[mat_summary_performance_beting_current_model$Beting %in% intersect(df_beting_config$Be,mat_summary_performance_beting_current_model$Beting),c("Beting",as.character(c(1:10)))]
  rownames(mat_summary_performance_beting_current_model) <- mat_summary_performance_beting_current_model$Beting
  mat_summary_performance_beting_current_model <- mat_summary_performance_beting_current_model[df_beting_config$Bet,]
  mat_summary_performance_beting_current_model[,-1] <- round(mat_summary_performance_beting_current_model[,-1]*100,2)
  rownames(mat_summary_performance_beting_current_model) <- NULL
  saveRDS(mat_summary_performance_beting_current_model,paste(path_output_perf,prediction_file,sep="/"))
}
################################### COmputing Beting Performances ############################



