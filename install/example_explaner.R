path_file_importance <- dir(path_output_importance,pattern=current_location_discipline_distance)
path_file_importance <- paste(path_output_importance,path_file_importance,sep="/")
if(file.exists(path_file_importance)){
  mat_importance_current <-read_xlsx(path_file_importance,sheet = 1)
  mat_importance_current <- as.data.frame(mat_importance_current) [,-1]
  mat_importance_current <- merge(mat_importance_current,aggregation[,c("Feature","Family")],by.x="variable",by.y="Feature")
  vec_features_importance <- mat_importance_current %>% 
    group_by(Family)  %>%
    top_n(1,percentage) %>%
    select(variable) %>%
    as.data.frame() %>%
    unlist()%>%
    as.vector()
}

vec_features_importance <-  intersect(vec_features_importance,colnames(mat_features_model_building))
mat_features_model_building[,vec_features_importance]


####
####




df_train_data <- readRDS(paste(path_output_training_data,dir(path_output_training_data,pattern=current_location_discipline_distance),sep="/"))
explainer <- lime::lime(x = as.data.frame(df_train_data[, vec_features_current_model]),
                        model = current_model)

testing_sample <- as.data.frame(mat_features_model_building[, vec_features_current_model])
explanations_sample = lime::explain(x = testing_sample,
                                    explainer = explainer,
                                    n_permutations = 5000,
                                    feature_select = "auto",
                                    n_labels = 2,    
                                    n_features = 5)
explanations_sample = 
  explanations_sample[order(explanations_sample$feature_weight, decreasing = TRUE),]
explanations_sample = as.data.frame(explanations_sample[explanations_sample$label=="High",])
mat_explanations_sample <- as.data.frame(matrix(NA,ncol=6,nrow=length(unique(mat_infos_target_race$Cheval))))
rownames(mat_explanations_sample) <- unique(mat_infos_target_race$Cheval)
colnames(mat_explanations_sample) <- c("Cheval",paste("Explanation_0",1:5,sep=""))
for(current_test_horse in unique(mat_infos_target_race$Cheval))
{
  explanations_sample_current <- explanations_sample[explanations_sample$case==current_test_horse,]
  explanations_sample_current <- explanations_sample_current[order(explanations_sample_current$feature_weight,decreasing = TRUE),]
  mat_explanations_sample[current_test_horse,"Cheval"] <- current_test_horse
  mat_explanations_sample[current_test_horse,-1] <- explanations_sample_current[,"feature_desc"]
}

mat_feature_impact_sample <- as.data.frame(matrix(NA,ncol=6,nrow=length(unique(mat_infos_target_race$Cheval))))
rownames(mat_feature_impact_sample) <- unique(mat_infos_target_race$Cheval)
colnames(mat_feature_impact_sample) <- c("Cheval",paste("Explanation_0",1:5,sep=""))
for(current_test_horse in unique(mat_infos_target_race$Cheval))
{
  mat_feature_impact_sample_current <- explanations_sample[explanations_sample$case==current_test_horse,]
  mat_feature_impact_sample_current <- mat_feature_impact_sample_current[order(mat_feature_impact_sample_current$feature_weight,decreasing = TRUE),]
  mat_feature_impact_sample[current_test_horse,"Cheval"] <- current_test_horse
  mat_feature_impact_sample[current_test_horse,-1] <- mat_feature_impact_sample_current[,"feature_weight"]
}

