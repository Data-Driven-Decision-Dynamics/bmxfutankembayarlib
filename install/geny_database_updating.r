################################ Loading required packages ################################
# Sys.setenv('JAVA_HOME'= "/usr/lib/jvm/default-java")
# .libPaths(new = c("/usr/local/lib/R/site-library","/home/futankembayar/R/x86_64-pc-linux-gnu-library/4.2","/usr/lib/R/site-library","/usr/lib/R/library"))
require(bmxFutankeMbayar)
require(readr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(data.table)
require(parallel)
require(gtools)
require(randomForest)
require(doMC)
require(bmxFutankeMbayar)
require(rvest)
require(stringr)
require(curl)
require(dplyr)
# require(qdap)
require(magrittr)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_df_historical_races_geny <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/"
################################ Setting Output Path ############################

################################ Reading current data ############################
df_historical_races_geny <- readRDS(paste(path_df_historical_races_geny,"mat_historical_races_geny.rds",sep="/"))

if(class(df_historical_races_geny$Date)!="Date"){
  df_historical_races_geny$Date <- as.Date(df_historical_races_geny$Date)
}

if(sum(is.na(df_historical_races_geny$Date))>0){
  df_historical_races_geny <- df_historical_races_geny[!is.na(df_historical_races_geny$Date),]
}

if(nrow(df_historical_races_geny)>0){
  df_historical_races_geny$Terrain <- gsub("Mâchefer","Cendrée",df_historical_races_geny$Terrain)
  df_historical_races_geny$Terrain <- gsub("Machefer","Cendrée",df_historical_races_geny$Terrain)
}

# vec_history_to_remove <- c("Criterium","Inedit","SA","Musique","Etat","Penetrometre","Reclamer","RECLAMER","Listed","Maiden","Amateur","AMATEUR","Apprenti","Conditions","Ecart")
# df_historical_races_geny <- df_historical_races_geny[,intersect(colnames(df_historical_races_geny),setdiff(colnames(df_historical_races_geny),vec_history_to_remove))]

idx_to_keep <- rownames(unique(df_historical_races_geny[,c("Date","Cheval","Discipline")]))
df_historical_races_geny <- df_historical_races_geny[idx_to_keep,]

most_recent_date <- as.Date(max(df_historical_races_geny$Date,na.rm = TRUE))
################################ Reading current data ############################

################################ Generate and store data ############################
if(most_recent_date<Sys.Date()-1){
  mat_new_races_pmu <- NULL
  if(most_recent_date<Sys.Date()){
    seq_date <- seq(most_recent_date+1, Sys.Date()-1, by = "day")
    vec_url_date_scrape_pmu   <- paste("http://www.geny.com/reunions-courses-pmu?date=",seq_date,sep="")
    vec_url_date_scrape_pmu <- rev(vec_url_date_scrape_pmu)
    for(i in 1:length(vec_url_date_scrape_pmu)) {
      vec_race_current_date_pmu <- get_race_given_date_geny(vec_url_date_scrape_pmu[i])
      vec_race_current_date_pmu <- setdiff(vec_race_current_date_pmu,"http://www.geny.com")
      if(length(vec_race_current_date_pmu)>0) {
        for(j in 1:length(vec_race_current_date_pmu)) {
          mat_infos_race_pmu <- try(get_performances_details_race_geny(vec_race_current_date_pmu[j]))
          if(class(mat_infos_race_pmu)!="try-error"){
            if(nrow(mat_infos_race_pmu)>0){
              mat_new_races_pmu <- rbind.fill(mat_new_races_pmu,mat_infos_race_pmu)
              print(paste(i,length(vec_url_date_scrape_pmu)))
            }
          }
        } 
      }
    }
    
    if(!is.null(mat_new_races_pmu)){
      if(nrow(mat_new_races_pmu)>0){
        mat_new_races_pmu$Race <- paste(mat_new_races_pmu$Date,mat_new_races_pmu$Heure,mat_new_races_pmu$Lieu,mat_new_races_pmu$Course,sep="_")
        mat_new_races_pmu$Gender  <- as.vector(gsub("FALSE","F",mat_new_races_pmu$Gender))
        mat_new_races_pmu$Epreuve <- as.vector(gsub("FALSE","F",mat_new_races_pmu$Epreuve))
        mat_new_races_pmu <- mat_new_races_pmu %>% distinct()
      }
    }
  }
  
  mat_new_races_pmh <- NULL
  if(most_recent_date<Sys.Date()-1){
    seq_date <- seq(most_recent_date+1, Sys.Date()-1, by = "day")
    vec_url_date_scrape_pmh   <- paste("http://www.geny.com/reunions-pmh?date=",seq_date,sep="")
    vec_url_date_scrape_pmh <- rev(vec_url_date_scrape_pmh)
    for(i in 1:length(vec_url_date_scrape_pmh)) {
      vec_race_current_date_pmh <- get_pmh_race_given_date_geny(vec_url_date_scrape_pmh[i])
      vec_race_current_date_pmh <- setdiff(vec_race_current_date_pmh,"http://www.geny.com")
      if(length(vec_race_current_date_pmh)>0) {
        for(j in 1:length(vec_race_current_date_pmh)) {
          mat_infos_race_pmh <- try(get_pmh_performances_details_race_geny(vec_race_current_date_pmh[j]))
          if(class(mat_infos_race_pmh)!="try-error"){
            if(nrow(mat_infos_race_pmh)>0){
              print(paste("Processing",paste(i,length(vec_url_date_scrape_pmh)),vec_race_current_date_pmh[j]))
              mat_new_races_pmh <- rbind.fill(mat_new_races_pmh,mat_infos_race_pmh)
              print(paste(paste(i,length(vec_url_date_scrape_pmh),vec_race_current_date_pmh[j]),"processed"))
            }
          }
        } 
      }
    }
    
    if(!is.null(mat_new_races_pmh)){
      if(nrow(mat_new_races_pmh)>0){
        mat_new_races_pmh$Race <- paste(mat_new_races_pmh$Date,mat_new_races_pmh$Heure,mat_new_races_pmh$Lieu,mat_new_races_pmh$Course,sep="_")
        mat_new_races_pmh$Gender  <- as.vector(gsub("FALSE","F",mat_new_races_pmh$Gender))
        mat_new_races_pmh$Epreuve <- as.vector(gsub("FALSE","F",mat_new_races_pmh$Epreuve))
        mat_new_races_pmh <- mat_new_races_pmh %>% distinct()
      }
    }
  }
  
  if(!is.null(mat_new_races_pmu) & !is.null(mat_new_races_pmh)){
    mat_new_races <- rbind.fill(mat_new_races_pmu,mat_new_races_pmh)
    mat_new_races <- mat_new_races[,intersect(colnames(mat_new_races),colnames(df_historical_races_geny))]
    mat_new_races <- mat_new_races %>%
      distinct()%>%
      as.data.frame()
    df_historical_races_geny <- rbind.fill(df_historical_races_geny,mat_new_races)
    df_historical_races_geny <- df_historical_races_geny %>% distinct()
    if(class(df_historical_races_geny$Date)!="Date"){
      df_historical_races_geny$Date <- as.Date(df_historical_races_geny$Date)
    }
    
    setwd(path_df_historical_races_geny)
    df_historical_races_geny$Terrain <- gsub("Sable Rose","Sable",df_historical_races_geny$Terrain)
    df_historical_races_geny$Terrain <- gsub("Mâchefer","Cendrée",df_historical_races_geny$Terrain)
    df_historical_races_geny$Terrain <- gsub("Machefer","Cendrée",df_historical_races_geny$Terrain)
    df_historical_races_geny$Lieu <- str_trim(gsub("\\[Matinée]","",df_historical_races_geny$Lieu))
    df_historical_races_geny$Lieu <- str_trim(gsub("- Genybet","",df_historical_races_geny$Lieu))
    df_historical_races_geny$Lieu <- str_trim(gsub(" Soir","",df_historical_races_geny$Lieu))
    df_historical_races_geny$Lieu <- qdap::bracketX(df_historical_races_geny$Lieu)
    df_historical_races_geny$Lieu <- str_trim(gsub("\\[Etats Unis","",df_historical_races_geny$Lieu))
    df_historical_races_geny$Lieu <- str_trim(gsub("\\{Emirats Arabes]","",df_historical_races_geny$Lieu))
    df_historical_races_geny$Lieu <- str_trim(gsub("Chantilly Midi","Chantilly",df_historical_races_geny$Lieu)) 
    df_historical_races_geny$Lieu <- str_trim(gsub("Deauville Midi","Deauville",df_historical_races_geny$Lieu)) 
    df_historical_races_geny$Lieu <- str_trim(gsub("Enghien-Midi","Enghien",df_historical_races_geny$Lieu)) 
    df_historical_races_geny <- df_historical_races_geny %>%
      mutate(Race = paste(Date,Heure,Lieu,Course,sep="_")) %>%
      as.data.frame()
    if(sum(is.na(df_historical_races_geny$Cheval))>0){
      df_historical_races_geny <- df_historical_races_geny[!is.na(df_historical_races_geny$Cheval),]
    }
    if(class(df_historical_races_geny$Date)!="Date"){
      df_historical_races_geny$Date <- as.Date(df_historical_races_geny$Date)
    }
    
    idx_to_keep <- rownames(unique(df_historical_races_geny[,c("Date","Cheval","Discipline")]))
    df_historical_races_geny <- df_historical_races_geny[idx_to_keep,]
    
    if(length(intersect("Gipsy Ajité",df_historical_races_geny$Cheval))>0){
      df_historical_races_geny$Cheval <- sub("Gipsy Ajité","Gipsy Ajite",df_historical_races_geny$Cheval)
    }
    saveRDS(df_historical_races_geny,"mat_historical_races_geny.rds")
  } 
}
################################ Generate and store data ############################

# if(nrow(df_historical_races_geny)>0){
#   list_rules_wins = list(Trot=c(50,25,13,6,3,2,1),Galop=c(50,20,15,10,5))
#   if(nrow(df_historical_races_geny)>0) {
#     idx_row_with_boost_order <- grep("Boost Ordre", df_historical_races_geny$Details)
#     if(length(idx_row_with_boost_order)>0){
#       for(idx_row in idx_row_with_boost_order )
#       {
#         place_horse_bad_row <- df_historical_races_geny[idx_row,"Place"]
#         current_discipline <- df_historical_races_geny[idx_row,"Discipline"]
#         mat_perf_race_complete <- df_historical_races_geny[df_historical_races_geny$Race == df_historical_races_geny[idx_row,"Race"],]
#         num_horses_finish <- sum(mat_perf_race_complete$Place<100)
#         if(current_discipline %in% c("Trot Attelé","Trot Monté")) {
#           vec_percent_amount <- list_rules_wins$Trot
#         } else {
#           vec_percent_amount <- list_rules_wins$Galop
#         }
#         details_race_to_correct <- sub("Boost Ordre : €","",df_historical_races_geny[idx_row,"Details"])
#         details_race_to_correct <- sub("Boost Ordre : 100.000€","",details_race_to_correct)
#         details_race_to_correct <- sub("Boost Ordre : 5.000€","",details_race_to_correct)
#         val_race_prize_corrected <- unlist(strsplit(details_race_to_correct,"-"))
#         val_race_prize_corrected <- grep("€",val_race_prize_corrected,value = TRUE)
#         if(length(val_race_prize_corrected)>0){
#           val_race_prize_corrected <- val_race_prize_corrected[1]
#           val_race_prize_corrected <- sub('€','',val_race_prize_corrected)
#           val_race_prize_corrected <- str_trim(unlist(strsplit(val_race_prize_corrected,"\n"))[1])
#           vec_val_race_prize_corrected <- unlist(strsplit(val_race_prize_corrected,""))
#           vec_res_test_numeric_val_race_prize_corrected <- unlist(lapply(vec_val_race_prize_corrected,as.numeric))
#           if(sum(is.na(vec_res_test_numeric_val_race_prize_corrected),na.rm=TRUE)){
#             vec_res_test_numeric_val_race_prize_corrected <- vec_res_test_numeric_val_race_prize_corrected[!is.na(vec_res_test_numeric_val_race_prize_corrected)]
#           }
#           val_race_prize_corrected <- as.numeric(paste(vec_res_test_numeric_val_race_prize_corrected,collapse = ""))
#           df_historical_races_geny[idx_row,"Prix"] <- val_race_prize_corrected
#           if(num_horses_finish>0 & !is.na(val_race_prize_corrected) & place_horse_bad_row <= length(vec_percent_amount)) {
#             amount_to_share <- (val_race_prize_corrected*vec_percent_amount[place_horse_bad_row])/100
#             df_historical_races_geny[idx_row,"Gains"] <- amount_to_share
#           }
#         }
#       }
#     }
#   }
# }