# if(current_discipline %in% c("Plat","Haies","Cross","Steeplechase")) {
#   vec_going_found <- unique(as.vector(df_historical_line_by_line_geny_rating_focus[!is.na(df_historical_line_by_line_geny_rating_focus$Going),"Going"]))
#   vec_going_found <- vec_going_found[!is.na(vec_going_found)]
#   list_res_rating_specific_compliled <- vector("list",length(vec_going_found))
#   names(list_res_rating_specific_compliled) <-  vec_going_found
# }
# 
# for (current_going in vec_going_found)
# {
#   id_current_going <- !is.na(df_historical_line_by_line_geny_rating_focus$Etat) & !is.na(df_historical_line_by_line_geny_rating_focus$Terrain) & df_historical_line_by_line_geny_rating_focus$Going==current_going & df_historical_line_by_line_geny_rating_focus$Discipline==current_discipline
#   if(sum(id_current_going,na.rm = TRUE)>=100){
#     mat_line_by_line_subset  <- df_historical_line_by_line_geny_rating_focus[id_current_going,]
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- transform(mat_line_by_line_subset,Score=1)
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- mat_line_by_line_subset %>% distinct()
#     number_race_specific <- length(unique(mat_line_by_line_subset$Race))
#     if(number_race_specific>5 & nrow(mat_line_by_line_subset)>=20) {
#       
#       mat_line_by_line_subset <- mat_line_by_line_subset [,c("Week","Winner","Looser","Score")]
#       mat_line_by_line_subset <- mat_line_by_line_subset[complete.cases(mat_line_by_line_subset),]
#       
#       infos_res_glicko_specific <-  PlayerRatings::glicko(mat_line_by_line_subset,history=TRUE)
#       mat_res_glicko_specific <- infos_res_glicko_specific$ratings
#       mat_res_glicko_specific <- mat_res_glicko_specific[,c("Player","Rating","Deviation","Games","Win","Loss")]
#       colnames(mat_res_glicko_specific) <- sub("Player","Cheval",colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Rating",paste("SPECIFIC_SCORE",current_going,sep="_"),colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Deviation",paste("SPECIFIC_RANKING_DEVIATION",current_going,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- transform(mat_res_glicko_specific,SPECIFIC_PERCENT_SUCCESS = (Win/Games)*100)
#       colnames(mat_res_glicko_specific) <- sub("SPECIFIC_PERCENT_SUCCESS",paste("SPECIFIC_PERCENT_SUCCESS",current_going,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- mat_res_glicko_specific [,c("Cheval",grep("SPECIFIC",colnames(mat_res_glicko_specific),value = TRUE))]
#       colnames(mat_res_glicko_specific) <- gsub("\\.","-",colnames(mat_res_glicko_specific))
#       
#       file_output_name <- paste("mat_ranking_monthly_current_glicko_specific_current_going",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(tolower(gsub(" ","-",gsub("è","e",gsub("é","e",current_going))))),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific,file=file_output_name)
#       rm(mat_res_glicko_specific)
#       gc()
#       
#       mat_res_glicko_specific_temporal <- as.data.frame(infos_res_glicko_specific$history[,,1])
#       colnames(mat_res_glicko_specific_temporal) <- mat_mapping_week_dates[mat_mapping_week_dates$Week %in% unique(mat_line_by_line_subset$Week),"Date"]
#       # rownames(mat_res_glicko_specific_temporal) <- gsub("\\."," ",rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal$Cheval <- as.vector(rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal <- mat_res_glicko_specific_temporal[, c("Cheval",setdiff(colnames(mat_res_glicko_specific_temporal),"Cheval"))]
#       
#       file_output_name <- paste("mat_ranking_monthly_temporal_glicko_specific_current_going",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(tolower(gsub(" ","-",gsub("è","e",gsub("é","e",current_going))))),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific_temporal,file=file_output_name)
#       rm(mat_res_glicko_specific_temporal)
#       gc()
#     }
#   }
# }

# vec_hippodrome_type_found <- unique(as.vector(df_historical_line_by_line_geny_rating_focus$Location))
# vec_hippodrome_type_found <- vec_hippodrome_type_found[!is.na(vec_hippodrome_type_found)]
# list_res_rating_specific_compliled <- vector("list",length(vec_hippodrome_type_found))
# names(list_res_rating_specific_compliled) <-  vec_hippodrome_type_found
# 
# for (hippodrome in vec_hippodrome_type_found)
# {
#   id_hippodrome_distance_cluster <- !is.na(df_historical_line_by_line_geny_rating_focus$Location) & !is.na(df_historical_line_by_line_geny_rating_focus$Cluster_Distance) & df_historical_line_by_line_geny_rating_focus$Location==hippodrome & df_historical_line_by_line_geny_rating_focus$Discipline==current_discipline
#   if(sum(id_hippodrome_distance_cluster,na.rm = TRUE)>=20){
#     mat_line_by_line_subset  <- df_historical_line_by_line_geny_rating_focus[id_hippodrome_distance_cluster,]
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- transform(mat_line_by_line_subset,Score=1)
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- mat_line_by_line_subset %>% distinct()
#     number_race_specific <- length(unique(mat_line_by_line_subset$Race))
#     if(number_race_specific>5 & nrow(mat_line_by_line_subset)>=20) {
# 
#       mat_line_by_line_subset <- mat_line_by_line_subset [,c("Week","Winner","Looser","Score")]
#       mat_line_by_line_subset <- mat_line_by_line_subset[complete.cases(mat_line_by_line_subset),]
# 
#       infos_res_glicko_specific <-  PlayerRatings::glicko(mat_line_by_line_subset,history=TRUE)
#       mat_res_glicko_specific <- infos_res_glicko_specific$ratings
#       mat_res_glicko_specific <- mat_res_glicko_specific[,c("Player","Rating","Deviation","Games","Win","Loss")]
#       colnames(mat_res_glicko_specific) <- sub("Player","Cheval",colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Rating",paste("SPECIFIC_SCORE",hippodrome,sep="_"),colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Deviation",paste("SPECIFIC_RANKING_DEVIATION",hippodrome,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- transform(mat_res_glicko_specific,SPECIFIC_PERCENT_SUCCESS = (Win/Games)*100)
#       colnames(mat_res_glicko_specific) <- sub("SPECIFIC_PERCENT_SUCCESS",paste("SPECIFIC_PERCENT_SUCCESS",hippodrome,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- mat_res_glicko_specific [,c("Cheval",grep("SPECIFIC",colnames(mat_res_glicko_specific),value = TRUE))]
#       colnames(mat_res_glicko_specific) <- gsub("\\.","-",colnames(mat_res_glicko_specific))
# 
#       file_output_name <- paste("mat_ranking_monthly_current_glicko_specific_hippodrome",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(hippodrome),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific,file=file_output_name)
#       rm(mat_res_glicko_specific)
#       gc()
# 
#       mat_res_glicko_specific_temporal <- as.data.frame(infos_res_glicko_specific$history[,,1])
#       colnames(mat_res_glicko_specific_temporal) <- mat_mapping_week_dates[mat_mapping_week_dates$Week %in% unique(mat_line_by_line_subset$Week),"Date"]
#       # rownames(mat_res_glicko_specific_temporal) <- gsub("\\."," ",rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal$Cheval <- as.vector(rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal <- mat_res_glicko_specific_temporal[, c("Cheval",setdiff(colnames(mat_res_glicko_specific_temporal),"Cheval"))]
# 
#       file_output_name <- paste("mat_ranking_monthly_temporal_glicko_specific_hippodrome",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(hippodrome),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific_temporal,file=file_output_name)
#       rm(mat_res_glicko_specific_temporal)
#       gc()
#     }
#   }
# }



# vec_cluster_type_found <- unique(as.vector(df_historical_line_by_line_geny_rating_focus$Cluster_Distance))
# vec_cluster_type_found <- vec_cluster_type_found[!is.na(vec_cluster_type_found)]
# list_res_rating_specific_compliled <- vector("list",length(vec_cluster_type_found))
# names(list_res_rating_specific_compliled) <-  vec_cluster_type_found
# 
# for (cluster_type in vec_cluster_type_found)
# {
#   id_distance_cluster <- !is.na(df_historical_line_by_line_geny_rating_focus$Cluster_Distance) & df_historical_line_by_line_geny_rating_focus$Cluster_Distance==cluster_type & df_historical_line_by_line_geny_rating_focus$Discipline==current_discipline
#   if(sum(id_distance_cluster)>=20){
#     mat_line_by_line_subset  <- df_historical_line_by_line_geny_rating_focus[id_distance_cluster,]
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- transform(mat_line_by_line_subset,Score=1)
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- mat_line_by_line_subset %>% distinct()
#     number_race_specific <- length(unique(mat_line_by_line_subset$Race))
#     if(number_race_specific>5 & nrow(mat_line_by_line_subset)>=20) {
#       
#       mat_line_by_line_subset <- mat_line_by_line_subset [,c("Week","Winner","Looser","Score")]
#       mat_line_by_line_subset <- mat_line_by_line_subset[complete.cases(mat_line_by_line_subset),]
#       
#       infos_res_glicko_specific <-  PlayerRatings::glicko(mat_line_by_line_subset,history=TRUE)
#       mat_res_glicko_specific <- infos_res_glicko_specific$ratings
#       mat_res_glicko_specific <- mat_res_glicko_specific[,c("Player","Rating","Deviation","Games","Win","Loss")]
#       colnames(mat_res_glicko_specific) <- sub("Player","Cheval",colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Rating",paste("SPECIFIC_SCORE",cluster_type,sep="_"),colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Deviation",paste("SPECIFIC_RANKING_DEVIATION",cluster_type,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- transform(mat_res_glicko_specific,SPECIFIC_PERCENT_SUCCESS = (Win/Games)*100)
#       colnames(mat_res_glicko_specific) <- sub("SPECIFIC_PERCENT_SUCCESS",paste("SPECIFIC_PERCENT_SUCCESS",cluster_type,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- mat_res_glicko_specific [,c("Cheval",grep("SPECIFIC",colnames(mat_res_glicko_specific),value = TRUE))]
#       colnames(mat_res_glicko_specific) <- gsub("\\.","-",colnames(mat_res_glicko_specific))
#       
#       file_output_name <- paste("mat_ranking_monthly_current_glicko_specific_cluster_distance",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(cluster_type),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific,file=file_output_name)
#       rm(mat_res_glicko_specific)
#       gc()
#       
#       mat_res_glicko_specific_temporal <- as.data.frame(infos_res_glicko_specific$history[,,1])
#       colnames(mat_res_glicko_specific_temporal) <- mat_mapping_week_dates[mat_mapping_week_dates$Week %in% unique(mat_line_by_line_subset$Week),"Date"]
#       # rownames(mat_res_glicko_specific_temporal) <- gsub("\\."," ",rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal$Cheval <- as.vector(rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal <- mat_res_glicko_specific_temporal[, c("Cheval",setdiff(colnames(mat_res_glicko_specific_temporal),"Cheval"))]
#       
#       file_output_name <- paste("mat_ranking_monthly_temporal_glicko_specific_cluster_distance",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(cluster_type),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific_temporal,file=file_output_name)
#       rm(mat_res_glicko_specific_temporal)
#       gc()
#     }
#   }
# }

# vec_tcd_type_found <- unique(as.vector(df_historical_line_by_line_geny_rating_focus$TCD))
# vec_tcd_type_found <- vec_tcd_type_found[!is.na(vec_tcd_type_found)]
# vec_tcd_type_found <- vec_tcd_type_found[-grep("NA",vec_tcd_type_found)]
# list_res_rating_tcd_compliled <- vector("list",length(vec_tcd_type_found))
# names(list_res_rating_tcd_compliled) <-  vec_tcd_type_found
# 
# for (tcd_type in vec_tcd_type_found)
# {
#   id_tcd <- !is.na(df_historical_line_by_line_geny_rating_focus$TCD) & df_historical_line_by_line_geny_rating_focus$TCD==tcd_type & df_historical_line_by_line_geny_rating_focus$Discipline==current_discipline
#   if(sum(id_tcd)>=20){
#     mat_line_by_line_subset  <- df_historical_line_by_line_geny_rating_focus[id_tcd,]
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- transform(mat_line_by_line_subset,Score=1)
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- mat_line_by_line_subset %>% distinct()
#     number_race_specific <- length(unique(mat_line_by_line_subset$Race))
#     if(number_race_specific>5 & nrow(mat_line_by_line_subset)>=20) {
#       
#       mat_line_by_line_subset <- mat_line_by_line_subset [,c("Week","Winner","Looser","Score")]
#       mat_line_by_line_subset <- mat_line_by_line_subset[complete.cases(mat_line_by_line_subset),]
#       
#       infos_res_glicko_specific <-  PlayerRatings::glicko(mat_line_by_line_subset,history=TRUE)
#       mat_res_glicko_specific <- infos_res_glicko_specific$ratings
#       mat_res_glicko_specific <- mat_res_glicko_specific[,c("Player","Rating","Deviation","Games","Win","Loss")]
#       colnames(mat_res_glicko_specific) <- sub("Player","Cheval",colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Rating",paste("SPECIFIC_SCORE",tcd_type,sep="_"),colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Deviation",paste("SPECIFIC_RANKING_DEVIATION",tcd_type,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- transform(mat_res_glicko_specific,SPECIFIC_PERCENT_SUCCESS = (Win/Games)*100)
#       colnames(mat_res_glicko_specific) <- sub("SPECIFIC_PERCENT_SUCCESS",paste("SPECIFIC_PERCENT_SUCCESS",tcd_type,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- mat_res_glicko_specific [,c("Cheval",grep("SPECIFIC",colnames(mat_res_glicko_specific),value = TRUE))]
#       colnames(mat_res_glicko_specific) <- gsub("\\.","-",colnames(mat_res_glicko_specific))
#       
#       file_output_name <- paste("mat_ranking_monthly_current_glicko_specific_terrain_corde_distance",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(tcd_type),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific,file=file_output_name)
#       rm(mat_res_glicko_specific)
#       gc()
#       
#       mat_res_glicko_specific_temporal <- as.data.frame(infos_res_glicko_specific$history[,,1])
#       colnames(mat_res_glicko_specific_temporal) <- mat_mapping_week_dates[mat_mapping_week_dates$Week %in% unique(mat_line_by_line_subset$Week),"Date"]
#       # rownames(mat_res_glicko_specific_temporal) <- gsub("\\."," ",rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal$Cheval <- as.vector(rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal <- mat_res_glicko_specific_temporal[, c("Cheval",setdiff(colnames(mat_res_glicko_specific_temporal),"Cheval"))]
#       
#       file_output_name <- paste("mat_ranking_monthly_temporal_glicko_specific_terrain_corde_distance",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(tcd_type),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific_temporal,file=file_output_name)
#       rm(mat_res_glicko_specific_temporal)
#       gc()
#     }
#   }
# }

# if(current_discipline %in% c("Plat","Haies","Cross","Steeplechase")) {
#   vec_etat_terrain_found <- unique(as.vector(df_historical_line_by_line_geny_rating_focus[!is.na(df_historical_line_by_line_geny_rating_focus$Etat),"Etat"]))
#   vec_etat_terrain_found <- vec_etat_terrain_found[!is.na(vec_etat_terrain_found)]
#   list_res_rating_specific_compliled <- vector("list",length(vec_etat_terrain_found))
#   names(list_res_rating_specific_compliled) <-  vec_etat_terrain_found
# }
# 
# for (etat_terrain in vec_etat_terrain_found)
# {
#   id_etat_terrain <- !is.na(df_historical_line_by_line_geny_rating_focus$Etat) & df_historical_line_by_line_geny_rating_focus$Etat==etat_terrain & df_historical_line_by_line_geny_rating_focus$Discipline==current_discipline
#   if(sum(id_etat_terrain,na.rm = TRUE)>=100){
#     mat_line_by_line_subset  <- df_historical_line_by_line_geny_rating_focus[id_etat_terrain,]
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- transform(mat_line_by_line_subset,Score=1)
#     mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
#     mat_line_by_line_subset  <- mat_line_by_line_subset %>% distinct()
#     number_race_specific <- length(unique(mat_line_by_line_subset$Race))
#     if(number_race_specific>5 & nrow(mat_line_by_line_subset)>=20) {
#       
#       mat_line_by_line_subset <- mat_line_by_line_subset [,c("Week","Winner","Looser","Score")]
#       mat_line_by_line_subset <- mat_line_by_line_subset[complete.cases(mat_line_by_line_subset),]
#       
#       infos_res_glicko_specific <-  PlayerRatings::glicko(mat_line_by_line_subset,history=TRUE)
#       mat_res_glicko_specific <- infos_res_glicko_specific$ratings
#       mat_res_glicko_specific <- mat_res_glicko_specific[,c("Player","Rating","Deviation","Games","Win","Loss")]
#       colnames(mat_res_glicko_specific) <- sub("Player","Cheval",colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Rating",paste("SPECIFIC_SCORE",etat_terrain,sep="_"),colnames(mat_res_glicko_specific))
#       colnames(mat_res_glicko_specific) <- sub("Deviation",paste("SPECIFIC_RANKING_DEVIATION",etat_terrain,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- transform(mat_res_glicko_specific,SPECIFIC_PERCENT_SUCCESS = (Win/Games)*100)
#       colnames(mat_res_glicko_specific) <- sub("SPECIFIC_PERCENT_SUCCESS",paste("SPECIFIC_PERCENT_SUCCESS",etat_terrain,sep="_"),colnames(mat_res_glicko_specific))
#       mat_res_glicko_specific <- mat_res_glicko_specific [,c("Cheval",grep("SPECIFIC",colnames(mat_res_glicko_specific),value = TRUE))]
#       colnames(mat_res_glicko_specific) <- gsub("\\.","-",colnames(mat_res_glicko_specific))
#       
#       file_output_name <- paste("mat_ranking_monthly_current_glicko_specific_etat_terrain",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(tolower(gsub(" ","-",gsub("è","e",gsub("é","e",etat_terrain))))),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific,file=file_output_name)
#       rm(mat_res_glicko_specific)
#       gc()
#       
#       mat_res_glicko_specific_temporal <- as.data.frame(infos_res_glicko_specific$history[,,1])
#       colnames(mat_res_glicko_specific_temporal) <- mat_mapping_week_dates[mat_mapping_week_dates$Week %in% unique(mat_line_by_line_subset$Week),"Date"]
#       # rownames(mat_res_glicko_specific_temporal) <- gsub("\\."," ",rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal$Cheval <- as.vector(rownames(mat_res_glicko_specific_temporal))
#       mat_res_glicko_specific_temporal <- mat_res_glicko_specific_temporal[, c("Cheval",setdiff(colnames(mat_res_glicko_specific_temporal),"Cheval"))]
#       
#       file_output_name <- paste("mat_ranking_monthly_temporal_glicko_specific_etat_terrain",paste(gsub("é","e",tolower(gsub(" ","_",current_discipline))),tolower(tolower(gsub(" ","-",gsub("è","e",gsub("é","e",etat_terrain))))),sep="_"),sep="_")
#       file_output_name <- paste(file_output_name,".rds",sep="")
#       file_output_name <-gsub(" ","_",file_output_name)
#       setwd(path_ranking_output)
#       saveRDS(mat_res_glicko_specific_temporal,file=file_output_name)
#       rm(mat_res_glicko_specific_temporal)
#       gc()
#     }
#   }
# }