######################################## Loading required Packages ########################################
require(data.table)
require(plyr)
require(dplyr)
require(PlayerRatings)
require(parallel)
require(magrittr)
require(foreach)
require(doParallel)
require(readr)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data  <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/raw/pairwise_comparisons"
path_output_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/pairwise_comparisons/mat_line_by_line_rating_geny.rds"
path_output_data_start <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/pairwise_comparisons_tmp"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- parallel::makeCluster(ceiling(host_nb_cores*0.75), outfile="")
registerDoMC(cores=ceiling(host_nb_cores*0.75))
################################ Parallelization Management ############################

################################ Setting Output Path ############################
files_line_by_line_rating <- dir(path=path_input_data,pattern = ".csv")
setwd(path_input_data)
vec_steps <- seq(1,length(files_line_by_line_rating),by=1000)
vec_steps[length(vec_steps)] <- length(files_line_by_line_rating)

for (step in 1:(length(vec_steps)-1)) {
  setwd(path_input_data)
  mat_line_by_line_rating_current <- NULL
  files_line_by_line_rating_current <- files_line_by_line_rating[vec_steps[step]:(vec_steps[step+1])] 
  for(current_file in files_line_by_line_rating_current)
  {
    mat_one_race <- read.csv(current_file)
    if(nrow(mat_one_race)>0){
      mat_line_by_line_rating_current <- rbind.fill(mat_line_by_line_rating_current,mat_one_race)
    }
  }
  setwd(path_output_data_start)
  saveRDS(mat_line_by_line_rating_current,paste(step,".rds",sep=""))
  print(step)
}

setwd(path_output_data_start)
mat_line_by_line_rating <- list.files(path=path_output_data_start, full.names = TRUE) %>%
  lapply(readRDS) %>%
  bind_rows %>%
  as.data.frame()
mat_line_by_line_rating <- mat_line_by_line_rating %>% distinct() %>% as.data.frame()
saveRDS(mat_line_by_line_rating,path_output_data)
################################ Setting Output Path ############################
