require(readxl)
require(magrittr)
require(dplyr)
require(tidyr)
require(stringr)

path_ref_tracks_characteristics_reference <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/canalturf/input/mat_tracks_characteristics.xlsx"
path_tracks_characteristics_mapping <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/input/mat_guess_mapping_letrot_reference_format.xlsx"
path_historical_data_letrot <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/letrot/output/02-processed/mat_historical_races_letrot.rds"

mat_tracks_characteristics_reference <- read_excel(path_ref_tracks_characteristics_reference,sheet = 1)
mat_tracks_characteristics_reference <- as.data.frame(mat_tracks_characteristics_reference)
mat_tracks_characteristics_reference <- mat_tracks_characteristics_reference[mat_tracks_characteristics_reference$Discipline %in% c("Trot Monté","Trot Attelé"),]
mat_tracks_characteristics_reference$Discipline <- gsub("Trot Attelé","Attelé",mat_tracks_characteristics_reference$Discipline)
mat_tracks_characteristics_reference$Discipline <- gsub("Trot Monté","Monté",mat_tracks_characteristics_reference$Discipline)
mat_tracks_characteristics_reference <- as.data.frame(mat_tracks_characteristics_reference)
mat_tracks_characteristics_reference$ID <- paste(mat_tracks_characteristics_reference$Lieu,mat_tracks_characteristics_reference$Discipline,mat_tracks_characteristics_reference$Distance,sep="_")

mat_tracks_characteristics_mapping <- read_excel(path_tracks_characteristics_mapping,sheet = 1)
mat_tracks_characteristics_mapping <- as.data.frame(mat_tracks_characteristics_mapping)


# mat_tracks_characteristics_reference$ID <- tolower(str_trim(gsub(" ","-",mat_tracks_characteristics_reference$ID)))
# mat_tracks_characteristics_reference$ID <- gsub("é","e",mat_tracks_characteristics_reference$ID)
# mat_tracks_characteristics_reference$ID <- gsub("è","e",mat_tracks_characteristics_reference$ID)
# 
# mat_historical_data_letrot <- readRDS(path_historical_data_letrot)
# mat_historical_data_letrot <-  mat_historical_data_letrot[,c("Lieu","Discipline","Distance","Date")]
# 
# mat_number_races_tracks <- mat_historical_data_letrot %>%
#   group_by(Lieu) %>%
#   summarise(Size=n(),First=min(Date,na.rm=TRUE),Last=max(Date,na.rm=TRUE)) %>%
#   as.data.frame()
# mat_number_races_tracks$Lieu <- str_to_title(mat_number_races_tracks$Lieu)
# 
# vec_tracks_letrot <- unique(mat_number_races_tracks$Lieu)
# vec_tracks_reference <- unique(unlist(mat_tracks_characteristics_reference$Lieu))
# 
# mat_guess_mapping_letrot_reference <- NULL
# for(target_term in vec_tracks_letrot)
# {
#   for(reference_term in vec_tracks_reference ){
#     sim_current <- stringsim(target_term,reference_term,method='jw')
#     mat_guess_mapping_letrot_reference_current <- data.frame(Letrot=target_term,Geny=reference_term,Distance=sim_current)
#     mat_guess_mapping_letrot_reference <- rbind(mat_guess_mapping_letrot_reference,mat_guess_mapping_letrot_reference_current)
#   }
# }
# 
# mat_guess_mapping_letrot_reference_format <- mat_guess_mapping_letrot_reference %>%
#   group_by(Letrot) %>%
#   top_n(1,Distance) %>%
#   as.data.frame()
# 
# mat_guess_mapping_letrot_reference_format <- merge(mat_guess_mapping_letrot_reference_format,mat_number_races_tracks,by.x="Letrot",by.y="Lieu",all.y="TRUE")
# write.xlsx(mat_guess_mapping_letrot_reference_format,"mat_guess_mapping_letrot_reference_format.xlsx")




# mat_tracks_characteristics_letrot <- mat_historical_data_letrot [,c("Lieu","Discipline","Distance")] %>% 
#   group_by(Lieu,Discipline,Distance) %>%
#   drop_na() %>%
#   dplyr::summarise(Size=n()) %>%
#   as.data.frame()
# mat_tracks_characteristics_letrot$Lieu <- str_to_title(mat_tracks_characteristics_letrot$Lieu)
# mat_tracks_characteristics_letrot <- merge(mat_tracks_characteristics_letrot,mat_number_races_tracks,by.x="Lieu",by.y="Lieu")
# 
# 
# 
# mat_tracks_characteristics_letrot$ID <- paste(mat_tracks_characteristics_letrot$Lieu,mat_tracks_characteristics_letrot$Discipline,mat_tracks_characteristics_letrot$Distance,sep="_")
# mat_tracks_characteristics_letrot$ID <- tolower(str_trim(gsub(" ","-",mat_tracks_characteristics_letrot$ID)))
# 
# mat_tracks_characteristics_reference_completed <- merge(mat_tracks_characteristics_letrot,mat_tracks_characteristics_reference[,c("ID","Corde","Depart","Terrain")],by.x="ID",by.y="ID",all.x=TRUE)

