######################################## Loading required Packages ########################################
require(bmxFutankeMbayar)
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
require(doMC)
require(PlayerRatings)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_line_by_line_output  =  "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/raw/pairwise_comparisons"
path_mat_historical_races = "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/output/pmu/processed/historical_performances/mat_historical_races_geny.rds"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
# host_os  <- as.vector(Sys.info()['sysname'])
# host_nb_cores <- parallel::detectCores()
# nb_cores_use <- parallel::makeCluster(ceiling(host_nb_cores*0.90), outfile="")
# registerDoMC(cores=ceiling(host_nb_cores*0.90))
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races <- readRDS(path_mat_historical_races)
vec_discipline <- as.vector(unique(mat_historical_races$Discipline))
vec_discipline <- vec_discipline[!is.na(vec_discipline)]
# vec_discipline <- setdiff(vec_discipline,c("Plat"))
# vec_discipline <- setdiff(vec_discipline,c("Trot Attelé"))
vec_discipline <- c("Cross","Steeplechase","Haies","Trot Monté","Plat","Trot Attelé") 
# vec_discipline <- c("Trot Attelé")
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
for(discipline in vec_discipline)
{
  mat_historical_races_focus <- subset(mat_historical_races,Discipline==discipline)
  mat_historical_races_focus <- mat_historical_races_focus[mat_historical_races_focus$Date>=as.Date("2021-04-14"),]
  vec_races <- as.vector(unique(mat_historical_races_focus$Race))
  vec_races <- vec_races[!is.na(vec_races)]
  print(paste("Starting the computation of line by line results for :",discipline))
  for(i in 1:length(vec_races)){
      mat_historical_races_current <- unique(mat_historical_races_focus[mat_historical_races_focus$Race==vec_races[i],])
      if(nrow(mat_historical_races_current)>1) {
        get_line_by_line_driver_jockey_race_geny (df_perf_race_complete=mat_historical_races_current,target_item="Horse")
      }
  }
  print(paste("Finished the computation of line by line results for :",discipline))
}
################################ Generate and store data ############################
