################################ Loading required packages ################################
require(fst)
require(readr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(data.table)
require(parallel)
require(magrittr)
require(bmxFutankeMbayar)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_input_data  <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/pmh/output/raw/historical_performances"
path_output_data <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/pmh/output/processed/historical_performances/mat_historical_races_pmh_geny.rds"
path_output_data_start <- "/mnt/Master-Data/Futanke-Mbayar/France/data/geny/pmh/output/processed/historical_performances_tmp"
################################ Setting Output Path ############################

################################ Performance files compilation ############################
vec_availables_files <- dir(path_input_data)
vec_steps <- seq(1,length(vec_availables_files),by=100)
vec_steps[length(vec_steps)] <- length(vec_availables_files)

for (step in 1:(length(vec_steps)-1)) {
  setwd(path_input_data)
  mat_historical_races_current <- NULL
  vec_availables_files_current <- vec_availables_files[vec_steps[step]:(vec_steps[step+1])] 
  for(current_file in vec_availables_files_current)
  {
    mat_one_race <- read.csv(current_file)
    if(nrow(mat_one_race)>0){
      if(sum(!is.na(mat_one_race$Epreuve))>0) {
        vec_epreuve <- lapply(as.character(mat_one_race$Epreuve),function(x){unlist(strsplit(x,"_"))[1]})
        vec_epreuve <- gsub("\n","",vec_epreuve)
        mat_one_race$Epreuve <- vec_epreuve
        for(current_column in colnames(mat_one_race)){
          if(class(mat_one_race[,current_column])=="factor"){
            mat_one_race[,current_column] <- as.vector(mat_one_race[,current_column])
          }
        }
      }
      mat_one_race$Gender  <- gsub("FALSE","F",mat_one_race$Gender)
      mat_one_race$Epreuve <- gsub("FALSE","F",mat_one_race$Epreuve)
      mat_historical_races_current <- rbind.fill(mat_historical_races_current,mat_one_race)
    }
  }
  setwd(path_output_data_start)
  saveRDS(mat_historical_races_current,paste(step,".rds",sep=""))
}

setwd(path_output_data_start)
mat_historical_races <- list.files(path=path_output_data_start, full.names = TRUE) %>%
  lapply(readRDS) %>%
  bind_rows %>%
  distinct() %>%
  as.data.frame()

mat_historical_races$Epreuve <- gsub("HATTELÉ","H",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("\\(GALOP)","",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("III POUR AQPS _ FEMELLESPLAT","III",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("III POUR ARABES PURSPLAT","III",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("ATTELÉ","",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("I POUR AQPSPLAT","I",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("III POUR ARABES PURSPLAT","III",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("\\I POUR A.Q.P.S.PLAT","I",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("POUR ARABES PURES","",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("POUR ARABES PURSPLAT","",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("POUR AQPSPLAT","",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- gsub("POUR AQPS","",mat_historical_races$Epreuve)
mat_historical_races$Epreuve <- str_trim(mat_historical_races$Epreuve)

# rownames(mat_historical_races) <- mat_historical_races$URL
# aa = subset(mat_historical_races,Epreuve == "QUALIFICATIVE")


# vec_good_race_categories <- unlist(lapply(mat_historical_races$Details,get_race_category_geny))
# mat_historical_races$Epreuve <- vec_good_race_categories
# mat_historical_races$Epreuve <- unlist(lapply(mat_historical_races$Epreuve,function(x){unlist(strsplit(x," "))[1]}))
saveRDS(mat_historical_races,path_output_data)
################################ Performance files compilation ############################

################################ Parallelization Management ############################
# host_os  <- as.vector(Sys.info()['sysname'])
# host_nb_cores <- parallel::detectCores()
# nb_cores_use <- parallel::makeCluster(ceiling(host_nb_cores*0.9), outfile="")
# registerDoMC(cores=ceiling(host_nb_cores*0.9))
################################ Parallelization Management ############################